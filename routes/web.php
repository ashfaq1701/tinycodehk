<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Auth::routes();

Route::get('/', 'Frontend\HomeController@index');

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/dashboard', 'DashboardController@index');

Route::get('/modules/import', 'ImportController@getModulesImport');
Route::post('/modules/import', 'ImportController@postModulesImport');

Route::post('uploads/file', 'UploadController@fileupload');
Route::post('/modules/related', 'ModulesController@relatedModules');
Route::post('/modules/child/attach', 'ModulesController@linkModels');

Route::group(['prefix' => 'modules'], function () {

Route::get('attachments', 'Modules\AttachmentController@index');
Route::get('attachments/search', 'Modules\AttachmentController@search');
Route::get('attachments/add', 'Modules\AttachmentController@create');
Route::get('attachments/api/{id}', 'Modules\AttachmentController@attachment');
Route::post('attachments', 'Modules\AttachmentController@store');
Route::get('attachments/{id}/delete', 'Modules\AttachmentController@delete');
Route::get('attachments/{id}', 'Modules\AttachmentController@get');
Route::post('attachments/{id}', 'Modules\AttachmentController@update');

Route::get('attendences', 'Modules\AttendenceController@index');
Route::get('attendences/search', 'Modules\AttendenceController@search');
Route::get('attendences/add', 'Modules\AttendenceController@create');
Route::get('attendences/api/{id}', 'Modules\AttendenceController@attendence');
Route::post('attendences', 'Modules\AttendenceController@store');
Route::get('attendences/{id}/delete', 'Modules\AttendenceController@delete');
Route::get('attendences/{id}', 'Modules\AttendenceController@get');
Route::post('attendences/{id}', 'Modules\AttendenceController@update');

Route::get('enrollments', 'Modules\EnrollmentController@index');
Route::get('enrollments/search', 'Modules\EnrollmentController@search');
Route::get('enrollments/add', 'Modules\EnrollmentController@create');
Route::get('enrollments/api/{id}', 'Modules\EnrollmentController@enrollment');
Route::get('enrollments/api/{id}/attendences', 'Modules\EnrollmentController@attendencesApi');
Route::get('enrollments/api/{id}/examScores', 'Modules\EnrollmentController@examScoresApi');
Route::get('enrollments/api/{id}/payments', 'Modules\EnrollmentController@paymentsApi');
Route::get('enrollments/api/{id}/testimonials', 'Modules\EnrollmentController@testimonialsApi');
Route::get('enrollments/api/{id}/programClasses', 'Modules\EnrollmentController@programClassesApi');
Route::get('enrollments/api/{id}/exams', 'Modules\EnrollmentController@examsApi');
Route::post('enrollments', 'Modules\EnrollmentController@store');
Route::get('enrollments/{id}/attendences', 'Modules\EnrollmentController@attendences');
Route::get('enrollments/{id}/examScores', 'Modules\EnrollmentController@examScores');
Route::get('enrollments/{id}/payments', 'Modules\EnrollmentController@payments');
Route::get('enrollments/{id}/testimonials', 'Modules\EnrollmentController@testimonials');
Route::get('enrollments/{id}/programClasses', 'Modules\EnrollmentController@programClasses');
Route::get('enrollments/{id}/exams', 'Modules\EnrollmentController@exams');
Route::get('enrollments/{id}/delete', 'Modules\EnrollmentController@delete');
Route::get('enrollments/{id}', 'Modules\EnrollmentController@get');
Route::post('enrollments/{id}', 'Modules\EnrollmentController@update');

Route::get('exams', 'Modules\ExamController@index');
Route::get('exams/search', 'Modules\ExamController@search');
Route::get('exams/add', 'Modules\ExamController@create');
Route::get('exams/api/{id}', 'Modules\ExamController@exam');
Route::get('exams/api/{id}/examScores', 'Modules\ExamController@examScoresApi');
Route::get('exams/api/{id}/enrollments', 'Modules\ExamController@enrollmentsApi');
Route::post('exams', 'Modules\ExamController@store');
Route::get('exams/{id}/examScores', 'Modules\ExamController@examScores');
Route::get('exams/{id}/enrollments', 'Modules\ExamController@enrollments');
Route::get('exams/{id}/delete', 'Modules\ExamController@delete');
Route::get('exams/{id}', 'Modules\ExamController@get');
Route::post('exams/{id}', 'Modules\ExamController@update');

Route::get('examScores', 'Modules\ExamScoreController@index');
Route::get('examScores/search', 'Modules\ExamScoreController@search');
Route::get('examScores/add', 'Modules\ExamScoreController@create');
Route::get('examScores/api/{id}', 'Modules\ExamScoreController@examScore');
Route::post('examScores', 'Modules\ExamScoreController@store');
Route::get('examScores/{id}/delete', 'Modules\ExamScoreController@delete');
Route::get('examScores/{id}', 'Modules\ExamScoreController@get');
Route::post('examScores/{id}', 'Modules\ExamScoreController@update');

Route::get('holidayCamps', 'Modules\HolidayCampController@index');
Route::get('holidayCamps/search', 'Modules\HolidayCampController@search');
Route::get('holidayCamps/add', 'Modules\HolidayCampController@create');
Route::get('holidayCamps/api/{id}', 'Modules\HolidayCampController@holidayCamp');
Route::get('holidayCamps/api/{id}/holidayCampCourses', 'Modules\HolidayCampController@holidayCampCoursesApi');
Route::post('holidayCamps', 'Modules\HolidayCampController@store');
Route::get('holidayCamps/{id}/holidayCampCourses', 'Modules\HolidayCampController@holidayCampCourses');
Route::get('holidayCamps/{id}/delete', 'Modules\HolidayCampController@delete');
Route::get('holidayCamps/{id}', 'Modules\HolidayCampController@get');
Route::post('holidayCamps/{id}', 'Modules\HolidayCampController@update');

Route::get('holidayCampCourses', 'Modules\HolidayCampCourseController@index');
Route::get('holidayCampCourses/search', 'Modules\HolidayCampCourseController@search');
Route::get('holidayCampCourses/add', 'Modules\HolidayCampCourseController@create');
Route::get('holidayCampCourses/api/{id}', 'Modules\HolidayCampCourseController@holidayCampCourse');
Route::get('holidayCampCourses/api/{id}/holidayCampCourseBringings', 'Modules\HolidayCampCourseController@holidayCampCourseBringingsApi');
Route::get('holidayCampCourses/api/{id}/holidayCampCourseFeatures', 'Modules\HolidayCampCourseController@holidayCampCourseFeaturesApi');
Route::get('holidayCampCourses/api/{id}/holidayCampCourseHighlights', 'Modules\HolidayCampCourseController@holidayCampCourseHighlightsApi');
Route::get('holidayCampCourses/api/{id}/holidayCampModules', 'Modules\HolidayCampCourseController@holidayCampModulesApi');
Route::get('holidayCampCourses/api/{id}/locations', 'Modules\HolidayCampCourseController@locationsApi');
Route::post('holidayCampCourses', 'Modules\HolidayCampCourseController@store');
Route::get('holidayCampCourses/{id}/holidayCampCourseBringings', 'Modules\HolidayCampCourseController@holidayCampCourseBringings');
Route::get('holidayCampCourses/{id}/holidayCampCourseFeatures', 'Modules\HolidayCampCourseController@holidayCampCourseFeatures');
Route::get('holidayCampCourses/{id}/holidayCampCourseHighlights', 'Modules\HolidayCampCourseController@holidayCampCourseHighlights');
Route::get('holidayCampCourses/{id}/holidayCampModules', 'Modules\HolidayCampCourseController@holidayCampModules');
Route::get('holidayCampCourses/{id}/locations', 'Modules\HolidayCampCourseController@locations');
Route::get('holidayCampCourses/{id}/delete', 'Modules\HolidayCampCourseController@delete');
Route::get('holidayCampCourses/{id}', 'Modules\HolidayCampCourseController@get');
Route::post('holidayCampCourses/{id}', 'Modules\HolidayCampCourseController@update');

Route::get('holidayCampCourseBringings', 'Modules\HolidayCampCourseBringingController@index');
Route::get('holidayCampCourseBringings/search', 'Modules\HolidayCampCourseBringingController@search');
Route::get('holidayCampCourseBringings/add', 'Modules\HolidayCampCourseBringingController@create');
Route::get('holidayCampCourseBringings/api/{id}', 'Modules\HolidayCampCourseBringingController@holidayCampCourseBringing');
Route::post('holidayCampCourseBringings', 'Modules\HolidayCampCourseBringingController@store');
Route::get('holidayCampCourseBringings/{id}/delete', 'Modules\HolidayCampCourseBringingController@delete');
Route::get('holidayCampCourseBringings/{id}', 'Modules\HolidayCampCourseBringingController@get');
Route::post('holidayCampCourseBringings/{id}', 'Modules\HolidayCampCourseBringingController@update');

Route::get('holidayCampCourseFeatures', 'Modules\HolidayCampCourseFeatureController@index');
Route::get('holidayCampCourseFeatures/search', 'Modules\HolidayCampCourseFeatureController@search');
Route::get('holidayCampCourseFeatures/add', 'Modules\HolidayCampCourseFeatureController@create');
Route::get('holidayCampCourseFeatures/api/{id}', 'Modules\HolidayCampCourseFeatureController@holidayCampCourseFeature');
Route::get('holidayCampCourseFeatures/api/{id}/holidayCampCourseSubFeatures', 'Modules\HolidayCampCourseFeatureController@holidayCampCourseSubFeaturesApi');
Route::post('holidayCampCourseFeatures', 'Modules\HolidayCampCourseFeatureController@store');
Route::get('holidayCampCourseFeatures/{id}/holidayCampCourseSubFeatures', 'Modules\HolidayCampCourseFeatureController@holidayCampCourseSubFeatures');
Route::get('holidayCampCourseFeatures/{id}/delete', 'Modules\HolidayCampCourseFeatureController@delete');
Route::get('holidayCampCourseFeatures/{id}', 'Modules\HolidayCampCourseFeatureController@get');
Route::post('holidayCampCourseFeatures/{id}', 'Modules\HolidayCampCourseFeatureController@update');

Route::get('holidayCampCourseHighlights', 'Modules\HolidayCampCourseHighlightController@index');
Route::get('holidayCampCourseHighlights/search', 'Modules\HolidayCampCourseHighlightController@search');
Route::get('holidayCampCourseHighlights/add', 'Modules\HolidayCampCourseHighlightController@create');
Route::get('holidayCampCourseHighlights/api/{id}', 'Modules\HolidayCampCourseHighlightController@holidayCampCourseHighlight');
Route::post('holidayCampCourseHighlights', 'Modules\HolidayCampCourseHighlightController@store');
Route::get('holidayCampCourseHighlights/{id}/delete', 'Modules\HolidayCampCourseHighlightController@delete');
Route::get('holidayCampCourseHighlights/{id}', 'Modules\HolidayCampCourseHighlightController@get');
Route::post('holidayCampCourseHighlights/{id}', 'Modules\HolidayCampCourseHighlightController@update');

Route::get('holidayCampCourseSubFeatures', 'Modules\HolidayCampCourseSubFeatureController@index');
Route::get('holidayCampCourseSubFeatures/search', 'Modules\HolidayCampCourseSubFeatureController@search');
Route::get('holidayCampCourseSubFeatures/add', 'Modules\HolidayCampCourseSubFeatureController@create');
Route::get('holidayCampCourseSubFeatures/api/{id}', 'Modules\HolidayCampCourseSubFeatureController@holidayCampCourseSubFeature');
Route::post('holidayCampCourseSubFeatures', 'Modules\HolidayCampCourseSubFeatureController@store');
Route::get('holidayCampCourseSubFeatures/{id}/delete', 'Modules\HolidayCampCourseSubFeatureController@delete');
Route::get('holidayCampCourseSubFeatures/{id}', 'Modules\HolidayCampCourseSubFeatureController@get');
Route::post('holidayCampCourseSubFeatures/{id}', 'Modules\HolidayCampCourseSubFeatureController@update');

Route::get('holidayCampModules', 'Modules\HolidayCampModuleController@index');
Route::get('holidayCampModules/search', 'Modules\HolidayCampModuleController@search');
Route::get('holidayCampModules/add', 'Modules\HolidayCampModuleController@create');
Route::get('holidayCampModules/api/{id}', 'Modules\HolidayCampModuleController@holidayCampModule');
Route::post('holidayCampModules', 'Modules\HolidayCampModuleController@store');
Route::get('holidayCampModules/{id}/delete', 'Modules\HolidayCampModuleController@delete');
Route::get('holidayCampModules/{id}', 'Modules\HolidayCampModuleController@get');
Route::post('holidayCampModules/{id}', 'Modules\HolidayCampModuleController@update');

Route::get('inHomePrograms', 'Modules\InHomeProgramController@index');
Route::get('inHomePrograms/search', 'Modules\InHomeProgramController@search');
Route::get('inHomePrograms/add', 'Modules\InHomeProgramController@create');
Route::get('inHomePrograms/api/{id}', 'Modules\InHomeProgramController@inHomeProgram');
Route::get('inHomePrograms/api/{id}/inHomeProgramFormats', 'Modules\InHomeProgramController@inHomeProgramFormatsApi');
Route::get('inHomePrograms/api/{id}/inHomeProgramPricings', 'Modules\InHomeProgramController@inHomeProgramPricingsApi');
Route::get('inHomePrograms/api/{id}/inHomeProgramSchedules', 'Modules\InHomeProgramController@inHomeProgramSchedulesApi');
Route::get('inHomePrograms/api/{id}/inHomeProgramStructures', 'Modules\InHomeProgramController@inHomeProgramStructuresApi');
Route::get('inHomePrograms/api/{id}/inHomeProgramTopics', 'Modules\InHomeProgramController@inHomeProgramTopicsApi');
Route::get('inHomePrograms/api/{id}/weekdays', 'Modules\InHomeProgramController@weekdaysApi');
Route::post('inHomePrograms', 'Modules\InHomeProgramController@store');
Route::get('inHomePrograms/{id}/inHomeProgramFormats', 'Modules\InHomeProgramController@inHomeProgramFormats');
Route::get('inHomePrograms/{id}/inHomeProgramPricings', 'Modules\InHomeProgramController@inHomeProgramPricings');
Route::get('inHomePrograms/{id}/inHomeProgramSchedules', 'Modules\InHomeProgramController@inHomeProgramSchedules');
Route::get('inHomePrograms/{id}/inHomeProgramStructures', 'Modules\InHomeProgramController@inHomeProgramStructures');
Route::get('inHomePrograms/{id}/inHomeProgramTopics', 'Modules\InHomeProgramController@inHomeProgramTopics');
Route::get('inHomePrograms/{id}/weekdays', 'Modules\InHomeProgramController@weekdays');
Route::get('inHomePrograms/{id}/delete', 'Modules\InHomeProgramController@delete');
Route::get('inHomePrograms/{id}', 'Modules\InHomeProgramController@get');
Route::post('inHomePrograms/{id}', 'Modules\InHomeProgramController@update');

Route::get('inHomeProgramFormats', 'Modules\InHomeProgramFormatController@index');
Route::get('inHomeProgramFormats/search', 'Modules\InHomeProgramFormatController@search');
Route::get('inHomeProgramFormats/add', 'Modules\InHomeProgramFormatController@create');
Route::get('inHomeProgramFormats/api/{id}', 'Modules\InHomeProgramFormatController@inHomeProgramFormat');
Route::post('inHomeProgramFormats', 'Modules\InHomeProgramFormatController@store');
Route::get('inHomeProgramFormats/{id}/delete', 'Modules\InHomeProgramFormatController@delete');
Route::get('inHomeProgramFormats/{id}', 'Modules\InHomeProgramFormatController@get');
Route::post('inHomeProgramFormats/{id}', 'Modules\InHomeProgramFormatController@update');

Route::get('inHomeProgramPricings', 'Modules\InHomeProgramPricingController@index');
Route::get('inHomeProgramPricings/search', 'Modules\InHomeProgramPricingController@search');
Route::get('inHomeProgramPricings/add', 'Modules\InHomeProgramPricingController@create');
Route::get('inHomeProgramPricings/api/{id}', 'Modules\InHomeProgramPricingController@inHomeProgramPricing');
Route::post('inHomeProgramPricings', 'Modules\InHomeProgramPricingController@store');
Route::get('inHomeProgramPricings/{id}/delete', 'Modules\InHomeProgramPricingController@delete');
Route::get('inHomeProgramPricings/{id}', 'Modules\InHomeProgramPricingController@get');
Route::post('inHomeProgramPricings/{id}', 'Modules\InHomeProgramPricingController@update');

Route::get('inHomeProgramSchedules', 'Modules\InHomeProgramScheduleController@index');
Route::get('inHomeProgramSchedules/search', 'Modules\InHomeProgramScheduleController@search');
Route::get('inHomeProgramSchedules/add', 'Modules\InHomeProgramScheduleController@create');
Route::get('inHomeProgramSchedules/api/{id}', 'Modules\InHomeProgramScheduleController@inHomeProgramSchedule');
Route::post('inHomeProgramSchedules', 'Modules\InHomeProgramScheduleController@store');
Route::get('inHomeProgramSchedules/{id}/delete', 'Modules\InHomeProgramScheduleController@delete');
Route::get('inHomeProgramSchedules/{id}', 'Modules\InHomeProgramScheduleController@get');
Route::post('inHomeProgramSchedules/{id}', 'Modules\InHomeProgramScheduleController@update');

Route::get('inHomeProgramStructures', 'Modules\InHomeProgramStructureController@index');
Route::get('inHomeProgramStructures/search', 'Modules\InHomeProgramStructureController@search');
Route::get('inHomeProgramStructures/add', 'Modules\InHomeProgramStructureController@create');
Route::get('inHomeProgramStructures/api/{id}', 'Modules\InHomeProgramStructureController@inHomeProgramStructure');
Route::post('inHomeProgramStructures', 'Modules\InHomeProgramStructureController@store');
Route::get('inHomeProgramStructures/{id}/delete', 'Modules\InHomeProgramStructureController@delete');
Route::get('inHomeProgramStructures/{id}', 'Modules\InHomeProgramStructureController@get');
Route::post('inHomeProgramStructures/{id}', 'Modules\InHomeProgramStructureController@update');

Route::get('inHomeProgramTopics', 'Modules\InHomeProgramTopicController@index');
Route::get('inHomeProgramTopics/search', 'Modules\InHomeProgramTopicController@search');
Route::get('inHomeProgramTopics/add', 'Modules\InHomeProgramTopicController@create');
Route::get('inHomeProgramTopics/api/{id}', 'Modules\InHomeProgramTopicController@inHomeProgramTopic');
Route::post('inHomeProgramTopics', 'Modules\InHomeProgramTopicController@store');
Route::get('inHomeProgramTopics/{id}/delete', 'Modules\InHomeProgramTopicController@delete');
Route::get('inHomeProgramTopics/{id}', 'Modules\InHomeProgramTopicController@get');
Route::post('inHomeProgramTopics/{id}', 'Modules\InHomeProgramTopicController@update');

Route::get('locations', 'Modules\LocationController@index');
Route::get('locations/search', 'Modules\LocationController@search');
Route::get('locations/add', 'Modules\LocationController@create');
Route::get('locations/api/{id}', 'Modules\LocationController@location');
Route::get('locations/api/{id}/holidayCampModules', 'Modules\LocationController@holidayCampModulesApi');
Route::get('locations/api/{id}/regularProgramClasseSchedules', 'Modules\LocationController@regularProgramClasseSchedulesApi');
Route::get('locations/api/{id}/holidayCampCourses', 'Modules\LocationController@holidayCampCoursesApi');
Route::post('locations', 'Modules\LocationController@store');
Route::get('locations/{id}/holidayCampModules', 'Modules\LocationController@holidayCampModules');
Route::get('locations/{id}/regularProgramClasseSchedules', 'Modules\LocationController@regularProgramClasseSchedules');
Route::get('locations/{id}/holidayCampCourses', 'Modules\LocationController@holidayCampCourses');
Route::get('locations/{id}/delete', 'Modules\LocationController@delete');
Route::get('locations/{id}', 'Modules\LocationController@get');
Route::post('locations/{id}', 'Modules\LocationController@update');

Route::get('payments', 'Modules\PaymentController@index');
Route::get('payments/search', 'Modules\PaymentController@search');
Route::get('payments/add', 'Modules\PaymentController@create');
Route::get('payments/api/{id}', 'Modules\PaymentController@payment');
Route::post('payments', 'Modules\PaymentController@store');
Route::get('payments/{id}/delete', 'Modules\PaymentController@delete');
Route::get('payments/{id}', 'Modules\PaymentController@get');
Route::post('payments/{id}', 'Modules\PaymentController@update');

Route::get('programClasses', 'Modules\ProgramClassController@index');
Route::get('programClasses/search', 'Modules\ProgramClassController@search');
Route::get('programClasses/add', 'Modules\ProgramClassController@create');
Route::get('programClasses/api/{id}', 'Modules\ProgramClassController@programClass');
Route::get('programClasses/api/{id}/attachments', 'Modules\ProgramClassController@attachmentsApi');
Route::get('programClasses/api/{id}/attendences', 'Modules\ProgramClassController@attendencesApi');
Route::get('programClasses/api/{id}/exams', 'Modules\ProgramClassController@examsApi');
Route::get('programClasses/api/{id}/enrollments', 'Modules\ProgramClassController@enrollmentsApi');
Route::post('programClasses', 'Modules\ProgramClassController@store');
Route::get('programClasses/{id}/attachments', 'Modules\ProgramClassController@attachments');
Route::get('programClasses/{id}/attendences', 'Modules\ProgramClassController@attendences');
Route::get('programClasses/{id}/exams', 'Modules\ProgramClassController@exams');
Route::get('programClasses/{id}/enrollments', 'Modules\ProgramClassController@enrollments');
Route::get('programClasses/{id}/delete', 'Modules\ProgramClassController@delete');
Route::get('programClasses/{id}', 'Modules\ProgramClassController@get');
Route::post('programClasses/{id}', 'Modules\ProgramClassController@update');

Route::get('programLevels', 'Modules\ProgramLevelController@index');
Route::get('programLevels/search', 'Modules\ProgramLevelController@search');
Route::get('programLevels/add', 'Modules\ProgramLevelController@create');
Route::get('programLevels/api/{id}', 'Modules\ProgramLevelController@programLevel');
Route::get('programLevels/api/{id}/holidayCampCourses', 'Modules\ProgramLevelController@holidayCampCoursesApi');
Route::post('programLevels', 'Modules\ProgramLevelController@store');
Route::get('programLevels/{id}/holidayCampCourses', 'Modules\ProgramLevelController@holidayCampCourses');
Route::get('programLevels/{id}/delete', 'Modules\ProgramLevelController@delete');
Route::get('programLevels/{id}', 'Modules\ProgramLevelController@get');
Route::post('programLevels/{id}', 'Modules\ProgramLevelController@update');

Route::get('regularPrograms', 'Modules\RegularProgramController@index');
Route::get('regularPrograms/search', 'Modules\RegularProgramController@search');
Route::get('regularPrograms/add', 'Modules\RegularProgramController@create');
Route::get('regularPrograms/api/{id}', 'Modules\RegularProgramController@regularProgram');
Route::get('regularPrograms/api/{id}/regularProgramEvents', 'Modules\RegularProgramController@regularProgramEventsApi');
Route::get('regularPrograms/api/{id}/regularProgramFeatures', 'Modules\RegularProgramController@regularProgramFeaturesApi');
Route::get('regularPrograms/api/{id}/regularProgramFormats', 'Modules\RegularProgramController@regularProgramFormatsApi');
Route::get('regularPrograms/api/{id}/regularProgramSemesters', 'Modules\RegularProgramController@regularProgramSemestersApi');
Route::get('regularPrograms/api/{id}/regularProgramStructures', 'Modules\RegularProgramController@regularProgramStructuresApi');
Route::post('regularPrograms', 'Modules\RegularProgramController@store');
Route::get('regularPrograms/{id}/regularProgramEvents', 'Modules\RegularProgramController@regularProgramEvents');
Route::get('regularPrograms/{id}/regularProgramFeatures', 'Modules\RegularProgramController@regularProgramFeatures');
Route::get('regularPrograms/{id}/regularProgramFormats', 'Modules\RegularProgramController@regularProgramFormats');
Route::get('regularPrograms/{id}/regularProgramSemesters', 'Modules\RegularProgramController@regularProgramSemesters');
Route::get('regularPrograms/{id}/regularProgramStructures', 'Modules\RegularProgramController@regularProgramStructures');
Route::get('regularPrograms/{id}/delete', 'Modules\RegularProgramController@delete');
Route::get('regularPrograms/{id}', 'Modules\RegularProgramController@get');
Route::post('regularPrograms/{id}', 'Modules\RegularProgramController@update');

Route::get('regularProgramClasseSchedules', 'Modules\RegularProgramClasseScheduleController@index');
Route::get('regularProgramClasseSchedules/search', 'Modules\RegularProgramClasseScheduleController@search');
Route::get('regularProgramClasseSchedules/add', 'Modules\RegularProgramClasseScheduleController@create');
Route::get('regularProgramClasseSchedules/api/{id}', 'Modules\RegularProgramClasseScheduleController@regularProgramClasseSchedule');
Route::post('regularProgramClasseSchedules', 'Modules\RegularProgramClasseScheduleController@store');
Route::get('regularProgramClasseSchedules/{id}/delete', 'Modules\RegularProgramClasseScheduleController@delete');
Route::get('regularProgramClasseSchedules/{id}', 'Modules\RegularProgramClasseScheduleController@get');
Route::post('regularProgramClasseSchedules/{id}', 'Modules\RegularProgramClasseScheduleController@update');

Route::get('regularProgramEvents', 'Modules\RegularProgramEventController@index');
Route::get('regularProgramEvents/search', 'Modules\RegularProgramEventController@search');
Route::get('regularProgramEvents/add', 'Modules\RegularProgramEventController@create');
Route::get('regularProgramEvents/api/{id}', 'Modules\RegularProgramEventController@regularProgramEvent');
Route::post('regularProgramEvents', 'Modules\RegularProgramEventController@store');
Route::get('regularProgramEvents/{id}/delete', 'Modules\RegularProgramEventController@delete');
Route::get('regularProgramEvents/{id}', 'Modules\RegularProgramEventController@get');
Route::post('regularProgramEvents/{id}', 'Modules\RegularProgramEventController@update');

Route::get('regularProgramFeatures', 'Modules\RegularProgramFeatureController@index');
Route::get('regularProgramFeatures/search', 'Modules\RegularProgramFeatureController@search');
Route::get('regularProgramFeatures/add', 'Modules\RegularProgramFeatureController@create');
Route::get('regularProgramFeatures/api/{id}', 'Modules\RegularProgramFeatureController@regularProgramFeature');
Route::post('regularProgramFeatures', 'Modules\RegularProgramFeatureController@store');
Route::get('regularProgramFeatures/{id}/delete', 'Modules\RegularProgramFeatureController@delete');
Route::get('regularProgramFeatures/{id}', 'Modules\RegularProgramFeatureController@get');
Route::post('regularProgramFeatures/{id}', 'Modules\RegularProgramFeatureController@update');

Route::get('regularProgramFormats', 'Modules\RegularProgramFormatController@index');
Route::get('regularProgramFormats/search', 'Modules\RegularProgramFormatController@search');
Route::get('regularProgramFormats/add', 'Modules\RegularProgramFormatController@create');
Route::get('regularProgramFormats/api/{id}', 'Modules\RegularProgramFormatController@regularProgramFormat');
Route::post('regularProgramFormats', 'Modules\RegularProgramFormatController@store');
Route::get('regularProgramFormats/{id}/delete', 'Modules\RegularProgramFormatController@delete');
Route::get('regularProgramFormats/{id}', 'Modules\RegularProgramFormatController@get');
Route::post('regularProgramFormats/{id}', 'Modules\RegularProgramFormatController@update');

Route::get('regularProgramSemesters', 'Modules\RegularProgramSemesterController@index');
Route::get('regularProgramSemesters/search', 'Modules\RegularProgramSemesterController@search');
Route::get('regularProgramSemesters/add', 'Modules\RegularProgramSemesterController@create');
Route::get('regularProgramSemesters/api/{id}', 'Modules\RegularProgramSemesterController@regularProgramSemester');
Route::get('regularProgramSemesters/api/{id}/regularProgramClasseSchedules', 'Modules\RegularProgramSemesterController@regularProgramClasseSchedulesApi');
Route::post('regularProgramSemesters', 'Modules\RegularProgramSemesterController@store');
Route::get('regularProgramSemesters/{id}/regularProgramClasseSchedules', 'Modules\RegularProgramSemesterController@regularProgramClasseSchedules');
Route::get('regularProgramSemesters/{id}/delete', 'Modules\RegularProgramSemesterController@delete');
Route::get('regularProgramSemesters/{id}', 'Modules\RegularProgramSemesterController@get');
Route::post('regularProgramSemesters/{id}', 'Modules\RegularProgramSemesterController@update');

Route::get('regularProgramStructures', 'Modules\RegularProgramStructureController@index');
Route::get('regularProgramStructures/search', 'Modules\RegularProgramStructureController@search');
Route::get('regularProgramStructures/add', 'Modules\RegularProgramStructureController@create');
Route::get('regularProgramStructures/api/{id}', 'Modules\RegularProgramStructureController@regularProgramStructure');
Route::post('regularProgramStructures', 'Modules\RegularProgramStructureController@store');
Route::get('regularProgramStructures/{id}/delete', 'Modules\RegularProgramStructureController@delete');
Route::get('regularProgramStructures/{id}', 'Modules\RegularProgramStructureController@get');
Route::post('regularProgramStructures/{id}', 'Modules\RegularProgramStructureController@update');

Route::get('roles', 'Modules\RoleController@index');
Route::get('roles/search', 'Modules\RoleController@search');
Route::get('roles/add', 'Modules\RoleController@create');
Route::get('roles/api/{id}', 'Modules\RoleController@role');
Route::get('roles/api/{id}/users', 'Modules\RoleController@usersApi');
Route::post('roles', 'Modules\RoleController@store');
Route::get('roles/{id}/users', 'Modules\RoleController@users');
Route::get('roles/{id}/delete', 'Modules\RoleController@delete');
Route::get('roles/{id}', 'Modules\RoleController@get');
Route::post('roles/{id}', 'Modules\RoleController@update');

Route::get('testimonials', 'Modules\TestimonialController@index');
Route::get('testimonials/search', 'Modules\TestimonialController@search');
Route::get('testimonials/add', 'Modules\TestimonialController@create');
Route::get('testimonials/api/{id}', 'Modules\TestimonialController@testimonial');
Route::post('testimonials', 'Modules\TestimonialController@store');
Route::get('testimonials/{id}/delete', 'Modules\TestimonialController@delete');
Route::get('testimonials/{id}', 'Modules\TestimonialController@get');
Route::post('testimonials/{id}', 'Modules\TestimonialController@update');

Route::get('users', 'Modules\UserController@index');
Route::get('users/search', 'Modules\UserController@search');
Route::get('users/add', 'Modules\UserController@create');
Route::get('users/api/{id}', 'Modules\UserController@user');
Route::get('users/api/{id}/enrollments', 'Modules\UserController@enrollmentsApi');
Route::get('users/api/{id}/payments', 'Modules\UserController@paymentsApi');
Route::get('users/api/{id}/users', 'Modules\UserController@usersApi');
Route::get('users/api/{id}/usersVouchers', 'Modules\UserController@usersVouchersApi');
Route::post('users', 'Modules\UserController@store');
Route::get('users/{id}/enrollments', 'Modules\UserController@enrollments');
Route::get('users/{id}/payments', 'Modules\UserController@payments');
Route::get('users/{id}/users', 'Modules\UserController@users');
Route::get('users/{id}/usersVouchers', 'Modules\UserController@usersVouchers');
Route::get('users/{id}/delete', 'Modules\UserController@delete');
Route::get('users/{id}', 'Modules\UserController@get');
Route::post('users/{id}', 'Modules\UserController@update');

Route::get('usersVouchers', 'Modules\UsersVoucherController@index');
Route::get('usersVouchers/search', 'Modules\UsersVoucherController@search');
Route::get('usersVouchers/add', 'Modules\UsersVoucherController@create');
Route::get('usersVouchers/api/{id}', 'Modules\UsersVoucherController@usersVoucher');
Route::get('usersVouchers/api/{id}/enrollments', 'Modules\UsersVoucherController@enrollmentsApi');
Route::post('usersVouchers', 'Modules\UsersVoucherController@store');
Route::get('usersVouchers/{id}/enrollments', 'Modules\UsersVoucherController@enrollments');
Route::get('usersVouchers/{id}/delete', 'Modules\UsersVoucherController@delete');
Route::get('usersVouchers/{id}', 'Modules\UsersVoucherController@get');
Route::post('usersVouchers/{id}', 'Modules\UsersVoucherController@update');

Route::get('vouchers', 'Modules\VoucherController@index');
Route::get('vouchers/search', 'Modules\VoucherController@search');
Route::get('vouchers/add', 'Modules\VoucherController@create');
Route::get('vouchers/api/{id}', 'Modules\VoucherController@voucher');
Route::get('vouchers/api/{id}/usersVouchers', 'Modules\VoucherController@usersVouchersApi');
Route::post('vouchers', 'Modules\VoucherController@store');
Route::get('vouchers/{id}/usersVouchers', 'Modules\VoucherController@usersVouchers');
Route::get('vouchers/{id}/delete', 'Modules\VoucherController@delete');
Route::get('vouchers/{id}', 'Modules\VoucherController@get');
Route::post('vouchers/{id}', 'Modules\VoucherController@update');

Route::get('weekdays', 'Modules\WeekdayController@index');
Route::get('weekdays/search', 'Modules\WeekdayController@search');
Route::get('weekdays/add', 'Modules\WeekdayController@create');
Route::get('weekdays/api/{id}', 'Modules\WeekdayController@weekday');
Route::get('weekdays/api/{id}/inHomeProgramSchedules', 'Modules\WeekdayController@inHomeProgramSchedulesApi');
Route::get('weekdays/api/{id}/regularProgramClasseSchedules', 'Modules\WeekdayController@regularProgramClasseSchedulesApi');
Route::get('weekdays/api/{id}/inHomePrograms', 'Modules\WeekdayController@inHomeProgramsApi');
Route::post('weekdays', 'Modules\WeekdayController@store');
Route::get('weekdays/{id}/inHomeProgramSchedules', 'Modules\WeekdayController@inHomeProgramSchedules');
Route::get('weekdays/{id}/regularProgramClasseSchedules', 'Modules\WeekdayController@regularProgramClasseSchedules');
Route::get('weekdays/{id}/inHomePrograms', 'Modules\WeekdayController@inHomePrograms');
Route::get('weekdays/{id}/delete', 'Modules\WeekdayController@delete');
Route::get('weekdays/{id}', 'Modules\WeekdayController@get');
Route::post('weekdays/{id}', 'Modules\WeekdayController@update');
	//routes will be appended here
});