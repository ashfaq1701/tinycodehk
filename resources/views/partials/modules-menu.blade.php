<div class="menu_section">
	<h3>System Modules</h3>
	<ul class="nav side-menu">

<li>
	<a href="/modules/attachments">
     	<i class="fa fa-laptop"></i>
		Attachments	</a>
</li>

<li>
	<a href="/modules/attendences">
     	<i class="fa fa-laptop"></i>
		Attendences	</a>
</li>

<li>
	<a href="/modules/enrollments">
     	<i class="fa fa-laptop"></i>
		Enrollments	</a>
</li>

<li>
	<a href="/modules/exams">
     	<i class="fa fa-laptop"></i>
		Exams	</a>
</li>

<li>
	<a href="/modules/examScores">
     	<i class="fa fa-laptop"></i>
		ExamScores	</a>
</li>

<li>
	<a href="/modules/holidayCamps">
     	<i class="fa fa-laptop"></i>
		HolidayCamps	</a>
</li>

<li>
	<a href="/modules/holidayCampCourses">
     	<i class="fa fa-laptop"></i>
		HolidayCampCourses	</a>
</li>

<li>
	<a href="/modules/holidayCampCourseBringings">
     	<i class="fa fa-laptop"></i>
		HolidayCampCourseBringings	</a>
</li>

<li>
	<a href="/modules/holidayCampCourseFeatures">
     	<i class="fa fa-laptop"></i>
		HolidayCampCourseFeatures	</a>
</li>

<li>
	<a href="/modules/holidayCampCourseHighlights">
     	<i class="fa fa-laptop"></i>
		HolidayCampCourseHighlights	</a>
</li>

<li>
	<a href="/modules/holidayCampCourseSubFeatures">
     	<i class="fa fa-laptop"></i>
		HolidayCampCourseSubFeatures	</a>
</li>

<li>
	<a href="/modules/holidayCampModules">
     	<i class="fa fa-laptop"></i>
		HolidayCampModules	</a>
</li>

<li>
	<a href="/modules/inHomePrograms">
     	<i class="fa fa-laptop"></i>
		InHomePrograms	</a>
</li>

<li>
	<a href="/modules/inHomeProgramFormats">
     	<i class="fa fa-laptop"></i>
		InHomeProgramFormats	</a>
</li>

<li>
	<a href="/modules/inHomeProgramPricings">
     	<i class="fa fa-laptop"></i>
		InHomeProgramPricings	</a>
</li>

<li>
	<a href="/modules/inHomeProgramSchedules">
     	<i class="fa fa-laptop"></i>
		InHomeProgramSchedules	</a>
</li>

<li>
	<a href="/modules/inHomeProgramStructures">
     	<i class="fa fa-laptop"></i>
		InHomeProgramStructures	</a>
</li>

<li>
	<a href="/modules/inHomeProgramTopics">
     	<i class="fa fa-laptop"></i>
		InHomeProgramTopics	</a>
</li>

<li>
	<a href="/modules/locations">
     	<i class="fa fa-laptop"></i>
		Locations	</a>
</li>

<li>
	<a href="/modules/payments">
     	<i class="fa fa-laptop"></i>
		Payments	</a>
</li>

<li>
	<a href="/modules/programClasses">
     	<i class="fa fa-laptop"></i>
		ProgramClasses	</a>
</li>

<li>
	<a href="/modules/programLevels">
     	<i class="fa fa-laptop"></i>
		ProgramLevels	</a>
</li>

<li>
	<a href="/modules/regularPrograms">
     	<i class="fa fa-laptop"></i>
		RegularPrograms	</a>
</li>

<li>
	<a href="/modules/regularProgramClasseSchedules">
     	<i class="fa fa-laptop"></i>
		RegularProgramClasseSchedules	</a>
</li>

<li>
	<a href="/modules/regularProgramEvents">
     	<i class="fa fa-laptop"></i>
		RegularProgramEvents	</a>
</li>

<li>
	<a href="/modules/regularProgramFeatures">
     	<i class="fa fa-laptop"></i>
		RegularProgramFeatures	</a>
</li>

<li>
	<a href="/modules/regularProgramFormats">
     	<i class="fa fa-laptop"></i>
		RegularProgramFormats	</a>
</li>

<li>
	<a href="/modules/regularProgramSemesters">
     	<i class="fa fa-laptop"></i>
		RegularProgramSemesters	</a>
</li>

<li>
	<a href="/modules/regularProgramStructures">
     	<i class="fa fa-laptop"></i>
		RegularProgramStructures	</a>
</li>

<li>
	<a href="/modules/roles">
     	<i class="fa fa-laptop"></i>
		Roles	</a>
</li>

<li>
	<a href="/modules/testimonials">
     	<i class="fa fa-laptop"></i>
		Testimonials	</a>
</li>

<li>
	<a href="/modules/users">
     	<i class="fa fa-laptop"></i>
		Users	</a>
</li>

<li>
	<a href="/modules/usersVouchers">
     	<i class="fa fa-laptop"></i>
		UsersVouchers	</a>
</li>

<li>
	<a href="/modules/vouchers">
     	<i class="fa fa-laptop"></i>
		Vouchers	</a>
</li>

<li>
	<a href="/modules/weekdays">
     	<i class="fa fa-laptop"></i>
		Weekdays	</a>
</li>
	<!-- Menu will be added here -->
	</ul>
</div>