@extends('layouts.blank')

@section('main_container')
	<div class="right_col col-md-offset-3" role="main">
		<div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading bg-muni-blue">
                    	Module Importer
                    </div>  
                    
					<div class="panel-body">                                          
                    	<div class="row">
                    		 <div class="form-group">
  								<label class="col-md-2 control-label" for="id">Select File</label>  
  								<div class="col-md-10">
  									<input type="file" id="module-file-upload" class="form-control"/>
  								</div>
							</div>                   
                    	</div>
                    </div>
                </div>
			</div>
		</div>
	</div>    
@endsection

@push('scripts')
	<script src="/js/importer.js"></script>
@endpush