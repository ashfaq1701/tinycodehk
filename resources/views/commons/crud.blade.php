@extends('layouts.blank')

@section('main_container')
	<div class="right_col col-md-offset-3" role="main">
		<div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading bg-muni-blue">
                    	@yield('crud-title')
                    </div>  
                    
					<div class="panel-body" id="crud-body">                                          
                    	<div class="row">
                    		<div class="col-md-2 col-md-offset-10">
                    			@yield('crud-right-menu')
                    		</div>
							@yield('crud-body')                       
                    	</div>
                    </div>
                </div>
			</div>
		</div>
	</div>    
@endsection

@push('scripts')
	<script src="/js/globals.js"></script>
	<script src="/js/crud.js"></script>
@endpush