<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="inHomeProgram" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allinHomePrograms as $inHomeProgram)
			<option value="{{ $inHomeProgram->id }}">{{ $inHomeProgram->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'inHomeProgramFormat')
			<th>InHomeProgramFormats			</th>
			@endif
						@if($origin != 'inHomeProgramPricing')
			<th>InHomeProgramPricings			</th>
			@endif
						@if($origin != 'inHomeProgramSchedule')
			<th>InHomeProgramSchedules			</th>
			@endif
						@if($origin != 'inHomeProgramStructure')
			<th>InHomeProgramStructures			</th>
			@endif
						@if($origin != 'inHomeProgramTopic')
			<th>InHomeProgramTopics			</th>
			@endif
						@if($origin != 'weekday')
			<th>Weekdays			</th>
			@endif
						<th>Name			</th>
						<th>From Age			</th>
						<th>To Age			</th>
						<th>Description 1			</th>
						<th>Description 2			</th>
						<th>Image 1			</th>
						<th>Image 2			</th>
						<th>Language			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($inHomePrograms as $inHomeProgram)
		<tr>
						<td>
				<a href="/modules/inHomePrograms/{{ $inHomeProgram->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/inHomePrograms/{{ $inHomeProgram->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'inHomeProgramFormat')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="inHomeProgram" data-module="inHomeProgramFormat" data-id="{{ $inHomeProgram->id }}">InHomeProgramFormats</a>
			</td>
			@endif
						@if($origin != 'inHomeProgramPricing')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="inHomeProgram" data-module="inHomeProgramPricing" data-id="{{ $inHomeProgram->id }}">InHomeProgramPricings</a>
			</td>
			@endif
						@if($origin != 'inHomeProgramSchedule')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="inHomeProgram" data-module="inHomeProgramSchedule" data-id="{{ $inHomeProgram->id }}">InHomeProgramSchedules</a>
			</td>
			@endif
						@if($origin != 'inHomeProgramStructure')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="inHomeProgram" data-module="inHomeProgramStructure" data-id="{{ $inHomeProgram->id }}">InHomeProgramStructures</a>
			</td>
			@endif
						@if($origin != 'inHomeProgramTopic')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="inHomeProgram" data-module="inHomeProgramTopic" data-id="{{ $inHomeProgram->id }}">InHomeProgramTopics</a>
			</td>
			@endif
						@if($origin != 'weekday')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="inHomeProgram" data-module="weekday" data-id="{{ $inHomeProgram->id }}">Weekdays</a>
			</td>
			@endif
							
							
						<td>{{ $inHomeProgram->name }}</td>
						<td>{{ $inHomeProgram->from_age }}</td>
						<td>{{ $inHomeProgram->to_age }}</td>
						<td>{{ $inHomeProgram->description_1 }}</td>
						<td>{{ $inHomeProgram->description_2 }}</td>
						<td>{{ $inHomeProgram->image_1 }}</td>
						<td>{{ $inHomeProgram->image_2 }}</td>
						<td>{{ $inHomeProgram->language }}</td>
					</tr>
		@endforeach
	</tbody>
</table>