@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/inHomePrograms/add">Add InHomeProgram</a>
@endsection

@section('crud-title')
	InHomePrograms
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>InHomeProgramFormats</th>
								<th>InHomeProgramPricings</th>
								<th>InHomeProgramSchedules</th>
								<th>InHomeProgramStructures</th>
								<th>InHomeProgramTopics</th>
								<th>Weekdays</th>
								<th>Name</th>
								<th>From Age</th>
								<th>To Age</th>
								<th>Description 1</th>
								<th>Description 2</th>
								<th>Image 1</th>
								<th>Image 2</th>
								<th>Language</th>
							</tr>
		</thead>
		<tbody>
			@foreach($inHomePrograms as $inHomeProgram)
			<tr>
								<td>
					<a href="/modules/inHomePrograms/{{ $inHomeProgram->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/inHomePrograms/{{ $inHomeProgram->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="inHomeProgram" data-module="inHomeProgramFormat" data-id="{{ $inHomeProgram->id }}">InHomeProgramFormats</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="inHomeProgram" data-module="inHomeProgramPricing" data-id="{{ $inHomeProgram->id }}">InHomeProgramPricings</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="inHomeProgram" data-module="inHomeProgramSchedule" data-id="{{ $inHomeProgram->id }}">InHomeProgramSchedules</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="inHomeProgram" data-module="inHomeProgramStructure" data-id="{{ $inHomeProgram->id }}">InHomeProgramStructures</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="inHomeProgram" data-module="inHomeProgramTopic" data-id="{{ $inHomeProgram->id }}">InHomeProgramTopics</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="belongsToMany" data-origin="inHomeProgram" data-module="weekday" data-id="{{ $inHomeProgram->id }}">Weekdays</a></td>
								
								
								<td>{{ $inHomeProgram->name }}</td>
								<td>{{ $inHomeProgram->from_age }}</td>
								<td>{{ $inHomeProgram->to_age }}</td>
								<td>{{ $inHomeProgram->description_1 }}</td>
								<td>{{ $inHomeProgram->description_2 }}</td>
								<td>{{ $inHomeProgram->image_1 }}</td>
								<td>{{ $inHomeProgram->image_2 }}</td>
								<td>{{ $inHomeProgram->language }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
