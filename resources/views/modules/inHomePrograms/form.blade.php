@extends('commons.crud')

@section('crud-title')
	Add InHomeProgram
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="inHomePrograms">
		
						
				<div class="row" id="rowLHnCG"><div id="col-h0ajf" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <input type="text" id="name" class="form-control control-form" name="name" placeholder="Name" value="{{ empty($inHomeProgram) ? '' : $inHomeProgram->name }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div><div id="col-C0idR" class="col-md-6"></div></div><div class="row" id="rowN9PQ5"><div id="col-GzSGW" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="from_age">From Age</label>  
  		                <input id="from_age" class="form-control control-form" type="number" name="from_age" placeholder="From Age" value="{{ empty($inHomeProgram) ? '' : $inHomeProgram->from_age }}" />
              		</div>
  	</div>
</div></div><div id="col-BqCJR" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="to_age">To Age</label>  
  		                <input id="to_age" class="form-control control-form" type="number" name="to_age" placeholder="To Age" value="{{ empty($inHomeProgram) ? '' : $inHomeProgram->to_age }}" />
              		</div>
  	</div>
</div></div></div><div class="row" id="rowEyKuL"><div id="col-1fqLI" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="description_1">Description 1</label>  
  		                <textarea id="description_1" class="form-control control-form" rows="4" name="description_1" placeholder="Description 1">{{ empty($inHomeProgram) ? '' : $inHomeProgram->description_1 }}</textarea>
                  		</div>
  	</div>
</div></div><div id="col-tVXNA" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="description_2">Description 2</label>  
  		                <textarea id="description_2" class="form-control control-form" rows="4" name="description_2" placeholder="Description 2">{{ empty($inHomeProgram) ? '' : $inHomeProgram->description_2 }}</textarea>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowx0fB5"><div id="col-dHxAz" class="col-md-6"><div class="form-group">
	<div class="row">
		<input type="hidden" name="image_1" id="image_1" class="control-form" value="{{ empty($inHomeProgram) ? '' : $inHomeProgram->image_1 }}"/>
  		<div class="col-sm-12 col-md-12">
  			<label class="control-label" for="image_1">Image 1</label>  
  			<input type="file" id="image_1_file_ip" class="form-control file_ip" placeholder="Image 1"/>
        </div>
  	</div>
</div></div><div id="col-tqena" class="col-md-6"><div class="form-group">
	<div class="row">
		<input type="hidden" name="image_2" id="image_2" class="control-form" value="{{ empty($inHomeProgram) ? '' : $inHomeProgram->image_2 }}"/>
  		<div class="col-sm-12 col-md-12">
  			<label class="control-label" for="image_2">Image 2</label>  
  		    <input type="file" id="image_2_file_ip" class="form-control file_ip" placeholder="Image 2"/>
        </div>
  	</div>
</div></div></div><div class="row" id="rowyGI6n"><div id="col-JAd8g" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="language">Language</label>  
  		                <input type="text" id="language" class="form-control control-form" name="language" placeholder="Language" value="{{ empty($inHomeProgram) ? '' : $inHomeProgram->language }}" maxlength="50"/>
                  		</div>
  	</div>
</div></div><div id="col-EFuP9" class="col-md-6"></div></div><div class="row" id="rowENh7h"><div id="col-htDa7" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		InHomeProgramFormats	</div>
	<div class="panel-body relationship-panel" data-model="InHomeProgramFormat" data-variable-name="inHomeProgramFormats" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="inHomeProgramFormats" data-type="many">
						<option value="0">Select</option>
						@foreach($allinHomeProgramFormats as $inHomeProgramFormat)
						<option value="{{ $inHomeProgramFormat->id }}">{{ $inHomeProgramFormat->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($inHomeProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="inHomeProgramFormats">
						<option value="">Select</option>
						@foreach($inHomeProgram->inHomeProgramFormats as $inHomeProgramFormat)
						<option value="{{ $inHomeProgramFormat->id }}">{{ $inHomeProgramFormat->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="inHomeProgramFormats" value="{{ $inHomeProgramFormatsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  		                <input type="text" data-field="title" class="form-control control-form" placeholder="Title" maxlength="200"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="inHomeProgramFormats" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-Fakge" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		InHomeProgramPricings	</div>
	<div class="panel-body relationship-panel" data-model="InHomeProgramPricing" data-variable-name="inHomeProgramPricings" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="inHomeProgramPricings" data-type="many">
						<option value="0">Select</option>
						@foreach($allinHomeProgramPricings as $inHomeProgramPricing)
						<option value="{{ $inHomeProgramPricing->id }}">{{ $inHomeProgramPricing->price }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($inHomeProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="inHomeProgramPricings">
						<option value="">Select</option>
						@foreach($inHomeProgram->inHomeProgramPricings as $inHomeProgramPricing)
						<option value="{{ $inHomeProgramPricing->id }}">{{ $inHomeProgramPricing->price }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="inHomeProgramPricings" value="{{ $inHomeProgramPricingsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Student Numbers</label>  
  		  		            <input data-field="student_numbers" class="form-control control-form" type="number" placeholder="Student Numbers" />
              		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Price</label>  
  		  		            <input data-field="price" class="form-control control-form" type="number" placeholder="Price" />
              		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="inHomeProgramPricings" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowpizrC"><div id="col-VeX7z" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		InHomeProgramSchedules	</div>
	<div class="panel-body relationship-panel" data-model="InHomeProgramSchedule" data-variable-name="inHomeProgramSchedules" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="inHomeProgramSchedules" data-type="many">
						<option value="0">Select</option>
						@foreach($allinHomeProgramSchedules as $inHomeProgramSchedule)
						<option value="{{ $inHomeProgramSchedule->id }}">{{ $inHomeProgramSchedule->from_time }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($inHomeProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="inHomeProgramSchedules">
						<option value="">Select</option>
						@foreach($inHomeProgram->inHomeProgramSchedules as $inHomeProgramSchedule)
						<option value="{{ $inHomeProgramSchedule->id }}">{{ $inHomeProgramSchedule->from_time }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="inHomeProgramSchedules" value="{{ $inHomeProgramSchedulesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Time</label>  
  		  		                <input type="text" data-field="from_time" class="form-control control-form" placeholder="From Time" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Time</label>  
  		  		                <input type="text" data-field="to_time" class="form-control control-form" placeholder="To Time" maxlength="45"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="inHomeProgramSchedules" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-SCO6m" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		InHomeProgramStructures	</div>
	<div class="panel-body relationship-panel" data-model="InHomeProgramStructure" data-variable-name="inHomeProgramStructures" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="inHomeProgramStructures" data-type="many">
						<option value="0">Select</option>
						@foreach($allinHomeProgramStructures as $inHomeProgramStructure)
						<option value="{{ $inHomeProgramStructure->id }}">{{ $inHomeProgramStructure->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($inHomeProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="inHomeProgramStructures">
						<option value="">Select</option>
						@foreach($inHomeProgram->inHomeProgramStructures as $inHomeProgramStructure)
						<option value="{{ $inHomeProgramStructure->id }}">{{ $inHomeProgramStructure->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="inHomeProgramStructures" value="{{ $inHomeProgramStructuresValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  		                <input type="text" data-field="title" class="form-control control-form" placeholder="Title" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Value</label>  
  		  		                <input type="text" data-field="value" class="form-control control-form" placeholder="Value" maxlength="45"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="inHomeProgramStructures" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowHao8c"><div id="col-sA4VR" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		InHomeProgramTopics	</div>
	<div class="panel-body relationship-panel" data-model="InHomeProgramTopic" data-variable-name="inHomeProgramTopics" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="inHomeProgramTopics" data-type="many">
						<option value="0">Select</option>
						@foreach($allinHomeProgramTopics as $inHomeProgramTopic)
						<option value="{{ $inHomeProgramTopic->id }}">{{ $inHomeProgramTopic->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($inHomeProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="inHomeProgramTopics">
						<option value="">Select</option>
						@foreach($inHomeProgram->inHomeProgramTopics as $inHomeProgramTopic)
						<option value="{{ $inHomeProgramTopic->id }}">{{ $inHomeProgramTopic->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="inHomeProgramTopics" value="{{ $inHomeProgramTopicsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  		                <input type="text" data-field="title" class="form-control control-form" placeholder="Title" maxlength="100"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="inHomeProgramTopics" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-ltXtE" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Weekdays	</div>
	<div class="panel-body relationship-panel" data-model="Weekday" data-variable-name="weekdays" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="weekdays" data-type="many">
						<option value="0">Select</option>
						@foreach($allweekdays as $weekday)
						<option value="{{ $weekday->id }}">{{ $weekday->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($inHomeProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="weekdays">
						<option value="">Select</option>
						@foreach($inHomeProgram->weekdays as $weekday)
						<option value="{{ $weekday->id }}">{{ $weekday->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="weekdays" value="{{ $weekdaysValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  		                <input type="text" data-field="name" class="form-control control-form" placeholder="Name" maxlength="45"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="weekdays" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div>	

<div class="row" id="rowMSTr1">
	<div id="col-mtREY" class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				Enrollments
			</div>
			<div class="panel-body relationship-panel" data-model="Enrollment"  data-variable-name="enrollments" data-type="many">
				<div class="well">
					<div class="row">
						<div class="col-md-8">
							<select class="form-control" data-module="enrollments" data-type="many">
								<option value="0">Select</option>
								@foreach($allEnrollments as $enrollment)
								<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
						</div>
					</div>
				</div>
				@if(!empty($inHomeProgram))
				<div class="well">
					<div class="row">
						<div class="col-md-12">
							<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="enrollments">
								<option value="">Select</option>
								@foreach($inHomeProgram->enrollments as $enrollment)
								<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				@endif
				<div class="row">
					<input type="hidden" class="related-entry" name="enrollments" value="{{ $enrollmentsValue or '' }}"/>
					<input type="hidden" class="active-entry" value=""/>
					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Course Type</label>  
  		  		                <select class="form-control" data-field="course_type" class="form-control control-form">
  		  		                	<option value="">Select</option>
  		  		                	<option value="inHome">In Home</option>
  		  		                	<option value="regular">Regular</option>
  		  		                	<option value="holidayCamp">Holiday Camp</option>
  		  		                </select>
                  			</div>
  						</div>
  					</div>
  					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Registration No</label>  
  		  		                <input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" maxlength="45"/>
                  			</div>
  						</div>
					</div>
					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Is Paid</label>  
  		  		                <input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" />
                  			</div>
  						</div>
					</div>
					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Total Payment</label>  
  		  		            	<input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" />
              				</div>
  						</div>
					</div>
					<div class="col-md-12">
						<button class="btn btn-success" data-module="enrollments" onclick="localSaveManyRelationship(event)">Save</button>
						<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

		<input type="hidden" name="primary" id="primary" value="{{ empty($inHomeProgram) ? 0 : $inHomeProgram->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/inHomeProgramsForm.js"></script>
@endpush