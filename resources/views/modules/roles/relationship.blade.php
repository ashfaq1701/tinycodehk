<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="role" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allroles as $role)
			<option value="{{ $role->id }}">{{ $role->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'user')
			<th>Users			</th>
			@endif
						<th>Name			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($roles as $role)
		<tr>
						<td>
				<a href="/modules/roles/{{ $role->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/roles/{{ $role->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'user')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="role" data-module="user" data-id="{{ $role->id }}">Users</a>
			</td>
			@endif
							
							
						<td>{{ $role->name }}</td>
					</tr>
		@endforeach
	</tbody>
</table>