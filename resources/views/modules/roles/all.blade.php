@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/roles/add">Add Role</a>
@endsection

@section('crud-title')
	Roles
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>Users</th>
								<th>Name</th>
							</tr>
		</thead>
		<tbody>
			@foreach($roles as $role)
			<tr>
								<td>
					<a href="/modules/roles/{{ $role->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/roles/{{ $role->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="role" data-module="user" data-id="{{ $role->id }}">Users</a></td>
								
								
								<td>{{ $role->name }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
