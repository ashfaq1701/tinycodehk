@extends('commons.crud')

@section('crud-title')
	Add Role
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="roles">
		
						
				<div class="row" id="rowiMjhU"><div id="col-8QiPv" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <input type="text" id="name" class="form-control control-form" name="name" placeholder="Name" value="{{ empty($role) ? '' : $role->name }}" maxlength="50"/>
                  		</div>
  	</div>
</div></div><div id="col-htIy8" class="col-md-6"></div></div><div class="row" id="rowBfrin"><div id="col-kdZoI" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Users	</div>
	<div class="panel-body relationship-panel" data-model="User" data-variable-name="users" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="users" data-type="many">
						<option value="0">Select</option>
						@foreach($allusers as $user)
						<option value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($role))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="users">
						<option value="">Select</option>
						@foreach($role->users as $user)
						<option value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="users" value="{{ $usersValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		       	<span class="required"> *</span>
    	  		                <textarea data-field="name" class="form-control control-form" rows="4" placeholder="Name"></textarea>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Email</label>  
  		       	<span class="required"> *</span>
    	  		                <textarea data-field="email" class="form-control control-form" rows="4" placeholder="Email"></textarea>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Stripe Id</label>  
  		  		                <input type="text" data-field="stripe_id" class="form-control control-form" placeholder="Stripe Id" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Card Brand</label>  
  		  		                <input type="text" data-field="card_brand" class="form-control control-form" placeholder="Card Brand" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Card Last 4</label>  
  		  		                <input type="text" data-field="card_last_4" class="form-control control-form" placeholder="Card Last 4" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Trial Ends At</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="trial_ends_at" placeholder="Trial Ends At" type="text" readonly>
          		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="users" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-WvcMq" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($role) ? 0 : $role->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/rolesForm.js"></script>
@endpush