@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/holidayCampCourseBringings/add">Add HolidayCampCourseBringing</a>
@endsection

@section('crud-title')
	HolidayCampCourseBringings
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>HolidayCampCourse</th>
								<th>Name</th>
							</tr>
		</thead>
		<tbody>
			@foreach($holidayCampCourseBringings as $holidayCampCourseBringing)
			<tr>
								<td>
					<a href="/modules/holidayCampCourseBringings/{{ $holidayCampCourseBringing->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/holidayCampCourseBringings/{{ $holidayCampCourseBringing->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="holidayCampCourseBringing" data-module="holidayCampCourse" data-id="{{ $holidayCampCourseBringing->id }}">HolidayCampCourse</a>
				</td>
								
								<td>{{ $holidayCampCourseBringing->name }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
