<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="holidayCampCourseBringing" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allholidayCampCourseBringings as $holidayCampCourseBringing)
			<option value="{{ $holidayCampCourseBringing->id }}">{{ $holidayCampCourseBringing->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'holidayCampCourse')
			<th>HolidayCampCourse			</th>
			@endif
						<th>Name			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($holidayCampCourseBringings as $holidayCampCourseBringing)
		<tr>
						<td>
				<a href="/modules/holidayCampCourseBringings/{{ $holidayCampCourseBringing->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/holidayCampCourseBringings/{{ $holidayCampCourseBringing->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'holidayCampCourse')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="holidayCampCourseBringing" data-module="holidayCampCourse" data-id="{{ $holidayCampCourseBringing->id }}">HolidayCampCourse</a>
			</td>
			@endif
							
						<td>{{ $holidayCampCourseBringing->name }}</td>
					</tr>
		@endforeach
	</tbody>
</table>