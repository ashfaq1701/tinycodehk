@extends('commons.crud')

@section('crud-title')
	Add HolidayCampCourseBringing
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="holidayCampCourseBringings">
		
						
				<div class="row" id="row8zAeL"><div id="col-yPYut" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <input type="text" id="name" class="form-control control-form" name="name" placeholder="Name" value="{{ empty($holidayCampCourseBringing) ? '' : $holidayCampCourseBringing->name }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div><div id="col-JenXH" class="col-md-6"></div></div><div class="row" id="rowG43eW"><div id="col-gcpd2" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourse	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourse" data-variable-name="holidayCampCourse" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="holidayCampCourses" data-type="one">
						<option value="0">Select</option>
						@foreach($allholidayCampCourses as $holidayCampCourse)
						<option value="{{ $holidayCampCourse->id }}">{{ $holidayCampCourse->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourse" value="{{ (!empty($holidayCampCourseBringing)) ? (!empty($holidayCampCourseBringing->holidayCampCourse) ? $holidayCampCourseBringing->holidayCampCourse->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($holidayCampCourseBringing) ? '' : (empty($holidayCampCourseBringing->holidayCampCourse) ? '' : $holidayCampCourseBringing->holidayCampCourse->name) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Icon</label>  
  		  						<input type="text" data-field="program_icon" class="form-control control-form" placeholder="Program Icon" value="{{ empty($holidayCampCourseBringing) ? '' : (empty($holidayCampCourseBringing->holidayCampCourse) ? '' : $holidayCampCourseBringing->holidayCampCourse->program_icon) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Cover Image</label>  
  		  						<input type="text" data-field="program_cover_image" class="form-control control-form" placeholder="Program Cover Image" value="{{ empty($holidayCampCourseBringing) ? '' : (empty($holidayCampCourseBringing->holidayCampCourse) ? '' : $holidayCampCourseBringing->holidayCampCourse->program_cover_image) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Video Url</label>  
  		  						<input type="text" data-field="program_video_url" class="form-control control-form" placeholder="Program Video Url" value="{{ empty($holidayCampCourseBringing) ? '' : (empty($holidayCampCourseBringing->holidayCampCourse) ? '' : $holidayCampCourseBringing->holidayCampCourse->program_video_url) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  						<input type="text" data-field="language" class="form-control control-form" placeholder="Language" value="{{ empty($holidayCampCourseBringing) ? '' : (empty($holidayCampCourseBringing->holidayCampCourse) ? '' : $holidayCampCourseBringing->holidayCampCourse->language) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($holidayCampCourseBringing) ? '' : (empty($holidayCampCourseBringing->holidayCampCourse) ? '' : $holidayCampCourseBringing->holidayCampCourse->description) }}</textarea>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourse" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-TLTAj" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($holidayCampCourseBringing) ? 0 : $holidayCampCourseBringing->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/holidayCampCourseBringingsForm.js"></script>
@endpush