<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="holidayCampCourseSubFeature" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allholidayCampCourseSubFeatures as $holidayCampCourseSubFeature)
			<option value="{{ $holidayCampCourseSubFeature->id }}">{{ $holidayCampCourseSubFeature->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'holidayCampCourseFeature')
			<th>HolidayCampCourseFeature			</th>
			@endif
						<th>Name			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($holidayCampCourseSubFeatures as $holidayCampCourseSubFeature)
		<tr>
						<td>
				<a href="/modules/holidayCampCourseSubFeatures/{{ $holidayCampCourseSubFeature->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/holidayCampCourseSubFeatures/{{ $holidayCampCourseSubFeature->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'holidayCampCourseFeature')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="holidayCampCourseSubFeature" data-module="holidayCampCourseFeature" data-id="{{ $holidayCampCourseSubFeature->id }}">HolidayCampCourseFeature</a>
			</td>
			@endif
							
						<td>{{ $holidayCampCourseSubFeature->name }}</td>
					</tr>
		@endforeach
	</tbody>
</table>