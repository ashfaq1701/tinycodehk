@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/holidayCampCourseSubFeatures/add">Add HolidayCampCourseSubFeature</a>
@endsection

@section('crud-title')
	HolidayCampCourseSubFeatures
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>HolidayCampCourseFeature</th>
								<th>Name</th>
							</tr>
		</thead>
		<tbody>
			@foreach($holidayCampCourseSubFeatures as $holidayCampCourseSubFeature)
			<tr>
								<td>
					<a href="/modules/holidayCampCourseSubFeatures/{{ $holidayCampCourseSubFeature->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/holidayCampCourseSubFeatures/{{ $holidayCampCourseSubFeature->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="holidayCampCourseSubFeature" data-module="holidayCampCourseFeature" data-id="{{ $holidayCampCourseSubFeature->id }}">HolidayCampCourseFeature</a>
				</td>
								
								<td>{{ $holidayCampCourseSubFeature->name }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
