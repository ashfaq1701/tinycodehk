@extends('commons.crud')

@section('crud-title')
	Add HolidayCampCourseSubFeature
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="holidayCampCourseSubFeatures">
		
						
				<div class="row" id="rowA3Cb6"><div id="col-KSjt0" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <textarea id="name" class="form-control control-form" rows="4" name="name" placeholder="Name">{{ empty($holidayCampCourseSubFeature) ? '' : $holidayCampCourseSubFeature->name }}</textarea>
                  		</div>
  	</div>
</div></div><div id="col-g4HoX" class="col-md-6"></div></div><div class="row" id="rowChM7p"><div id="col-Px1WR" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourseFeature	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourseFeature" data-variable-name="holidayCampCourseFeature" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="holidayCampCourseFeatures" data-type="one">
						<option value="0">Select</option>
						@foreach($allholidayCampCourseFeatures as $holidayCampCourseFeature)
						<option value="{{ $holidayCampCourseFeature->id }}">{{ $holidayCampCourseFeature->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourseFeature" value="{{ (!empty($holidayCampCourseSubFeature)) ? (!empty($holidayCampCourseSubFeature->holidayCampCourseFeature) ? $holidayCampCourseSubFeature->holidayCampCourseFeature->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($holidayCampCourseSubFeature) ? '' : (empty($holidayCampCourseSubFeature->holidayCampCourseFeature) ? '' : $holidayCampCourseSubFeature->holidayCampCourseFeature->name) }}" maxlength="200"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourseFeature" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-HkvPT" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($holidayCampCourseSubFeature) ? 0 : $holidayCampCourseSubFeature->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/holidayCampCourseSubFeaturesForm.js"></script>
@endpush