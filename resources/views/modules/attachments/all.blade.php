@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/attachments/add">Add Attachment</a>
@endsection

@section('crud-title')
	Attachments
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>ProgramClass</th>
								<th>Location</th>
								<th>Mime Type</th>
							</tr>
		</thead>
		<tbody>
			@foreach($attachments as $attachment)
			<tr>
								<td>
					<a href="/modules/attachments/{{ $attachment->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/attachments/{{ $attachment->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="attachment" data-module="programClass" data-id="{{ $attachment->id }}">ProgramClass</a>
				</td>
								
								<td>{{ $attachment->location }}</td>
								<td>{{ $attachment->mime_type }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
