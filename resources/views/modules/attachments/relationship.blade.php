<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="attachment" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allattachments as $attachment)
			<option value="{{ $attachment->id }}">{{ $attachment->location }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'programClass')
			<th>ProgramClass			</th>
			@endif
						<th>Location			</th>
						<th>Mime Type			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($attachments as $attachment)
		<tr>
						<td>
				<a href="/modules/attachments/{{ $attachment->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/attachments/{{ $attachment->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'programClass')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="attachment" data-module="programClass" data-id="{{ $attachment->id }}">ProgramClass</a>
			</td>
			@endif
							
						<td>{{ $attachment->location }}</td>
						<td>{{ $attachment->mime_type }}</td>
					</tr>
		@endforeach
	</tbody>
</table>