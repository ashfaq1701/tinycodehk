@extends('commons.crud')

@section('crud-title')
	Add Attachment
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="attachments">
		
						
				<div class="row" id="rowPzewK"><div id="col-0p5JL" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="location">Location</label>  
  		                <input type="text" id="location" class="form-control control-form" name="location" placeholder="Location" value="{{ empty($attachment) ? '' : $attachment->location }}" maxlength="200"/>
                  		</div>
  	</div>
</div></div><div id="col-yMff4" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="mime_type">Mime Type</label>  
  		                <input type="text" id="mime_type" class="form-control control-form" name="mime_type" placeholder="Mime Type" value="{{ empty($attachment) ? '' : $attachment->mime_type }}" maxlength="50"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowWbfqA"><div id="col-FLP9C" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		ProgramClass	</div>
	<div class="panel-body relationship-panel" data-model="ProgramClass" data-variable-name="programClass" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="programClasses" data-type="one">
						<option value="0">Select</option>
						@foreach($allprogramClasses as $programClass)
						<option value="{{ $programClass->id }}">{{ $programClass->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="programClass" value="{{ (!empty($attachment)) ? (!empty($attachment->programClass) ? $attachment->programClass->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  						<input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" value="{{ empty($attachment) ? '' : (empty($attachment->programClass) ? '' : $attachment->programClass->course_type) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Time</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="from_time" placeholder="From Time" type="text" value="{{ empty($attachment) ? '' : (empty($attachment->programClass) ? '' : $attachment->programClass->from_time) }}" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Time</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="to_time" placeholder="To Time" type="text" value="{{ empty($attachment) ? '' : (empty($attachment->programClass) ? '' : $attachment->programClass->to_time) }}" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  						<input type="text" data-field="title" class="form-control control-form" placeholder="Title" value="{{ empty($attachment) ? '' : (empty($attachment->programClass) ? '' : $attachment->programClass->title) }}" maxlength="55"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($attachment) ? '' : (empty($attachment->programClass) ? '' : $attachment->programClass->description) }}</textarea>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="programClass" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-Nqfrd" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($attachment) ? 0 : $attachment->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/attachmentsForm.js"></script>
@endpush