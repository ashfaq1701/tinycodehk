<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="testimonial" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($alltestimonials as $testimonial)
			<option value="{{ $testimonial->id }}">{{ $testimonial->student_name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'enrollment')
			<th>Enrollment			</th>
			@endif
						<th>Testimonial			</th>
						<th>Student Name			</th>
						<th>Class			</th>
						<th>School			</th>
						<th>Course			</th>
						<th>Roll			</th>
						<th>Term			</th>
						<th>Year			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($testimonials as $testimonial)
		<tr>
						<td>
				<a href="/modules/testimonials/{{ $testimonial->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/testimonials/{{ $testimonial->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'enrollment')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="testimonial" data-module="enrollment" data-id="{{ $testimonial->id }}">Enrollment</a>
			</td>
			@endif
							
						<td>{{ $testimonial->testimonial }}</td>
						<td>{{ $testimonial->student_name }}</td>
						<td>{{ $testimonial->class }}</td>
						<td>{{ $testimonial->school }}</td>
						<td>{{ $testimonial->course }}</td>
						<td>{{ $testimonial->roll }}</td>
						<td>{{ $testimonial->term }}</td>
						<td>{{ $testimonial->year }}</td>
					</tr>
		@endforeach
	</tbody>
</table>