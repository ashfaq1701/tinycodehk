@extends('commons.crud')

@section('crud-title')
	Add Testimonial
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="testimonials">
		
						
				<div class="row" id="rowFYYLX"><div id="col-YZrnO" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="testimonial">Testimonial</label>  
  		                <textarea id="testimonial" class="form-control control-form" rows="4" name="testimonial" placeholder="Testimonial">{{ empty($testimonial) ? '' : $testimonial->testimonial }}</textarea>
                  		</div>
  	</div>
</div></div><div id="col-HuKsE" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="student_name">Student Name</label>  
  		                <input type="text" id="student_name" class="form-control control-form" name="student_name" placeholder="Student Name" value="{{ empty($testimonial) ? '' : $testimonial->student_name }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowVvbMw"><div id="col-CdMha" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="class">Class</label>  
  		                <input type="text" id="class" class="form-control control-form" name="class" placeholder="Class" value="{{ empty($testimonial) ? '' : $testimonial->class }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div><div id="col-3jvqG" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="school">School</label>  
  		                <input type="text" id="school" class="form-control control-form" name="school" placeholder="School" value="{{ empty($testimonial) ? '' : $testimonial->school }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowDaIuA"><div id="col-zH0Vb" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="course">Course</label>  
  		                <input type="text" id="course" class="form-control control-form" name="course" placeholder="Course" value="{{ empty($testimonial) ? '' : $testimonial->course }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div><div id="col-Xpfb0" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="roll">Roll</label>  
  		                <input type="text" id="roll" class="form-control control-form" name="roll" placeholder="Roll" value="{{ empty($testimonial) ? '' : $testimonial->roll }}" maxlength="10"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowTBMax"><div id="col-ahSck" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="term">Term</label>  
  		                <input type="text" id="term" class="form-control control-form" name="term" placeholder="Term" value="{{ empty($testimonial) ? '' : $testimonial->term }}" maxlength="20"/>
                  		</div>
  	</div>
</div></div><div id="col-cf63y" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="year">Year</label>  
  		                <input type="text" id="year" class="form-control control-form" name="year" placeholder="Year" value="{{ empty($testimonial) ? '' : $testimonial->year }}" maxlength="4"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowWkVdm"><div id="col-0Po8w" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Enrollment	</div>
	<div class="panel-body relationship-panel" data-model="Enrollment" data-variable-name="enrollment" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="enrollments" data-type="one">
						<option value="0">Select</option>
						@foreach($allenrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="enrollment" value="{{ (!empty($testimonial)) ? (!empty($testimonial->enrollment) ? $testimonial->enrollment->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  						<input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" value="{{ empty($testimonial) ? '' : (empty($testimonial->enrollment) ? '' : $testimonial->enrollment->course_type) }}" maxlength="15"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Registration No</label>  
  		  						<input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" value="{{ empty($testimonial) ? '' : (empty($testimonial->enrollment) ? '' : $testimonial->enrollment->registration_no) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Is Paid</label>  
  		  						<input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" value="{{ empty($testimonial) ? '' : (empty($testimonial->enrollment) ? '' : $testimonial->enrollment->is_paid) }}" />
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Total Payment</label>  
  		  		        	<input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" value="{{ empty($testimonial) ? '' : (empty($testimonial->enrollment) ? '' : $testimonial->enrollment->total_payment) }}" />
        	  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="enrollment" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-SS4UF" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($testimonial) ? 0 : $testimonial->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/testimonialsForm.js"></script>
@endpush