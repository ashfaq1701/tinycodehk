@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/testimonials/add">Add Testimonial</a>
@endsection

@section('crud-title')
	Testimonials
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>Enrollment</th>
								<th>Testimonial</th>
								<th>Student Name</th>
								<th>Class</th>
								<th>School</th>
								<th>Course</th>
								<th>Roll</th>
								<th>Term</th>
								<th>Year</th>
							</tr>
		</thead>
		<tbody>
			@foreach($testimonials as $testimonial)
			<tr>
								<td>
					<a href="/modules/testimonials/{{ $testimonial->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/testimonials/{{ $testimonial->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="testimonial" data-module="enrollment" data-id="{{ $testimonial->id }}">Enrollment</a>
				</td>
								
								<td>{{ $testimonial->testimonial }}</td>
								<td>{{ $testimonial->student_name }}</td>
								<td>{{ $testimonial->class }}</td>
								<td>{{ $testimonial->school }}</td>
								<td>{{ $testimonial->course }}</td>
								<td>{{ $testimonial->roll }}</td>
								<td>{{ $testimonial->term }}</td>
								<td>{{ $testimonial->year }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
