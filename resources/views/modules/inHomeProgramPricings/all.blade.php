@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/inHomeProgramPricings/add">Add InHomeProgramPricing</a>
@endsection

@section('crud-title')
	InHomeProgramPricings
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>InHomeProgram</th>
								<th>Student Numbers</th>
								<th>Price</th>
							</tr>
		</thead>
		<tbody>
			@foreach($inHomeProgramPricings as $inHomeProgramPricing)
			<tr>
								<td>
					<a href="/modules/inHomeProgramPricings/{{ $inHomeProgramPricing->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/inHomeProgramPricings/{{ $inHomeProgramPricing->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="inHomeProgramPricing" data-module="inHomeProgram" data-id="{{ $inHomeProgramPricing->id }}">InHomeProgram</a>
				</td>
								
								<td>{{ $inHomeProgramPricing->student_numbers }}</td>
								<td>{{ $inHomeProgramPricing->price }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
