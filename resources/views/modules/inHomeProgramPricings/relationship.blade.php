<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="inHomeProgramPricing" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allinHomeProgramPricings as $inHomeProgramPricing)
			<option value="{{ $inHomeProgramPricing->id }}">{{ $inHomeProgramPricing->price }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'inHomeProgram')
			<th>InHomeProgram			</th>
			@endif
						<th>Student Numbers			</th>
						<th>Price			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($inHomeProgramPricings as $inHomeProgramPricing)
		<tr>
						<td>
				<a href="/modules/inHomeProgramPricings/{{ $inHomeProgramPricing->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/inHomeProgramPricings/{{ $inHomeProgramPricing->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'inHomeProgram')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="inHomeProgramPricing" data-module="inHomeProgram" data-id="{{ $inHomeProgramPricing->id }}">InHomeProgram</a>
			</td>
			@endif
							
						<td>{{ $inHomeProgramPricing->student_numbers }}</td>
						<td>{{ $inHomeProgramPricing->price }}</td>
					</tr>
		@endforeach
	</tbody>
</table>