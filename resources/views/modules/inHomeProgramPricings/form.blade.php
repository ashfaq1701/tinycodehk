@extends('commons.crud')

@section('crud-title')
	Add InHomeProgramPricing
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="inHomeProgramPricings">
		
						
				<div class="row" id="rowvCQ1w"><div id="col-FFAto" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="student_numbers">Student Numbers</label>  
  		                <input id="student_numbers" class="form-control control-form" type="number" name="student_numbers" placeholder="Student Numbers" value="{{ empty($inHomeProgramPricing) ? '' : $inHomeProgramPricing->student_numbers }}" />
              		</div>
  	</div>
</div></div><div id="col-PRYRI" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="price">Price</label>  
  		                <input id="price" class="form-control control-form" type="number" name="price" placeholder="Price" value="{{ empty($inHomeProgramPricing) ? '' : $inHomeProgramPricing->price }}" />
              		</div>
  	</div>
</div></div></div><div class="row" id="rowp2Bvg"><div id="col-UH7Ro" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		InHomeProgram	</div>
	<div class="panel-body relationship-panel" data-model="InHomeProgram" data-variable-name="inHomeProgram" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="inHomePrograms" data-type="one">
						<option value="0">Select</option>
						@foreach($allinHomePrograms as $inHomeProgram)
						<option value="{{ $inHomeProgram->id }}">{{ $inHomeProgram->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="inHomeProgram" value="{{ (!empty($inHomeProgramPricing)) ? (!empty($inHomeProgramPricing->inHomeProgram) ? $inHomeProgramPricing->inHomeProgram->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($inHomeProgramPricing) ? '' : (empty($inHomeProgramPricing->inHomeProgram) ? '' : $inHomeProgramPricing->inHomeProgram->name) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Age</label>  
  		  		        	<input data-field="from_age" class="form-control control-form" type="number" placeholder="From Age" value="{{ empty($inHomeProgramPricing) ? '' : (empty($inHomeProgramPricing->inHomeProgram) ? '' : $inHomeProgramPricing->inHomeProgram->from_age) }}" />
        	  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Age</label>  
  		  		        	<input data-field="to_age" class="form-control control-form" type="number" placeholder="To Age" value="{{ empty($inHomeProgramPricing) ? '' : (empty($inHomeProgramPricing->inHomeProgram) ? '' : $inHomeProgramPricing->inHomeProgram->to_age) }}" />
        	  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description 1</label>  
  		  		            	<textarea data-field="description_1" class="form-control control-form" rows="4" placeholder="Description 1">{{ empty($inHomeProgramPricing) ? '' : (empty($inHomeProgramPricing->inHomeProgram) ? '' : $inHomeProgramPricing->inHomeProgram->description_1) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description 2</label>  
  		  		            	<textarea data-field="description_2" class="form-control control-form" rows="4" placeholder="Description 2">{{ empty($inHomeProgramPricing) ? '' : (empty($inHomeProgramPricing->inHomeProgram) ? '' : $inHomeProgramPricing->inHomeProgram->description_2) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Image 1</label>  
  		  						<input type="text" data-field="image_1" class="form-control control-form" placeholder="Image 1" value="{{ empty($inHomeProgramPricing) ? '' : (empty($inHomeProgramPricing->inHomeProgram) ? '' : $inHomeProgramPricing->inHomeProgram->image_1) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Image 2</label>  
  		  						<input type="text" data-field="image_2" class="form-control control-form" placeholder="Image 2" value="{{ empty($inHomeProgramPricing) ? '' : (empty($inHomeProgramPricing->inHomeProgram) ? '' : $inHomeProgramPricing->inHomeProgram->image_2) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  						<input type="text" data-field="language" class="form-control control-form" placeholder="Language" value="{{ empty($inHomeProgramPricing) ? '' : (empty($inHomeProgramPricing->inHomeProgram) ? '' : $inHomeProgramPricing->inHomeProgram->language) }}" maxlength="50"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="inHomeProgram" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-k597T" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($inHomeProgramPricing) ? 0 : $inHomeProgramPricing->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/inHomeProgramPricingsForm.js"></script>
@endpush