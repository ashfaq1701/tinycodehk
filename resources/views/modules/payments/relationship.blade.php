<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="payment" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allpayments as $payment)
			<option value="{{ $payment->id }}">{{ $payment->amount }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'enrollment')
			<th>Enrollment			</th>
			@endif
						@if($origin != 'user')
			<th>User			</th>
			@endif
						<th>Amount			</th>
						<th>Invoice Id			</th>
						<th>Charge Id			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($payments as $payment)
		<tr>
						<td>
				<a href="/modules/payments/{{ $payment->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/payments/{{ $payment->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'enrollment')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="payment" data-module="enrollment" data-id="{{ $payment->id }}">Enrollment</a>
			</td>
			@endif
						@if($origin != 'user')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="payment" data-module="user" data-id="{{ $payment->id }}">User</a>
			</td>
			@endif
							
						<td>{{ $payment->amount }}</td>
						<td>{{ $payment->invoice_id }}</td>
						<td>{{ $payment->charge_id }}</td>
					</tr>
		@endforeach
	</tbody>
</table>