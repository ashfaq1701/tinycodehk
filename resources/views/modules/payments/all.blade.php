@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/payments/add">Add Payment</a>
@endsection

@section('crud-title')
	Payments
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>Enrollment</th>
								<th>User</th>
								<th>Amount</th>
								<th>Invoice Id</th>
								<th>Charge Id</th>
							</tr>
		</thead>
		<tbody>
			@foreach($payments as $payment)
			<tr>
								<td>
					<a href="/modules/payments/{{ $payment->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/payments/{{ $payment->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="payment" data-module="enrollment" data-id="{{ $payment->id }}">Enrollment</a>
				</td>
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="payment" data-module="user" data-id="{{ $payment->id }}">User</a>
				</td>
								
								<td>{{ $payment->amount }}</td>
								<td>{{ $payment->invoice_id }}</td>
								<td>{{ $payment->charge_id }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
