@extends('commons.crud')

@section('crud-title')
	Add Payment
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="payments">
		
						
				<div class="row" id="row4AKoe"><div id="col-rzhTh" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="amount">Amount</label>  
  		                <input id="amount" class="form-control control-form" type="number" name="amount" placeholder="Amount" value="{{ empty($payment) ? '' : $payment->amount }}" />
              		</div>
  	</div>
</div></div><div id="col-TvT4K" class="col-md-6"></div></div><div class="row" id="rowFv9Gu"><div id="col-6wdlJ" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="invoice_id">Invoice Id</label>  
  		                <input type="text" id="invoice_id" class="form-control control-form" name="invoice_id" placeholder="Invoice Id" value="{{ empty($payment) ? '' : $payment->invoice_id }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div><div id="col-9UvuJ" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="charge_id">Charge Id</label>  
  		                <input type="text" id="charge_id" class="form-control control-form" name="charge_id" placeholder="Charge Id" value="{{ empty($payment) ? '' : $payment->charge_id }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowanM7B"><div id="col-7uNiF" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Enrollment	</div>
	<div class="panel-body relationship-panel" data-model="Enrollment" data-variable-name="enrollment" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="enrollments" data-type="one">
						<option value="0">Select</option>
						@foreach($allenrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="enrollment" value="{{ (!empty($payment)) ? (!empty($payment->enrollment) ? $payment->enrollment->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  						<input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" value="{{ empty($payment) ? '' : (empty($payment->enrollment) ? '' : $payment->enrollment->course_type) }}" maxlength="15"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Registration No</label>  
  		  						<input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" value="{{ empty($payment) ? '' : (empty($payment->enrollment) ? '' : $payment->enrollment->registration_no) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Is Paid</label>  
  		  						<input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" value="{{ empty($payment) ? '' : (empty($payment->enrollment) ? '' : $payment->enrollment->is_paid) }}" />
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Total Payment</label>  
  		  		        	<input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" value="{{ empty($payment) ? '' : (empty($payment->enrollment) ? '' : $payment->enrollment->total_payment) }}" />
        	  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="enrollment" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-gtxRw" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		User	</div>
	<div class="panel-body relationship-panel" data-model="User" data-variable-name="user" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="users" data-type="one">
						<option value="0">Select</option>
						@foreach($allusers as $user)
						<option value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="user" value="{{ (!empty($payment)) ? (!empty($payment->user) ? $payment->user->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		       	<span class="required"> *</span>
    	  		            	<textarea data-field="name" class="form-control control-form" rows="4" placeholder="Name">{{ empty($payment) ? '' : (empty($payment->user) ? '' : $payment->user->name) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Email</label>  
  		       	<span class="required"> *</span>
    	  		            	<textarea data-field="email" class="form-control control-form" rows="4" placeholder="Email">{{ empty($payment) ? '' : (empty($payment->user) ? '' : $payment->user->email) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Stripe Id</label>  
  		  						<input type="text" data-field="stripe_id" class="form-control control-form" placeholder="Stripe Id" value="{{ empty($payment) ? '' : (empty($payment->user) ? '' : $payment->user->stripe_id) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Card Brand</label>  
  		  						<input type="text" data-field="card_brand" class="form-control control-form" placeholder="Card Brand" value="{{ empty($payment) ? '' : (empty($payment->user) ? '' : $payment->user->card_brand) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Card Last 4</label>  
  		  						<input type="text" data-field="card_last_4" class="form-control control-form" placeholder="Card Last 4" value="{{ empty($payment) ? '' : (empty($payment->user) ? '' : $payment->user->card_last_4) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Trial Ends At</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="trial_ends_at" placeholder="Trial Ends At" type="text" value="{{ empty($payment) ? '' : (empty($payment->user) ? '' : $payment->user->trial_ends_at) }}" readonly>
          		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="user" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($payment) ? 0 : $payment->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/paymentsForm.js"></script>
@endpush