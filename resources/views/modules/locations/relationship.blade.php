<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="location" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($alllocations as $location)
			<option value="{{ $location->id }}">{{ $location->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'holidayCampModule')
			<th>HolidayCampModules			</th>
			@endif
						@if($origin != 'regularProgramClasseSchedule')
			<th>RegularProgramClasseSchedules			</th>
			@endif
						@if($origin != 'holidayCampCourse')
			<th>HolidayCampCourses			</th>
			@endif
						<th>Name			</th>
						<th>Address Line 1			</th>
						<th>Address Line 2			</th>
						<th>City			</th>
						<th>State			</th>
						<th>Country			</th>
						<th>Mobile			</th>
						<th>Email			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($locations as $location)
		<tr>
						<td>
				<a href="/modules/locations/{{ $location->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/locations/{{ $location->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'holidayCampModule')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="location" data-module="holidayCampModule" data-id="{{ $location->id }}">HolidayCampModules</a>
			</td>
			@endif
						@if($origin != 'regularProgramClasseSchedule')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="location" data-module="regularProgramClasseSchedule" data-id="{{ $location->id }}">RegularProgramClasseSchedules</a>
			</td>
			@endif
						@if($origin != 'holidayCampCourse')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="location" data-module="holidayCampCourse" data-id="{{ $location->id }}">HolidayCampCourses</a>
			</td>
			@endif
							
							
						<td>{{ $location->name }}</td>
						<td>{{ $location->address_line_1 }}</td>
						<td>{{ $location->address_line_2 }}</td>
						<td>{{ $location->city }}</td>
						<td>{{ $location->state }}</td>
						<td>{{ $location->country }}</td>
						<td>{{ $location->mobile }}</td>
						<td>{{ $location->email }}</td>
					</tr>
		@endforeach
	</tbody>
</table>