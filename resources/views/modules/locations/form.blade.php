@extends('commons.crud')

@section('crud-title')
	Add Location
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="locations">
		
						
				<div class="row" id="rowpATPu"><div id="col-wLfEx" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <input type="text" id="name" class="form-control control-form" name="name" placeholder="Name" value="{{ empty($location) ? '' : $location->name }}" maxlength="200"/>
                  		</div>
  	</div>
</div></div><div id="col-dTLDC" class="col-md-6"></div></div><div class="row" id="rowi6wAI"><div id="col-ll0P5" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="address_line_1">Address Line 1</label>  
  		                <input type="text" id="address_line_1" class="form-control control-form" name="address_line_1" placeholder="Address Line 1" value="{{ empty($location) ? '' : $location->address_line_1 }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div><div id="col-gM1Bx" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="address_line_2">Address Line 2</label>  
  		                <input type="text" id="address_line_2" class="form-control control-form" name="address_line_2" placeholder="Address Line 2" value="{{ empty($location) ? '' : $location->address_line_2 }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowbsUfq"><div id="col-mlGvp" class="col-md-4"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="city">City</label>  
  		                <input type="text" id="city" class="form-control control-form" name="city" placeholder="City" value="{{ empty($location) ? '' : $location->city }}" maxlength="50"/>
                  		</div>
  	</div>
</div></div><div id="col-zppR8" class="col-md-4"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="state">State</label>  
  		                <input type="text" id="state" class="form-control control-form" name="state" placeholder="State" value="{{ empty($location) ? '' : $location->state }}" maxlength="50"/>
                  		</div>
  	</div>
</div></div><div id="col-KXzal" class="col-md-4"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="country">Country</label>  
  		                <input type="text" id="country" class="form-control control-form" name="country" placeholder="Country" value="{{ empty($location) ? '' : $location->country }}" maxlength="50"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowGjg42"><div id="col-ADcb6" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="mobile">Mobile</label>  
  		                <input type="text" id="mobile" class="form-control control-form" name="mobile" placeholder="Mobile" value="{{ empty($location) ? '' : $location->mobile }}" maxlength="30"/>
                  		</div>
  	</div>
</div></div><div id="col-Z2M9C" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="email">Email</label>  
  		                <input type="text" id="email" class="form-control control-form" name="email" placeholder="Email" value="{{ empty($location) ? '' : $location->email }}" maxlength="60"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowNhKZV"><div id="col-Nx0cI" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampModules	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampModule" data-variable-name="holidayCampModules" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="holidayCampModules" data-type="many">
						<option value="0">Select</option>
						@foreach($allholidayCampModules as $holidayCampModule)
						<option value="{{ $holidayCampModule->id }}">{{ $holidayCampModule->module_code }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($location))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="holidayCampModules">
						<option value="">Select</option>
						@foreach($location->holidayCampModules as $holidayCampModule)
						<option value="{{ $holidayCampModule->id }}">{{ $holidayCampModule->module_code }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampModules" value="{{ $holidayCampModulesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Module Code</label>  
  		  		                <input type="text" data-field="module_code" class="form-control control-form" placeholder="Module Code" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Date</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="from_date" placeholder="From Date" type="text" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Date</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="to_date" placeholder="To Date" type="text" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Time</label>  
  		  		                <input type="text" data-field="from_time" class="form-control control-form" placeholder="From Time" maxlength="20"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Time</label>  
  		  		                <input type="text" data-field="to_time" class="form-control control-form" placeholder="To Time" maxlength="20"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Regular Price</label>  
  		  		            <input data-field="regular_price" class="form-control control-form" type="number" placeholder="Regular Price" />
              		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampModules" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-qI2n7" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgramClasseSchedules	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgramClasseSchedule" data-variable-name="regularProgramClasseSchedules" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="regularProgramClasseSchedules" data-type="many">
						<option value="0">Select</option>
						@foreach($allregularProgramClasseSchedules as $regularProgramClasseSchedule)
						<option value="{{ $regularProgramClasseSchedule->id }}">{{ $regularProgramClasseSchedule->from_time }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($location))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="regularProgramClasseSchedules">
						<option value="">Select</option>
						@foreach($location->regularProgramClasseSchedules as $regularProgramClasseSchedule)
						<option value="{{ $regularProgramClasseSchedule->id }}">{{ $regularProgramClasseSchedule->from_time }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgramClasseSchedules" value="{{ $regularProgramClasseSchedulesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Time</label>  
  		  		                <input type="text" data-field="from_time" class="form-control control-form" placeholder="From Time" maxlength="50"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Time</label>  
  		  		                <input type="text" data-field="to_time" class="form-control control-form" placeholder="To Time" maxlength="50"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgramClasseSchedules" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowsNl1d"><div id="col-aLk4p" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourses	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourse" data-variable-name="holidayCampCourses" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="holidayCampCourses" data-type="many">
						<option value="0">Select</option>
						@foreach($allholidayCampCourses as $holidayCampCourse)
						<option value="{{ $holidayCampCourse->id }}">{{ $holidayCampCourse->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($location))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="holidayCampCourses">
						<option value="">Select</option>
						@foreach($location->holidayCampCourses as $holidayCampCourse)
						<option value="{{ $holidayCampCourse->id }}">{{ $holidayCampCourse->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourses" value="{{ $holidayCampCoursesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  		                <input type="text" data-field="name" class="form-control control-form" placeholder="Name" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Icon</label>  
  		  		                <input type="text" data-field="program_icon" class="form-control control-form" placeholder="Program Icon" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Cover Image</label>  
  		  		                <input type="text" data-field="program_cover_image" class="form-control control-form" placeholder="Program Cover Image" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Video Url</label>  
  		  		                <input type="text" data-field="program_video_url" class="form-control control-form" placeholder="Program Video Url" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  		                <input type="text" data-field="language" class="form-control control-form" placeholder="Language" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		                <textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description"></textarea>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourses" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-LJrNJ" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($location) ? 0 : $location->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/locationsForm.js"></script>
@endpush