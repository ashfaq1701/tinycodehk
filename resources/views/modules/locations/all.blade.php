@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/locations/add">Add Location</a>
@endsection

@section('crud-title')
	Locations
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>HolidayCampModules</th>
								<th>RegularProgramClasseSchedules</th>
								<th>HolidayCampCourses</th>
								<th>Name</th>
								<th>Address Line 1</th>
								<th>Address Line 2</th>
								<th>City</th>
								<th>State</th>
								<th>Country</th>
								<th>Mobile</th>
								<th>Email</th>
							</tr>
		</thead>
		<tbody>
			@foreach($locations as $location)
			<tr>
								<td>
					<a href="/modules/locations/{{ $location->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/locations/{{ $location->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="location" data-module="holidayCampModule" data-id="{{ $location->id }}">HolidayCampModules</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="location" data-module="regularProgramClasseSchedule" data-id="{{ $location->id }}">RegularProgramClasseSchedules</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="belongsToMany" data-origin="location" data-module="holidayCampCourse" data-id="{{ $location->id }}">HolidayCampCourses</a></td>
								
								
								<td>{{ $location->name }}</td>
								<td>{{ $location->address_line_1 }}</td>
								<td>{{ $location->address_line_2 }}</td>
								<td>{{ $location->city }}</td>
								<td>{{ $location->state }}</td>
								<td>{{ $location->country }}</td>
								<td>{{ $location->mobile }}</td>
								<td>{{ $location->email }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
