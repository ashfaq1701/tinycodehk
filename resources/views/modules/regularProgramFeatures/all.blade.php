@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/regularProgramFeatures/add">Add RegularProgramFeature</a>
@endsection

@section('crud-title')
	RegularProgramFeatures
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>RegularProgram</th>
								<th>Title</th>
								<th>Description</th>
							</tr>
		</thead>
		<tbody>
			@foreach($regularProgramFeatures as $regularProgramFeature)
			<tr>
								<td>
					<a href="/modules/regularProgramFeatures/{{ $regularProgramFeature->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/regularProgramFeatures/{{ $regularProgramFeature->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="regularProgramFeature" data-module="regularProgram" data-id="{{ $regularProgramFeature->id }}">RegularProgram</a>
				</td>
								
								<td>{{ $regularProgramFeature->title }}</td>
								<td>{{ $regularProgramFeature->description }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
