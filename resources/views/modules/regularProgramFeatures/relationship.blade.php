<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="regularProgramFeature" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allregularProgramFeatures as $regularProgramFeature)
			<option value="{{ $regularProgramFeature->id }}">{{ $regularProgramFeature->title }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'regularProgram')
			<th>RegularProgram			</th>
			@endif
						<th>Title			</th>
						<th>Description			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($regularProgramFeatures as $regularProgramFeature)
		<tr>
						<td>
				<a href="/modules/regularProgramFeatures/{{ $regularProgramFeature->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/regularProgramFeatures/{{ $regularProgramFeature->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'regularProgram')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="regularProgramFeature" data-module="regularProgram" data-id="{{ $regularProgramFeature->id }}">RegularProgram</a>
			</td>
			@endif
							
						<td>{{ $regularProgramFeature->title }}</td>
						<td>{{ $regularProgramFeature->description }}</td>
					</tr>
		@endforeach
	</tbody>
</table>