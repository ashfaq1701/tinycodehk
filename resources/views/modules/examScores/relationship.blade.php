<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="examScore" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allexamScores as $examScore)
			<option value="{{ $examScore->id }}">{{ $examScore->marks }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'enrollment')
			<th>Enrollment			</th>
			@endif
						@if($origin != 'exam')
			<th>Exam			</th>
			@endif
						<th>Marks			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($examScores as $examScore)
		<tr>
						<td>
				<a href="/modules/examScores/{{ $examScore->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/examScores/{{ $examScore->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'enrollment')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="examScore" data-module="enrollment" data-id="{{ $examScore->id }}">Enrollment</a>
			</td>
			@endif
						@if($origin != 'exam')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="examScore" data-module="exam" data-id="{{ $examScore->id }}">Exam</a>
			</td>
			@endif
							
						<td>{{ $examScore->marks }}</td>
					</tr>
		@endforeach
	</tbody>
</table>