@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/examScores/add">Add ExamScore</a>
@endsection

@section('crud-title')
	ExamScores
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>Enrollment</th>
								<th>Exam</th>
								<th>Marks</th>
							</tr>
		</thead>
		<tbody>
			@foreach($examScores as $examScore)
			<tr>
								<td>
					<a href="/modules/examScores/{{ $examScore->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/examScores/{{ $examScore->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="examScore" data-module="enrollment" data-id="{{ $examScore->id }}">Enrollment</a>
				</td>
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="examScore" data-module="exam" data-id="{{ $examScore->id }}">Exam</a>
				</td>
								
								<td>{{ $examScore->marks }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
