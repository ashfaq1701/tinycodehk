@extends('commons.crud')

@section('crud-title')
	Add ExamScore
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="examScores">
		
						
				<div class="row" id="rowrxMG5"><div id="col-f1ltr" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="marks">Marks</label>  
  		                <input id="marks" class="form-control control-form" type="number" name="marks" placeholder="Marks" value="{{ empty($examScore) ? '' : $examScore->marks }}" />
              		</div>
  	</div>
</div></div><div id="col-NTZvH" class="col-md-6"></div></div><div class="row" id="rowQeI83"><div id="col-WhJCq" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Enrollment	</div>
	<div class="panel-body relationship-panel" data-model="Enrollment" data-variable-name="enrollment" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="enrollments" data-type="one">
						<option value="0">Select</option>
						@foreach($allenrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="enrollment" value="{{ (!empty($examScore)) ? (!empty($examScore->enrollment) ? $examScore->enrollment->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  						<input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" value="{{ empty($examScore) ? '' : (empty($examScore->enrollment) ? '' : $examScore->enrollment->course_type) }}" maxlength="15"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Registration No</label>  
  		  						<input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" value="{{ empty($examScore) ? '' : (empty($examScore->enrollment) ? '' : $examScore->enrollment->registration_no) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Is Paid</label>  
  		  						<input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" value="{{ empty($examScore) ? '' : (empty($examScore->enrollment) ? '' : $examScore->enrollment->is_paid) }}" />
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Total Payment</label>  
  		  		        	<input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" value="{{ empty($examScore) ? '' : (empty($examScore->enrollment) ? '' : $examScore->enrollment->total_payment) }}" />
        	  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="enrollment" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-BbMz9" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Exam	</div>
	<div class="panel-body relationship-panel" data-model="Exam" data-variable-name="exam" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="exams" data-type="one">
						<option value="0">Select</option>
						@foreach($allexams as $exam)
						<option value="{{ $exam->id }}">{{ $exam->total_marks }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="exam" value="{{ (!empty($examScore)) ? (!empty($examScore->exam) ? $examScore->exam->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  						<input type="text" data-field="title" class="form-control control-form" placeholder="Title" value="{{ empty($examScore) ? '' : (empty($examScore->exam) ? '' : $examScore->exam->title) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Exam Date Time</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="exam_date_time" placeholder="Exam Date Time" type="text" value="{{ empty($examScore) ? '' : (empty($examScore->exam) ? '' : $examScore->exam->exam_date_time) }}" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($examScore) ? '' : (empty($examScore->exam) ? '' : $examScore->exam->description) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Total Marks</label>  
  		  		        	<input data-field="total_marks" class="form-control control-form" type="number" placeholder="Total Marks" value="{{ empty($examScore) ? '' : (empty($examScore->exam) ? '' : $examScore->exam->total_marks) }}" />
        	  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="exam" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($examScore) ? 0 : $examScore->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/examScoresForm.js"></script>
@endpush