<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="programClass" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allprogramClasses as $programClass)
			<option value="{{ $programClass->id }}">{{ $programClass->title }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'attachment')
			<th>Attachments			</th>
			@endif
						@if($origin != 'attendence')
			<th>Attendences			</th>
			@endif
						@if($origin != 'exam')
			<th>Exams			</th>
			@endif
						@if($origin != 'enrollment')
			<th>Enrollments			</th>
			@endif
						<th>Course Type			</th>
						<th>From Time			</th>
						<th>To Time			</th>
						<th>Title			</th>
						<th>Description			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($programClasses as $programClass)
		<tr>
						<td>
				<a href="/modules/programClasses/{{ $programClass->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/programClasses/{{ $programClass->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'attachment')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="programClass" data-module="attachment" data-id="{{ $programClass->id }}">Attachments</a>
			</td>
			@endif
						@if($origin != 'attendence')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="programClass" data-module="attendence" data-id="{{ $programClass->id }}">Attendences</a>
			</td>
			@endif
						@if($origin != 'exam')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="programClass" data-module="exam" data-id="{{ $programClass->id }}">Exams</a>
			</td>
			@endif
						@if($origin != 'enrollment')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="programClass" data-module="enrollment" data-id="{{ $programClass->id }}">Enrollments</a>
			</td>
			@endif
							
							
						<td>{{ $programClass->course_type }}</td>
						<td>{{ $programClass->from_time }}</td>
						<td>{{ $programClass->to_time }}</td>
						<td>{{ $programClass->title }}</td>
						<td>{{ $programClass->description }}</td>
					</tr>
		@endforeach
	</tbody>
</table>