@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/programClasses/add">Add ProgramClass</a>
@endsection

@section('crud-title')
	ProgramClasses
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>Attachments</th>
								<th>Attendences</th>
								<th>Exams</th>
								<th>Enrollments</th>
								<th>Course Type</th>
								<th>From Time</th>
								<th>To Time</th>
								<th>Title</th>
								<th>Description</th>
							</tr>
		</thead>
		<tbody>
			@foreach($programClasses as $programClass)
			<tr>
								<td>
					<a href="/modules/programClasses/{{ $programClass->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/programClasses/{{ $programClass->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="programClass" data-module="attachment" data-id="{{ $programClass->id }}">Attachments</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="programClass" data-module="attendence" data-id="{{ $programClass->id }}">Attendences</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="programClass" data-module="exam" data-id="{{ $programClass->id }}">Exams</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="belongsToMany" data-origin="programClass" data-module="enrollment" data-id="{{ $programClass->id }}">Enrollments</a></td>
								
								
								<td>{{ $programClass->course_type }}</td>
								<td>{{ $programClass->from_time }}</td>
								<td>{{ $programClass->to_time }}</td>
								<td>{{ $programClass->title }}</td>
								<td>{{ $programClass->description }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
