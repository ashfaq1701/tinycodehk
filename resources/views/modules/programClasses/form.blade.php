@extends('commons.crud')

@section('crud-title')
	Add ProgramClass
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="programClasses">
		
						
				<div class="row" id="rownOZir"><div id="col-pbx1M" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="course_type">Course Type</label>  
  		                <input type="text" id="course_type" class="form-control control-form" name="course_type" placeholder="Course Type" value="{{ empty($programClass) ? '' : $programClass->course_type }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div><div id="col-8K2D9" class="col-md-6"></div></div><div class="row" id="rowwnwpC"><div id="col-9ctDG" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="from_time">From Time</label>  
  		        	<input class="form_datetime form-control" id="from_time" name="from_time" placeholder="From Time" type="text" value="{{ empty($programClass) ? '' : $programClass->from_time }}" readonly>
          		</div>
  	</div>
</div></div><div id="col-Jhmv9" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="to_time">To Time</label>  
  		        	<input class="form_datetime form-control" id="to_time" name="to_time" placeholder="To Time" type="text" value="{{ empty($programClass) ? '' : $programClass->to_time }}" readonly>
          		</div>
  	</div>
</div></div></div><div class="row" id="rowy8UCq"><div id="col-sRAy4" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="title">Title</label>  
  		                <input type="text" id="title" class="form-control control-form" name="title" placeholder="Title" value="{{ empty($programClass) ? '' : $programClass->title }}" maxlength="55"/>
                  		</div>
  	</div>
</div></div><div id="col-uv2RK" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="description">Description</label>  
  		                <textarea id="description" class="form-control control-form" rows="4" name="description" placeholder="Description">{{ empty($programClass) ? '' : $programClass->description }}</textarea>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowsIa9s"><div id="col-AqsmO" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Attachments	</div>
	<div class="panel-body relationship-panel" data-model="Attachment" data-variable-name="attachments" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="attachments" data-type="many">
						<option value="0">Select</option>
						@foreach($allattachments as $attachment)
						<option value="{{ $attachment->id }}">{{ $attachment->location }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($programClass))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="attachments">
						<option value="">Select</option>
						@foreach($programClass->attachments as $attachment)
						<option value="{{ $attachment->id }}">{{ $attachment->location }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="attachments" value="{{ $attachmentsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Location</label>  
  		  		                <input type="text" data-field="location" class="form-control control-form" placeholder="Location" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Mime Type</label>  
  		  		                <input type="text" data-field="mime_type" class="form-control control-form" placeholder="Mime Type" maxlength="50"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="attachments" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-rlEbU" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Attendences	</div>
	<div class="panel-body relationship-panel" data-model="Attendence" data-variable-name="attendences" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="attendences" data-type="many">
						<option value="0">Select</option>
						@foreach($allattendences as $attendence)
						<option value="{{ $attendence->id }}">{{ $attendence->attended }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($programClass))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="attendences">
						<option value="">Select</option>
						@foreach($programClass->attendences as $attendence)
						<option value="{{ $attendence->id }}">{{ $attendence->attended }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="attendences" value="{{ $attendencesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Attended</label>  
  		  		                <input type="text" data-field="attended" class="form-control control-form" placeholder="Attended" />
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="attendences" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowmtlqx"><div id="col-MLC7v" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Exams	</div>
	<div class="panel-body relationship-panel" data-model="Exam" data-variable-name="exams" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="exams" data-type="many">
						<option value="0">Select</option>
						@foreach($allexams as $exam)
						<option value="{{ $exam->id }}">{{ $exam->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($programClass))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="exams">
						<option value="">Select</option>
						@foreach($programClass->exams as $exam)
						<option value="{{ $exam->id }}">{{ $exam->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="exams" value="{{ $examsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  		                <input type="text" data-field="title" class="form-control control-form" placeholder="Title" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Exam Date Time</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="exam_date_time" placeholder="Exam Date Time" type="text" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		                <textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description"></textarea>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Total Marks</label>  
  		  		            <input data-field="total_marks" class="form-control control-form" type="number" placeholder="Total Marks" />
              		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="exams" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-ei8Ia" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Enrollments	</div>
	<div class="panel-body relationship-panel" data-model="Enrollment" data-variable-name="enrollments" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="enrollments" data-type="many">
						<option value="0">Select</option>
						@foreach($allenrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($programClass))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="enrollments">
						<option value="">Select</option>
						@foreach($programClass->enrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="enrollments" value="{{ $enrollmentsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  		                <input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" maxlength="15"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Registration No</label>  
  		  		                <input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Is Paid</label>  
  		  		                <input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" />
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Total Payment</label>  
  		  		            <input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" />
              		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="enrollments" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($programClass) ? 0 : $programClass->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/programClassesForm.js"></script>
@endpush