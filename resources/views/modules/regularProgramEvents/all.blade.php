@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/regularProgramEvents/add">Add RegularProgramEvent</a>
@endsection

@section('crud-title')
	RegularProgramEvents
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>RegularProgram</th>
								<th>Title</th>
								<th>Description</th>
							</tr>
		</thead>
		<tbody>
			@foreach($regularProgramEvents as $regularProgramEvent)
			<tr>
								<td>
					<a href="/modules/regularProgramEvents/{{ $regularProgramEvent->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/regularProgramEvents/{{ $regularProgramEvent->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="regularProgramEvent" data-module="regularProgram" data-id="{{ $regularProgramEvent->id }}">RegularProgram</a>
				</td>
								
								<td>{{ $regularProgramEvent->title }}</td>
								<td>{{ $regularProgramEvent->description }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
