<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="inHomeProgramStructure" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allinHomeProgramStructures as $inHomeProgramStructure)
			<option value="{{ $inHomeProgramStructure->id }}">{{ $inHomeProgramStructure->title }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'inHomeProgram')
			<th>InHomeProgram			</th>
			@endif
						<th>Title			</th>
						<th>Value			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($inHomeProgramStructures as $inHomeProgramStructure)
		<tr>
						<td>
				<a href="/modules/inHomeProgramStructures/{{ $inHomeProgramStructure->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/inHomeProgramStructures/{{ $inHomeProgramStructure->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'inHomeProgram')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="inHomeProgramStructure" data-module="inHomeProgram" data-id="{{ $inHomeProgramStructure->id }}">InHomeProgram</a>
			</td>
			@endif
							
						<td>{{ $inHomeProgramStructure->title }}</td>
						<td>{{ $inHomeProgramStructure->value }}</td>
					</tr>
		@endforeach
	</tbody>
</table>