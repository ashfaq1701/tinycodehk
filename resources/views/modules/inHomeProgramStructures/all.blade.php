@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/inHomeProgramStructures/add">Add InHomeProgramStructure</a>
@endsection

@section('crud-title')
	InHomeProgramStructures
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>InHomeProgram</th>
								<th>Title</th>
								<th>Value</th>
							</tr>
		</thead>
		<tbody>
			@foreach($inHomeProgramStructures as $inHomeProgramStructure)
			<tr>
								<td>
					<a href="/modules/inHomeProgramStructures/{{ $inHomeProgramStructure->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/inHomeProgramStructures/{{ $inHomeProgramStructure->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="inHomeProgramStructure" data-module="inHomeProgram" data-id="{{ $inHomeProgramStructure->id }}">InHomeProgram</a>
				</td>
								
								<td>{{ $inHomeProgramStructure->title }}</td>
								<td>{{ $inHomeProgramStructure->value }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
