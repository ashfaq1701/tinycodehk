@extends('commons.crud')

@section('crud-title')
	Add Weekday
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="weekdays">
		
						
				<div class="row" id="rowJV7Hp"><div id="col-5jsom" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <input type="text" id="name" class="form-control control-form" name="name" placeholder="Name" value="{{ empty($weekday) ? '' : $weekday->name }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div><div id="col-hgxJP" class="col-md-6"></div></div><div class="row" id="row4GIcS"><div id="col-Xhxq9" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		InHomeProgramSchedules	</div>
	<div class="panel-body relationship-panel" data-model="InHomeProgramSchedule" data-variable-name="inHomeProgramSchedules" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="inHomeProgramSchedules" data-type="many">
						<option value="0">Select</option>
						@foreach($allinHomeProgramSchedules as $inHomeProgramSchedule)
						<option value="{{ $inHomeProgramSchedule->id }}">{{ $inHomeProgramSchedule->from_time }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($weekday))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="inHomeProgramSchedules">
						<option value="">Select</option>
						@foreach($weekday->inHomeProgramSchedules as $inHomeProgramSchedule)
						<option value="{{ $inHomeProgramSchedule->id }}">{{ $inHomeProgramSchedule->from_time }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="inHomeProgramSchedules" value="{{ $inHomeProgramSchedulesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Time</label>  
  		  		                <input type="text" data-field="from_time" class="form-control control-form" placeholder="From Time" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Time</label>  
  		  		                <input type="text" data-field="to_time" class="form-control control-form" placeholder="To Time" maxlength="45"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="inHomeProgramSchedules" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-0CtFT" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgramClasseSchedules	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgramClasseSchedule" data-variable-name="regularProgramClasseSchedules" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="regularProgramClasseSchedules" data-type="many">
						<option value="0">Select</option>
						@foreach($allregularProgramClasseSchedules as $regularProgramClasseSchedule)
						<option value="{{ $regularProgramClasseSchedule->id }}">{{ $regularProgramClasseSchedule->from_time }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($weekday))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="regularProgramClasseSchedules">
						<option value="">Select</option>
						@foreach($weekday->regularProgramClasseSchedules as $regularProgramClasseSchedule)
						<option value="{{ $regularProgramClasseSchedule->id }}">{{ $regularProgramClasseSchedule->from_time }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgramClasseSchedules" value="{{ $regularProgramClasseSchedulesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Time</label>  
  		  		                <input type="text" data-field="from_time" class="form-control control-form" placeholder="From Time" maxlength="50"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Time</label>  
  		  		                <input type="text" data-field="to_time" class="form-control control-form" placeholder="To Time" maxlength="50"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgramClasseSchedules" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="row82G6D"><div id="col-1JFAj" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		InHomePrograms	</div>
	<div class="panel-body relationship-panel" data-model="InHomeProgram" data-variable-name="inHomePrograms" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="inHomePrograms" data-type="many">
						<option value="0">Select</option>
						@foreach($allinHomePrograms as $inHomeProgram)
						<option value="{{ $inHomeProgram->id }}">{{ $inHomeProgram->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($weekday))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="inHomePrograms">
						<option value="">Select</option>
						@foreach($weekday->inHomePrograms as $inHomeProgram)
						<option value="{{ $inHomeProgram->id }}">{{ $inHomeProgram->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="inHomePrograms" value="{{ $inHomeProgramsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  		                <input type="text" data-field="name" class="form-control control-form" placeholder="Name" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Age</label>  
  		  		            <input data-field="from_age" class="form-control control-form" type="number" placeholder="From Age" />
              		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Age</label>  
  		  		            <input data-field="to_age" class="form-control control-form" type="number" placeholder="To Age" />
              		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description 1</label>  
  		  		                <textarea data-field="description_1" class="form-control control-form" rows="4" placeholder="Description 1"></textarea>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description 2</label>  
  		  		                <textarea data-field="description_2" class="form-control control-form" rows="4" placeholder="Description 2"></textarea>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Image 1</label>  
  		  		                <input type="text" data-field="image_1" class="form-control control-form" placeholder="Image 1" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Image 2</label>  
  		  		                <input type="text" data-field="image_2" class="form-control control-form" placeholder="Image 2" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  		                <input type="text" data-field="language" class="form-control control-form" placeholder="Language" maxlength="50"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="inHomePrograms" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-upwhJ" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($weekday) ? 0 : $weekday->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/weekdaysForm.js"></script>
@endpush