<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="weekday" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allweekdays as $weekday)
			<option value="{{ $weekday->id }}">{{ $weekday->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'inHomeProgramSchedule')
			<th>InHomeProgramSchedules			</th>
			@endif
						@if($origin != 'regularProgramClasseSchedule')
			<th>RegularProgramClasseSchedules			</th>
			@endif
						@if($origin != 'inHomeProgram')
			<th>InHomePrograms			</th>
			@endif
						<th>Name			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($weekdays as $weekday)
		<tr>
						<td>
				<a href="/modules/weekdays/{{ $weekday->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/weekdays/{{ $weekday->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'inHomeProgramSchedule')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="weekday" data-module="inHomeProgramSchedule" data-id="{{ $weekday->id }}">InHomeProgramSchedules</a>
			</td>
			@endif
						@if($origin != 'regularProgramClasseSchedule')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="weekday" data-module="regularProgramClasseSchedule" data-id="{{ $weekday->id }}">RegularProgramClasseSchedules</a>
			</td>
			@endif
						@if($origin != 'inHomeProgram')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="weekday" data-module="inHomeProgram" data-id="{{ $weekday->id }}">InHomePrograms</a>
			</td>
			@endif
							
							
						<td>{{ $weekday->name }}</td>
					</tr>
		@endforeach
	</tbody>
</table>