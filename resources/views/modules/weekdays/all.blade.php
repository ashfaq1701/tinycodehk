@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/weekdays/add">Add Weekday</a>
@endsection

@section('crud-title')
	Weekdays
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>InHomeProgramSchedules</th>
								<th>RegularProgramClasseSchedules</th>
								<th>InHomePrograms</th>
								<th>Name</th>
							</tr>
		</thead>
		<tbody>
			@foreach($weekdays as $weekday)
			<tr>
								<td>
					<a href="/modules/weekdays/{{ $weekday->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/weekdays/{{ $weekday->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="weekday" data-module="inHomeProgramSchedule" data-id="{{ $weekday->id }}">InHomeProgramSchedules</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="weekday" data-module="regularProgramClasseSchedule" data-id="{{ $weekday->id }}">RegularProgramClasseSchedules</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="belongsToMany" data-origin="weekday" data-module="inHomeProgram" data-id="{{ $weekday->id }}">InHomePrograms</a></td>
								
								
								<td>{{ $weekday->name }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
