<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="regularProgram" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allregularPrograms as $regularProgram)
			<option value="{{ $regularProgram->id }}">{{ $regularProgram->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'regularProgramEvent')
			<th>RegularProgramEvents			</th>
			@endif
						@if($origin != 'regularProgramFeature')
			<th>RegularProgramFeatures			</th>
			@endif
						@if($origin != 'regularProgramFormat')
			<th>RegularProgramFormats			</th>
			@endif
						@if($origin != 'regularProgramSemester')
			<th>RegularProgramSemesters			</th>
			@endif
						@if($origin != 'regularProgramStructure')
			<th>RegularProgramStructures			</th>
			@endif
						<th>Name			</th>
						<th>From Age			</th>
						<th>To Age			</th>
						<th>Description			</th>
						<th>Language			</th>
						<th>Bringings			</th>
						<th>Icon			</th>
						<th>Feat Image			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($regularPrograms as $regularProgram)
		<tr>
						<td>
				<a href="/modules/regularPrograms/{{ $regularProgram->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/regularPrograms/{{ $regularProgram->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'regularProgramEvent')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="regularProgram" data-module="regularProgramEvent" data-id="{{ $regularProgram->id }}">RegularProgramEvents</a>
			</td>
			@endif
						@if($origin != 'regularProgramFeature')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="regularProgram" data-module="regularProgramFeature" data-id="{{ $regularProgram->id }}">RegularProgramFeatures</a>
			</td>
			@endif
						@if($origin != 'regularProgramFormat')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="regularProgram" data-module="regularProgramFormat" data-id="{{ $regularProgram->id }}">RegularProgramFormats</a>
			</td>
			@endif
						@if($origin != 'regularProgramSemester')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="regularProgram" data-module="regularProgramSemester" data-id="{{ $regularProgram->id }}">RegularProgramSemesters</a>
			</td>
			@endif
						@if($origin != 'regularProgramStructure')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="regularProgram" data-module="regularProgramStructure" data-id="{{ $regularProgram->id }}">RegularProgramStructures</a>
			</td>
			@endif
							
							
						<td>{{ $regularProgram->name }}</td>
						<td>{{ $regularProgram->from_age }}</td>
						<td>{{ $regularProgram->to_age }}</td>
						<td>{{ $regularProgram->description }}</td>
						<td>{{ $regularProgram->language }}</td>
						<td>{{ $regularProgram->bringings }}</td>
						<td>{{ $regularProgram->icon }}</td>
						<td>{{ $regularProgram->feat_image }}</td>
					</tr>
		@endforeach
	</tbody>
</table>