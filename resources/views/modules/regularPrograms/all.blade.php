@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/regularPrograms/add">Add RegularProgram</a>
@endsection

@section('crud-title')
	RegularPrograms
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>RegularProgramEvents</th>
								<th>RegularProgramFeatures</th>
								<th>RegularProgramFormats</th>
								<th>RegularProgramSemesters</th>
								<th>RegularProgramStructures</th>
								<th>Name</th>
								<th>From Age</th>
								<th>To Age</th>
								<th>Description</th>
								<th>Language</th>
								<th>Bringings</th>
								<th>Icon</th>
								<th>Feat Image</th>
							</tr>
		</thead>
		<tbody>
			@foreach($regularPrograms as $regularProgram)
			<tr>
								<td>
					<a href="/modules/regularPrograms/{{ $regularProgram->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/regularPrograms/{{ $regularProgram->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="regularProgram" data-module="regularProgramEvent" data-id="{{ $regularProgram->id }}">RegularProgramEvents</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="regularProgram" data-module="regularProgramFeature" data-id="{{ $regularProgram->id }}">RegularProgramFeatures</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="regularProgram" data-module="regularProgramFormat" data-id="{{ $regularProgram->id }}">RegularProgramFormats</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="regularProgram" data-module="regularProgramSemester" data-id="{{ $regularProgram->id }}">RegularProgramSemesters</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="regularProgram" data-module="regularProgramStructure" data-id="{{ $regularProgram->id }}">RegularProgramStructures</a></td>
								
								
								<td>{{ $regularProgram->name }}</td>
								<td>{{ $regularProgram->from_age }}</td>
								<td>{{ $regularProgram->to_age }}</td>
								<td>{{ $regularProgram->description }}</td>
								<td>{{ $regularProgram->language }}</td>
								<td>{{ $regularProgram->bringings }}</td>
								<td>{{ $regularProgram->icon }}</td>
								<td>{{ $regularProgram->feat_image }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
