@extends('commons.crud')

@section('crud-title')
	Add RegularProgram
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="regularPrograms">
		
						
				<div class="row" id="rowsUP0L"><div id="col-GEemi" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <input type="text" id="name" class="form-control control-form" name="name" placeholder="Name" value="{{ empty($regularProgram) ? '' : $regularProgram->name }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div><div id="col-bSPcJ" class="col-md-6"></div></div><div class="row" id="row2uz14"><div id="col-f81Ov" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="from_age">From Age</label>  
  		                <input id="from_age" class="form-control control-form" type="number" name="from_age" placeholder="From Age" value="{{ empty($regularProgram) ? '' : $regularProgram->from_age }}" />
              		</div>
  	</div>
</div></div><div id="col-loRa0" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="to_age">To Age</label>  
  		                <input id="to_age" class="form-control control-form" type="number" name="to_age" placeholder="To Age" value="{{ empty($regularProgram) ? '' : $regularProgram->to_age }}" />
              		</div>
  	</div>
</div></div></div><div class="row" id="rowQcC0d"><div id="col-pTMxw" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="description">Description</label>  
  		                <textarea id="description" class="form-control control-form" rows="4" name="description" placeholder="Description">{{ empty($regularProgram) ? '' : $regularProgram->description }}</textarea>
                  		</div>
  	</div>
</div></div><div id="col-1VGQF" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="language">Language</label>  
  		                <input type="text" id="language" class="form-control control-form" name="language" placeholder="Language" value="{{ empty($regularProgram) ? '' : $regularProgram->language }}" maxlength="50"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowGw9YW"><div id="col-L7IAD" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="bringings">Bringings</label>  
  		                <input type="text" id="bringings" class="form-control control-form" name="bringings" placeholder="Bringings" value="{{ empty($regularProgram) ? '' : $regularProgram->bringings }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div><div id="col-1Nxz6" class="col-md-6"><div class="form-group">
	<div class="row">
		<input type="hidden" name="feat_image" id="feat_image" class="control-form" value="{{ empty($regularProgram) ? '' : $regularProgram->feat_image }}"/>
  		<div class="col-sm-12 col-md-12">
  			<label class="control-label" for="feat_image">Feat Image</label>  
  			<input type="file" id="feat_image_file_ip" class="form-control file_ip" placeholder="Feat Image"/>
        </div>
  	</div>
</div></div></div><div class="row" id="rowHyTw6"><div id="col-G0tJy" class="col-md-6"><div class="form-group">
	<div class="row">
		<input type="hidden" name="icon" id="icon" class="control-form" value="{{ empty($regularProgram) ? '' : $regularProgram->icon }}"/>
  		<div class="col-sm-12 col-md-12">
  			<label class="control-label" for="icon">Icon</label>  
  			<input type="file" id="icon_file_ip" class="form-control file_ip" placeholder="Icon"/>
        </div>
  	</div>
</div></div><div id="col-aZgx0" class="col-md-6"></div></div><div class="row" id="rowi0An4"><div id="col-WDqvM" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgramEvents	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgramEvent" data-variable-name="regularProgramEvents" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="regularProgramEvents" data-type="many">
						<option value="0">Select</option>
						@foreach($allregularProgramEvents as $regularProgramEvent)
						<option value="{{ $regularProgramEvent->id }}">{{ $regularProgramEvent->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($regularProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="regularProgramEvents">
						<option value="">Select</option>
						@foreach($regularProgram->regularProgramEvents as $regularProgramEvent)
						<option value="{{ $regularProgramEvent->id }}">{{ $regularProgramEvent->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgramEvents" value="{{ $regularProgramEventsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  		                <textarea data-field="title" class="form-control control-form" rows="4" placeholder="Title"></textarea>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		                <textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description"></textarea>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgramEvents" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-Rep08" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgramFeatures	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgramFeature" data-variable-name="regularProgramFeatures" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="regularProgramFeatures" data-type="many">
						<option value="0">Select</option>
						@foreach($allregularProgramFeatures as $regularProgramFeature)
						<option value="{{ $regularProgramFeature->id }}">{{ $regularProgramFeature->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($regularProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="regularProgramFeatures">
						<option value="">Select</option>
						@foreach($regularProgram->regularProgramFeatures as $regularProgramFeature)
						<option value="{{ $regularProgramFeature->id }}">{{ $regularProgramFeature->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgramFeatures" value="{{ $regularProgramFeaturesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  		                <input type="text" data-field="title" class="form-control control-form" placeholder="Title" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		                <textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description"></textarea>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgramFeatures" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowdLO8l"><div id="col-gFdQx" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgramFormats	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgramFormat" data-variable-name="regularProgramFormats" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="regularProgramFormats" data-type="many">
						<option value="0">Select</option>
						@foreach($allregularProgramFormats as $regularProgramFormat)
						<option value="{{ $regularProgramFormat->id }}">{{ $regularProgramFormat->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($regularProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="regularProgramFormats">
						<option value="">Select</option>
						@foreach($regularProgram->regularProgramFormats as $regularProgramFormat)
						<option value="{{ $regularProgramFormat->id }}">{{ $regularProgramFormat->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgramFormats" value="{{ $regularProgramFormatsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  		                <input type="text" data-field="title" class="form-control control-form" placeholder="Title" maxlength="200"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgramFormats" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-10ogY" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgramSemesters	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgramSemester" data-variable-name="regularProgramSemesters" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="regularProgramSemesters" data-type="many">
						<option value="0">Select</option>
						@foreach($allregularProgramSemesters as $regularProgramSemester)
						<option value="{{ $regularProgramSemester->id }}">{{ $regularProgramSemester->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($regularProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="regularProgramSemesters">
						<option value="">Select</option>
						@foreach($regularProgram->regularProgramSemesters as $regularProgramSemester)
						<option value="{{ $regularProgramSemester->id }}">{{ $regularProgramSemester->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgramSemesters" value="{{ $regularProgramSemestersValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Year</label>  
  		  		                <input type="text" data-field="year" class="form-control control-form" placeholder="Year" maxlength="4"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  		                <input type="text" data-field="name" class="form-control control-form" placeholder="Name" maxlength="10"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Month</label>  
  		  		                <input type="text" data-field="from_month" class="form-control control-form" placeholder="From Month" maxlength="15"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Month</label>  
  		  		                <input type="text" data-field="to_month" class="form-control control-form" placeholder="To Month" maxlength="15"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Duration Weeks</label>  
  		  		                <input type="text" data-field="duration_weeks" class="form-control control-form" placeholder="Duration Weeks" maxlength="20"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Regular Price</label>  
  		  		            <input data-field="regular_price" class="form-control control-form" type="number" placeholder="Regular Price" />
              		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgramSemesters" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowR5zR9"><div id="col-R8AZg" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgramStructures	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgramStructure" data-variable-name="regularProgramStructures" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="regularProgramStructures" data-type="many">
						<option value="0">Select</option>
						@foreach($allregularProgramStructures as $regularProgramStructure)
						<option value="{{ $regularProgramStructure->id }}">{{ $regularProgramStructure->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($regularProgram))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="regularProgramStructures">
						<option value="">Select</option>
						@foreach($regularProgram->regularProgramStructures as $regularProgramStructure)
						<option value="{{ $regularProgramStructure->id }}">{{ $regularProgramStructure->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgramStructures" value="{{ $regularProgramStructuresValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  		                <textarea data-field="title" class="form-control control-form" rows="4" placeholder="Title"></textarea>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgramStructures" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-2UzSk" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($regularProgram) ? 0 : $regularProgram->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/regularProgramsForm.js"></script>
@endpush