@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/regularProgramStructures/add">Add RegularProgramStructure</a>
@endsection

@section('crud-title')
	RegularProgramStructures
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>RegularProgram</th>
								<th>Title</th>
							</tr>
		</thead>
		<tbody>
			@foreach($regularProgramStructures as $regularProgramStructure)
			<tr>
								<td>
					<a href="/modules/regularProgramStructures/{{ $regularProgramStructure->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/regularProgramStructures/{{ $regularProgramStructure->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="regularProgramStructure" data-module="regularProgram" data-id="{{ $regularProgramStructure->id }}">RegularProgram</a>
				</td>
								
								<td>{{ $regularProgramStructure->title }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
