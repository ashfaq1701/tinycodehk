@extends('commons.crud')

@section('crud-title')
	Add RegularProgramStructure
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="regularProgramStructures">
		
						
				<div class="row" id="rowYpIbg"><div id="col-rbjNU" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="title">Title</label>  
  		                <textarea id="title" class="form-control control-form" rows="4" name="title" placeholder="Title">{{ empty($regularProgramStructure) ? '' : $regularProgramStructure->title }}</textarea>
                  		</div>
  	</div>
</div></div><div id="col-Yq6tf" class="col-md-6"></div></div><div class="row" id="rowhEV0Z"><div id="col-5vURj" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgram	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgram" data-variable-name="regularProgram" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="regularPrograms" data-type="one">
						<option value="0">Select</option>
						@foreach($allregularPrograms as $regularProgram)
						<option value="{{ $regularProgram->id }}">{{ $regularProgram->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgram" value="{{ (!empty($regularProgramStructure)) ? (!empty($regularProgramStructure->regularProgram) ? $regularProgramStructure->regularProgram->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($regularProgramStructure) ? '' : (empty($regularProgramStructure->regularProgram) ? '' : $regularProgramStructure->regularProgram->name) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Age</label>  
  		  		        	<input data-field="from_age" class="form-control control-form" type="number" placeholder="From Age" value="{{ empty($regularProgramStructure) ? '' : (empty($regularProgramStructure->regularProgram) ? '' : $regularProgramStructure->regularProgram->from_age) }}" />
        	  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Age</label>  
  		  		        	<input data-field="to_age" class="form-control control-form" type="number" placeholder="To Age" value="{{ empty($regularProgramStructure) ? '' : (empty($regularProgramStructure->regularProgram) ? '' : $regularProgramStructure->regularProgram->to_age) }}" />
        	  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($regularProgramStructure) ? '' : (empty($regularProgramStructure->regularProgram) ? '' : $regularProgramStructure->regularProgram->description) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  						<input type="text" data-field="language" class="form-control control-form" placeholder="Language" value="{{ empty($regularProgramStructure) ? '' : (empty($regularProgramStructure->regularProgram) ? '' : $regularProgramStructure->regularProgram->language) }}" maxlength="50"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Bringings</label>  
  		  						<input type="text" data-field="bringings" class="form-control control-form" placeholder="Bringings" value="{{ empty($regularProgramStructure) ? '' : (empty($regularProgramStructure->regularProgram) ? '' : $regularProgramStructure->regularProgram->bringings) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Icon</label>  
  		  						<input type="text" data-field="icon" class="form-control control-form" placeholder="Icon" value="{{ empty($regularProgramStructure) ? '' : (empty($regularProgramStructure->regularProgram) ? '' : $regularProgramStructure->regularProgram->icon) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Feat Image</label>  
  		  						<input type="text" data-field="feat_image" class="form-control control-form" placeholder="Feat Image" value="{{ empty($regularProgramStructure) ? '' : (empty($regularProgramStructure->regularProgram) ? '' : $regularProgramStructure->regularProgram->feat_image) }}" maxlength="200"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgram" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-24QRk" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($regularProgramStructure) ? 0 : $regularProgramStructure->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/regularProgramStructuresForm.js"></script>
@endpush