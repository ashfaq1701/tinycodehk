<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="regularProgramStructure" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allregularProgramStructures as $regularProgramStructure)
			<option value="{{ $regularProgramStructure->id }}">{{ $regularProgramStructure->title }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'regularProgram')
			<th>RegularProgram			</th>
			@endif
						<th>Title			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($regularProgramStructures as $regularProgramStructure)
		<tr>
						<td>
				<a href="/modules/regularProgramStructures/{{ $regularProgramStructure->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/regularProgramStructures/{{ $regularProgramStructure->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'regularProgram')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="regularProgramStructure" data-module="regularProgram" data-id="{{ $regularProgramStructure->id }}">RegularProgram</a>
			</td>
			@endif
							
						<td>{{ $regularProgramStructure->title }}</td>
					</tr>
		@endforeach
	</tbody>
</table>