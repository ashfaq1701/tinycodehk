@extends('commons.crud')

@section('crud-title')
	Add HolidayCampCourseHighlight
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="holidayCampCourseHighlights">
		
						
				<div class="row" id="rowDdbkC"><div id="col-JcWQN" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="title">Title</label>  
  		                <textarea id="title" class="form-control control-form" rows="4" name="title" placeholder="Title">{{ empty($holidayCampCourseHighlight) ? '' : $holidayCampCourseHighlight->title }}</textarea>
                  		</div>
  	</div>
</div></div><div id="col-GZHMo" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="description">Description</label>  
  		                <textarea id="description" class="form-control control-form" rows="4" name="description" placeholder="Description">{{ empty($holidayCampCourseHighlight) ? '' : $holidayCampCourseHighlight->description }}</textarea>
                  		</div>
  	</div>
</div></div></div><div class="row" id="row7CIeu"><div id="col-0q9PF" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourse	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourse" data-variable-name="holidayCampCourse" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="holidayCampCourses" data-type="one">
						<option value="0">Select</option>
						@foreach($allholidayCampCourses as $holidayCampCourse)
						<option value="{{ $holidayCampCourse->id }}">{{ $holidayCampCourse->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourse" value="{{ (!empty($holidayCampCourseHighlight)) ? (!empty($holidayCampCourseHighlight->holidayCampCourse) ? $holidayCampCourseHighlight->holidayCampCourse->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($holidayCampCourseHighlight) ? '' : (empty($holidayCampCourseHighlight->holidayCampCourse) ? '' : $holidayCampCourseHighlight->holidayCampCourse->name) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Icon</label>  
  		  						<input type="text" data-field="program_icon" class="form-control control-form" placeholder="Program Icon" value="{{ empty($holidayCampCourseHighlight) ? '' : (empty($holidayCampCourseHighlight->holidayCampCourse) ? '' : $holidayCampCourseHighlight->holidayCampCourse->program_icon) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Cover Image</label>  
  		  						<input type="text" data-field="program_cover_image" class="form-control control-form" placeholder="Program Cover Image" value="{{ empty($holidayCampCourseHighlight) ? '' : (empty($holidayCampCourseHighlight->holidayCampCourse) ? '' : $holidayCampCourseHighlight->holidayCampCourse->program_cover_image) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Video Url</label>  
  		  						<input type="text" data-field="program_video_url" class="form-control control-form" placeholder="Program Video Url" value="{{ empty($holidayCampCourseHighlight) ? '' : (empty($holidayCampCourseHighlight->holidayCampCourse) ? '' : $holidayCampCourseHighlight->holidayCampCourse->program_video_url) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  						<input type="text" data-field="language" class="form-control control-form" placeholder="Language" value="{{ empty($holidayCampCourseHighlight) ? '' : (empty($holidayCampCourseHighlight->holidayCampCourse) ? '' : $holidayCampCourseHighlight->holidayCampCourse->language) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($holidayCampCourseHighlight) ? '' : (empty($holidayCampCourseHighlight->holidayCampCourse) ? '' : $holidayCampCourseHighlight->holidayCampCourse->description) }}</textarea>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourse" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-S69HH" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($holidayCampCourseHighlight) ? 0 : $holidayCampCourseHighlight->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/holidayCampCourseHighlightsForm.js"></script>
@endpush