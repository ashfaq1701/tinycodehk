<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="holidayCampCourseHighlight" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allholidayCampCourseHighlights as $holidayCampCourseHighlight)
			<option value="{{ $holidayCampCourseHighlight->id }}">{{ $holidayCampCourseHighlight->title }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'holidayCampCourse')
			<th>HolidayCampCourse			</th>
			@endif
						<th>Title			</th>
						<th>Description			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($holidayCampCourseHighlights as $holidayCampCourseHighlight)
		<tr>
						<td>
				<a href="/modules/holidayCampCourseHighlights/{{ $holidayCampCourseHighlight->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/holidayCampCourseHighlights/{{ $holidayCampCourseHighlight->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'holidayCampCourse')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="holidayCampCourseHighlight" data-module="holidayCampCourse" data-id="{{ $holidayCampCourseHighlight->id }}">HolidayCampCourse</a>
			</td>
			@endif
							
						<td>{{ $holidayCampCourseHighlight->title }}</td>
						<td>{{ $holidayCampCourseHighlight->description }}</td>
					</tr>
		@endforeach
	</tbody>
</table>