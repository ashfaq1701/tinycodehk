@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/holidayCampCourseHighlights/add">Add HolidayCampCourseHighlight</a>
@endsection

@section('crud-title')
	HolidayCampCourseHighlights
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>HolidayCampCourse</th>
								<th>Title</th>
								<th>Description</th>
							</tr>
		</thead>
		<tbody>
			@foreach($holidayCampCourseHighlights as $holidayCampCourseHighlight)
			<tr>
								<td>
					<a href="/modules/holidayCampCourseHighlights/{{ $holidayCampCourseHighlight->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/holidayCampCourseHighlights/{{ $holidayCampCourseHighlight->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="holidayCampCourseHighlight" data-module="holidayCampCourse" data-id="{{ $holidayCampCourseHighlight->id }}">HolidayCampCourse</a>
				</td>
								
								<td>{{ $holidayCampCourseHighlight->title }}</td>
								<td>{{ $holidayCampCourseHighlight->description }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
