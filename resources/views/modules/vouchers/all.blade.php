@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/vouchers/add">Add Voucher</a>
@endsection

@section('crud-title')
	Vouchers
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>UsersVouchers</th>
								<th>Voucher Code</th>
								<th>Voucher Type</th>
								<th>Percent Discount</th>
								<th>Flat Discount</th>
							</tr>
		</thead>
		<tbody>
			@foreach($vouchers as $voucher)
			<tr>
								<td>
					<a href="/modules/vouchers/{{ $voucher->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/vouchers/{{ $voucher->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="voucher" data-module="usersVoucher" data-id="{{ $voucher->id }}">UsersVouchers</a></td>
								
								
								<td>{{ $voucher->voucher_code }}</td>
								<td>{{ $voucher->voucher_type }}</td>
								<td>{{ $voucher->percent_discount }}</td>
								<td>{{ $voucher->flat_discount }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
