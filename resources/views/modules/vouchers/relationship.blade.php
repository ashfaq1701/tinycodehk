<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="voucher" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allvouchers as $voucher)
			<option value="{{ $voucher->id }}">{{ $voucher->voucher_code }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'usersVoucher')
			<th>UsersVouchers			</th>
			@endif
						<th>Voucher Code			</th>
						<th>Voucher Type			</th>
						<th>Percent Discount			</th>
						<th>Flat Discount			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($vouchers as $voucher)
		<tr>
						<td>
				<a href="/modules/vouchers/{{ $voucher->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/vouchers/{{ $voucher->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'usersVoucher')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="voucher" data-module="usersVoucher" data-id="{{ $voucher->id }}">UsersVouchers</a>
			</td>
			@endif
							
							
						<td>{{ $voucher->voucher_code }}</td>
						<td>{{ $voucher->voucher_type }}</td>
						<td>{{ $voucher->percent_discount }}</td>
						<td>{{ $voucher->flat_discount }}</td>
					</tr>
		@endforeach
	</tbody>
</table>