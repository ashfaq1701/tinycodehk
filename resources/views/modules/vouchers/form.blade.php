@extends('commons.crud')

@section('crud-title')
	Add Voucher
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="vouchers">
		
						
				<div class="row" id="rowTya4k"><div id="col-HF7TX" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="voucher_code">Voucher Code</label>  
  		                <input type="text" id="voucher_code" class="form-control control-form" name="voucher_code" placeholder="Voucher Code" value="{{ empty($voucher) ? '' : $voucher->voucher_code }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div><div id="col-wiGBS" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="voucher_type">Voucher Type</label>  
  		                <input type="text" id="voucher_type" class="form-control control-form" name="voucher_type" placeholder="Voucher Type" value="{{ empty($voucher) ? '' : $voucher->voucher_type }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowAnBN4"><div id="col-qRZrt" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="percent_discount">Percent Discount</label>  
  		                <input id="percent_discount" class="form-control control-form" type="number" name="percent_discount" placeholder="Percent Discount" value="{{ empty($voucher) ? '' : $voucher->percent_discount }}" />
              		</div>
  	</div>
</div></div><div id="col-TR5Sf" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="flat_discount">Flat Discount</label>  
  		                <input id="flat_discount" class="form-control control-form" type="number" name="flat_discount" placeholder="Flat Discount" value="{{ empty($voucher) ? '' : $voucher->flat_discount }}" />
              		</div>
  	</div>
</div></div></div><div class="row" id="rowoAnTD"><div id="col-7PKkj" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		UsersVouchers	</div>
	<div class="panel-body relationship-panel" data-model="UsersVoucher" data-variable-name="usersVouchers" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="usersVouchers" data-type="many">
						<option value="0">Select</option>
						@foreach($allusersVouchers as $usersVoucher)
						<option value="{{ $usersVoucher->id }}">{{ $usersVoucher->valid_till }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($voucher))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="usersVouchers">
						<option value="">Select</option>
						@foreach($voucher->usersVouchers as $usersVoucher)
						<option value="{{ $usersVoucher->id }}">{{ $usersVoucher->valid_till }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="usersVouchers" value="{{ $usersVouchersValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Valid Till</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="valid_till" placeholder="Valid Till" type="text" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Is Used</label>  
  		  		                <input type="text" data-field="is_used" class="form-control control-form" placeholder="Is Used" />
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="usersVouchers" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-ledWr" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($voucher) ? 0 : $voucher->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/vouchersForm.js"></script>
@endpush