@extends('commons.crud')

@section('crud-title')
	Add User
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="users">
		
						
				<div class="row" id="rowr7sXh"><div id="col-JYU7Q" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		       	<span class="required"> *</span>
    	                <textarea id="name" class="form-control control-form" rows="4" name="name" placeholder="Name">{{ empty($user) ? '' : $user->name }}</textarea>
                  		</div>
  	</div>
</div></div><div id="col-ctiF9" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="email">Email</label>  
  		       	<span class="required"> *</span>
    	                <textarea id="email" class="form-control control-form" rows="4" name="email" placeholder="Email">{{ empty($user) ? '' : $user->email }}</textarea>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowVMUxS"><div id="col-ebu3U" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="stripe_id">Stripe Id</label>  
  		                <input type="text" id="stripe_id" class="form-control control-form" name="stripe_id" placeholder="Stripe Id" value="{{ empty($user) ? '' : $user->stripe_id }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div><div id="col-sDRVS" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="card_brand">Card Brand</label>  
  		                <input type="text" id="card_brand" class="form-control control-form" name="card_brand" placeholder="Card Brand" value="{{ empty($user) ? '' : $user->card_brand }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowJD6gb"><div id="col-bwPv1" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="card_last_4">Card Last 4</label>  
  		                <input type="text" id="card_last_4" class="form-control control-form" name="card_last_4" placeholder="Card Last 4" value="{{ empty($user) ? '' : $user->card_last_4 }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div><div id="col-cS6oD" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="trial_ends_at">Trial Ends At</label>  
  		        	<input class="form_datetime form-control" id="trial_ends_at" name="trial_ends_at" placeholder="Trial Ends At" type="text" value="{{ empty($user) ? '' : $user->trial_ends_at }}" readonly>
          		</div>
  	</div>
</div></div></div><div class="row" id="rowZHvLm"><div id="col-vvEpP" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Enrollments	</div>
	<div class="panel-body relationship-panel" data-model="Enrollment" data-variable-name="enrollments" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="enrollments" data-type="many">
						<option value="0">Select</option>
						@foreach($allenrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($user))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="enrollments">
						<option value="">Select</option>
						@foreach($user->enrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="enrollments" value="{{ $enrollmentsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  		                <input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" maxlength="15"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Registration No</label>  
  		  		                <input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Is Paid</label>  
  		  		                <input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" />
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Total Payment</label>  
  		  		            <input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" />
              		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="enrollments" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-okbXj" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Payments	</div>
	<div class="panel-body relationship-panel" data-model="Payment" data-variable-name="payments" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="payments" data-type="many">
						<option value="0">Select</option>
						@foreach($allpayments as $payment)
						<option value="{{ $payment->id }}">{{ $payment->amount }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($user))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="payments">
						<option value="">Select</option>
						@foreach($user->payments as $payment)
						<option value="{{ $payment->id }}">{{ $payment->amount }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="payments" value="{{ $paymentsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Amount</label>  
  		  		            <input data-field="amount" class="form-control control-form" type="number" placeholder="Amount" />
              		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Invoice Id</label>  
  		  		                <input type="text" data-field="invoice_id" class="form-control control-form" placeholder="Invoice Id" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Charge Id</label>  
  		  		                <input type="text" data-field="charge_id" class="form-control control-form" placeholder="Charge Id" maxlength="100"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="payments" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowM4PT8"><div id="col-qHiqI" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Users	</div>
	<div class="panel-body relationship-panel" data-model="User" data-variable-name="users" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="users" data-type="many">
						<option value="0">Select</option>
						@foreach($allusers as $user)
						<option value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($user))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="users">
						<option value="">Select</option>
						@foreach($user->users as $user)
						<option value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="users" value="{{ $usersValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		       	<span class="required"> *</span>
    	  		                <textarea data-field="name" class="form-control control-form" rows="4" placeholder="Name"></textarea>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Email</label>  
  		       	<span class="required"> *</span>
    	  		                <textarea data-field="email" class="form-control control-form" rows="4" placeholder="Email"></textarea>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Stripe Id</label>  
  		  		                <input type="text" data-field="stripe_id" class="form-control control-form" placeholder="Stripe Id" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Card Brand</label>  
  		  		                <input type="text" data-field="card_brand" class="form-control control-form" placeholder="Card Brand" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Card Last 4</label>  
  		  		                <input type="text" data-field="card_last_4" class="form-control control-form" placeholder="Card Last 4" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Trial Ends At</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="trial_ends_at" placeholder="Trial Ends At" type="text" readonly>
          		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="users" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-g91bv" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		UsersVouchers	</div>
	<div class="panel-body relationship-panel" data-model="UsersVoucher" data-variable-name="usersVouchers" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="usersVouchers" data-type="many">
						<option value="0">Select</option>
						@foreach($allusersVouchers as $usersVoucher)
						<option value="{{ $usersVoucher->id }}">{{ $usersVoucher->valid_till }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($user))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="usersVouchers">
						<option value="">Select</option>
						@foreach($user->usersVouchers as $usersVoucher)
						<option value="{{ $usersVoucher->id }}">{{ $usersVoucher->valid_till }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="usersVouchers" value="{{ $usersVouchersValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Valid Till</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="valid_till" placeholder="Valid Till" type="text" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Is Used</label>  
  		  		                <input type="text" data-field="is_used" class="form-control control-form" placeholder="Is Used" />
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="usersVouchers" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowPlaWN"><div id="col-GhDvG" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Role	</div>
	<div class="panel-body relationship-panel" data-model="Role" data-variable-name="role" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="roles" data-type="one">
						<option value="0">Select</option>
						@foreach($allroles as $role)
						<option value="{{ $role->id }}">{{ $role->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="role" value="{{ (!empty($user)) ? (!empty($user->role) ? $user->role->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($user) ? '' : (empty($user->role) ? '' : $user->role->name) }}" maxlength="50"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="role" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-4kj4A" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($user) ? 0 : $user->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/usersForm.js"></script>
@endpush