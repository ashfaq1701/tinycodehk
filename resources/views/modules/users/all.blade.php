@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/users/add">Add User</a>
@endsection

@section('crud-title')
	Users
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>Enrollments</th>
								<th>Payments</th>
								<th>Users</th>
								<th>UsersVouchers</th>
								<th>Role</th>
								<th>Name</th>
								<th>Email</th>
								<th>Stripe Id</th>
								<th>Card Brand</th>
								<th>Card Last 4</th>
								<th>Trial Ends At</th>
							</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr>
								<td>
					<a href="/modules/users/{{ $user->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/users/{{ $user->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="user" data-module="enrollment" data-id="{{ $user->id }}">Enrollments</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="user" data-module="payment" data-id="{{ $user->id }}">Payments</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="user" data-module="user" data-id="{{ $user->id }}">Users</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="user" data-module="usersVoucher" data-id="{{ $user->id }}">UsersVouchers</a></td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="user" data-module="role" data-id="{{ $user->id }}">Role</a>
				</td>
								
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
								<td>{{ $user->stripe_id }}</td>
								<td>{{ $user->card_brand }}</td>
								<td>{{ $user->card_last_4 }}</td>
								<td>{{ $user->trial_ends_at }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
