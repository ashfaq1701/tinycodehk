<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="user" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allusers as $user)
			<option value="{{ $user->id }}">{{ $user->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'enrollment')
			<th>Enrollments			</th>
			@endif
						@if($origin != 'payment')
			<th>Payments			</th>
			@endif
						@if($origin != 'user')
			<th>Users			</th>
			@endif
						@if($origin != 'usersVoucher')
			<th>UsersVouchers			</th>
			@endif
						@if($origin != 'role')
			<th>Role			</th>
			@endif
						<th>Name			</th>
						<th>Email			</th>
						<th>Stripe Id			</th>
						<th>Card Brand			</th>
						<th>Card Last 4			</th>
						<th>Trial Ends At			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($users as $user)
		<tr>
						<td>
				<a href="/modules/users/{{ $user->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/users/{{ $user->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'enrollment')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="user" data-module="enrollment" data-id="{{ $user->id }}">Enrollments</a>
			</td>
			@endif
						@if($origin != 'payment')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="user" data-module="payment" data-id="{{ $user->id }}">Payments</a>
			</td>
			@endif
						@if($origin != 'user')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="user" data-module="user" data-id="{{ $user->id }}">Users</a>
			</td>
			@endif
						@if($origin != 'usersVoucher')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="user" data-module="usersVoucher" data-id="{{ $user->id }}">UsersVouchers</a>
			</td>
			@endif
							
						@if($origin != 'role')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="user" data-module="role" data-id="{{ $user->id }}">Role</a>
			</td>
			@endif
							
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->stripe_id }}</td>
						<td>{{ $user->card_brand }}</td>
						<td>{{ $user->card_last_4 }}</td>
						<td>{{ $user->trial_ends_at }}</td>
					</tr>
		@endforeach
	</tbody>
</table>