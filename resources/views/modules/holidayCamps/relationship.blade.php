<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="holidayCamp" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allholidayCamps as $holidayCamp)
			<option value="{{ $holidayCamp->id }}">{{ $holidayCamp->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'holidayCampCourse')
			<th>HolidayCampCourses			</th>
			@endif
						<th>Name			</th>
						<th>Description			</th>
						<th>From Date			</th>
						<th>To Date			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($holidayCamps as $holidayCamp)
		<tr>
						<td>
				<a href="/modules/holidayCamps/{{ $holidayCamp->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/holidayCamps/{{ $holidayCamp->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'holidayCampCourse')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="holidayCamp" data-module="holidayCampCourse" data-id="{{ $holidayCamp->id }}">HolidayCampCourses</a>
			</td>
			@endif
							
							
						<td>{{ $holidayCamp->name }}</td>
						<td>{{ $holidayCamp->description }}</td>
						<td>{{ $holidayCamp->from_date }}</td>
						<td>{{ $holidayCamp->to_date }}</td>
					</tr>
		@endforeach
	</tbody>
</table>