@extends('commons.crud')

@section('crud-title')
	Add HolidayCamp
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="holidayCamps">
		
						
				<div class="row" id="rowO4wDg"><div id="col-k5uJm" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <input type="text" id="name" class="form-control control-form" name="name" placeholder="Name" value="{{ empty($holidayCamp) ? '' : $holidayCamp->name }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div><div id="col-wUVeE" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="description">Description</label>  
  		                <textarea id="description" class="form-control control-form" rows="4" name="description" placeholder="Description">{{ empty($holidayCamp) ? '' : $holidayCamp->description }}</textarea>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowgHBuo"><div id="col-CjTba" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="from_date">From Date</label>  
  		        	<input class="form_datetime form-control" id="from_date" name="from_date" placeholder="From Date" type="text" value="{{ empty($holidayCamp) ? '' : $holidayCamp->from_date }}" readonly>
          		</div>
  	</div>
</div></div><div id="col-fRzU2" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="to_date">To Date</label>  
  		        	<input class="form_datetime form-control" id="to_date" name="to_date" placeholder="To Date" type="text" value="{{ empty($holidayCamp) ? '' : $holidayCamp->to_date }}" readonly>
          		</div>
  	</div>
</div></div></div><div class="row" id="rowKK8MG"><div id="col-nmlyg" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourses	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourse" data-variable-name="holidayCampCourses" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="holidayCampCourses" data-type="many">
						<option value="0">Select</option>
						@foreach($allholidayCampCourses as $holidayCampCourse)
						<option value="{{ $holidayCampCourse->id }}">{{ $holidayCampCourse->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($holidayCamp))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="holidayCampCourses">
						<option value="">Select</option>
						@foreach($holidayCamp->holidayCampCourses as $holidayCampCourse)
						<option value="{{ $holidayCampCourse->id }}">{{ $holidayCampCourse->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourses" value="{{ $holidayCampCoursesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  		                <input type="text" data-field="name" class="form-control control-form" placeholder="Name" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Icon</label>  
  		  		                <input type="text" data-field="program_icon" class="form-control control-form" placeholder="Program Icon" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Cover Image</label>  
  		  		                <input type="text" data-field="program_cover_image" class="form-control control-form" placeholder="Program Cover Image" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Video Url</label>  
  		  		                <input type="text" data-field="program_video_url" class="form-control control-form" placeholder="Program Video Url" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  		                <input type="text" data-field="language" class="form-control control-form" placeholder="Language" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		                <textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description"></textarea>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourses" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-jG8FI" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($holidayCamp) ? 0 : $holidayCamp->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/holidayCampsForm.js"></script>
@endpush