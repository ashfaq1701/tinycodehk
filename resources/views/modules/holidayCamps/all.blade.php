@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/holidayCamps/add">Add HolidayCamp</a>
@endsection

@section('crud-title')
	HolidayCamps
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>HolidayCampCourses</th>
								<th>Name</th>
								<th>Description</th>
								<th>From Date</th>
								<th>To Date</th>
							</tr>
		</thead>
		<tbody>
			@foreach($holidayCamps as $holidayCamp)
			<tr>
								<td>
					<a href="/modules/holidayCamps/{{ $holidayCamp->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/holidayCamps/{{ $holidayCamp->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="holidayCamp" data-module="holidayCampCourse" data-id="{{ $holidayCamp->id }}">HolidayCampCourses</a></td>
								
								
								<td>{{ $holidayCamp->name }}</td>
								<td>{{ $holidayCamp->description }}</td>
								<td>{{ $holidayCamp->from_date }}</td>
								<td>{{ $holidayCamp->to_date }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
