@extends('commons.crud')

@section('crud-title')
	Add HolidayCampCourseFeature
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="holidayCampCourseFeatures">
		
						
				<div class="row" id="row3qvM6"><div id="col-OsibX" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <input type="text" id="name" class="form-control control-form" name="name" placeholder="Name" value="{{ empty($holidayCampCourseFeature) ? '' : $holidayCampCourseFeature->name }}" maxlength="200"/>
                  		</div>
  	</div>
</div></div><div id="col-azCCU" class="col-md-6"></div></div><div class="row" id="rowDlKlf"><div id="col-Vzjyt" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourseSubFeatures	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourseSubFeature" data-variable-name="holidayCampCourseSubFeatures" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="holidayCampCourseSubFeatures" data-type="many">
						<option value="0">Select</option>
						@foreach($allholidayCampCourseSubFeatures as $holidayCampCourseSubFeature)
						<option value="{{ $holidayCampCourseSubFeature->id }}">{{ $holidayCampCourseSubFeature->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($holidayCampCourseFeature))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="holidayCampCourseSubFeatures">
						<option value="">Select</option>
						@foreach($holidayCampCourseFeature->holidayCampCourseSubFeatures as $holidayCampCourseSubFeature)
						<option value="{{ $holidayCampCourseSubFeature->id }}">{{ $holidayCampCourseSubFeature->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourseSubFeatures" value="{{ $holidayCampCourseSubFeaturesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  		                <textarea data-field="name" class="form-control control-form" rows="4" placeholder="Name"></textarea>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourseSubFeatures" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-yVOfL" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourse	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourse" data-variable-name="holidayCampCourse" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="holidayCampCourses" data-type="one">
						<option value="0">Select</option>
						@foreach($allholidayCampCourses as $holidayCampCourse)
						<option value="{{ $holidayCampCourse->id }}">{{ $holidayCampCourse->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourse" value="{{ (!empty($holidayCampCourseFeature)) ? (!empty($holidayCampCourseFeature->holidayCampCourse) ? $holidayCampCourseFeature->holidayCampCourse->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($holidayCampCourseFeature) ? '' : (empty($holidayCampCourseFeature->holidayCampCourse) ? '' : $holidayCampCourseFeature->holidayCampCourse->name) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Icon</label>  
  		  						<input type="text" data-field="program_icon" class="form-control control-form" placeholder="Program Icon" value="{{ empty($holidayCampCourseFeature) ? '' : (empty($holidayCampCourseFeature->holidayCampCourse) ? '' : $holidayCampCourseFeature->holidayCampCourse->program_icon) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Cover Image</label>  
  		  						<input type="text" data-field="program_cover_image" class="form-control control-form" placeholder="Program Cover Image" value="{{ empty($holidayCampCourseFeature) ? '' : (empty($holidayCampCourseFeature->holidayCampCourse) ? '' : $holidayCampCourseFeature->holidayCampCourse->program_cover_image) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Video Url</label>  
  		  						<input type="text" data-field="program_video_url" class="form-control control-form" placeholder="Program Video Url" value="{{ empty($holidayCampCourseFeature) ? '' : (empty($holidayCampCourseFeature->holidayCampCourse) ? '' : $holidayCampCourseFeature->holidayCampCourse->program_video_url) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  						<input type="text" data-field="language" class="form-control control-form" placeholder="Language" value="{{ empty($holidayCampCourseFeature) ? '' : (empty($holidayCampCourseFeature->holidayCampCourse) ? '' : $holidayCampCourseFeature->holidayCampCourse->language) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($holidayCampCourseFeature) ? '' : (empty($holidayCampCourseFeature->holidayCampCourse) ? '' : $holidayCampCourseFeature->holidayCampCourse->description) }}</textarea>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourse" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($holidayCampCourseFeature) ? 0 : $holidayCampCourseFeature->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/holidayCampCourseFeaturesForm.js"></script>
@endpush