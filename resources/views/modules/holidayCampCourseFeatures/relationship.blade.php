<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="holidayCampCourseFeature" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allholidayCampCourseFeatures as $holidayCampCourseFeature)
			<option value="{{ $holidayCampCourseFeature->id }}">{{ $holidayCampCourseFeature->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'holidayCampCourseSubFeature')
			<th>HolidayCampCourseSubFeatures			</th>
			@endif
						@if($origin != 'holidayCampCourse')
			<th>HolidayCampCourse			</th>
			@endif
						<th>Name			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($holidayCampCourseFeatures as $holidayCampCourseFeature)
		<tr>
						<td>
				<a href="/modules/holidayCampCourseFeatures/{{ $holidayCampCourseFeature->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/holidayCampCourseFeatures/{{ $holidayCampCourseFeature->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'holidayCampCourseSubFeature')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="holidayCampCourseFeature" data-module="holidayCampCourseSubFeature" data-id="{{ $holidayCampCourseFeature->id }}">HolidayCampCourseSubFeatures</a>
			</td>
			@endif
							
						@if($origin != 'holidayCampCourse')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="holidayCampCourseFeature" data-module="holidayCampCourse" data-id="{{ $holidayCampCourseFeature->id }}">HolidayCampCourse</a>
			</td>
			@endif
							
						<td>{{ $holidayCampCourseFeature->name }}</td>
					</tr>
		@endforeach
	</tbody>
</table>