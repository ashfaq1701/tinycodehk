@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/holidayCampCourseFeatures/add">Add HolidayCampCourseFeature</a>
@endsection

@section('crud-title')
	HolidayCampCourseFeatures
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>HolidayCampCourseSubFeatures</th>
								<th>HolidayCampCourse</th>
								<th>Name</th>
							</tr>
		</thead>
		<tbody>
			@foreach($holidayCampCourseFeatures as $holidayCampCourseFeature)
			<tr>
								<td>
					<a href="/modules/holidayCampCourseFeatures/{{ $holidayCampCourseFeature->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/holidayCampCourseFeatures/{{ $holidayCampCourseFeature->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="holidayCampCourseFeature" data-module="holidayCampCourseSubFeature" data-id="{{ $holidayCampCourseFeature->id }}">HolidayCampCourseSubFeatures</a></td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="holidayCampCourseFeature" data-module="holidayCampCourse" data-id="{{ $holidayCampCourseFeature->id }}">HolidayCampCourse</a>
				</td>
								
								<td>{{ $holidayCampCourseFeature->name }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
