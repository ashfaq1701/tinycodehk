@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/inHomeProgramFormats/add">Add InHomeProgramFormat</a>
@endsection

@section('crud-title')
	InHomeProgramFormats
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>InHomeProgram</th>
								<th>Title</th>
							</tr>
		</thead>
		<tbody>
			@foreach($inHomeProgramFormats as $inHomeProgramFormat)
			<tr>
								<td>
					<a href="/modules/inHomeProgramFormats/{{ $inHomeProgramFormat->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/inHomeProgramFormats/{{ $inHomeProgramFormat->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="inHomeProgramFormat" data-module="inHomeProgram" data-id="{{ $inHomeProgramFormat->id }}">InHomeProgram</a>
				</td>
								
								<td>{{ $inHomeProgramFormat->title }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
