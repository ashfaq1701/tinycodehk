@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/programLevels/add">Add ProgramLevel</a>
@endsection

@section('crud-title')
	ProgramLevels
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>HolidayCampCourses</th>
								<th>Name</th>
							</tr>
		</thead>
		<tbody>
			@foreach($programLevels as $programLevel)
			<tr>
								<td>
					<a href="/modules/programLevels/{{ $programLevel->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/programLevels/{{ $programLevel->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="programLevel" data-module="holidayCampCourse" data-id="{{ $programLevel->id }}">HolidayCampCourses</a></td>
								
								
								<td>{{ $programLevel->name }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
