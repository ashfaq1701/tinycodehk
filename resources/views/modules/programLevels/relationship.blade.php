<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="programLevel" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allprogramLevels as $programLevel)
			<option value="{{ $programLevel->id }}">{{ $programLevel->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'holidayCampCourse')
			<th>HolidayCampCourses			</th>
			@endif
						<th>Name			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($programLevels as $programLevel)
		<tr>
						<td>
				<a href="/modules/programLevels/{{ $programLevel->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/programLevels/{{ $programLevel->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'holidayCampCourse')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="programLevel" data-module="holidayCampCourse" data-id="{{ $programLevel->id }}">HolidayCampCourses</a>
			</td>
			@endif
							
							
						<td>{{ $programLevel->name }}</td>
					</tr>
		@endforeach
	</tbody>
</table>