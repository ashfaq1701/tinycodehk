@extends('commons.crud')

@section('crud-title')
	Add RegularProgramSemester
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="regularProgramSemesters">
		
						
				<div class="row" id="rowwQ5Jl"><div id="col-cM8N9" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="year">Year</label>  
  		                <input type="text" id="year" class="form-control control-form" name="year" placeholder="Year" value="{{ empty($regularProgramSemester) ? '' : $regularProgramSemester->year }}" maxlength="4"/>
                  		</div>
  	</div>
</div></div><div id="col-d67cl" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <input type="text" id="name" class="form-control control-form" name="name" placeholder="Name" value="{{ empty($regularProgramSemester) ? '' : $regularProgramSemester->name }}" maxlength="10"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowNs1SK"><div id="col-idJuW" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="from_month">From Month</label>  
  		                <input type="text" id="from_month" class="form-control control-form" name="from_month" placeholder="From Month" value="{{ empty($regularProgramSemester) ? '' : $regularProgramSemester->from_month }}" maxlength="15"/>
                  		</div>
  	</div>
</div></div><div id="col-yz3kw" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="to_month">To Month</label>  
  		                <input type="text" id="to_month" class="form-control control-form" name="to_month" placeholder="To Month" value="{{ empty($regularProgramSemester) ? '' : $regularProgramSemester->to_month }}" maxlength="15"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowxX0QD"><div id="col-oTY5R" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="duration_weeks">Duration Weeks</label>  
  		                <input type="text" id="duration_weeks" class="form-control control-form" name="duration_weeks" placeholder="Duration Weeks" value="{{ empty($regularProgramSemester) ? '' : $regularProgramSemester->duration_weeks }}" maxlength="20"/>
                  		</div>
  	</div>
</div></div><div id="col-3VEK7" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="regular_price">Regular Price</label>  
  		                <input id="regular_price" class="form-control control-form" type="number" name="regular_price" placeholder="Regular Price" value="{{ empty($regularProgramSemester) ? '' : $regularProgramSemester->regular_price }}" />
              		</div>
  	</div>
</div></div></div><div class="row" id="rowTwu1N"><div id="col-VtFtz" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgramClasseSchedules	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgramClasseSchedule" data-variable-name="regularProgramClasseSchedules" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="regularProgramClasseSchedules" data-type="many">
						<option value="0">Select</option>
						@foreach($allregularProgramClasseSchedules as $regularProgramClasseSchedule)
						<option value="{{ $regularProgramClasseSchedule->id }}">{{ $regularProgramClasseSchedule->from_time }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($regularProgramSemester))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="regularProgramClasseSchedules">
						<option value="">Select</option>
						@foreach($regularProgramSemester->regularProgramClasseSchedules as $regularProgramClasseSchedule)
						<option value="{{ $regularProgramClasseSchedule->id }}">{{ $regularProgramClasseSchedule->from_time }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgramClasseSchedules" value="{{ $regularProgramClasseSchedulesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Time</label>  
  		  		                <input type="text" data-field="from_time" class="form-control control-form" placeholder="From Time" maxlength="50"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Time</label>  
  		  		                <input type="text" data-field="to_time" class="form-control control-form" placeholder="To Time" maxlength="50"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgramClasseSchedules" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-ESmgi" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgram	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgram" data-variable-name="regularProgram" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="regularPrograms" data-type="one">
						<option value="0">Select</option>
						@foreach($allregularPrograms as $regularProgram)
						<option value="{{ $regularProgram->id }}">{{ $regularProgram->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgram" value="{{ (!empty($regularProgramSemester)) ? (!empty($regularProgramSemester->regularProgram) ? $regularProgramSemester->regularProgram->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($regularProgramSemester) ? '' : (empty($regularProgramSemester->regularProgram) ? '' : $regularProgramSemester->regularProgram->name) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Age</label>  
  		  		        	<input data-field="from_age" class="form-control control-form" type="number" placeholder="From Age" value="{{ empty($regularProgramSemester) ? '' : (empty($regularProgramSemester->regularProgram) ? '' : $regularProgramSemester->regularProgram->from_age) }}" />
        	  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Age</label>  
  		  		        	<input data-field="to_age" class="form-control control-form" type="number" placeholder="To Age" value="{{ empty($regularProgramSemester) ? '' : (empty($regularProgramSemester->regularProgram) ? '' : $regularProgramSemester->regularProgram->to_age) }}" />
        	  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($regularProgramSemester) ? '' : (empty($regularProgramSemester->regularProgram) ? '' : $regularProgramSemester->regularProgram->description) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  						<input type="text" data-field="language" class="form-control control-form" placeholder="Language" value="{{ empty($regularProgramSemester) ? '' : (empty($regularProgramSemester->regularProgram) ? '' : $regularProgramSemester->regularProgram->language) }}" maxlength="50"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Bringings</label>  
  		  						<input type="text" data-field="bringings" class="form-control control-form" placeholder="Bringings" value="{{ empty($regularProgramSemester) ? '' : (empty($regularProgramSemester->regularProgram) ? '' : $regularProgramSemester->regularProgram->bringings) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Icon</label>  
  		  						<input type="text" data-field="icon" class="form-control control-form" placeholder="Icon" value="{{ empty($regularProgramSemester) ? '' : (empty($regularProgramSemester->regularProgram) ? '' : $regularProgramSemester->regularProgram->icon) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Feat Image</label>  
  		  						<input type="text" data-field="feat_image" class="form-control control-form" placeholder="Feat Image" value="{{ empty($regularProgramSemester) ? '' : (empty($regularProgramSemester->regularProgram) ? '' : $regularProgramSemester->regularProgram->feat_image) }}" maxlength="200"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgram" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div>

<div class="row" id="rowMSTr1">
	<div id="col-mtREY" class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				Enrollments
			</div>
			<div class="panel-body relationship-panel" data-model="Enrollment"  data-variable-name="enrollments" data-type="many">
				<div class="well">
					<div class="row">
						<div class="col-md-8">
							<select class="form-control" data-module="enrollments" data-type="many">
								<option value="0">Select</option>
								@foreach($allEnrollments as $enrollment)
								<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
						</div>
					</div>
				</div>
				@if(!empty($regularProgramSemester))
				<div class="well">
					<div class="row">
						<div class="col-md-12">
							<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="enrollments">
								<option value="">Select</option>
								@foreach($regularProgramSemester->enrollments as $enrollment)
								<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				@endif
				<div class="row">
					<input type="hidden" class="related-entry" name="enrollments" value="{{ $enrollmentsValue or '' }}"/>
					<input type="hidden" class="active-entry" value=""/>
					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Course Type</label>  
  		  		                <select class="form-control" data-field="course_type" class="form-control control-form">
  		  		                	<option value="">Select</option>
  		  		                	<option value="inHome">In Home</option>
  		  		                	<option value="regular">Regular</option>
  		  		                	<option value="holidayCamp">Holiday Camp</option>
  		  		                </select>
                  			</div>
  						</div>
  					</div>
  					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Registration No</label>  
  		  		                <input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" maxlength="45"/>
                  			</div>
  						</div>
					</div>
					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Is Paid</label>  
  		  		                <input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" />
                  			</div>
  						</div>
					</div>
					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Total Payment</label>  
  		  		            	<input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" />
              				</div>
  						</div>
					</div>
					<div class="col-md-12">
						<button class="btn btn-success" data-module="enrollments" onclick="localSaveManyRelationship(event)">Save</button>
						<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

			<input type="hidden" name="primary" id="primary" value="{{ empty($regularProgramSemester) ? 0 : $regularProgramSemester->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/regularProgramSemestersForm.js"></script>
@endpush