<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="regularProgramSemester" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allregularProgramSemesters as $regularProgramSemester)
			<option value="{{ $regularProgramSemester->id }}">{{ $regularProgramSemester->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'regularProgramClasseSchedule')
			<th>RegularProgramClasseSchedules			</th>
			@endif
						@if($origin != 'regularProgram')
			<th>RegularProgram			</th>
			@endif
						<th>Year			</th>
						<th>Name			</th>
						<th>From Month			</th>
						<th>To Month			</th>
						<th>Duration Weeks			</th>
						<th>Regular Price			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($regularProgramSemesters as $regularProgramSemester)
		<tr>
						<td>
				<a href="/modules/regularProgramSemesters/{{ $regularProgramSemester->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/regularProgramSemesters/{{ $regularProgramSemester->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'regularProgramClasseSchedule')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="regularProgramSemester" data-module="regularProgramClasseSchedule" data-id="{{ $regularProgramSemester->id }}">RegularProgramClasseSchedules</a>
			</td>
			@endif
							
						@if($origin != 'regularProgram')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="regularProgramSemester" data-module="regularProgram" data-id="{{ $regularProgramSemester->id }}">RegularProgram</a>
			</td>
			@endif
							
						<td>{{ $regularProgramSemester->year }}</td>
						<td>{{ $regularProgramSemester->name }}</td>
						<td>{{ $regularProgramSemester->from_month }}</td>
						<td>{{ $regularProgramSemester->to_month }}</td>
						<td>{{ $regularProgramSemester->duration_weeks }}</td>
						<td>{{ $regularProgramSemester->regular_price }}</td>
					</tr>
		@endforeach
	</tbody>
</table>