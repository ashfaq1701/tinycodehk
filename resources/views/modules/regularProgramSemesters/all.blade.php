@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/regularProgramSemesters/add">Add RegularProgramSemester</a>
@endsection

@section('crud-title')
	RegularProgramSemesters
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>RegularProgramClasseSchedules</th>
								<th>RegularProgram</th>
								<th>Year</th>
								<th>Name</th>
								<th>From Month</th>
								<th>To Month</th>
								<th>Duration Weeks</th>
								<th>Regular Price</th>
							</tr>
		</thead>
		<tbody>
			@foreach($regularProgramSemesters as $regularProgramSemester)
			<tr>
								<td>
					<a href="/modules/regularProgramSemesters/{{ $regularProgramSemester->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/regularProgramSemesters/{{ $regularProgramSemester->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="regularProgramSemester" data-module="regularProgramClasseSchedule" data-id="{{ $regularProgramSemester->id }}">RegularProgramClasseSchedules</a></td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="regularProgramSemester" data-module="regularProgram" data-id="{{ $regularProgramSemester->id }}">RegularProgram</a>
				</td>
								
								<td>{{ $regularProgramSemester->year }}</td>
								<td>{{ $regularProgramSemester->name }}</td>
								<td>{{ $regularProgramSemester->from_month }}</td>
								<td>{{ $regularProgramSemester->to_month }}</td>
								<td>{{ $regularProgramSemester->duration_weeks }}</td>
								<td>{{ $regularProgramSemester->regular_price }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
