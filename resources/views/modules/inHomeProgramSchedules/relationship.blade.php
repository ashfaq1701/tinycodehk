<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="inHomeProgramSchedule" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allinHomeProgramSchedules as $inHomeProgramSchedule)
			<option value="{{ $inHomeProgramSchedule->id }}">{{ $inHomeProgramSchedule->from_time }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'inHomeProgram')
			<th>InHomeProgram			</th>
			@endif
						@if($origin != 'weekday')
			<th>Weekday			</th>
			@endif
						<th>From Time			</th>
						<th>To Time			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($inHomeProgramSchedules as $inHomeProgramSchedule)
		<tr>
						<td>
				<a href="/modules/inHomeProgramSchedules/{{ $inHomeProgramSchedule->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/inHomeProgramSchedules/{{ $inHomeProgramSchedule->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'inHomeProgram')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="inHomeProgramSchedule" data-module="inHomeProgram" data-id="{{ $inHomeProgramSchedule->id }}">InHomeProgram</a>
			</td>
			@endif
						@if($origin != 'weekday')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="inHomeProgramSchedule" data-module="weekday" data-id="{{ $inHomeProgramSchedule->id }}">Weekday</a>
			</td>
			@endif
							
						<td>{{ $inHomeProgramSchedule->from_time }}</td>
						<td>{{ $inHomeProgramSchedule->to_time }}</td>
					</tr>
		@endforeach
	</tbody>
</table>