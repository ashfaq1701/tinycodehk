@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/inHomeProgramSchedules/add">Add InHomeProgramSchedule</a>
@endsection

@section('crud-title')
	InHomeProgramSchedules
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>InHomeProgram</th>
								<th>Weekday</th>
								<th>From Time</th>
								<th>To Time</th>
							</tr>
		</thead>
		<tbody>
			@foreach($inHomeProgramSchedules as $inHomeProgramSchedule)
			<tr>
								<td>
					<a href="/modules/inHomeProgramSchedules/{{ $inHomeProgramSchedule->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/inHomeProgramSchedules/{{ $inHomeProgramSchedule->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="inHomeProgramSchedule" data-module="inHomeProgram" data-id="{{ $inHomeProgramSchedule->id }}">InHomeProgram</a>
				</td>
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="inHomeProgramSchedule" data-module="weekday" data-id="{{ $inHomeProgramSchedule->id }}">Weekday</a>
				</td>
								
								<td>{{ $inHomeProgramSchedule->from_time }}</td>
								<td>{{ $inHomeProgramSchedule->to_time }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
