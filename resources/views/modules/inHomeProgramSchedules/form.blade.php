@extends('commons.crud')

@section('crud-title')
	Add InHomeProgramSchedule
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="inHomeProgramSchedules">
		
						
				<div class="row" id="rowLXNlx"><div id="col-0g0MN" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="from_time">From Time</label>  
  		                <input type="text" id="from_time" class="form-control control-form" name="from_time" placeholder="From Time" value="{{ empty($inHomeProgramSchedule) ? '' : $inHomeProgramSchedule->from_time }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div><div id="col-uqgSc" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="to_time">To Time</label>  
  		                <input type="text" id="to_time" class="form-control control-form" name="to_time" placeholder="To Time" value="{{ empty($inHomeProgramSchedule) ? '' : $inHomeProgramSchedule->to_time }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowAs1VW"><div id="col-L2LvJ" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		InHomeProgram	</div>
	<div class="panel-body relationship-panel" data-model="InHomeProgram" data-variable-name="inHomeProgram" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="inHomePrograms" data-type="one">
						<option value="0">Select</option>
						@foreach($allinHomePrograms as $inHomeProgram)
						<option value="{{ $inHomeProgram->id }}">{{ $inHomeProgram->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="inHomeProgram" value="{{ (!empty($inHomeProgramSchedule)) ? (!empty($inHomeProgramSchedule->inHomeProgram) ? $inHomeProgramSchedule->inHomeProgram->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($inHomeProgramSchedule) ? '' : (empty($inHomeProgramSchedule->inHomeProgram) ? '' : $inHomeProgramSchedule->inHomeProgram->name) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Age</label>  
  		  		        	<input data-field="from_age" class="form-control control-form" type="number" placeholder="From Age" value="{{ empty($inHomeProgramSchedule) ? '' : (empty($inHomeProgramSchedule->inHomeProgram) ? '' : $inHomeProgramSchedule->inHomeProgram->from_age) }}" />
        	  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Age</label>  
  		  		        	<input data-field="to_age" class="form-control control-form" type="number" placeholder="To Age" value="{{ empty($inHomeProgramSchedule) ? '' : (empty($inHomeProgramSchedule->inHomeProgram) ? '' : $inHomeProgramSchedule->inHomeProgram->to_age) }}" />
        	  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description 1</label>  
  		  		            	<textarea data-field="description_1" class="form-control control-form" rows="4" placeholder="Description 1">{{ empty($inHomeProgramSchedule) ? '' : (empty($inHomeProgramSchedule->inHomeProgram) ? '' : $inHomeProgramSchedule->inHomeProgram->description_1) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description 2</label>  
  		  		            	<textarea data-field="description_2" class="form-control control-form" rows="4" placeholder="Description 2">{{ empty($inHomeProgramSchedule) ? '' : (empty($inHomeProgramSchedule->inHomeProgram) ? '' : $inHomeProgramSchedule->inHomeProgram->description_2) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Image 1</label>  
  		  						<input type="text" data-field="image_1" class="form-control control-form" placeholder="Image 1" value="{{ empty($inHomeProgramSchedule) ? '' : (empty($inHomeProgramSchedule->inHomeProgram) ? '' : $inHomeProgramSchedule->inHomeProgram->image_1) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Image 2</label>  
  		  						<input type="text" data-field="image_2" class="form-control control-form" placeholder="Image 2" value="{{ empty($inHomeProgramSchedule) ? '' : (empty($inHomeProgramSchedule->inHomeProgram) ? '' : $inHomeProgramSchedule->inHomeProgram->image_2) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  						<input type="text" data-field="language" class="form-control control-form" placeholder="Language" value="{{ empty($inHomeProgramSchedule) ? '' : (empty($inHomeProgramSchedule->inHomeProgram) ? '' : $inHomeProgramSchedule->inHomeProgram->language) }}" maxlength="50"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="inHomeProgram" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-QMCBt" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Weekday	</div>
	<div class="panel-body relationship-panel" data-model="Weekday" data-variable-name="weekday" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="weekdays" data-type="one">
						<option value="0">Select</option>
						@foreach($allweekdays as $weekday)
						<option value="{{ $weekday->id }}">{{ $weekday->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="weekday" value="{{ (!empty($inHomeProgramSchedule)) ? (!empty($inHomeProgramSchedule->weekday) ? $inHomeProgramSchedule->weekday->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($inHomeProgramSchedule) ? '' : (empty($inHomeProgramSchedule->weekday) ? '' : $inHomeProgramSchedule->weekday->name) }}" maxlength="45"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="weekday" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($inHomeProgramSchedule) ? 0 : $inHomeProgramSchedule->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/inHomeProgramSchedulesForm.js"></script>
@endpush