@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/usersVouchers/add">Add UsersVoucher</a>
@endsection

@section('crud-title')
	UsersVouchers
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>Enrollments</th>
								<th>Voucher</th>
								<th>User</th>
								<th>Valid Till</th>
								<th>Is Used</th>
							</tr>
		</thead>
		<tbody>
			@foreach($usersVouchers as $usersVoucher)
			<tr>
								<td>
					<a href="/modules/usersVouchers/{{ $usersVoucher->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/usersVouchers/{{ $usersVoucher->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="usersVoucher" data-module="enrollment" data-id="{{ $usersVoucher->id }}">Enrollments</a></td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="usersVoucher" data-module="voucher" data-id="{{ $usersVoucher->id }}">Voucher</a>
				</td>
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="usersVoucher" data-module="user" data-id="{{ $usersVoucher->id }}">User</a>
				</td>
								
								<td>{{ $usersVoucher->valid_till }}</td>
								<td>{{ $usersVoucher->is_used }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
