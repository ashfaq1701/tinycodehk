@extends('commons.crud')

@section('crud-title')
	Add UsersVoucher
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="usersVouchers">
		
						
				<div class="row" id="rowa1PLX"><div id="col-GVrzx" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="valid_till">Valid Till</label>  
  		        	<input class="form_datetime form-control" id="valid_till" name="valid_till" placeholder="Valid Till" type="text" value="{{ empty($usersVoucher) ? '' : $usersVoucher->valid_till }}" readonly>
          		</div>
  	</div>
</div></div><div id="col-ccZgo" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="is_used">Is Used</label>  
  		                <input type="text" id="is_used" class="form-control control-form" name="is_used" placeholder="Is Used" value="{{ empty($usersVoucher) ? '' : $usersVoucher->is_used }}" />
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowZkDpq"><div id="col-XklyW" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Enrollments	</div>
	<div class="panel-body relationship-panel" data-model="Enrollment" data-variable-name="enrollments" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="enrollments" data-type="many">
						<option value="0">Select</option>
						@foreach($allenrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($usersVoucher))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="enrollments">
						<option value="">Select</option>
						@foreach($usersVoucher->enrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="enrollments" value="{{ $enrollmentsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  		                <input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" maxlength="15"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Registration No</label>  
  		  		                <input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Is Paid</label>  
  		  		                <input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" />
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Total Payment</label>  
  		  		            <input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" />
              		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="enrollments" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-4d2Gl" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Voucher	</div>
	<div class="panel-body relationship-panel" data-model="Voucher" data-variable-name="voucher" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="vouchers" data-type="one">
						<option value="0">Select</option>
						@foreach($allvouchers as $voucher)
						<option value="{{ $voucher->id }}">{{ $voucher->voucher_code }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="voucher" value="{{ (!empty($usersVoucher)) ? (!empty($usersVoucher->voucher) ? $usersVoucher->voucher->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Voucher Code</label>  
  		  						<input type="text" data-field="voucher_code" class="form-control control-form" placeholder="Voucher Code" value="{{ empty($usersVoucher) ? '' : (empty($usersVoucher->voucher) ? '' : $usersVoucher->voucher->voucher_code) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Voucher Type</label>  
  		  						<input type="text" data-field="voucher_type" class="form-control control-form" placeholder="Voucher Type" value="{{ empty($usersVoucher) ? '' : (empty($usersVoucher->voucher) ? '' : $usersVoucher->voucher->voucher_type) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Percent Discount</label>  
  		  		        	<input data-field="percent_discount" class="form-control control-form" type="number" placeholder="Percent Discount" value="{{ empty($usersVoucher) ? '' : (empty($usersVoucher->voucher) ? '' : $usersVoucher->voucher->percent_discount) }}" />
        	  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Flat Discount</label>  
  		  		        	<input data-field="flat_discount" class="form-control control-form" type="number" placeholder="Flat Discount" value="{{ empty($usersVoucher) ? '' : (empty($usersVoucher->voucher) ? '' : $usersVoucher->voucher->flat_discount) }}" />
        	  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="voucher" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowJlq9x"><div id="col-gzmAr" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		User	</div>
	<div class="panel-body relationship-panel" data-model="User" data-variable-name="user" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="users" data-type="one">
						<option value="0">Select</option>
						@foreach($allusers as $user)
						<option value="{{ $user->id }}">{{ $user->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="user" value="{{ (!empty($usersVoucher)) ? (!empty($usersVoucher->user) ? $usersVoucher->user->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		       	<span class="required"> *</span>
    	  		            	<textarea data-field="name" class="form-control control-form" rows="4" placeholder="Name">{{ empty($usersVoucher) ? '' : (empty($usersVoucher->user) ? '' : $usersVoucher->user->name) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Email</label>  
  		       	<span class="required"> *</span>
    	  		            	<textarea data-field="email" class="form-control control-form" rows="4" placeholder="Email">{{ empty($usersVoucher) ? '' : (empty($usersVoucher->user) ? '' : $usersVoucher->user->email) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Stripe Id</label>  
  		  						<input type="text" data-field="stripe_id" class="form-control control-form" placeholder="Stripe Id" value="{{ empty($usersVoucher) ? '' : (empty($usersVoucher->user) ? '' : $usersVoucher->user->stripe_id) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Card Brand</label>  
  		  						<input type="text" data-field="card_brand" class="form-control control-form" placeholder="Card Brand" value="{{ empty($usersVoucher) ? '' : (empty($usersVoucher->user) ? '' : $usersVoucher->user->card_brand) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Card Last 4</label>  
  		  						<input type="text" data-field="card_last_4" class="form-control control-form" placeholder="Card Last 4" value="{{ empty($usersVoucher) ? '' : (empty($usersVoucher->user) ? '' : $usersVoucher->user->card_last_4) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Trial Ends At</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="trial_ends_at" placeholder="Trial Ends At" type="text" value="{{ empty($usersVoucher) ? '' : (empty($usersVoucher->user) ? '' : $usersVoucher->user->trial_ends_at) }}" readonly>
          		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="user" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-SWBuQ" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($usersVoucher) ? 0 : $usersVoucher->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/usersVouchersForm.js"></script>
@endpush