<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="usersVoucher" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allusersVouchers as $usersVoucher)
			<option value="{{ $usersVoucher->id }}">{{ $usersVoucher->valid_till }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'enrollment')
			<th>Enrollments			</th>
			@endif
						@if($origin != 'voucher')
			<th>Voucher			</th>
			@endif
						@if($origin != 'user')
			<th>User			</th>
			@endif
						<th>Valid Till			</th>
						<th>Is Used			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($usersVouchers as $usersVoucher)
		<tr>
						<td>
				<a href="/modules/usersVouchers/{{ $usersVoucher->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/usersVouchers/{{ $usersVoucher->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'enrollment')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="usersVoucher" data-module="enrollment" data-id="{{ $usersVoucher->id }}">Enrollments</a>
			</td>
			@endif
							
						@if($origin != 'voucher')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="usersVoucher" data-module="voucher" data-id="{{ $usersVoucher->id }}">Voucher</a>
			</td>
			@endif
						@if($origin != 'user')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="usersVoucher" data-module="user" data-id="{{ $usersVoucher->id }}">User</a>
			</td>
			@endif
							
						<td>{{ $usersVoucher->valid_till }}</td>
						<td>{{ $usersVoucher->is_used }}</td>
					</tr>
		@endforeach
	</tbody>
</table>