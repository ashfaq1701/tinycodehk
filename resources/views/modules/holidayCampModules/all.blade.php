@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/holidayCampModules/add">Add HolidayCampModule</a>
@endsection

@section('crud-title')
	HolidayCampModules
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>HolidayCampCourse</th>
								<th>Location</th>
								<th>Module Code</th>
								<th>From Date</th>
								<th>To Date</th>
								<th>From Time</th>
								<th>To Time</th>
								<th>Regular Price</th>
							</tr>
		</thead>
		<tbody>
			@foreach($holidayCampModules as $holidayCampModule)
			<tr>
								<td>
					<a href="/modules/holidayCampModules/{{ $holidayCampModule->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/holidayCampModules/{{ $holidayCampModule->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="holidayCampModule" data-module="holidayCampCourse" data-id="{{ $holidayCampModule->id }}">HolidayCampCourse</a>
				</td>
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="holidayCampModule" data-module="location" data-id="{{ $holidayCampModule->id }}">Location</a>
				</td>
								
								<td>{{ $holidayCampModule->module_code }}</td>
								<td>{{ $holidayCampModule->from_date }}</td>
								<td>{{ $holidayCampModule->to_date }}</td>
								<td>{{ $holidayCampModule->from_time }}</td>
								<td>{{ $holidayCampModule->to_time }}</td>
								<td>{{ $holidayCampModule->regular_price }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
