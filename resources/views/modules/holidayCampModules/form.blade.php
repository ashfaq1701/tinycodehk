@extends('commons.crud')

@section('crud-title')
	Add HolidayCampModule
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="holidayCampModules">
		
						
				<div class="row" id="rowcvhhO"><div id="col-efgt3" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="module_code">Module Code</label>  
  		                <input type="text" id="module_code" class="form-control control-form" name="module_code" placeholder="Module Code" value="{{ empty($holidayCampModule) ? '' : $holidayCampModule->module_code }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div><div id="col-RLnzU" class="col-md-6"></div></div><div class="row" id="rowrM7we"><div id="col-IwBkA" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="from_date">From Date</label>  
  		        	<input class="form_datetime form-control" id="from_date" name="from_date" placeholder="From Date" type="text" value="{{ empty($holidayCampModule) ? '' : $holidayCampModule->from_date }}" readonly>
          		</div>
  	</div>
</div></div><div id="col-BRPsx" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="to_date">To Date</label>  
  		        	<input class="form_datetime form-control" id="to_date" name="to_date" placeholder="To Date" type="text" value="{{ empty($holidayCampModule) ? '' : $holidayCampModule->to_date }}" readonly>
          		</div>
  	</div>
</div></div></div><div class="row" id="rowXbMfo"><div id="col-uTYOu" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="from_time">From Time</label>  
  		                <input type="text" id="from_time" class="form-control control-form" name="from_time" placeholder="From Time" value="{{ empty($holidayCampModule) ? '' : $holidayCampModule->from_time }}" maxlength="20"/>
                  		</div>
  	</div>
</div></div><div id="col-Gzp92" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="to_time">To Time</label>  
  		                <input type="text" id="to_time" class="form-control control-form" name="to_time" placeholder="To Time" value="{{ empty($holidayCampModule) ? '' : $holidayCampModule->to_time }}" maxlength="20"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowDQOzO"><div id="col-KNZGL" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="regular_price">Regular Price</label>  
  		                <input id="regular_price" class="form-control control-form" type="number" name="regular_price" placeholder="Regular Price" value="{{ empty($holidayCampModule) ? '' : $holidayCampModule->regular_price }}" />
              		</div>
  	</div>
</div></div><div id="col-NmkCu" class="col-md-6"></div></div><div class="row" id="rowDgIxu"><div id="col-KszIo" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourse	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourse" data-variable-name="holidayCampCourse" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="holidayCampCourses" data-type="one">
						<option value="0">Select</option>
						@foreach($allholidayCampCourses as $holidayCampCourse)
						<option value="{{ $holidayCampCourse->id }}">{{ $holidayCampCourse->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourse" value="{{ (!empty($holidayCampModule)) ? (!empty($holidayCampModule->holidayCampCourse) ? $holidayCampModule->holidayCampCourse->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->holidayCampCourse) ? '' : $holidayCampModule->holidayCampCourse->name) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Icon</label>  
  		  						<input type="text" data-field="program_icon" class="form-control control-form" placeholder="Program Icon" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->holidayCampCourse) ? '' : $holidayCampModule->holidayCampCourse->program_icon) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Cover Image</label>  
  		  						<input type="text" data-field="program_cover_image" class="form-control control-form" placeholder="Program Cover Image" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->holidayCampCourse) ? '' : $holidayCampModule->holidayCampCourse->program_cover_image) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Program Video Url</label>  
  		  						<input type="text" data-field="program_video_url" class="form-control control-form" placeholder="Program Video Url" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->holidayCampCourse) ? '' : $holidayCampModule->holidayCampCourse->program_video_url) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Language</label>  
  		  						<input type="text" data-field="language" class="form-control control-form" placeholder="Language" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->holidayCampCourse) ? '' : $holidayCampModule->holidayCampCourse->language) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->holidayCampCourse) ? '' : $holidayCampModule->holidayCampCourse->description) }}</textarea>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourse" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div>

<div id="col-mQAZm" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Location	</div>
	<div class="panel-body relationship-panel" data-model="Location" data-variable-name="location" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="locations" data-type="one">
						<option value="0">Select</option>
						@foreach($alllocations as $location)
						<option value="{{ $location->id }}">{{ $location->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="location" value="{{ (!empty($holidayCampModule)) ? (!empty($holidayCampModule->location) ? $holidayCampModule->location->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->location) ? '' : $holidayCampModule->location->name) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Address Line 1</label>  
  		  						<input type="text" data-field="address_line_1" class="form-control control-form" placeholder="Address Line 1" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->location) ? '' : $holidayCampModule->location->address_line_1) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Address Line 2</label>  
  		  						<input type="text" data-field="address_line_2" class="form-control control-form" placeholder="Address Line 2" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->location) ? '' : $holidayCampModule->location->address_line_2) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">City</label>  
  		  						<input type="text" data-field="city" class="form-control control-form" placeholder="City" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->location) ? '' : $holidayCampModule->location->city) }}" maxlength="50"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">State</label>  
  		  						<input type="text" data-field="state" class="form-control control-form" placeholder="State" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->location) ? '' : $holidayCampModule->location->state) }}" maxlength="50"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Country</label>  
  		  						<input type="text" data-field="country" class="form-control control-form" placeholder="Country" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->location) ? '' : $holidayCampModule->location->country) }}" maxlength="50"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Mobile</label>  
  		  						<input type="text" data-field="mobile" class="form-control control-form" placeholder="Mobile" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->location) ? '' : $holidayCampModule->location->mobile) }}" maxlength="30"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Email</label>  
  		  						<input type="text" data-field="email" class="form-control control-form" placeholder="Email" value="{{ empty($holidayCampModule) ? '' : (empty($holidayCampModule->location) ? '' : $holidayCampModule->location->email) }}" maxlength="60"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="location" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div>

<div class="row" id="rowMSTr1">
	<div id="col-mtREY" class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				Enrollments
			</div>
			<div class="panel-body relationship-panel" data-model="Enrollment"  data-variable-name="enrollments" data-type="many">
				<div class="well">
					<div class="row">
						<div class="col-md-8">
							<select class="form-control" data-module="enrollments" data-type="many">
								<option value="0">Select</option>
								@foreach($allEnrollments as $enrollment)
								<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-2">
							<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
						</div>
					</div>
				</div>
				@if(!empty($holidayCampModule))
				<div class="well">
					<div class="row">
						<div class="col-md-12">
							<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="enrollments">
								<option value="">Select</option>
								@foreach($holidayCampModule->enrollments as $enrollment)
								<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				@endif
				<div class="row">
					<input type="hidden" class="related-entry" name="enrollments" value="{{ $enrollmentsValue or '' }}"/>
					<input type="hidden" class="active-entry" value=""/>
					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Course Type</label>  
  		  		                <select class="form-control" data-field="course_type" class="form-control control-form">
  		  		                	<option value="">Select</option>
  		  		                	<option value="inHome">In Home</option>
  		  		                	<option value="regular">Regular</option>
  		  		                	<option value="holidayCamp">Holiday Camp</option>
  		  		                </select>
                  			</div>
  						</div>
  					</div>
  					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Registration No</label>  
  		  		                <input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" maxlength="45"/>
                  			</div>
  						</div>
					</div>
					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Is Paid</label>  
  		  		                <input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" />
                  			</div>
  						</div>
					</div>
					<div class="form-group">
						<div class="row">
  							<div class="col-sm-12 col-md-12">
  								<label class="control-label">Total Payment</label>  
  		  		            	<input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" />
              				</div>
  						</div>
					</div>
					<div class="col-md-12">
						<button class="btn btn-success" data-module="enrollments" onclick="localSaveManyRelationship(event)">Save</button>
						<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
				
				<input type="hidden" name="primary" id="primary" value="{{ empty($holidayCampModule) ? 0 : $holidayCampModule->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/holidayCampModulesForm.js"></script>
@endpush