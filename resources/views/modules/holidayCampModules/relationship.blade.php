<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="holidayCampModule" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allholidayCampModules as $holidayCampModule)
			<option value="{{ $holidayCampModule->id }}">{{ $holidayCampModule->module_code }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'holidayCampCourse')
			<th>HolidayCampCourse			</th>
			@endif
						@if($origin != 'location')
			<th>Location			</th>
			@endif
						<th>Module Code			</th>
						<th>From Date			</th>
						<th>To Date			</th>
						<th>From Time			</th>
						<th>To Time			</th>
						<th>Regular Price			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($holidayCampModules as $holidayCampModule)
		<tr>
						<td>
				<a href="/modules/holidayCampModules/{{ $holidayCampModule->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/holidayCampModules/{{ $holidayCampModule->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'holidayCampCourse')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="holidayCampModule" data-module="holidayCampCourse" data-id="{{ $holidayCampModule->id }}">HolidayCampCourse</a>
			</td>
			@endif
						@if($origin != 'location')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="holidayCampModule" data-module="location" data-id="{{ $holidayCampModule->id }}">Location</a>
			</td>
			@endif
							
						<td>{{ $holidayCampModule->module_code }}</td>
						<td>{{ $holidayCampModule->from_date }}</td>
						<td>{{ $holidayCampModule->to_date }}</td>
						<td>{{ $holidayCampModule->from_time }}</td>
						<td>{{ $holidayCampModule->to_time }}</td>
						<td>{{ $holidayCampModule->regular_price }}</td>
					</tr>
		@endforeach
	</tbody>
</table>