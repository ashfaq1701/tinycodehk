<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="enrollment" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allenrollments as $enrollment)
			<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'attendence')
			<th>Attendences			</th>
			@endif
						@if($origin != 'examScore')
			<th>ExamScores			</th>
			@endif
						@if($origin != 'payment')
			<th>Payments			</th>
			@endif
						@if($origin != 'testimonial')
			<th>Testimonials			</th>
			@endif
						@if($origin != 'programClass')
			<th>ProgramClasses			</th>
			@endif
						@if($origin != 'exam')
			<th>Exams			</th>
			@endif
						@if($origin != 'usersVoucher')
			<th>UsersVoucher			</th>
			@endif
						@if($origin != 'user')
			<th>User			</th>
			@endif
						<th>Course Type			</th>
						<th>Registration No			</th>
						<th>Is Paid			</th>
						<th>Total Payment			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($enrollments as $enrollment)
		<tr>
						<td>
				<a href="/modules/enrollments/{{ $enrollment->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/enrollments/{{ $enrollment->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'attendence')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="enrollment" data-module="attendence" data-id="{{ $enrollment->id }}">Attendences</a>
			</td>
			@endif
						@if($origin != 'examScore')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="enrollment" data-module="examScore" data-id="{{ $enrollment->id }}">ExamScores</a>
			</td>
			@endif
						@if($origin != 'payment')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="enrollment" data-module="payment" data-id="{{ $enrollment->id }}">Payments</a>
			</td>
			@endif
						@if($origin != 'testimonial')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="enrollment" data-module="testimonial" data-id="{{ $enrollment->id }}">Testimonials</a>
			</td>
			@endif
						@if($origin != 'programClass')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="enrollment" data-module="programClass" data-id="{{ $enrollment->id }}">ProgramClasses</a>
			</td>
			@endif
						@if($origin != 'exam')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="enrollment" data-module="exam" data-id="{{ $enrollment->id }}">Exams</a>
			</td>
			@endif
							
						@if($origin != 'usersVoucher')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="enrollment" data-module="usersVoucher" data-id="{{ $enrollment->id }}">UsersVoucher</a>
			</td>
			@endif
						@if($origin != 'user')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="enrollment" data-module="user" data-id="{{ $enrollment->id }}">User</a>
			</td>
			@endif
							
						<td>{{ $enrollment->course_type }}</td>
						<td>{{ $enrollment->registration_no }}</td>
						<td>{{ $enrollment->is_paid }}</td>
						<td>{{ $enrollment->total_payment }}</td>
					</tr>
		@endforeach
	</tbody>
</table>