@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/enrollments/add">Add Enrollment</a>
@endsection

@section('crud-title')
	Enrollments
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>Attendences</th>
								<th>ExamScores</th>
								<th>Payments</th>
								<th>Testimonials</th>
								<th>ProgramClasses</th>
								<th>Exams</th>
								<th>UsersVoucher</th>
								<th>User</th>
								<th>Course Type</th>
								<th>Registration No</th>
								<th>Is Paid</th>
								<th>Total Payment</th>
							</tr>
		</thead>
		<tbody>
			@foreach($enrollments as $enrollment)
			<tr>
								<td>
					<a href="/modules/enrollments/{{ $enrollment->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/enrollments/{{ $enrollment->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="enrollment" data-module="attendence" data-id="{{ $enrollment->id }}">Attendences</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="enrollment" data-module="examScore" data-id="{{ $enrollment->id }}">ExamScores</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="enrollment" data-module="payment" data-id="{{ $enrollment->id }}">Payments</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="enrollment" data-module="testimonial" data-id="{{ $enrollment->id }}">Testimonials</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="belongsToMany" data-origin="enrollment" data-module="programClass" data-id="{{ $enrollment->id }}">ProgramClasses</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="belongsToMany" data-origin="enrollment" data-module="exam" data-id="{{ $enrollment->id }}">Exams</a></td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="enrollment" data-module="usersVoucher" data-id="{{ $enrollment->id }}">UsersVoucher</a>
				</td>
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="enrollment" data-module="user" data-id="{{ $enrollment->id }}">User</a>
				</td>
								
								<td>{{ $enrollment->course_type }}</td>
								<td>{{ $enrollment->registration_no }}</td>
								<td>{{ $enrollment->is_paid }}</td>
								<td>{{ $enrollment->total_payment }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
