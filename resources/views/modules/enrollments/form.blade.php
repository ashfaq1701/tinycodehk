@extends('commons.crud')

@section('crud-title')
	Add Enrollment
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="enrollments">
		<div class="row" id="rowkK8yv">
			<div id="col-xQtq7" class="col-md-6">
				<div class="form-group">
					<div class="row">
  						<div class="col-sm-12 col-md-12">
  							<label class="control-label" for="course_type">Course Type</label>  
  		    				<input type="text" id="course_type" class="form-control control-form" name="course_type" placeholder="Course Type" value="{{ empty($enrollment) ? '' : $enrollment->course_type }}" maxlength="15"/>
            			</div>
  					</div>
				</div>
			</div>
			<div id="col-UrCQf" class="col-md-6">
				<div class="form-group">
					<div class="row">
  						<div class="col-sm-12 col-md-12">
  							<label class="control-label" for="registration_no">Registration No</label>  
  		                	<input type="text" id="registration_no" class="form-control control-form" name="registration_no" placeholder="Registration No" value="{{ empty($enrollment) ? '' : $enrollment->registration_no }}" maxlength="45"/>
                  		</div>
  					</div>
				</div>
			</div>
		</div>
		<div class="row" id="row1TEKO">
			<div id="col-SY62z" class="col-md-6">
				<div class="form-group">
					<div class="row">
  						<div class="col-sm-12 col-md-12">
  							<label class="control-label" for="is_paid">Is Paid</label>  
  		                	<input type="text" id="is_paid" class="form-control control-form" name="is_paid" placeholder="Is Paid" value="{{ empty($enrollment) ? '' : $enrollment->is_paid }}" />
                  		</div>
  					</div>
				</div>
			</div>
			<div id="col-n19IA" class="col-md-6">
				<div class="form-group">
					<div class="row">
  						<div class="col-sm-12 col-md-12">
  							<label class="control-label" for="total_payment">Total Payment</label>  
  		                	<input id="total_payment" class="form-control control-form" type="number" name="total_payment" placeholder="Total Payment" value="{{ empty($enrollment) ? '' : $enrollment->total_payment }}" />
              			</div>
  					</div>
				</div>
			</div>
		</div>
		<div class="row" id="row4l3yZ">
			<div id="col-nWSNL" class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Attendences
					</div>
					<div class="panel-body relationship-panel" data-model="Attendence" data-variable-name="attendences" data-type="many">
						<div class="well">
							<div class="row">
								<div class="col-md-8">
									<select class="form-control" data-module="attendences" data-type="many">
										<option value="0">Select</option>
										@foreach($allattendences as $attendence)
										<option value="{{ $attendence->id }}">{{ $attendence->attended }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-2">
									<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
								</div>
							</div>
						</div>
						@if(!empty($enrollment))
						<div class="well">
							<div class="row">
								<div class="col-md-12">
									<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="attendences">
										<option value="">Select</option>
										@foreach($enrollment->attendences as $attendence)
										<option value="{{ $attendence->id }}">{{ $attendence->attended }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						@endif
						<div class="row">
							<input type="hidden" class="related-entry" name="attendences" value="{{ $attendencesValue or '' }}"/>
							<input type="hidden" class="active-entry" value=""/>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">
  											Attended
  										</label>  
  		  		                		<input type="text" data-field="attended" class="form-control control-form" placeholder="Attended" />
                  					</div>
  								</div>
							</div>
							<div class="col-md-12">
								<button class="btn btn-success" data-module="attendences" onclick="localSaveManyRelationship(event)">Save</button>
								<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="col-oJ70Q" class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						ExamScores
					</div>
					<div class="panel-body relationship-panel" data-model="ExamScore" data-variable-name="examScores" data-type="many">		
						<div class="well">
							<div class="row">
								<div class="col-md-8">
									<select class="form-control" data-module="examScores" data-type="many">
										<option value="0">Select</option>
										@foreach($allexamScores as $examScore)
										<option value="{{ $examScore->id }}">{{ $examScore->marks }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-2">
									<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
								</div>
							</div>
						</div>
						@if(!empty($enrollment))
						<div class="well">
							<div class="row">
								<div class="col-md-12">
									<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="examScores">
										<option value="">Select</option>
										@foreach($enrollment->examScores as $examScore)
										<option value="{{ $examScore->id }}">{{ $examScore->marks }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						@endif
						<div class="row">
							<input type="hidden" class="related-entry" name="examScores" value="{{ $examScoresValue or '' }}"/>
							<input type="hidden" class="active-entry" value=""/>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Marks</label>  
  		  		            			<input data-field="marks" class="form-control control-form" type="number" placeholder="Marks" />
              						</div>
  								</div>
							</div>
							<div class="col-md-12">
								<button class="btn btn-success" data-module="examScores" onclick="localSaveManyRelationship(event)">Save</button>
								<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="rowMYf37">
			<div id="col-afrXd" class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Payments
					</div>
					<div class="panel-body relationship-panel" data-model="Payment" data-variable-name="payments" data-type="many">		
						<div class="well">
							<div class="row">
								<div class="col-md-8">
									<select class="form-control" data-module="payments" data-type="many">
										<option value="0">Select</option>
										@foreach($allpayments as $payment)
										<option value="{{ $payment->id }}">{{ $payment->amount }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-2">
									<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
								</div>
							</div>
						</div>
						@if(!empty($enrollment))
						<div class="well">
							<div class="row">
								<div class="col-md-12">
									<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="payments">
										<option value="">Select</option>
										@foreach($enrollment->payments as $payment)
										<option value="{{ $payment->id }}">{{ $payment->amount }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						@endif
						<div class="row">
							<input type="hidden" class="related-entry" name="payments" value="{{ $paymentsValue or '' }}"/>
							<input type="hidden" class="active-entry" value=""/>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Amount</label>  
  		  		            			<input data-field="amount" class="form-control control-form" type="number" placeholder="Amount" />
              						</div>
  								</div>
							</div>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Invoice Id</label>  
  		  		                		<input type="text" data-field="invoice_id" class="form-control control-form" placeholder="Invoice Id" maxlength="100"/>
                  					</div>
  								</div>
							</div>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Charge Id</label>  
  		  		                		<input type="text" data-field="charge_id" class="form-control control-form" placeholder="Charge Id" maxlength="100"/>
                  					</div>
  								</div>
							</div>
							<div class="col-md-12">
								<button class="btn btn-success" data-module="payments" onclick="localSaveManyRelationship(event)">Save</button>
								<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="col-nmXI7" class="col-md-6">
				<div class="panel panel-primary">
					<div class="panel-heading">
						Testimonials
					</div>
					<div class="panel-body relationship-panel" data-model="Testimonial" data-variable-name="testimonials" data-type="many">
						<div class="well">
							<div class="row">
								<div class="col-md-8">
									<select class="form-control" data-module="testimonials" data-type="many">
										<option value="0">Select</option>
										@foreach($alltestimonials as $testimonial)
										<option value="{{ $testimonial->id }}">{{ $testimonial->student_name }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-2">
									<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
								</div>
							</div>
						</div>
						@if(!empty($enrollment))
						<div class="well">
							<div class="row">
								<div class="col-md-12">
									<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="testimonials">
										<option value="">Select</option>
										@foreach($enrollment->testimonials as $testimonial)
										<option value="{{ $testimonial->id }}">{{ $testimonial->student_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						@endif
						<div class="row">
							<input type="hidden" class="related-entry" name="testimonials" value="{{ $testimonialsValue or '' }}"/>
							<input type="hidden" class="active-entry" value=""/>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Testimonial</label>  
  		  		                		<textarea data-field="testimonial" class="form-control control-form" rows="4" placeholder="Testimonial"></textarea>
                  					</div>
  								</div>
							</div>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Student Name</label>  
  		  		                		<input type="text" data-field="student_name" class="form-control control-form" placeholder="Student Name" maxlength="100"/>
                  					</div>
  								</div>
							</div>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Class</label>  
  		  		                		<input type="text" data-field="class" class="form-control control-form" placeholder="Class" maxlength="45"/>
                  					</div>
  								</div>
							</div>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">School</label>  
  		  		                		<input type="text" data-field="school" class="form-control control-form" placeholder="School" maxlength="100"/>
                  					</div>
  								</div>
							</div>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Course</label>  
  		  		                		<input type="text" data-field="course" class="form-control control-form" placeholder="Course" maxlength="100"/>
                  					</div>
  								</div>
							</div>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Roll</label>  
  		  		                		<input type="text" data-field="roll" class="form-control control-form" placeholder="Roll" maxlength="10"/>
                  					</div>
  								</div>
							</div>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Term</label>  
  		  		                		<input type="text" data-field="term" class="form-control control-form" placeholder="Term" maxlength="20"/>
                  					</div>
  								</div>
							</div>
							<div class="form-group">
								<div class="row">
  									<div class="col-sm-12 col-md-12">
  										<label class="control-label">Year</label>  
  		  		                		<input type="text" data-field="year" class="form-control control-form" placeholder="Year" maxlength="4"/>
                  					</div>
  								</div>
								</div>
								<div class="col-md-12">
									<button class="btn btn-success" data-module="testimonials" onclick="localSaveManyRelationship(event)">Save</button>
									<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row" id="rowQKuHB">
				<div id="col-2rjVs" class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							UsersVoucher
						</div>
						<div class="panel-body relationship-panel" data-model="UsersVoucher" data-variable-name="usersVoucher" data-type="one">
							<div class="well">
								<div class="row">
									<div class="col-md-4">
										<select class="form-control" data-module="usersVouchers" data-type="one">
											<option value="0">Select</option>
											@foreach($allusersVouchers as $usersVoucher)
											<option value="{{ $usersVoucher->id }}">{{ $usersVoucher->valid_till }}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-2">
										<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
									</div>
								</div>
							</div>
							<div class="row">
								<input type="hidden" class="related-entry" name="usersVoucher" value="{{ (!empty($enrollment)) ? (!empty($enrollment->usersVoucher) ? $enrollment->usersVoucher->id : '') : '' }}"/>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Valid Till</label>  
  		  		        					<input class="form_datetime form-control control-form" data-field="valid_till" placeholder="Valid Till" type="text" value="{{ empty($enrollment) ? '' : (empty($enrollment->usersVoucher) ? '' : $enrollment->usersVoucher->valid_till) }}" readonly>
          								</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Is Used</label>  
  		  									<input type="text" data-field="is_used" class="form-control control-form" placeholder="Is Used" value="{{ empty($enrollment) ? '' : (empty($enrollment->usersVoucher) ? '' : $enrollment->usersVoucher->is_used) }}" />
				  						</div>
  									</div>
								</div>			
								<div class="col-md-12">
									<button class="btn btn-success" data-module="usersVoucher" onclick="localSaveManyRelationship(event)">Save</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="col-9wjKi" class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							User	
						</div>
						<div class="panel-body relationship-panel" data-model="User" data-variable-name="user" data-type="one">
							<div class="well">
								<div class="row">
									<div class="col-md-4">
										<select class="form-control" data-module="users" data-type="one">
											<option value="0">Select</option>
											@foreach($allusers as $user)
											<option value="{{ $user->id }}">{{ $user->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-2">
										<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
									</div>
								</div>
							</div>
							<div class="row">
								<input type="hidden" class="related-entry" name="user" value="{{ (!empty($enrollment)) ? (!empty($enrollment->user) ? $enrollment->user->id : '') : '' }}"/>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Name</label>  
  		       								<span class="required"> *</span>
    	  		            				<textarea data-field="name" class="form-control control-form" rows="4" placeholder="Name">{{ empty($enrollment) ? '' : (empty($enrollment->user) ? '' : $enrollment->user->name) }}</textarea>
				  						</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Email</label>  
  		       								<span class="required"> *</span>
    	  		            				<textarea data-field="email" class="form-control control-form" rows="4" placeholder="Email">{{ empty($enrollment) ? '' : (empty($enrollment->user) ? '' : $enrollment->user->email) }}</textarea>
				  						</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Stripe Id</label>  
  		  									<input type="text" data-field="stripe_id" class="form-control control-form" placeholder="Stripe Id" value="{{ empty($enrollment) ? '' : (empty($enrollment->user) ? '' : $enrollment->user->stripe_id) }}" maxlength="100"/>
				  						</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Card Brand</label>  
  		  									<input type="text" data-field="card_brand" class="form-control control-form" placeholder="Card Brand" value="{{ empty($enrollment) ? '' : (empty($enrollment->user) ? '' : $enrollment->user->card_brand) }}" maxlength="45"/>
				  						</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Card Last 4</label>  
  		  									<input type="text" data-field="card_last_4" class="form-control control-form" placeholder="Card Last 4" value="{{ empty($enrollment) ? '' : (empty($enrollment->user) ? '' : $enrollment->user->card_last_4) }}" maxlength="45"/>
				  						</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Trial Ends At</label>  
  		  		        					<input class="form_datetime form-control control-form" data-field="trial_ends_at" placeholder="Trial Ends At" type="text" value="{{ empty($enrollment) ? '' : (empty($enrollment->user) ? '' : $enrollment->user->trial_ends_at) }}" readonly>
          								</div>
  									</div>
								</div>			
								<div class="col-md-12">
									<button class="btn btn-success" data-module="user" onclick="localSaveManyRelationship(event)">Save</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row" id="rowYNHV8">
				<div id="col-4EdS2" class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							ProgramClasses
						</div>
						<div class="panel-body relationship-panel" data-model="ProgramClass" data-variable-name="programClasses" data-type="many">			
							<div class="well">
								<div class="row">
									<div class="col-md-8">
										<select class="form-control" data-module="programClasses" data-type="many">
											<option value="0">Select</option>
											@foreach($allprogramClasses as $programClass)
											<option value="{{ $programClass->id }}">{{ $programClass->title }}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-2">
										<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
									</div>
								</div>
							</div>
							@if(!empty($enrollment))
							<div class="well">
								<div class="row">
									<div class="col-md-12">
										<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="programClasses">
											<option value="">Select</option>
											@foreach($enrollment->programClasses as $programClass)
											<option value="{{ $programClass->id }}">{{ $programClass->title }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							@endif
							<div class="row">
								<input type="hidden" class="related-entry" name="programClasses" value="{{ $programClassesValue or '' }}"/>
								<input type="hidden" class="active-entry" value=""/>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Course Type</label>  
  		  		                			<input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" maxlength="45"/>
                  						</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">From Time</label>  
  		  		        					<input class="form_datetime form-control control-form" data-field="from_time" placeholder="From Time" type="text" readonly>
          								</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">To Time</label>  
  		  		        					<input class="form_datetime form-control control-form" data-field="to_time" placeholder="To Time" type="text" readonly>
          								</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Title</label>  
  		  		                			<input type="text" data-field="title" class="form-control control-form" placeholder="Title" maxlength="55"/>
                  						</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Description</label>  
  		  		                			<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description"></textarea>
                  						</div>
  									</div>
								</div>			
								<div class="col-md-12">
									<button class="btn btn-success" data-module="programClasses" onclick="localSaveManyRelationship(event)">Save</button>
									<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="col-flmXv" class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading">
							Exams
						</div>
						<div class="panel-body relationship-panel" data-model="Exam" data-variable-name="exams" data-type="many">		
							<div class="well">
								<div class="row">
									<div class="col-md-8">
										<select class="form-control" data-module="exams" data-type="many">
											<option value="0">Select</option>
											@foreach($allexams as $exam)
											<option value="{{ $exam->id }}">{{ $exam->title }}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-2">
										<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
									</div>
								</div>
							</div>
							@if(!empty($enrollment))
							<div class="well">
								<div class="row">
									<div class="col-md-12">
										<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="exams">
											<option value="">Select</option>
											@foreach($enrollment->exams as $exam)
											<option value="{{ $exam->id }}">{{ $exam->title }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							@endif
							<div class="row">
								<input type="hidden" class="related-entry" name="exams" value="{{ $examsValue or '' }}"/>
								<input type="hidden" class="active-entry" value=""/>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Title</label>  
  		  		               				<input type="text" data-field="title" class="form-control control-form" placeholder="Title" maxlength="200"/>
                  						</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Exam Date Time</label>  
  		  		        					<input class="form_datetime form-control control-form" data-field="exam_date_time" placeholder="Exam Date Time" type="text" readonly>
          								</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Description</label>  
  		  		                			<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description"></textarea>
                  						</div>
  									</div>
								</div>
								<div class="form-group">
									<div class="row">
  										<div class="col-sm-12 col-md-12">
  											<label class="control-label">Total Marks</label>  
  		  		            				<input data-field="total_marks" class="form-control control-form" type="number" placeholder="Total Marks" />
              							</div>
  									</div>
								</div>			
								<div class="col-md-12">
									<button class="btn btn-success" data-module="exams" onclick="localSaveManyRelationship(event)">Save</button>
									<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>				
			<input type="hidden" name="primary" id="primary" value="{{ empty($enrollment) ? 0 : $enrollment->id }}"/>
			{{ csrf_field() }}
			<div class="form-group">
  				<div class="col-md-4 col-md-offset-4">
    				<button id="save" type="submit" class="btn btn-primary">Save</button>
  				</div>
			</div>
		</form>
	@endsection

	@push('scripts')
		<script type="text/javascript" src="/js/modules/enrollmentsForm.js"></script>
	@endpush