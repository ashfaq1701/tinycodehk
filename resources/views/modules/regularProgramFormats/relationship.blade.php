<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="regularProgramFormat" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allregularProgramFormats as $regularProgramFormat)
			<option value="{{ $regularProgramFormat->id }}">{{ $regularProgramFormat->title }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'regularProgram')
			<th>RegularProgram			</th>
			@endif
						<th>Title			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($regularProgramFormats as $regularProgramFormat)
		<tr>
						<td>
				<a href="/modules/regularProgramFormats/{{ $regularProgramFormat->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/regularProgramFormats/{{ $regularProgramFormat->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'regularProgram')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="regularProgramFormat" data-module="regularProgram" data-id="{{ $regularProgramFormat->id }}">RegularProgram</a>
			</td>
			@endif
							
						<td>{{ $regularProgramFormat->title }}</td>
					</tr>
		@endforeach
	</tbody>
</table>