@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/regularProgramFormats/add">Add RegularProgramFormat</a>
@endsection

@section('crud-title')
	RegularProgramFormats
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>RegularProgram</th>
								<th>Title</th>
							</tr>
		</thead>
		<tbody>
			@foreach($regularProgramFormats as $regularProgramFormat)
			<tr>
								<td>
					<a href="/modules/regularProgramFormats/{{ $regularProgramFormat->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/regularProgramFormats/{{ $regularProgramFormat->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="regularProgramFormat" data-module="regularProgram" data-id="{{ $regularProgramFormat->id }}">RegularProgram</a>
				</td>
								
								<td>{{ $regularProgramFormat->title }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
