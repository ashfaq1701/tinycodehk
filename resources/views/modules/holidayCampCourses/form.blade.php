@extends('commons.crud')

@section('crud-title')
	Add HolidayCampCourse
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="holidayCampCourses">
		
						
				<div class="row" id="rowItiVO"><div id="col-MrCO6" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="name">Name</label>  
  		                <input type="text" id="name" class="form-control control-form" name="name" placeholder="Name" value="{{ empty($holidayCampCourse) ? '' : $holidayCampCourse->name }}" maxlength="100"/>
                  		</div>
  	</div>
</div></div><div id="col-T442h" class="col-md-6"></div></div><div class="row" id="rowsE7sL"><div id="col-YZKDg" class="col-md-6"><div class="form-group">
	<div class="row">
		<input type="hidden" name="program_icon" id="program_icon" class="control-form" value="{{ empty($holidayCampCourse) ? '' : $holidayCampCourse->program_icon }}"/>
  		<div class="col-sm-12 col-md-12">
  			<label class="control-label" for="program_icon">Program Icon</label>  
  			<input type="file" id="program_icon_file_ip" class="form-control file_ip" placeholder="Program Icon"/>
        </div>
  	</div>
</div></div><div id="col-pDy6P" class="col-md-6"><div class="form-group">
	<div class="row">
		<input type="hidden" name="program_cover_image" class="control-form" id="program_cover_image" value="{{ empty($holidayCampCourse) ? '' : $holidayCampCourse->program_cover_image }}"/>
  		<div class="col-sm-12 col-md-12">
  			<label class="control-label" for="program_cover_image">Program Cover Image</label>  
  			<input type="file" id="program_cover_image_file_ip" class="form-control file_ip" placeholder="Program Cover Image"/>
        </div>
  	</div>
</div></div></div><div class="row" id="rowor8z7"><div id="col-k203O" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="program_video_url">Program Video Url</label>  
  		                <input type="text" id="program_video_url" class="form-control control-form" name="program_video_url" placeholder="Program Video Url" value="{{ empty($holidayCampCourse) ? '' : $holidayCampCourse->program_video_url }}" maxlength="200"/>
                  		</div>
  	</div>
</div></div><div id="col-YaiAl" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="language">Language</label>  
  		                <input type="text" id="language" class="form-control control-form" name="language" placeholder="Language" value="{{ empty($holidayCampCourse) ? '' : $holidayCampCourse->language }}" maxlength="45"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="row5qHY2"><div id="col-e6POy" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="description">Description</label>  
  		                <textarea id="description" class="form-control control-form" rows="4" name="description" placeholder="Description">{{ empty($holidayCampCourse) ? '' : $holidayCampCourse->description }}</textarea>
                  		</div>
  	</div>
</div></div><div id="col-3yPu2" class="col-md-6"></div></div>

<div class="row" id="rowcitJp"><div id="col-B8lSk" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourseBringings	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourseBringing" data-variable-name="holidayCampCourseBringings" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="holidayCampCourseBringings" data-type="many">
						<option value="0">Select</option>
						@foreach($allholidayCampCourseBringings as $holidayCampCourseBringing)
						<option value="{{ $holidayCampCourseBringing->id }}">{{ $holidayCampCourseBringing->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($holidayCampCourse))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="holidayCampCourseBringings">
						<option value="">Select</option>
						@foreach($holidayCampCourse->holidayCampCourseBringings as $holidayCampCourseBringing)
						<option value="{{ $holidayCampCourseBringing->id }}">{{ $holidayCampCourseBringing->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourseBringings" value="{{ $holidayCampCourseBringingsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  		                <input type="text" data-field="name" class="form-control control-form" placeholder="Name" maxlength="100"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourseBringings" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-hznUK" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourseFeatures	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourseFeature" data-variable-name="holidayCampCourseFeatures" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="holidayCampCourseFeatures" data-type="many">
						<option value="0">Select</option>
						@foreach($allholidayCampCourseFeatures as $holidayCampCourseFeature)
						<option value="{{ $holidayCampCourseFeature->id }}">{{ $holidayCampCourseFeature->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($holidayCampCourse))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="holidayCampCourseFeatures">
						<option value="">Select</option>
						@foreach($holidayCampCourse->holidayCampCourseFeatures as $holidayCampCourseFeature)
						<option value="{{ $holidayCampCourseFeature->id }}">{{ $holidayCampCourseFeature->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourseFeatures" value="{{ $holidayCampCourseFeaturesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  		                <input type="text" data-field="name" class="form-control control-form" placeholder="Name" maxlength="200"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourseFeatures" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowlJT6J"><div id="col-muzIX" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampCourseHighlights	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampCourseHighlight" data-variable-name="holidayCampCourseHighlights" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="holidayCampCourseHighlights" data-type="many">
						<option value="0">Select</option>
						@foreach($allholidayCampCourseHighlights as $holidayCampCourseHighlight)
						<option value="{{ $holidayCampCourseHighlight->id }}">{{ $holidayCampCourseHighlight->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($holidayCampCourse))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="holidayCampCourseHighlights">
						<option value="">Select</option>
						@foreach($holidayCampCourse->holidayCampCourseHighlights as $holidayCampCourseHighlight)
						<option value="{{ $holidayCampCourseHighlight->id }}">{{ $holidayCampCourseHighlight->title }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampCourseHighlights" value="{{ $holidayCampCourseHighlightsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  		                <textarea data-field="title" class="form-control control-form" rows="4" placeholder="Title"></textarea>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		                <textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description"></textarea>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampCourseHighlights" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-27A9U" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCampModules	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCampModule" data-variable-name="holidayCampModules" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="holidayCampModules" data-type="many">
						<option value="0">Select</option>
						@foreach($allholidayCampModules as $holidayCampModule)
						<option value="{{ $holidayCampModule->id }}">{{ $holidayCampModule->module_code }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($holidayCampCourse))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="holidayCampModules">
						<option value="">Select</option>
						@foreach($holidayCampCourse->holidayCampModules as $holidayCampModule)
						<option value="{{ $holidayCampModule->id }}">{{ $holidayCampModule->module_code }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCampModules" value="{{ $holidayCampModulesValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Module Code</label>  
  		  		                <input type="text" data-field="module_code" class="form-control control-form" placeholder="Module Code" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Date</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="from_date" placeholder="From Date" type="text" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Date</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="to_date" placeholder="To Date" type="text" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Time</label>  
  		  		                <input type="text" data-field="from_time" class="form-control control-form" placeholder="From Time" maxlength="20"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Time</label>  
  		  		                <input type="text" data-field="to_time" class="form-control control-form" placeholder="To Time" maxlength="20"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Regular Price</label>  
  		  		            <input data-field="regular_price" class="form-control control-form" type="number" placeholder="Regular Price" />
              		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCampModules" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowW9AsH"><div id="col-WLsqZ" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		HolidayCamp	</div>
	<div class="panel-body relationship-panel" data-model="HolidayCamp" data-variable-name="holidayCamp" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="holidayCamps" data-type="one">
						<option value="0">Select</option>
						@foreach($allholidayCamps as $holidayCamp)
						<option value="{{ $holidayCamp->id }}">{{ $holidayCamp->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="holidayCamp" value="{{ (!empty($holidayCampCourse)) ? (!empty($holidayCampCourse->holidayCamp) ? $holidayCampCourse->holidayCamp->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($holidayCampCourse) ? '' : (empty($holidayCampCourse->holidayCamp) ? '' : $holidayCampCourse->holidayCamp->name) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($holidayCampCourse) ? '' : (empty($holidayCampCourse->holidayCamp) ? '' : $holidayCampCourse->holidayCamp->description) }}</textarea>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Date</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="from_date" placeholder="From Date" type="text" value="{{ empty($holidayCampCourse) ? '' : (empty($holidayCampCourse->holidayCamp) ? '' : $holidayCampCourse->holidayCamp->from_date) }}" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Date</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="to_date" placeholder="To Date" type="text" value="{{ empty($holidayCampCourse) ? '' : (empty($holidayCampCourse->holidayCamp) ? '' : $holidayCampCourse->holidayCamp->to_date) }}" readonly>
          		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="holidayCamp" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-QT6vA" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		ProgramLevel	</div>
	<div class="panel-body relationship-panel" data-model="ProgramLevel" data-variable-name="programLevel" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="programLevels" data-type="one">
						<option value="0">Select</option>
						@foreach($allprogramLevels as $programLevel)
						<option value="{{ $programLevel->id }}">{{ $programLevel->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="programLevel" value="{{ (!empty($holidayCampCourse)) ? (!empty($holidayCampCourse->programLevel) ? $holidayCampCourse->programLevel->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($holidayCampCourse) ? '' : (empty($holidayCampCourse->programLevel) ? '' : $holidayCampCourse->programLevel->name) }}" maxlength="45"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="programLevel" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowQk3ZN"><div id="col-Vmq3q" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Locations	</div>
	<div class="panel-body relationship-panel" data-model="Location" data-variable-name="locations" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="locations" data-type="many">
						<option value="0">Select</option>
						@foreach($alllocations as $location)
						<option value="{{ $location->id }}">{{ $location->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($holidayCampCourse))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="locations">
						<option value="">Select</option>
						@foreach($holidayCampCourse->locations as $location)
						<option value="{{ $location->id }}">{{ $location->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="locations" value="{{ $locationsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  		                <input type="text" data-field="name" class="form-control control-form" placeholder="Name" maxlength="200"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Address Line 1</label>  
  		  		                <input type="text" data-field="address_line_1" class="form-control control-form" placeholder="Address Line 1" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Address Line 2</label>  
  		  		                <input type="text" data-field="address_line_2" class="form-control control-form" placeholder="Address Line 2" maxlength="100"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">City</label>  
  		  		                <input type="text" data-field="city" class="form-control control-form" placeholder="City" maxlength="50"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">State</label>  
  		  		                <input type="text" data-field="state" class="form-control control-form" placeholder="State" maxlength="50"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Country</label>  
  		  		                <input type="text" data-field="country" class="form-control control-form" placeholder="Country" maxlength="50"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Mobile</label>  
  		  		                <input type="text" data-field="mobile" class="form-control control-form" placeholder="Mobile" maxlength="30"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Email</label>  
  		  		                <input type="text" data-field="email" class="form-control control-form" placeholder="Email" maxlength="60"/>
                  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="locations" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-FMLNS" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($holidayCampCourse) ? 0 : $holidayCampCourse->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/holidayCampCoursesForm.js"></script>
@endpush