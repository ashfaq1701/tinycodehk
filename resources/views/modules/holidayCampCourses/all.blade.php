@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/holidayCampCourses/add">Add HolidayCampCourse</a>
@endsection

@section('crud-title')
	HolidayCampCourses
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>HolidayCampCourseBringings</th>
								<th>HolidayCampCourseFeatures</th>
								<th>HolidayCampCourseHighlights</th>
								<th>HolidayCampModules</th>
								<th>Locations</th>
								<th>HolidayCamp</th>
								<th>ProgramLevel</th>
								<th>Name</th>
								<th>Program Icon</th>
								<th>Program Cover Image</th>
								<th>Program Video Url</th>
								<th>Language</th>
							</tr>
		</thead>
		<tbody>
			@foreach($holidayCampCourses as $holidayCampCourse)
			<tr>
								<td>
					<a href="/modules/holidayCampCourses/{{ $holidayCampCourse->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/holidayCampCourses/{{ $holidayCampCourse->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="holidayCampCourse" data-module="holidayCampCourseBringing" data-id="{{ $holidayCampCourse->id }}">HolidayCampCourseBringings</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="holidayCampCourse" data-module="holidayCampCourseFeature" data-id="{{ $holidayCampCourse->id }}">HolidayCampCourseFeatures</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="holidayCampCourse" data-module="holidayCampCourseHighlight" data-id="{{ $holidayCampCourse->id }}">HolidayCampCourseHighlights</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="holidayCampCourse" data-module="holidayCampModule" data-id="{{ $holidayCampCourse->id }}">HolidayCampModules</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="belongsToMany" data-origin="holidayCampCourse" data-module="location" data-id="{{ $holidayCampCourse->id }}">Locations</a></td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="holidayCampCourse" data-module="holidayCamp" data-id="{{ $holidayCampCourse->id }}">HolidayCamp</a>
				</td>
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="holidayCampCourse" data-module="programLevel" data-id="{{ $holidayCampCourse->id }}">ProgramLevel</a>
				</td>
								
								<td>{{ $holidayCampCourse->name }}</td>
								<td>{{ $holidayCampCourse->program_icon }}</td>
								<td>{{ $holidayCampCourse->program_cover_image }}</td>
								<td>{{ $holidayCampCourse->program_video_url }}</td>
								<td>{{ $holidayCampCourse->language }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
