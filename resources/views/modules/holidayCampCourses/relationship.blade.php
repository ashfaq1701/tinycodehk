<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="holidayCampCourse" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allholidayCampCourses as $holidayCampCourse)
			<option value="{{ $holidayCampCourse->id }}">{{ $holidayCampCourse->name }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'holidayCampCourseBringing')
			<th>HolidayCampCourseBringings			</th>
			@endif
						@if($origin != 'holidayCampCourseFeature')
			<th>HolidayCampCourseFeatures			</th>
			@endif
						@if($origin != 'holidayCampCourseHighlight')
			<th>HolidayCampCourseHighlights			</th>
			@endif
						@if($origin != 'holidayCampModule')
			<th>HolidayCampModules			</th>
			@endif
						@if($origin != 'location')
			<th>Locations			</th>
			@endif
						@if($origin != 'holidayCamp')
			<th>HolidayCamp			</th>
			@endif
						@if($origin != 'programLevel')
			<th>ProgramLevel			</th>
			@endif
						<th>Name			</th>
						<th>Program Icon			</th>
						<th>Program Cover Image			</th>
						<th>Program Video Url			</th>
						<th>Language			</th>
						<th>Description			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($holidayCampCourses as $holidayCampCourse)
		<tr>
						<td>
				<a href="/modules/holidayCampCourses/{{ $holidayCampCourse->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/holidayCampCourses/{{ $holidayCampCourse->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'holidayCampCourseBringing')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="holidayCampCourse" data-module="holidayCampCourseBringing" data-id="{{ $holidayCampCourse->id }}">HolidayCampCourseBringings</a>
			</td>
			@endif
						@if($origin != 'holidayCampCourseFeature')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="holidayCampCourse" data-module="holidayCampCourseFeature" data-id="{{ $holidayCampCourse->id }}">HolidayCampCourseFeatures</a>
			</td>
			@endif
						@if($origin != 'holidayCampCourseHighlight')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="holidayCampCourse" data-module="holidayCampCourseHighlight" data-id="{{ $holidayCampCourse->id }}">HolidayCampCourseHighlights</a>
			</td>
			@endif
						@if($origin != 'holidayCampModule')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="holidayCampCourse" data-module="holidayCampModule" data-id="{{ $holidayCampCourse->id }}">HolidayCampModules</a>
			</td>
			@endif
						@if($origin != 'location')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="holidayCampCourse" data-module="location" data-id="{{ $holidayCampCourse->id }}">Locations</a>
			</td>
			@endif
							
						@if($origin != 'holidayCamp')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="holidayCampCourse" data-module="holidayCamp" data-id="{{ $holidayCampCourse->id }}">HolidayCamp</a>
			</td>
			@endif
						@if($origin != 'programLevel')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="holidayCampCourse" data-module="programLevel" data-id="{{ $holidayCampCourse->id }}">ProgramLevel</a>
			</td>
			@endif
							
						<td>{{ $holidayCampCourse->name }}</td>
						<td>{{ $holidayCampCourse->program_icon }}</td>
						<td>{{ $holidayCampCourse->program_cover_image }}</td>
						<td>{{ $holidayCampCourse->program_video_url }}</td>
						<td>{{ $holidayCampCourse->language }}</td>
						<td>{{ $holidayCampCourse->description }}</td>
					</tr>
		@endforeach
	</tbody>
</table>