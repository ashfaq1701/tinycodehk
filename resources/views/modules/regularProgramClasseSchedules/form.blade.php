@extends('commons.crud')

@section('crud-title')
	Add RegularProgramClasseSchedule
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="regularProgramClasseSchedules">
		
						
				<div class="row" id="rowYgpOU"><div id="col-bazKq" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="from_time">From Time</label>  
  		                <input type="text" id="from_time" class="form-control control-form" name="from_time" placeholder="From Time" value="{{ empty($regularProgramClasseSchedule) ? '' : $regularProgramClasseSchedule->from_time }}" maxlength="50"/>
                  		</div>
  	</div>
</div></div><div id="col-hKbAm" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="to_time">To Time</label>  
  		                <input type="text" id="to_time" class="form-control control-form" name="to_time" placeholder="To Time" value="{{ empty($regularProgramClasseSchedule) ? '' : $regularProgramClasseSchedule->to_time }}" maxlength="50"/>
                  		</div>
  	</div>
</div></div></div><div class="row" id="rowcdQLH"><div id="col-pKJYS" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		RegularProgramSemester	</div>
	<div class="panel-body relationship-panel" data-model="RegularProgramSemester" data-variable-name="regularProgramSemester" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="regularProgramSemesters" data-type="one">
						<option value="0">Select</option>
						@foreach($allregularProgramSemesters as $regularProgramSemester)
						<option value="{{ $regularProgramSemester->id }}">{{ $regularProgramSemester->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="regularProgramSemester" value="{{ (!empty($regularProgramClasseSchedule)) ? (!empty($regularProgramClasseSchedule->regularProgramSemester) ? $regularProgramClasseSchedule->regularProgramSemester->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Year</label>  
  		  						<input type="text" data-field="year" class="form-control control-form" placeholder="Year" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->regularProgramSemester) ? '' : $regularProgramClasseSchedule->regularProgramSemester->year) }}" maxlength="4"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->regularProgramSemester) ? '' : $regularProgramClasseSchedule->regularProgramSemester->name) }}" maxlength="10"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Month</label>  
  		  						<input type="text" data-field="from_month" class="form-control control-form" placeholder="From Month" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->regularProgramSemester) ? '' : $regularProgramClasseSchedule->regularProgramSemester->from_month) }}" maxlength="15"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Month</label>  
  		  						<input type="text" data-field="to_month" class="form-control control-form" placeholder="To Month" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->regularProgramSemester) ? '' : $regularProgramClasseSchedule->regularProgramSemester->to_month) }}" maxlength="15"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Duration Weeks</label>  
  		  						<input type="text" data-field="duration_weeks" class="form-control control-form" placeholder="Duration Weeks" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->regularProgramSemester) ? '' : $regularProgramClasseSchedule->regularProgramSemester->duration_weeks) }}" maxlength="20"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Regular Price</label>  
  		  		        	<input data-field="regular_price" class="form-control control-form" type="number" placeholder="Regular Price" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->regularProgramSemester) ? '' : $regularProgramClasseSchedule->regularProgramSemester->regular_price) }}" />
        	  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="regularProgramSemester" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-13AsO" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Location	</div>
	<div class="panel-body relationship-panel" data-model="Location" data-variable-name="location" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="locations" data-type="one">
						<option value="0">Select</option>
						@foreach($alllocations as $location)
						<option value="{{ $location->id }}">{{ $location->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="location" value="{{ (!empty($regularProgramClasseSchedule)) ? (!empty($regularProgramClasseSchedule->location) ? $regularProgramClasseSchedule->location->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->location) ? '' : $regularProgramClasseSchedule->location->name) }}" maxlength="200"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Address Line 1</label>  
  		  						<input type="text" data-field="address_line_1" class="form-control control-form" placeholder="Address Line 1" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->location) ? '' : $regularProgramClasseSchedule->location->address_line_1) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Address Line 2</label>  
  		  						<input type="text" data-field="address_line_2" class="form-control control-form" placeholder="Address Line 2" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->location) ? '' : $regularProgramClasseSchedule->location->address_line_2) }}" maxlength="100"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">City</label>  
  		  						<input type="text" data-field="city" class="form-control control-form" placeholder="City" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->location) ? '' : $regularProgramClasseSchedule->location->city) }}" maxlength="50"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">State</label>  
  		  						<input type="text" data-field="state" class="form-control control-form" placeholder="State" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->location) ? '' : $regularProgramClasseSchedule->location->state) }}" maxlength="50"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Country</label>  
  		  						<input type="text" data-field="country" class="form-control control-form" placeholder="Country" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->location) ? '' : $regularProgramClasseSchedule->location->country) }}" maxlength="50"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Mobile</label>  
  		  						<input type="text" data-field="mobile" class="form-control control-form" placeholder="Mobile" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->location) ? '' : $regularProgramClasseSchedule->location->mobile) }}" maxlength="30"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Email</label>  
  		  						<input type="text" data-field="email" class="form-control control-form" placeholder="Email" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->location) ? '' : $regularProgramClasseSchedule->location->email) }}" maxlength="60"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="location" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowT04aL"><div id="col-43th9" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Weekday	</div>
	<div class="panel-body relationship-panel" data-model="Weekday" data-variable-name="weekday" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="weekdays" data-type="one">
						<option value="0">Select</option>
						@foreach($allweekdays as $weekday)
						<option value="{{ $weekday->id }}">{{ $weekday->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="weekday" value="{{ (!empty($regularProgramClasseSchedule)) ? (!empty($regularProgramClasseSchedule->weekday) ? $regularProgramClasseSchedule->weekday->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Name</label>  
  		  						<input type="text" data-field="name" class="form-control control-form" placeholder="Name" value="{{ empty($regularProgramClasseSchedule) ? '' : (empty($regularProgramClasseSchedule->weekday) ? '' : $regularProgramClasseSchedule->weekday->name) }}" maxlength="45"/>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="weekday" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-nqfwW" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($regularProgramClasseSchedule) ? 0 : $regularProgramClasseSchedule->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/regularProgramClasseSchedulesForm.js"></script>
@endpush