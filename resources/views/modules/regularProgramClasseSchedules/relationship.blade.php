<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="regularProgramClasseSchedule" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allregularProgramClasseSchedules as $regularProgramClasseSchedule)
			<option value="{{ $regularProgramClasseSchedule->id }}">{{ $regularProgramClasseSchedule->from_time }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'regularProgramSemester')
			<th>RegularProgramSemester			</th>
			@endif
						@if($origin != 'location')
			<th>Location			</th>
			@endif
						@if($origin != 'weekday')
			<th>Weekday			</th>
			@endif
						<th>From Time			</th>
						<th>To Time			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($regularProgramClasseSchedules as $regularProgramClasseSchedule)
		<tr>
						<td>
				<a href="/modules/regularProgramClasseSchedules/{{ $regularProgramClasseSchedule->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/regularProgramClasseSchedules/{{ $regularProgramClasseSchedule->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'regularProgramSemester')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="regularProgramClasseSchedule" data-module="regularProgramSemester" data-id="{{ $regularProgramClasseSchedule->id }}">RegularProgramSemester</a>
			</td>
			@endif
						@if($origin != 'location')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="regularProgramClasseSchedule" data-module="location" data-id="{{ $regularProgramClasseSchedule->id }}">Location</a>
			</td>
			@endif
						@if($origin != 'weekday')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="regularProgramClasseSchedule" data-module="weekday" data-id="{{ $regularProgramClasseSchedule->id }}">Weekday</a>
			</td>
			@endif
							
						<td>{{ $regularProgramClasseSchedule->from_time }}</td>
						<td>{{ $regularProgramClasseSchedule->to_time }}</td>
					</tr>
		@endforeach
	</tbody>
</table>