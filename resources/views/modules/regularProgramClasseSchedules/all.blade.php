@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/regularProgramClasseSchedules/add">Add RegularProgramClasseSchedule</a>
@endsection

@section('crud-title')
	RegularProgramClasseSchedules
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>RegularProgramSemester</th>
								<th>Location</th>
								<th>Weekday</th>
								<th>From Time</th>
								<th>To Time</th>
							</tr>
		</thead>
		<tbody>
			@foreach($regularProgramClasseSchedules as $regularProgramClasseSchedule)
			<tr>
								<td>
					<a href="/modules/regularProgramClasseSchedules/{{ $regularProgramClasseSchedule->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/regularProgramClasseSchedules/{{ $regularProgramClasseSchedule->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="regularProgramClasseSchedule" data-module="regularProgramSemester" data-id="{{ $regularProgramClasseSchedule->id }}">RegularProgramSemester</a>
				</td>
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="regularProgramClasseSchedule" data-module="location" data-id="{{ $regularProgramClasseSchedule->id }}">Location</a>
				</td>
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="regularProgramClasseSchedule" data-module="weekday" data-id="{{ $regularProgramClasseSchedule->id }}">Weekday</a>
				</td>
								
								<td>{{ $regularProgramClasseSchedule->from_time }}</td>
								<td>{{ $regularProgramClasseSchedule->to_time }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
