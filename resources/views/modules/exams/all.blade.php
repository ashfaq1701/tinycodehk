@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/exams/add">Add Exam</a>
@endsection

@section('crud-title')
	Exams
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>ExamScores</th>
								<th>Enrollments</th>
								<th>ProgramClass</th>
								<th>Title</th>
								<th>Exam Date Time</th>
								<th>Description</th>
								<th>Total Marks</th>
							</tr>
		</thead>
		<tbody>
			@foreach($exams as $exam)
			<tr>
								<td>
					<a href="/modules/exams/{{ $exam->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/exams/{{ $exam->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="hasMany" data-origin="exam" data-module="examScore" data-id="{{ $exam->id }}">ExamScores</a></td>
								<td><a href="#" class="relationship-link" data-type="many" data-relationship-type="belongsToMany" data-origin="exam" data-module="enrollment" data-id="{{ $exam->id }}">Enrollments</a></td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="exam" data-module="programClass" data-id="{{ $exam->id }}">ProgramClass</a>
				</td>
								
								<td>{{ $exam->title }}</td>
								<td>{{ $exam->exam_date_time }}</td>
								<td>{{ $exam->description }}</td>
								<td>{{ $exam->total_marks }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
