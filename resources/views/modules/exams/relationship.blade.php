<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="exam" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allexams as $exam)
			<option value="{{ $exam->id }}">{{ $exam->title }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'examScore')
			<th>ExamScores			</th>
			@endif
						@if($origin != 'enrollment')
			<th>Enrollments			</th>
			@endif
						@if($origin != 'programClass')
			<th>ProgramClass			</th>
			@endif
						<th>Title			</th>
						<th>Exam Date Time			</th>
						<th>Description			</th>
						<th>Total Marks			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($exams as $exam)
		<tr>
						<td>
				<a href="/modules/exams/{{ $exam->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/exams/{{ $exam->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
						@if($origin != 'examScore')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="exam" data-module="examScore" data-id="{{ $exam->id }}">ExamScores</a>
			</td>
			@endif
						@if($origin != 'enrollment')
			<td>
				<a href="#" class="relationship-link" data-type="many" data-origin="exam" data-module="enrollment" data-id="{{ $exam->id }}">Enrollments</a>
			</td>
			@endif
							
						@if($origin != 'programClass')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="exam" data-module="programClass" data-id="{{ $exam->id }}">ProgramClass</a>
			</td>
			@endif
							
						<td>{{ $exam->title }}</td>
						<td>{{ $exam->exam_date_time }}</td>
						<td>{{ $exam->description }}</td>
						<td>{{ $exam->total_marks }}</td>
					</tr>
		@endforeach
	</tbody>
</table>