@extends('commons.crud')

@section('crud-title')
	Add Exam
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="exams">
		
						
				<div class="row" id="rowZe8PU"><div id="col-PWEKR" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="title">Title</label>  
  		                <input type="text" id="title" class="form-control control-form" name="title" placeholder="Title" value="{{ empty($exam) ? '' : $exam->title }}" maxlength="200"/>
                  		</div>
  	</div>
</div></div><div id="col-1i1z1" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="exam_date_time">Exam Date Time</label>  
  		        	<input class="form_datetime form-control" id="exam_date_time" name="exam_date_time" placeholder="Exam Date Time" type="text" value="{{ empty($exam) ? '' : $exam->exam_date_time }}" readonly>
          		</div>
  	</div>
</div></div></div><div class="row" id="rowhQEzm"><div id="col-Otg5h" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="description">Description</label>  
  		                <textarea id="description" class="form-control control-form" rows="4" name="description" placeholder="Description">{{ empty($exam) ? '' : $exam->description }}</textarea>
                  		</div>
  	</div>
</div></div><div id="col-4qYWg" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="total_marks">Total Marks</label>  
  		                <input id="total_marks" class="form-control control-form" type="number" name="total_marks" placeholder="Total Marks" value="{{ empty($exam) ? '' : $exam->total_marks }}" />
              		</div>
  	</div>
</div></div></div><div class="row" id="row1eyJk"><div id="col-2Q8Jp" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		ExamScores	</div>
	<div class="panel-body relationship-panel" data-model="ExamScore" data-variable-name="examScores" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="examScores" data-type="many">
						<option value="0">Select</option>
						@foreach($allexamScores as $examScore)
						<option value="{{ $examScore->id }}">{{ $examScore->marks }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($exam))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="examScores">
						<option value="">Select</option>
						@foreach($exam->examScores as $examScore)
						<option value="{{ $examScore->id }}">{{ $examScore->marks }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="examScores" value="{{ $examScoresValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Marks</label>  
  		  		            <input data-field="marks" class="form-control control-form" type="number" placeholder="Marks" />
              		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="examScores" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-9y43y" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		ProgramClass	</div>
	<div class="panel-body relationship-panel" data-model="ProgramClass" data-variable-name="programClass" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="programClasses" data-type="one">
						<option value="0">Select</option>
						@foreach($allprogramClasses as $programClass)
						<option value="{{ $programClass->id }}">{{ $programClass->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="programClass" value="{{ (!empty($exam)) ? (!empty($exam->programClass) ? $exam->programClass->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  						<input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" value="{{ empty($exam) ? '' : (empty($exam->programClass) ? '' : $exam->programClass->course_type) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Time</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="from_time" placeholder="From Time" type="text" value="{{ empty($exam) ? '' : (empty($exam->programClass) ? '' : $exam->programClass->from_time) }}" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Time</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="to_time" placeholder="To Time" type="text" value="{{ empty($exam) ? '' : (empty($exam->programClass) ? '' : $exam->programClass->to_time) }}" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  						<input type="text" data-field="title" class="form-control control-form" placeholder="Title" value="{{ empty($exam) ? '' : (empty($exam->programClass) ? '' : $exam->programClass->title) }}" maxlength="55"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($exam) ? '' : (empty($exam->programClass) ? '' : $exam->programClass->description) }}</textarea>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="programClass" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div><div class="row" id="rowx1JvH"><div id="col-hrxCl" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Enrollments	</div>
	<div class="panel-body relationship-panel" data-model="Enrollment" data-variable-name="enrollments" data-type="many">
				
		<div class="well">
			<div class="row">
				<div class="col-md-8">
					<select class="form-control" data-module="enrollments" data-type="many">
						<option value="0">Select</option>
						@foreach($allenrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		@if(!empty($exam))
		<div class="well">
			<div class="row">
				<div class="col-md-12">
					<select class="form-control" onchange="loadExistingManyRelationship(event)" data-module="enrollments">
						<option value="">Select</option>
						@foreach($exam->enrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		@endif
		<div class="row">
			<input type="hidden" class="related-entry" name="enrollments" value="{{ $enrollmentsValue or '' }}"/>
			<input type="hidden" class="active-entry" value=""/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  		                <input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" maxlength="15"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Registration No</label>  
  		  		                <input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" maxlength="45"/>
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Is Paid</label>  
  		  		                <input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" />
                  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Total Payment</label>  
  		  		            <input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" />
              		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="enrollments" onclick="localSaveManyRelationship(event)">Save</button>
				<button class="btn btn-default" onclick="refreshContainer(event)">New</button>
			</div>
		</div>
	</div>
</div></div><div id="col-oAdV1" class="col-md-6"></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($exam) ? 0 : $exam->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/examsForm.js"></script>
@endpush