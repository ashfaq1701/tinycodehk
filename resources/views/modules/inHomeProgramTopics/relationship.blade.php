<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="inHomeProgramTopic" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allinHomeProgramTopics as $inHomeProgramTopic)
			<option value="{{ $inHomeProgramTopic->id }}">{{ $inHomeProgramTopic->title }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'inHomeProgram')
			<th>InHomeProgram			</th>
			@endif
						<th>Title			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($inHomeProgramTopics as $inHomeProgramTopic)
		<tr>
						<td>
				<a href="/modules/inHomeProgramTopics/{{ $inHomeProgramTopic->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/inHomeProgramTopics/{{ $inHomeProgramTopic->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'inHomeProgram')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="inHomeProgramTopic" data-module="inHomeProgram" data-id="{{ $inHomeProgramTopic->id }}">InHomeProgram</a>
			</td>
			@endif
							
						<td>{{ $inHomeProgramTopic->title }}</td>
					</tr>
		@endforeach
	</tbody>
</table>