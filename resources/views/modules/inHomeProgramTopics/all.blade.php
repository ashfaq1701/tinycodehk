@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/inHomeProgramTopics/add">Add InHomeProgramTopic</a>
@endsection

@section('crud-title')
	InHomeProgramTopics
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>InHomeProgram</th>
								<th>Title</th>
							</tr>
		</thead>
		<tbody>
			@foreach($inHomeProgramTopics as $inHomeProgramTopic)
			<tr>
								<td>
					<a href="/modules/inHomeProgramTopics/{{ $inHomeProgramTopic->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/inHomeProgramTopics/{{ $inHomeProgramTopic->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="inHomeProgramTopic" data-module="inHomeProgram" data-id="{{ $inHomeProgramTopic->id }}">InHomeProgram</a>
				</td>
								
								<td>{{ $inHomeProgramTopic->title }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
