@extends('commons.crud')

@section('crud-right-menu')
    <a class="btn btn-default" href="/modules/attendences/add">Add Attendence</a>
@endsection

@section('crud-title')
	Attendences
@endsection

@section('crud-body')
	<table class="datatables" class="display" width="100%">
		<thead>
			<tr>
								<th></th>
								<th>Enrollment</th>
								<th>ProgramClass</th>
								<th>Attended</th>
							</tr>
		</thead>
		<tbody>
			@foreach($attendences as $attendence)
			<tr>
								<td>
					<a href="/modules/attendences/{{ $attendence->id }}/delete">
						<i class="fa fa-minus-circle" aria-hidden="true"></i>
					</a>
					<a href="/modules/attendences/{{ $attendence->id }}">
						<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
					</a>
				</td>
								
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="attendence" data-module="enrollment" data-id="{{ $attendence->id }}">Enrollment</a>
				</td>
								<td>
					<a href="#" class="relationship-link" data-type="one" data-relationship-type="belongsTo" data-origin="attendence" data-module="programClass" data-id="{{ $attendence->id }}">ProgramClass</a>
				</td>
								
								<td>{{ $attendence->attended }}</td>
							</tr>
			@endforeach
		</tbody>
	</table>
@endsection
