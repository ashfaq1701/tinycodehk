<div class="row link-relationship" data-type="{{ $type }}" data-origin="{{ $origin }}" data-target="attendence" data-id="{{ $id }}" data-relationship-type="{{ $relationshipType }}">
		<div class="col-md-1">
		<label>Select</label>
	</div>
	<div class="col-md-9">
		<select class="form-control">
			<option value="">Select</option>
			@foreach($allattendences as $attendence)
			<option value="{{ $attendence->id }}">{{ $attendence->attended }}</option>
			@endforeach
		</select>
	</div>
	<div class="col-md-2">
		<button class="btn btn-success" onclick="performLinkup(event)">Link</button>
	</div>
</div>
<table class="datatables child" width="100%">
	<thead>
		<tr>
			<th>
			</th>
						@if($origin != 'enrollment')
			<th>Enrollment			</th>
			@endif
						@if($origin != 'programClass')
			<th>ProgramClass			</th>
			@endif
						<th>Attended			</th>
					</tr>
	</thead>
	<tbody>
		@foreach($attendences as $attendence)
		<tr>
						<td>
				<a href="/modules/attendences/{{ $attendence->id }}/delete">
					<i class="fa fa-minus-circle" aria-hidden="true"></i>
				</a>
				<a href="/modules/attendences/{{ $attendence->id }}">
					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
				</a>
			</td>
							
						@if($origin != 'enrollment')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="attendence" data-module="enrollment" data-id="{{ $attendence->id }}">Enrollment</a>
			</td>
			@endif
						@if($origin != 'programClass')
			<td>
				<a href="#" class="relationship-link" data-type="one" data-origin="attendence" data-module="programClass" data-id="{{ $attendence->id }}">ProgramClass</a>
			</td>
			@endif
							
						<td>{{ $attendence->attended }}</td>
					</tr>
		@endforeach
	</tbody>
</table>