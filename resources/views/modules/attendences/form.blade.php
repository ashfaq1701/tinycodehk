@extends('commons.crud')

@section('crud-title')
	Add Attendence
@endsection

@section('crud-body')
	@if (count($errors) > 0)
    	<div class="alert alert-danger">
        	<ul>
            	@foreach ($errors->all() as $error)
                	<li>{{ $error }}</li>
           		@endforeach
        	</ul>
    	</div>
	@endif
	<form class="form-horizontal crud-form" method="POST" data-attr="attendences">
		
						
				<div class="row" id="rowBm8RL"><div id="col-7Pe3E" class="col-md-6"><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label" for="attended">Attended</label>  
  		                <input type="text" id="attended" class="form-control control-form" name="attended" placeholder="Attended" value="{{ empty($attendence) ? '' : $attendence->attended }}" />
                  		</div>
  	</div>
</div></div><div id="col-P8uW0" class="col-md-6"></div></div><div class="row" id="rowPk94S"><div id="col-uNg51" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		Enrollment	</div>
	<div class="panel-body relationship-panel" data-model="Enrollment" data-variable-name="enrollment" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="enrollments" data-type="one">
						<option value="0">Select</option>
						@foreach($allenrollments as $enrollment)
						<option value="{{ $enrollment->id }}">{{ $enrollment->registration_no }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="enrollment" value="{{ (!empty($attendence)) ? (!empty($attendence->enrollment) ? $attendence->enrollment->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  						<input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" value="{{ empty($attendence) ? '' : (empty($attendence->enrollment) ? '' : $attendence->enrollment->course_type) }}" maxlength="15"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Registration No</label>  
  		  						<input type="text" data-field="registration_no" class="form-control control-form" placeholder="Registration No" value="{{ empty($attendence) ? '' : (empty($attendence->enrollment) ? '' : $attendence->enrollment->registration_no) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Is Paid</label>  
  		  						<input type="text" data-field="is_paid" class="form-control control-form" placeholder="Is Paid" value="{{ empty($attendence) ? '' : (empty($attendence->enrollment) ? '' : $attendence->enrollment->is_paid) }}" />
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Total Payment</label>  
  		  		        	<input data-field="total_payment" class="form-control control-form" type="number" placeholder="Total Payment" value="{{ empty($attendence) ? '' : (empty($attendence->enrollment) ? '' : $attendence->enrollment->total_payment) }}" />
        	  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="enrollment" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div><div id="col-lQEJ2" class="col-md-6"><div class="panel panel-primary">
	<div class="panel-heading">
		ProgramClass	</div>
	<div class="panel-body relationship-panel" data-model="ProgramClass" data-variable-name="programClass" data-type="one">
				<div class="well">
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" data-module="programClasses" data-type="one">
						<option value="0">Select</option>
						@foreach($allprogramClasses as $programClass)
						<option value="{{ $programClass->id }}">{{ $programClass->title }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-2">
					<button class="btn btn-success" onclick="selectItemOnForm(event)">Link</button>
				</div>
			</div>
		</div>
		<div class="row">
			<input type="hidden" class="related-entry" name="programClass" value="{{ (!empty($attendence)) ? (!empty($attendence->programClass) ? $attendence->programClass->id : '') : '' }}"/>
			<div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Course Type</label>  
  		  						<input type="text" data-field="course_type" class="form-control control-form" placeholder="Course Type" value="{{ empty($attendence) ? '' : (empty($attendence->programClass) ? '' : $attendence->programClass->course_type) }}" maxlength="45"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">From Time</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="from_time" placeholder="From Time" type="text" value="{{ empty($attendence) ? '' : (empty($attendence->programClass) ? '' : $attendence->programClass->from_time) }}" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">To Time</label>  
  		  		        	<input class="form_datetime form-control control-form" data-field="to_time" placeholder="To Time" type="text" value="{{ empty($attendence) ? '' : (empty($attendence->programClass) ? '' : $attendence->programClass->to_time) }}" readonly>
          		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Title</label>  
  		  						<input type="text" data-field="title" class="form-control control-form" placeholder="Title" value="{{ empty($attendence) ? '' : (empty($attendence->programClass) ? '' : $attendence->programClass->title) }}" maxlength="55"/>
				  		</div>
  	</div>
</div><div class="form-group">
	<div class="row">
  		<div class="col-sm-12 col-md-12">
  		<label class="control-label">Description</label>  
  		  		            	<textarea data-field="description" class="form-control control-form" rows="4" placeholder="Description">{{ empty($attendence) ? '' : (empty($attendence->programClass) ? '' : $attendence->programClass->description) }}</textarea>
				  		</div>
  	</div>
</div>			<div class="col-md-12">
				<button class="btn btn-success" data-module="programClass" onclick="localSaveManyRelationship(event)">Save</button>
			</div>
		</div>
	</div>
</div></div></div>				<input type="hidden" name="primary" id="primary" value="{{ empty($attendence) ? 0 : $attendence->id }}"/>
		{{ csrf_field() }}
				<div class="form-group">
  			<div class="col-md-4 col-md-offset-4">
    			<button id="save" type="submit" class="btn btn-primary">Save</button>
  			</div>
		</div>
	
	</form>
@endsection

@push('scripts')
	<script type="text/javascript" src="/js/modules/attendencesForm.js"></script>
@endpush