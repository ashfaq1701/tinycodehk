<nav id="primary-menu">
	<ul>
		<li class="current"><a href="/"><div>Home</div></a></li>
		<li><a href="/about-us"><div>About Us</div></a>
			<ul>
				<li><a href="/about-us"><div>About Us</div></a></li>
				<li><a href="/why-coding"><div>Why Coding</div></a></li>
				<li><a href="/careers"><div>Careers</div></a></li>
			</ul>
		</li>
		<li><a href="/group-programs"><div>Group Programs</div></a></li>
		<li><a href="/holiday-camps"><div>Holiday Camps</div></a></li>
		<li><a href="/in-home-programs"><div>In Home Programs</div></a></li>
		<li><a href="/contact-us"><div>Contact Us</div></a></li>
	</ul>
	@include('frontend.partials.nav-search-bar')
</nav>