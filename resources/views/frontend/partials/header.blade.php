<header id="header" class="full-header">
	<div id="header-wrap">
		<div class="container clearfix">
			<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
			<div id="logo">
				<a href="index.html" class="standard-logo" data-dark-logo="/images/logo-dark.png"><img src="images/logo.png" alt="Canvas Logo"></a>
				<a href="index.html" class="retina-logo" data-dark-logo="/images/logo-dark@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
			</div>
			@include('frontend.partials.navbar')
		</div>
	</div>
</header>