<footer id="footer" class="dark">
	@include('frontend.partials.footer-widgets')
	@include('frontend.partials.copyrights')
</footer>