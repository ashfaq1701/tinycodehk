var dataTable = null;

jQuery(document).ready(function($)
{
        $.ajaxSetup({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
        });

        dataTable = $('.datatables').DataTable(
        {
                "scrollX": true
        });

        $(".form_datetime").datetimepicker({
        format: "yyyy-mm-dd hh:ii:ss"
    });
});