jQuery(document).ready(function($)
{
	$('.crud-form').on('submit', function(e)
	{
		var form = $(this);
		var dataAttr = form.attr('data-attr');
		var primaryId = $('#primary').val();
		if(primaryId == '0')
		{
			form.attr('action', '/modules/'+dataAttr);
		}
		else
		{
			form.attr('action', '/modules/'+dataAttr+'/'+primaryId);
		}
	});
	
	$('#crud-body').on('click', '.relationship-link', function(e)
	{
		e.preventDefault();
	    var tr = $(this).closest('tr');
	    var table = $(this).closest('table').dataTable().api();
	    var row = table.row(tr);
	    if(!row.child.isShown())
	    {
	    	var targetLink = e.target;
	        var dataType = $(targetLink).attr("data-type");
	        var dataOrigin = $(targetLink).attr("data-origin");
	        var dataModule = $(targetLink).attr("data-module");
	        var dataId = $(targetLink).attr("data-id");
	        $.ajax({
	        	type: 'POST',
	            url: '/modules/related',
	            data: {
	            	dataType: dataType,
	                dataOrigin: dataOrigin,
	                dataModule: dataModule,
	                dataId: dataId
	           },
	           success: function(response)
	           {
	        	   row.child(response).show();
	           }
	       });
	    }
	    else
	    {
	    	row.child.hide();
	    }
	});

});