<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Enrollment extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'user_id' => 'integer',
			'course_id' => 'integer',
			'course_type' => 'string',
			'registration_no' => 'string',
			'is_paid' => 'boolean',
			'total_payment' => 'decimal',
			'voucher_applied_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'enrollments';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'user_id', 'course_id', 'course_type', 'registration_no', 'is_paid', 'total_payment', 'voucher_applied_id'];


			public function usersVoucher() {
        return $this->belongsTo(\App\Models\UsersVoucher::class, "voucher_applied_id", "id");
		}
		
			public function user() {
        return $this->belongsTo(\App\Models\User::class, "user_id", "id");
		}
		
			public function programClasses() {
        return $this->belongsToMany(\App\Models\ProgramClass::class, 'attendence', "enrollment_id", "class_id");
		}
		
			public function exams() {
        return $this->belongsToMany(\App\Models\Exam::class, 'exam_scores', "enrollment_id", "exam_id");
		}
		
			public function attendences() {
        return $this->hasMany(\App\Models\Attendence::class, "enrollment_id", "id");
		}
		
			public function examScores() {
        return $this->hasMany(\App\Models\ExamScore::class, "enrollment_id", "id");
		}
		
			public function payments() {
        return $this->hasMany(\App\Models\Payment::class, "enrollment_id", "id");
		}
		
			public function testimonials() {
        return $this->hasMany(\App\Models\Testimonial::class, "enrollment_id", "id");
		}
		
		public function course()
		{
			return $this->morphTo();
		}
}

