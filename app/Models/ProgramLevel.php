<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class ProgramLevel extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'string',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'program_levels';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name'];


			public function holidayCampCourses() {
        return $this->hasMany(\App\Models\HolidayCampCourse::class, "program_level_id", "id");
		}
		
}

