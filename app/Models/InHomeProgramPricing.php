<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class InHomeProgramPricing extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'student_numbers' => 'integer',
			'price' => 'decimal',
			'in_home_program_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'in_home_program_pricing';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'student_numbers', 'price', 'in_home_program_id'];


			public function inHomeProgram() {
        return $this->belongsTo(\App\Models\InHomeProgram::class, "in_home_program_id", "id");
		}
		
}

