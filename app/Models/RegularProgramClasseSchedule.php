<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class RegularProgramClasseSchedule extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'from_time' => 'string',
			'to_time' => 'string',
			'location_id' => 'integer',
			'weekday_id' => 'integer',
			'regular_program_semester_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'regular_program_classe_schedules';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'from_time', 'to_time', 'location_id', 'weekday_id', 'regular_program_semester_id'];


			public function regularProgramSemester() {
        return $this->belongsTo(\App\Models\RegularProgramSemester::class, "regular_program_semester_id", "id");
		}
		
			public function location() {
        return $this->belongsTo(\App\Models\Location::class, "location_id", "id");
		}
		
			public function weekday() {
        return $this->belongsTo(\App\Models\Weekday::class, "weekday_id", "id");
		}
		
}

