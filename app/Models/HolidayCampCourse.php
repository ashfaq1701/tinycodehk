<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class HolidayCampCourse extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'string',
			'program_icon' => 'string',
			'program_cover_image' => 'string',
			'program_video_url' => 'string',
			'language' => 'string',
			'description' => 'text',
			'holiday_camp_id' => 'integer',
			'program_level_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'holiday_camp_courses';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name', 'program_icon', 'program_cover_image', 'program_video_url', 'language', 'description', 'holiday_camp_id', 'program_level_id'];


			public function holidayCamp() {
        return $this->belongsTo(\App\Models\HolidayCamp::class, "holiday_camp_id", "id");
		}
		
			public function programLevel() {
        return $this->belongsTo(\App\Models\ProgramLevel::class, "program_level_id", "id");
		}
		
			public function locations() {
        return $this->belongsToMany(\App\Models\Location::class, 'holiday_camp_modules', "holiday_camp_course_id", "location_id");
		}
		
			public function holidayCampCourseBringings() {
        return $this->hasMany(\App\Models\HolidayCampCourseBringing::class, "holiday_camp_course_id", "id");
		}
		
			public function holidayCampCourseFeatures() {
        return $this->hasMany(\App\Models\HolidayCampCourseFeature::class, "holiday_camp_course_id", "id");
		}
		
			public function holidayCampCourseHighlights() {
        return $this->hasMany(\App\Models\HolidayCampCourseHighlight::class, "holiday_camp_course_id", "id");
		}
		
			public function holidayCampModules() {
        return $this->hasMany(\App\Models\HolidayCampModule::class, "holiday_camp_course_id", "id");
		}
		
}

