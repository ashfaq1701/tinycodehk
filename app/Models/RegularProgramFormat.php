<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class RegularProgramFormat extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'title' => 'string',
			'regular_program_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'regular_program_formats';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'title', 'regular_program_id'];


			public function regularProgram() {
        return $this->belongsTo(\App\Models\RegularProgram::class, "regular_program_id", "id");
		}
		
}

