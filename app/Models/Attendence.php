<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Attendence extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'enrollment_id' => 'integer',
			'class_id' => 'integer',
			'attended' => 'boolean',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'attendence';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'enrollment_id', 'class_id', 'attended'];


			public function enrollment() {
        return $this->belongsTo(\App\Models\Enrollment::class, "enrollment_id", "id");
		}
		
			public function programClass() {
        return $this->belongsTo(\App\Models\ProgramClass::class, "class_id", "id");
		}
		
}

