<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class HolidayCampCourseFeature extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'string',
			'holiday_camp_course_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'holiday_camp_course_features';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name', 'holiday_camp_course_id'];


			public function holidayCampCourse() {
        return $this->belongsTo(\App\Models\HolidayCampCourse::class, "holiday_camp_course_id", "id");
		}
		
			public function holidayCampCourseSubFeatures() {
        return $this->hasMany(\App\Models\HolidayCampCourseSubFeature::class, "holiday_camp_course_feature_id", "id");
		}
		
}

