<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class InHomeProgramTopic extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'title' => 'string',
			'in_home_program_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'in_home_program_topics';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'title', 'in_home_program_id'];


			public function inHomeProgram() {
        return $this->belongsTo(\App\Models\InHomeProgram::class, "in_home_program_id", "id");
		}
		
}

