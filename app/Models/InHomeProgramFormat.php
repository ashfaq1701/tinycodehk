<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class InHomeProgramFormat extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'title' => 'string',
			'regular_program_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'in_home_program_formats';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'title', 'regular_program_id'];


			public function inHomeProgram() {
        return $this->belongsTo(\App\Models\InHomeProgram::class, "regular_program_id", "id");
		}
		
}

