<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Location extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'string',
			'address_line_1' => 'string',
			'address_line_2' => 'string',
			'city' => 'string',
			'state' => 'string',
			'country' => 'string',
			'mobile' => 'string',
			'email' => 'string',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'locations';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name', 'address_line_1', 'address_line_2', 'city', 'state', 'country', 'mobile', 'email'];


			public function holidayCampCourses() {
        return $this->belongsToMany(\App\Models\HolidayCampCourse::class, 'holiday_camp_modules', "location_id", "holiday_camp_course_id");
		}
		
			public function holidayCampModules() {
        return $this->hasMany(\App\Models\HolidayCampModule::class, "location_id", "id");
		}
		
			public function regularProgramClasseSchedules() {
        return $this->hasMany(\App\Models\RegularProgramClasseSchedule::class, "location_id", "id");
		}
		
}

