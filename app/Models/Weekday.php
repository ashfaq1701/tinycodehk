<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Weekday extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'string',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'weekdays';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name'];


			public function inHomePrograms() {
        return $this->belongsToMany(\App\Models\InHomeProgram::class, 'in_home_program_schedule', "weekday_id", "in_home_program_id");
		}
		
			public function inHomeProgramSchedules() {
        return $this->hasMany(\App\Models\InHomeProgramSchedule::class, "weekday_id", "id");
		}
		
			public function regularProgramClasseSchedules() {
        return $this->hasMany(\App\Models\RegularProgramClasseSchedule::class, "weekday_id", "id");
		}
		
}

