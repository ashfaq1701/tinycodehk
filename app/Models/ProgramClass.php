<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class ProgramClass extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'course_id' => 'integer',
			'course_type' => 'string',
			'from_time' => 'datetime',
			'to_time' => 'datetime',
			'title' => 'string',
			'description' => 'text',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'program_classes';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'course_id', 'course_type', 'from_time', 'to_time', 'title', 'description'];


			public function enrollments() {
        return $this->belongsToMany(\App\Models\Enrollment::class, 'attendence', "class_id", "enrollment_id");
		}
		
			public function attachments() {
        return $this->hasMany(\App\Models\Attachment::class, "class_id", "id");
		}
		
			public function attendences() {
        return $this->hasMany(\App\Models\Attendence::class, "class_id", "id");
		}
		
			public function exams() {
        return $this->hasMany(\App\Models\Exam::class, "class_id", "id");
		}
		
}

