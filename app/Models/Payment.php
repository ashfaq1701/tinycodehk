<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Payment extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'amount' => 'decimal',
			'enrollment_id' => 'integer',
			'user_id' => 'integer',
			'invoice_id' => 'string',
			'charge_id' => 'string',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'payments';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'amount', 'enrollment_id', 'user_id', 'invoice_id', 'charge_id'];


			public function enrollment() {
        return $this->belongsTo(\App\Models\Enrollment::class, "enrollment_id", "id");
		}
		
			public function user() {
        return $this->belongsTo(\App\Models\User::class, "user_id", "id");
		}
		
}

