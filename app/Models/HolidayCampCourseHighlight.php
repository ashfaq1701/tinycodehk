<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class HolidayCampCourseHighlight extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'title' => 'string',
			'description' => 'text',
			'holiday_camp_course_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'holiday_camp_course_highlights';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'title', 'description', 'holiday_camp_course_id'];


			public function holidayCampCourse() {
        return $this->belongsTo(\App\Models\HolidayCampCourse::class, "holiday_camp_course_id", "id");
		}
		
}

