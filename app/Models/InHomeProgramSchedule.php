<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class InHomeProgramSchedule extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'in_home_program_id' => 'integer',
			'from_time' => 'string',
			'to_time' => 'string',
			'weekday_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'in_home_program_schedule';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'in_home_program_id', 'from_time', 'to_time', 'weekday_id'];


			public function inHomeProgram() {
        return $this->belongsTo(\App\Models\InHomeProgram::class, "in_home_program_id", "id");
		}
		
			public function weekday() {
        return $this->belongsTo(\App\Models\Weekday::class, "weekday_id", "id");
		}
		
}

