<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class InHomeProgram extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'string',
			'from_age' => 'integer',
			'to_age' => 'integer',
			'description_1' => 'text',
			'description_2' => 'text',
			'image_1' => 'string',
			'image_2' => 'string',
			'language' => 'string',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'in_home_programs';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name', 'from_age', 'to_age', 'description_1', 'description_2', 'image_1', 'image_2', 'language'];


		public function weekdays() {
        	return $this->belongsToMany(\App\Models\Weekday::class, 'in_home_program_schedule', "in_home_program_id", "weekday_id");
		}
		
		public function inHomeProgramFormats() {
        	return $this->hasMany(\App\Models\InHomeProgramFormat::class, "regular_program_id", "id");
		}
		
		public function inHomeProgramPricings() {
        	return $this->hasMany(\App\Models\InHomeProgramPricing::class, "in_home_program_id", "id");
		}
		
		public function inHomeProgramSchedules() {
        	return $this->hasMany(\App\Models\InHomeProgramSchedule::class, "in_home_program_id", "id");
		}
		
		public function inHomeProgramStructures() {
        	return $this->hasMany(\App\Models\InHomeProgramStructure::class, "in_home_program_id", "id");
		}
		
		public function inHomeProgramTopics() {
        	return $this->hasMany(\App\Models\InHomeProgramTopic::class, "in_home_program_id", "id");
		}
		
		public function enrollments()
		{
			return $this->morphMany('App\Models\Enrollment', 'enrollment');
		}
		
}

