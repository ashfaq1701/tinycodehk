<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Role extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'string',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'roles';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name'];


			public function users() {
        return $this->hasMany(\App\Models\User::class, "role_id", "id");
		}
		
}

