<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class HolidayCamp extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'string',
			'description' => 'text',
			'from_date' => 'datetime',
			'to_date' => 'datetime',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'holiday_camps';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name', 'description', 'from_date', 'to_date'];


			public function holidayCampCourses() {
        return $this->hasMany(\App\Models\HolidayCampCourse::class, "holiday_camp_id", "id");
		}
		
}

