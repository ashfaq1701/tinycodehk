<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class RegularProgramEvent extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'title' => 'string',
			'description' => 'text',
			'regular_program_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'regular_program_events';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'title', 'description', 'regular_program_id'];


			public function regularProgram() {
        return $this->belongsTo(\App\Models\RegularProgram::class, "regular_program_id", "id");
		}
		
}

