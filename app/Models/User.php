<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class User extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'string',
			'email' => 'string',
			'password' => 'string',
			'remember_token' => 'string',
			'parent_id' => 'integer',
			'stripe_id' => 'string',
			'card_brand' => 'string',
			'card_last_4' => 'string',
			'trial_ends_at' => 'datetime',
			'role_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'users';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name', 'email', 'password', 'remember_token', 'parent_id', 'stripe_id', 'card_brand', 'card_last_4', 'trial_ends_at', 'role_id'];


			public function role() {
        return $this->belongsTo(\App\Models\Role::class, "role_id", "id");
		}
		
			public function enrollments() {
        return $this->hasMany(\App\Models\Enrollment::class, "user_id", "id");
		}
		
			public function payments() {
        return $this->hasMany(\App\Models\Payment::class, "user_id", "id");
		}
		
			public function users() {
        return $this->hasMany(\App\Models\User::class, "parent_id", "id");
		}
		
			public function usersVouchers() {
        return $this->hasMany(\App\Models\UsersVoucher::class, "user_id", "id");
		}
		
}

