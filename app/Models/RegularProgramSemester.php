<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class RegularProgramSemester extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'year' => 'string',
			'name' => 'string',
			'from_month' => 'string',
			'to_month' => 'string',
			'duration_weeks' => 'string',
			'regular_price' => 'decimal',
			'regular_program_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'regular_program_semester';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'year', 'name', 'from_month', 'to_month', 'duration_weeks', 'regular_price', 'regular_program_id'];


		public function regularProgram() {
        	return $this->belongsTo(\App\Models\RegularProgram::class, "regular_program_id", "id");
		}
		
		public function regularProgramClasseSchedules() {
        	return $this->hasMany(\App\Models\RegularProgramClasseSchedule::class, "regular_program_semester_id", "id");
		}
		
		public function enrollments()
		{
			return $this->morphMany('App\Models\Enrollment', 'enrollment');
		}
		
}

