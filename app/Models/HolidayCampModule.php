<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class HolidayCampModule extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'module_code' => 'string',
			'from_date' => 'datetime',
			'to_date' => 'datetime',
			'from_time' => 'string',
			'to_time' => 'string',
			'regular_price' => 'decimal',
			'holiday_camp_course_id' => 'integer',
			'location_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'holiday_camp_modules';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'module_code', 'from_date', 'to_date', 'from_time', 'to_time', 'regular_price', 'holiday_camp_course_id', 'location_id'];


			public function holidayCampCourse() {
        return $this->belongsTo(\App\Models\HolidayCampCourse::class, "holiday_camp_course_id", "id");
		}
		
			public function location() {
        return $this->belongsTo(\App\Models\Location::class, "location_id", "id");
		}
		
		public function enrollments()
		{
			return $this->morphMany('App\Models\Enrollment', 'enrollment');
		}
		
}

