<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class ExamScore extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'enrollment_id' => 'integer',
			'marks' => 'float',
			'exam_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'exam_scores';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'enrollment_id', 'marks', 'exam_id'];


			public function enrollment() {
        return $this->belongsTo(\App\Models\Enrollment::class, "enrollment_id", "id");
		}
		
			public function exam() {
        return $this->belongsTo(\App\Models\Exam::class, "exam_id", "id");
		}
		
}

