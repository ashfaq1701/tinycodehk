<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Testimonial extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'testimonial' => 'text',
			'student_name' => 'string',
			'class' => 'string',
			'school' => 'string',
			'course' => 'string',
			'roll' => 'string',
			'term' => 'string',
			'year' => 'string',
			'enrollment_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'testimonials';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'testimonial', 'student_name', 'class', 'school', 'course', 'roll', 'term', 'year', 'enrollment_id'];


			public function enrollment() {
        return $this->belongsTo(\App\Models\Enrollment::class, "enrollment_id", "id");
		}
		
}

