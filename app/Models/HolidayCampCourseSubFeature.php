<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class HolidayCampCourseSubFeature extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'text',
			'holiday_camp_course_feature_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'holiday_camp_course_sub_features';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name', 'holiday_camp_course_feature_id'];


			public function holidayCampCourseFeature() {
        return $this->belongsTo(\App\Models\HolidayCampCourseFeature::class, "holiday_camp_course_feature_id", "id");
		}
		
}

