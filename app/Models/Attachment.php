<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Attachment extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'location' => 'string',
			'mime_type' => 'string',
			'class_id' => 'integer',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'attachments';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'location', 'mime_type', 'class_id'];


			public function programClass() {
        return $this->belongsTo(\App\Models\ProgramClass::class, "class_id", "id");
		}
		
}

