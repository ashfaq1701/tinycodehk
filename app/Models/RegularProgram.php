<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class RegularProgram extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'name' => 'string',
			'from_age' => 'integer',
			'to_age' => 'integer',
			'description' => 'text',
			'language' => 'string',
			'bringings' => 'string',
			'icon' => 'string',
			'feat_image' => 'string',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'regular_programs';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name', 'from_age', 'to_age', 'description', 'language', 'bringings', 'icon', 'feat_image'];


			public function regularProgramEvents() {
        return $this->hasMany(\App\Models\RegularProgramEvent::class, "regular_program_id", "id");
		}
		
			public function regularProgramFeatures() {
        return $this->hasMany(\App\Models\RegularProgramFeature::class, "regular_program_id", "id");
		}
		
			public function regularProgramFormats() {
        return $this->hasMany(\App\Models\RegularProgramFormat::class, "regular_program_id", "id");
		}
		
			public function regularProgramSemesters() {
        return $this->hasMany(\App\Models\RegularProgramSemester::class, "regular_program_id", "id");
		}
		
			public function regularProgramStructures() {
        return $this->hasMany(\App\Models\RegularProgramStructure::class, "regular_program_id", "id");
		}
		
}

