<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Exam extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'title' => 'string',
			'exam_date_time' => 'datetime',
			'description' => 'text',
			'class_id' => 'integer',
			'total_marks' => 'float',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'exams';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'title', 'exam_date_time', 'description', 'class_id', 'total_marks'];


			public function programClass() {
        return $this->belongsTo(\App\Models\ProgramClass::class, "class_id", "id");
		}
		
			public function enrollments() {
        return $this->belongsToMany(\App\Models\Enrollment::class, 'exam_scores', "exam_id", "enrollment_id");
		}
		
			public function examScores() {
        return $this->hasMany(\App\Models\ExamScore::class, "exam_id", "id");
		}
		
}

