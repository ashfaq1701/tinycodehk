<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class Voucher extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'voucher_code' => 'string',
			'voucher_type' => 'string',
			'percent_discount' => 'float',
			'flat_discount' => 'float',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'vouchers';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'voucher_code', 'voucher_type', 'percent_discount', 'flat_discount'];


			public function usersVouchers() {
        return $this->hasMany(\App\Models\UsersVoucher::class, "voucher_id", "id");
		}
		
}

