<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\SearchableTrait;

class UsersVoucher extends Model {
use SearchableTrait;

protected $searchable = [
	'columns' => [
			'id' => 'integer',
			'user_id' => 'integer',
			'voucher_id' => 'integer',
			'valid_till' => 'datetime',
			'is_used' => 'boolean',
			'created_at' => 'datetime',
			'updated_at' => 'datetime',
		]
];


    /**
     * Generated
     */
    protected $connection='crud';
	public $timestamps = false;
    protected $table = 'users_vouchers';
    protected $primaryKey = "id";
    protected $fillable = ['id', 'user_id', 'voucher_id', 'valid_till', 'is_used'];


			public function voucher() {
        return $this->belongsTo(\App\Models\Voucher::class, "voucher_id", "id");
		}
		
			public function user() {
        return $this->belongsTo(\App\Models\User::class, "user_id", "id");
		}
		
			public function enrollments() {
        return $this->hasMany(\App\Models\Enrollment::class, "voucher_applied_id", "id");
		}
		
}

