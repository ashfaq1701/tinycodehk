<?php

namespace App\Repositories;

use ZipArchive;

class ImportRepository
{
	public function importModules($file)
	{
		if(is_dir(public_path('uploads/files/').'generated'))
		{
			deleteDir(public_path('uploads/files/').'generated');
			mkdir(public_path('uploads/files/').'generated');
		}
		else
		{
			mkdir(public_path('uploads/files/').'generated');
		}
		$zip = new ZipArchive;
		if ($zip->open(public_path('uploads/files/'.$file)) === TRUE) {
			$zip->extractTo(public_path('uploads/files/generated'));
			$zip->close();
		}
		$files = scandir(public_path('uploads/files/generated'));
		foreach ($files as $currentFile)
		{
			if ($zip->open(public_path('uploads/files/generated/'.$currentFile)) === TRUE) {
				$currentFileName = pathinfo($currentFile, PATHINFO_FILENAME);
				mkdir(public_path('uploads/files/generated/'.$currentFileName));
				$zip->extractTo(public_path('uploads/files/generated/'.$currentFileName));
				$zip->close();
				$this->importSingleModule(public_path('uploads/files/generated/'.$currentFileName));
			}
		}
	}
	
	public function importSingleModule($path)
	{	
		$controllerPath = $path.'/controller';
		$jsPath = $path.'/js';
		$menuPath = $path.'/menu';
		$modelPath = $path.'/model';
		$routesPath = $path.'/routes';
		$viewsPath = $path.'/views';
		
		$pathParts = explode('/', $path);
		$lastPart = $pathParts[count($pathParts)-1];
		$smallLastPart = lcfirst($lastPart);
		$smallLastPart = str_plural($smallLastPart);
		
		$files = scandir($controllerPath);
		
		foreach ($files as $file)
		{
			$firstChar = substr($file, 0, 1);
			if(ctype_alpha($firstChar))
			{
				copy($controllerPath.'/'.$file, app_path('Http/Controllers/Modules/'.$file));
			}
		}
		
		$files = scandir($jsPath);
		foreach ($files as $file)
		{
			$firstChar = substr($file, 0, 1);
			if(ctype_alpha($firstChar))
			{
				copy($jsPath.'/'.$file, public_path('/js/modules/'.$file));
			}
		}
		
		$files = scandir($modelPath);
		foreach ($files as $file)
		{
			$firstChar = substr($file, 0, 1);
			if(ctype_alpha($firstChar))
			{
				copy($modelPath.'/'.$file, app_path('Models/'.$file));
			}
		}
		
		$files = scandir($viewsPath);
		foreach ($files as $file)
		{
			$firstChar = substr($file, 0, 1);
			if(ctype_alpha($firstChar))
			{
				if(!is_dir(resource_path('views/modules/'.lcfirst($smallLastPart))))
				{
					mkdir(resource_path('views/modules/'.lcfirst($smallLastPart)));
				}
				copy($viewsPath.'/'.$file, resource_path('views/modules/'.lcfirst($smallLastPart).'/'.$file));
			}
		}
		
		$modRoutesFile = $routesPath.'/routes.php';
		$modRoutesFileContent = file_get_contents($modRoutesFile);
		$routesFile = base_path('routes/web.php');
		$routesFileContent = file_get_contents($routesFile);
		if (strpos($routesFileContent, $modRoutesFileContent) == false)
		{
			$insertPos = strrpos($routesFileContent, '//routes will be appended here');
			$newRoutesFileContent = substr_replace($routesFileContent, "\n".$modRoutesFileContent."\n", $insertPos - 1, 0);
			file_put_contents(base_path('routes/web.php'), $newRoutesFileContent);
		}
		
		$modMenuFile = $menuPath.'/menu-item.php';
		$modMenuFileContent = file_get_contents($modMenuFile);
		$menuFile = resource_path('views/partials/modules-menu.blade.php');
		$menuFileContent = file_get_contents($menuFile);
		if (strpos($menuFileContent, $modMenuFileContent) == false)
		{
			$insertPos = strrpos($menuFileContent, '<!-- Menu will be added here -->');
			$newMenuFileContent = substr_replace($menuFileContent, "\n".$modMenuFileContent."\n", $insertPos - 1, 0);
			file_put_contents(resource_path('views/partials/modules-menu.blade.php'), $newMenuFileContent);
		}
	}
}
