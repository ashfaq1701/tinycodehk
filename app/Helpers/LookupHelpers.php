<?php 

function getLookupOptions($lookupId)
{
	$lookups = \DB::connection('crud')->table('Lookup')->where('Lookup_Type', $lookupId)->get();
	$lookupsArray = [];
	foreach ($lookups as $lookup)
	{
		$lookupsArray[] = [
				'value' => $lookup->Lookup_ID,
				'label' => $lookup->Description
		];
	}
	return $lookupsArray;
}

function strEndsWith($haystack, $needle)
{
	$length = strlen($needle);
	if ($length == 0) {
		return true;
	}

	return (substr($haystack, -$length) === $needle);
}

function getUnsavedRelationship($unsavedRelationship)
{
	$primaryKey = $unsavedRelationship['primary'];
	$model = $unsavedRelationship['model'];
	$modelName = '\\App\\Models\\'.$model;
	$data = $unsavedRelationship['data'];
	$nonEmptyCount = 0;
	
	foreach ($data as $key=>$value)
	{
		if((!strEndswith($key, '_PARENT_PRIMARY_KEY')) && !empty($value))
		{
			++$nonEmptyCount;
		}
	}
	$modelInstance = null;
	if(empty($primaryKey))
	{
		$modelInstance = new $modelName();
	}
	else
	{
		$modelInstance = $modelName::find($primaryKey);
	}
	if(!empty($modelInstance))
	{
		foreach ($data as $k => $v)
		{
			$key = str_replace('_PARENT_PRIMARY_KEY', '', $k);
			$modelInstance->$key = $v;
		}
	}
	$modelInstance->save();
	return $modelInstance;
}

function deleteDir($dir) {
	$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
	$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
	foreach($files as $file) {
		if ($file->isDir()){
			rmdir($file->getRealPath());
		}
		else {
			unlink($file->getRealPath());
		}
	}
	rmdir($dir);
}

?>