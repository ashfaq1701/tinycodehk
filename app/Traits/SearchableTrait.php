<?php 

namespace App\Traits;

use App\Eloquent\Builder;
use DateTime;
use Carbon\Carbon;

trait SearchableTrait
{
	public function scopeSearch(Builder $q, $search)
	{
		$cols = $this->searchable['columns'];
		$count = count($cols);
		$i = 0;
		foreach ($cols as $col => $type)
		{
			if($i == 0)
			{
				if (DateTime::createFromFormat('Y-m-d', $search) !== false) 
				{
					if($type == 'datetime' || $type == 'date')
					{
						$q = $q->whereDate($col, $search);
					}
				}
				else if(is_numeric($search))
				{
					if(($type == 'integer') || ($type == 'smallint') || ($type == 'boolean') || ($type == 'decimal'))
					{
						$q = $q->where($col, $search);
					}
					else if($type != 'datetime' && $type != 'date')
					{
						$q = $q->where($col, $search);
						$q = $q->orWhere($col, 'LIKE', $search.'%');
						$q = $q->orWhere($col, 'LIKE', '%'.$search.'%');
					}
					
				}
				else
				{
					if(!(($type == 'integer') || ($type == 'smallint') || ($type == 'boolean') || ($type == 'decimal') || ($type == 'date') || ($type == 'datetime')))
					{
						$q = $q->where($col, $search);
						$q = $q->orWhere($col, 'LIKE', $search.'%');
						$q = $q->orWhere($col, 'LIKE', '%'.$search.'%');
					}
				}
			}
			else
			{
				if (DateTime::createFromFormat('Y-m-d', $search) !== false)
				{
					if($type == 'datetime' || $type == 'date')
					{
						$q = $q->where($col, Carbon::createFromFormat('Y-m-d', $search));
					}
				}
				else if(is_numeric($search))
				{
					if(($type == 'integer') || ($type == 'smallint') || ($type == 'boolean') || ($type == 'decimal'))
					{
						$q = $q->orWhere($col, $search);
					}
					else if($type != 'datetime' && $type != 'date')
					{
						$q = $q->orWhere($col, $search);
						$q = $q->orWhere($col, 'LIKE', $search.'%');
						$q = $q->orWhere($col, 'LIKE', '%'.$search.'%');
					}
				}
				else
				{
					if(!(($type == 'integer') || ($type == 'smallint') || ($type == 'boolean') || ($type == 'decimal') || ($type == 'date') || ($type == 'datetime')))
					{
						$q = $q->orWhere($col, $search);
						$q = $q->orWhere($col, 'LIKE', $search.'%');
						$q = $q->orWhere($col, 'LIKE', '%'.$search.'%');
					}
				}
				
			}
			$i++;
		}
		return $q;
	}
}

?>