<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ImportRepository;

class ImportController extends Controller
{
	protected $importRepository;
	
	public function __construct(ImportRepository $importRepository)
	{
		$this->importRepository = $importRepository;
	}
	
	public function getModulesImport()
	{
		return view('commons.importer');
	}
	
	public function postModulesImport(Request $request)
	{
		$filename = $request->input('file');
		$this->importRepository->importModules($filename);
		return response()->json(['data' => 'Modules imported successfully']);
	}
}

?>