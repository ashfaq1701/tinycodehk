<?php 


namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ModulesController extends Controller
{
	public function relatedModules(Request $request)
	{
		$dataType = $request->input('dataType');
		$dataOrigin = $request->input('dataOrigin');
		$dataModule = $request->input('dataModule');
		$dataIdStr = $request->input('dataId');
		$relationshipType = $request->input('relationshipType');
		$dataId = intval($dataIdStr);
		$originClassName = '\\App\\Models\\'.ucfirst($dataOrigin);
		$origin = $originClassName::find($dataId);
		$targetViewFolder = str_plural($dataModule);
		$targetMethod = null;
		if($dataType == 'one')
		{
			$targetMethod = $dataModule;
		}
		else
		{
			$targetMethod = str_plural($dataModule);
		}
		$targetEntities = $origin->{$targetMethod};
		if($dataType == 'one')
		{
			$targetEntities = [$targetEntities];
		}
		$moduleClassName = '\\App\\Models\\'.ucfirst($dataModule);
		$allModules = $moduleClassName::all();
		return view('modules.'.$targetViewFolder.'.relationship', ['type' => $dataType, 'all'.str_plural($dataModule) => $allModules, 'origin' => $dataOrigin, 'relationshipType' => $relationshipType,'id' => $dataIdStr, $targetViewFolder => $targetEntities]);
	}
	
	public function linkModels(Request $request)
	{
		$module = $request->input('module');
		$origin = $request->input('origin');
		$parent = $request->input('parent');
		$type = $request->input('type');
		$relationshipType = $request->input('relationshipType');
		$primaryKeys = $request->input('primaryKeys');
	
		$module = ucfirst($module);
		$origin = ucfirst($origin);
	
		$targetClassName = '\\App\\Models\\'.$module;
		$originClassName = '\\App\\Models\\'.$origin;
	
		if($type == 'one')
		{
			$targetFunctionName = lcfirst($module);
		}
		else
		{
			$targetFunctionName = str_plural(lcfirst($module));
		}
	
		$parentModel = $originClassName::find($parent);
		$childModel = $targetClassName::find($primaryKeys);
	
		if(!empty($childModel))
		{
			if($relationshipType == 'hasOne' || $relationshipType == 'hasMany' || $relationshipType == 'belongsToMany')
			{
				$parentModel->{$targetFunctionName}()->save($childModel);
			}
			else
			{
				$parentModel->{$targetFunctionName}()->associate($childModel);
			}
		}
		return response()->json($childModel);
	}
}

?>