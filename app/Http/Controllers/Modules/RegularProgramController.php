<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RegularProgram;
use App\Models\RegularProgramEvent;
use App\Models\RegularProgramFeature;
use App\Models\RegularProgramFormat;
use App\Models\RegularProgramSemester;
use App\Models\RegularProgramStructure;
use Log;

class RegularProgramController extends Controller
{	
	public function index()
	{
		$regularPrograms = RegularProgram::with('regularProgramEvents','regularProgramFeatures','regularProgramFormats','regularProgramSemesters','regularProgramStructures')->get();
		return view('modules.regularPrograms.all', ['regularPrograms' => $regularPrograms]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = RegularProgram::with('regularProgramEvents','regularProgramFeatures','regularProgramFormats','regularProgramSemesters','regularProgramStructures')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				return view('modules.regularPrograms.form', [
					'allregularProgramEvents' => RegularProgramEvent::all(),
					'allregularProgramFeatures' => RegularProgramFeature::all(),
					'allregularProgramFormats' => RegularProgramFormat::all(),
					'allregularProgramSemesters' => RegularProgramSemester::all(),
					'allregularProgramStructures' => RegularProgramStructure::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$regularProgram = new RegularProgram();
				if(!empty($data['id']))
		{
			$regularProgram->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$regularProgram->name = $data['name'];
		}
				if(!empty($data['from_age']))
		{
			$regularProgram->from_age = $data['from_age'];
		}
				if(!empty($data['to_age']))
		{
			$regularProgram->to_age = $data['to_age'];
		}
				if(!empty($data['description']))
		{
			$regularProgram->description = $data['description'];
		}
				if(!empty($data['language']))
		{
			$regularProgram->language = $data['language'];
		}
				if(!empty($data['bringings']))
		{
			$regularProgram->bringings = $data['bringings'];
		}
				if(!empty($data['icon']))
		{
			$regularProgram->icon = $data['icon'];
		}
				if(!empty($data['feat_image']))
		{
			$regularProgram->feat_image = $data['feat_image'];
		}
				$regularProgram->save();
				if(!empty($data['regularProgramEvents']))
		{
			$regularProgramEvents = explode(',', $data['regularProgramEvents']);
			foreach ($regularProgramEvents as $singleRegularProgramEvent)
			{
				$regularProgramEvent = RegularProgramEvent::find($singleRegularProgramEvent);
								$regularProgram->regularProgramEvents()->save($regularProgramEvent);
							}
		}
		$unsavedregularProgramEvents = $unsavedRelationships['regularProgramEvents'];
		foreach($unsavedregularProgramEvents as $unsavedregularProgramEvent)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramEvent);
			if(!empty($modelInstance))
			{
								$regularProgram->regularProgramEvents()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramFeatures']))
		{
			$regularProgramFeatures = explode(',', $data['regularProgramFeatures']);
			foreach ($regularProgramFeatures as $singleRegularProgramFeature)
			{
				$regularProgramFeature = RegularProgramFeature::find($singleRegularProgramFeature);
								$regularProgram->regularProgramFeatures()->save($regularProgramFeature);
							}
		}
		$unsavedregularProgramFeatures = $unsavedRelationships['regularProgramFeatures'];
		foreach($unsavedregularProgramFeatures as $unsavedregularProgramFeature)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramFeature);
			if(!empty($modelInstance))
			{
								$regularProgram->regularProgramFeatures()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramFormats']))
		{
			$regularProgramFormats = explode(',', $data['regularProgramFormats']);
			foreach ($regularProgramFormats as $singleRegularProgramFormat)
			{
				$regularProgramFormat = RegularProgramFormat::find($singleRegularProgramFormat);
								$regularProgram->regularProgramFormats()->save($regularProgramFormat);
							}
		}
		$unsavedregularProgramFormats = $unsavedRelationships['regularProgramFormats'];
		foreach($unsavedregularProgramFormats as $unsavedregularProgramFormat)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramFormat);
			if(!empty($modelInstance))
			{
								$regularProgram->regularProgramFormats()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramSemesters']))
		{
			$regularProgramSemesters = explode(',', $data['regularProgramSemesters']);
			foreach ($regularProgramSemesters as $singleRegularProgramSemester)
			{
				$regularProgramSemester = RegularProgramSemester::find($singleRegularProgramSemester);
								$regularProgram->regularProgramSemesters()->save($regularProgramSemester);
							}
		}
		$unsavedregularProgramSemesters = $unsavedRelationships['regularProgramSemesters'];
		foreach($unsavedregularProgramSemesters as $unsavedregularProgramSemester)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramSemester);
			if(!empty($modelInstance))
			{
								$regularProgram->regularProgramSemesters()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramStructures']))
		{
			$regularProgramStructures = explode(',', $data['regularProgramStructures']);
			foreach ($regularProgramStructures as $singleRegularProgramStructure)
			{
				$regularProgramStructure = RegularProgramStructure::find($singleRegularProgramStructure);
								$regularProgram->regularProgramStructures()->save($regularProgramStructure);
							}
		}
		$unsavedregularProgramStructures = $unsavedRelationships['regularProgramStructures'];
		foreach($unsavedregularProgramStructures as $unsavedregularProgramStructure)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramStructure);
			if(!empty($modelInstance))
			{
								$regularProgram->regularProgramStructures()->save($modelInstance);
							}
		}
						$regularProgram->save();
		return redirect('/modules/regularPrograms');
	}
	
	public function get($id)
	{
		$regularProgram = RegularProgram::with('regularProgramEvents','regularProgramFeatures','regularProgramFormats','regularProgramSemesters','regularProgramStructures')->find($id);
						$regularProgramEventsArray = [];
		foreach($regularProgram->regularProgramEvents as $regularProgramEvent)
		{
			
			$regularProgramEventsArray[] = $regularProgramEvent->id;
		}
		$regularProgramEventsValue = implode(',', $regularProgramEventsArray);
				$regularProgramFeaturesArray = [];
		foreach($regularProgram->regularProgramFeatures as $regularProgramFeature)
		{
			
			$regularProgramFeaturesArray[] = $regularProgramFeature->id;
		}
		$regularProgramFeaturesValue = implode(',', $regularProgramFeaturesArray);
				$regularProgramFormatsArray = [];
		foreach($regularProgram->regularProgramFormats as $regularProgramFormat)
		{
			
			$regularProgramFormatsArray[] = $regularProgramFormat->id;
		}
		$regularProgramFormatsValue = implode(',', $regularProgramFormatsArray);
				$regularProgramSemestersArray = [];
		foreach($regularProgram->regularProgramSemesters as $regularProgramSemester)
		{
			
			$regularProgramSemestersArray[] = $regularProgramSemester->id;
		}
		$regularProgramSemestersValue = implode(',', $regularProgramSemestersArray);
				$regularProgramStructuresArray = [];
		foreach($regularProgram->regularProgramStructures as $regularProgramStructure)
		{
			
			$regularProgramStructuresArray[] = $regularProgramStructure->id;
		}
		$regularProgramStructuresValue = implode(',', $regularProgramStructuresArray);
				return view('modules.regularPrograms.form', [
			'regularProgram' => $regularProgram,
							'allregularProgramEvents' => RegularProgramEvent::all(),
				'regularProgramEventsValue' => $regularProgramEventsValue,
							'allregularProgramFeatures' => RegularProgramFeature::all(),
				'regularProgramFeaturesValue' => $regularProgramFeaturesValue,
							'allregularProgramFormats' => RegularProgramFormat::all(),
				'regularProgramFormatsValue' => $regularProgramFormatsValue,
							'allregularProgramSemesters' => RegularProgramSemester::all(),
				'regularProgramSemestersValue' => $regularProgramSemestersValue,
							'allregularProgramStructures' => RegularProgramStructure::all(),
				'regularProgramStructuresValue' => $regularProgramStructuresValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$regularProgram = RegularProgram::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$regularProgram->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$regularProgram->name = $data['name'];
		}
				if(!empty($data['from_age']))
		{
			$regularProgram->from_age = $data['from_age'];
		}
				if(!empty($data['to_age']))
		{
			$regularProgram->to_age = $data['to_age'];
		}
				if(!empty($data['description']))
		{
			$regularProgram->description = $data['description'];
		}
				if(!empty($data['language']))
		{
			$regularProgram->language = $data['language'];
		}
				if(!empty($data['bringings']))
		{
			$regularProgram->bringings = $data['bringings'];
		}
				if(!empty($data['icon']))
		{
			$regularProgram->icon = $data['icon'];
		}
				if(!empty($data['feat_image']))
		{
			$regularProgram->feat_image = $data['feat_image'];
		}
				$regularProgram->save();
				if(!empty($data['regularProgramEvents']))
		{
			$regularProgramEvents = explode(',', $data['regularProgramEvents']);
			foreach ($regularProgramEvents as $singleRegularProgramEvent)
			{
				$regularProgramEvent = RegularProgramEvent::find($singleRegularProgramEvent);
								$regularProgram->regularProgramEvents()->save($regularProgramEvent);
							}
		}
		$unsavedregularProgramEvents = $unsavedRelationships['regularProgramEvents'];
		foreach($unsavedregularProgramEvents as $unsavedregularProgramEvent)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramEvent);
			if(!empty($modelInstance))
			{
								$regularProgram->regularProgramEvents()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramFeatures']))
		{
			$regularProgramFeatures = explode(',', $data['regularProgramFeatures']);
			foreach ($regularProgramFeatures as $singleRegularProgramFeature)
			{
				$regularProgramFeature = RegularProgramFeature::find($singleRegularProgramFeature);
								$regularProgram->regularProgramFeatures()->save($regularProgramFeature);
							}
		}
		$unsavedregularProgramFeatures = $unsavedRelationships['regularProgramFeatures'];
		foreach($unsavedregularProgramFeatures as $unsavedregularProgramFeature)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramFeature);
			if(!empty($modelInstance))
			{
								$regularProgram->regularProgramFeatures()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramFormats']))
		{
			$regularProgramFormats = explode(',', $data['regularProgramFormats']);
			foreach ($regularProgramFormats as $singleRegularProgramFormat)
			{
				$regularProgramFormat = RegularProgramFormat::find($singleRegularProgramFormat);
								$regularProgram->regularProgramFormats()->save($regularProgramFormat);
							}
		}
		$unsavedregularProgramFormats = $unsavedRelationships['regularProgramFormats'];
		foreach($unsavedregularProgramFormats as $unsavedregularProgramFormat)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramFormat);
			if(!empty($modelInstance))
			{
								$regularProgram->regularProgramFormats()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramSemesters']))
		{
			$regularProgramSemesters = explode(',', $data['regularProgramSemesters']);
			foreach ($regularProgramSemesters as $singleRegularProgramSemester)
			{
				$regularProgramSemester = RegularProgramSemester::find($singleRegularProgramSemester);
								$regularProgram->regularProgramSemesters()->save($regularProgramSemester);
							}
		}
		$unsavedregularProgramSemesters = $unsavedRelationships['regularProgramSemesters'];
		foreach($unsavedregularProgramSemesters as $unsavedregularProgramSemester)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramSemester);
			if(!empty($modelInstance))
			{
								$regularProgram->regularProgramSemesters()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramStructures']))
		{
			$regularProgramStructures = explode(',', $data['regularProgramStructures']);
			foreach ($regularProgramStructures as $singleRegularProgramStructure)
			{
				$regularProgramStructure = RegularProgramStructure::find($singleRegularProgramStructure);
								$regularProgram->regularProgramStructures()->save($regularProgramStructure);
							}
		}
		$unsavedregularProgramStructures = $unsavedRelationships['regularProgramStructures'];
		foreach($unsavedregularProgramStructures as $unsavedregularProgramStructure)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramStructure);
			if(!empty($modelInstance))
			{
								$regularProgram->regularProgramStructures()->save($modelInstance);
							}
		}
						$regularProgram->save();
		return redirect('/modules/regularPrograms');
	}
	
	public function delete($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgram->delete();
		return redirect('/modules/regularPrograms');
	}
	
	public function regularProgram($id)
	{
		$regularProgram = RegularProgram::with('regularProgramEvents','regularProgramFeatures','regularProgramFormats','regularProgramSemesters','regularProgramStructures')->find($id);
		return response()->json($regularProgram);
	}
	
		public function regularProgramEvents($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgramEvents = $regularProgram->regularProgramEvents;
		for ($i = 0; $i < count($regularProgramEvents); $i++)
		{
			$regularProgramEvent = $regularProgramEvents[$i];
			$regularProgramEvent->regularProgram = $regularProgram;
			$regularProgramEvents[$i] = $regularProgramEvent;
		}
		return view('modules.regularProgramEvents.all', ['regularProgramEvents' => $regularProgramEvents]);
	}
	
	public function regularProgramEventsApi($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgramEvents = $regularProgram->regularProgramEvents;
		return response()->json($regularProgramEvents->all());
	}
		public function regularProgramFeatures($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgramFeatures = $regularProgram->regularProgramFeatures;
		for ($i = 0; $i < count($regularProgramFeatures); $i++)
		{
			$regularProgramFeature = $regularProgramFeatures[$i];
			$regularProgramFeature->regularProgram = $regularProgram;
			$regularProgramFeatures[$i] = $regularProgramFeature;
		}
		return view('modules.regularProgramFeatures.all', ['regularProgramFeatures' => $regularProgramFeatures]);
	}
	
	public function regularProgramFeaturesApi($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgramFeatures = $regularProgram->regularProgramFeatures;
		return response()->json($regularProgramFeatures->all());
	}
		public function regularProgramFormats($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgramFormats = $regularProgram->regularProgramFormats;
		for ($i = 0; $i < count($regularProgramFormats); $i++)
		{
			$regularProgramFormat = $regularProgramFormats[$i];
			$regularProgramFormat->regularProgram = $regularProgram;
			$regularProgramFormats[$i] = $regularProgramFormat;
		}
		return view('modules.regularProgramFormats.all', ['regularProgramFormats' => $regularProgramFormats]);
	}
	
	public function regularProgramFormatsApi($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgramFormats = $regularProgram->regularProgramFormats;
		return response()->json($regularProgramFormats->all());
	}
		public function regularProgramSemesters($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgramSemesters = $regularProgram->regularProgramSemesters;
		for ($i = 0; $i < count($regularProgramSemesters); $i++)
		{
			$regularProgramSemester = $regularProgramSemesters[$i];
			$regularProgramSemester->regularProgram = $regularProgram;
			$regularProgramSemesters[$i] = $regularProgramSemester;
		}
		return view('modules.regularProgramSemesters.all', ['regularProgramSemesters' => $regularProgramSemesters]);
	}
	
	public function regularProgramSemestersApi($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgramSemesters = $regularProgram->regularProgramSemesters;
		return response()->json($regularProgramSemesters->all());
	}
		public function regularProgramStructures($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgramStructures = $regularProgram->regularProgramStructures;
		for ($i = 0; $i < count($regularProgramStructures); $i++)
		{
			$regularProgramStructure = $regularProgramStructures[$i];
			$regularProgramStructure->regularProgram = $regularProgram;
			$regularProgramStructures[$i] = $regularProgramStructure;
		}
		return view('modules.regularProgramStructures.all', ['regularProgramStructures' => $regularProgramStructures]);
	}
	
	public function regularProgramStructuresApi($id)
	{
		$regularProgram = RegularProgram::find($id);
		$regularProgramStructures = $regularProgram->regularProgramStructures;
		return response()->json($regularProgramStructures->all());
	}
	}
?>