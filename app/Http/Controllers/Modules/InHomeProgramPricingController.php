<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InHomeProgramPricing;
use App\Models\InHomeProgram;
use Log;

class InHomeProgramPricingController extends Controller
{	
	public function index()
	{
		$inHomeProgramPricings = InHomeProgramPricing::with('inHomeProgram')->get();
		return view('modules.inHomeProgramPricings.all', ['inHomeProgramPricings' => $inHomeProgramPricings]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = InHomeProgramPricing::with('inHomeProgram')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$inHomePrograms = InHomeProgram::all();
				return view('modules.inHomeProgramPricings.form', [
					'inHomePrograms' => $inHomePrograms,
			'allinHomePrograms' => InHomeProgram::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$inHomeProgramPricing = new InHomeProgramPricing();
				if(!empty($data['id']))
		{
			$inHomeProgramPricing->id = $data['id'];
		}
				if(!empty($data['student_numbers']))
		{
			$inHomeProgramPricing->student_numbers = $data['student_numbers'];
		}
				if(!empty($data['price']))
		{
			$inHomeProgramPricing->price = $data['price'];
		}
				if(!empty($data['in_home_program_id']))
		{
			$inHomeProgramPricing->in_home_program_id = $data['in_home_program_id'];
		}
				$inHomeProgramPricing->save();
						if(!empty($data['inHomeProgram']))
		{
			$inHomeProgram = InHomeProgram::find($data['inHomeProgram']);
						$inHomeProgramPricing->inHomeProgram()->associate($inHomeProgram);
					}
		$unsavedinHomeProgram = $unsavedRelationships['inHomeProgram'];
		if(!empty($unsavedinHomeProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
						$inHomeProgramPricing->inHomeProgram()->associate($modelInstance);
					}
				$inHomeProgramPricing->save();
		return redirect('/modules/inHomeProgramPricings');
	}
	
	public function get($id)
	{
		$inHomeProgramPricing = InHomeProgramPricing::with('inHomeProgram')->find($id);
				$inHomePrograms = InHomeProgram::all();
						return view('modules.inHomeProgramPricings.form', [
			'inHomeProgramPricing' => $inHomeProgramPricing,
							'inHomePrograms' => $inHomePrograms,
				'allinHomePrograms' => InHomeProgram::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$inHomeProgramPricing = InHomeProgramPricing::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$inHomeProgramPricing->id = $data['id'];
		}
				if(!empty($data['student_numbers']))
		{
			$inHomeProgramPricing->student_numbers = $data['student_numbers'];
		}
				if(!empty($data['price']))
		{
			$inHomeProgramPricing->price = $data['price'];
		}
				if(!empty($data['in_home_program_id']))
		{
			$inHomeProgramPricing->in_home_program_id = $data['in_home_program_id'];
		}
				$inHomeProgramPricing->save();
						if(!empty($data['inHomeProgram']))
		{
			$inHomeProgram = InHomeProgram::find($data['inHomeProgram']);
						$inHomeProgramPricing->inHomeProgram()->associate($inHomeProgram);
					}
		$unsavedinHomeProgram = $unsavedRelationships['inHomeProgram'];
		if(!empty($unsavedinHomeProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
						$inHomeProgramPricing->inHomeProgram()->associate($modelInstance);
					}
				$inHomeProgramPricing->save();
		return redirect('/modules/inHomeProgramPricings');
	}
	
	public function delete($id)
	{
		$inHomeProgramPricing = InHomeProgramPricing::find($id);
		$inHomeProgramPricing->delete();
		return redirect('/modules/inHomeProgramPricings');
	}
	
	public function inHomeProgramPricing($id)
	{
		$inHomeProgramPricing = InHomeProgramPricing::with('inHomeProgram')->find($id);
		return response()->json($inHomeProgramPricing);
	}
	
	}
?>