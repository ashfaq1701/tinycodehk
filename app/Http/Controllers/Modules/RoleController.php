<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;
use Log;

class RoleController extends Controller
{	
	public function index()
	{
		$roles = Role::with('users')->get();
		return view('modules.roles.all', ['roles' => $roles]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = Role::with('users')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				return view('modules.roles.form', [
					'allusers' => User::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$role = new Role();
				if(!empty($data['id']))
		{
			$role->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$role->name = $data['name'];
		}
				$role->save();
				if(!empty($data['users']))
		{
			$users = explode(',', $data['users']);
			foreach ($users as $singleUser)
			{
				$user = User::find($singleUser);
								$role->users()->save($user);
							}
		}
		$unsavedusers = $unsavedRelationships['users'];
		foreach($unsavedusers as $unsaveduser)
		{
			$modelInstance = getUnsavedRelationship($unsaveduser);
			if(!empty($modelInstance))
			{
								$role->users()->save($modelInstance);
							}
		}
						$role->save();
		return redirect('/modules/roles');
	}
	
	public function get($id)
	{
		$role = Role::with('users')->find($id);
						$usersArray = [];
		foreach($role->users as $user)
		{
			
			$usersArray[] = $user->id;
		}
		$usersValue = implode(',', $usersArray);
				return view('modules.roles.form', [
			'role' => $role,
							'allusers' => User::all(),
				'usersValue' => $usersValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$role = Role::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$role->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$role->name = $data['name'];
		}
				$role->save();
				if(!empty($data['users']))
		{
			$users = explode(',', $data['users']);
			foreach ($users as $singleUser)
			{
				$user = User::find($singleUser);
								$role->users()->save($user);
							}
		}
		$unsavedusers = $unsavedRelationships['users'];
		foreach($unsavedusers as $unsaveduser)
		{
			$modelInstance = getUnsavedRelationship($unsaveduser);
			if(!empty($modelInstance))
			{
								$role->users()->save($modelInstance);
							}
		}
						$role->save();
		return redirect('/modules/roles');
	}
	
	public function delete($id)
	{
		$role = Role::find($id);
		$role->delete();
		return redirect('/modules/roles');
	}
	
	public function role($id)
	{
		$role = Role::with('users')->find($id);
		return response()->json($role);
	}
	
		public function users($id)
	{
		$role = Role::find($id);
		$users = $role->users;
		for ($i = 0; $i < count($users); $i++)
		{
			$user = $users[$i];
			$user->role = $role;
			$users[$i] = $user;
		}
		return view('modules.users.all', ['users' => $users]);
	}
	
	public function usersApi($id)
	{
		$role = Role::find($id);
		$users = $role->users;
		return response()->json($users->all());
	}
	}
?>