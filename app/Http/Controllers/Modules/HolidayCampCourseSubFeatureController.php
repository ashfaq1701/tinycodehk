<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HolidayCampCourseSubFeature;
use App\Models\HolidayCampCourseFeature;
use Log;

class HolidayCampCourseSubFeatureController extends Controller
{	
	public function index()
	{
		$holidayCampCourseSubFeatures = HolidayCampCourseSubFeature::with('holidayCampCourseFeature')->get();
		return view('modules.holidayCampCourseSubFeatures.all', ['holidayCampCourseSubFeatures' => $holidayCampCourseSubFeatures]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = HolidayCampCourseSubFeature::with('holidayCampCourseFeature')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$holidayCampCourseFeatures = HolidayCampCourseFeature::all();
				return view('modules.holidayCampCourseSubFeatures.form', [
					'holidayCampCourseFeatures' => $holidayCampCourseFeatures,
			'allholidayCampCourseFeatures' => HolidayCampCourseFeature::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$holidayCampCourseSubFeature = new HolidayCampCourseSubFeature();
				if(!empty($data['id']))
		{
			$holidayCampCourseSubFeature->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$holidayCampCourseSubFeature->name = $data['name'];
		}
				if(!empty($data['holiday_camp_course_feature_id']))
		{
			$holidayCampCourseSubFeature->holiday_camp_course_feature_id = $data['holiday_camp_course_feature_id'];
		}
				$holidayCampCourseSubFeature->save();
						if(!empty($data['holidayCampCourseFeature']))
		{
			$holidayCampCourseFeature = HolidayCampCourseFeature::find($data['holidayCampCourseFeature']);
						$holidayCampCourseSubFeature->holidayCampCourseFeature()->associate($holidayCampCourseFeature);
					}
		$unsavedholidayCampCourseFeature = $unsavedRelationships['holidayCampCourseFeature'];
		if(!empty($unsavedholidayCampCourseFeature))
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourseFeature);
						$holidayCampCourseSubFeature->holidayCampCourseFeature()->associate($modelInstance);
					}
				$holidayCampCourseSubFeature->save();
		return redirect('/modules/holidayCampCourseSubFeatures');
	}
	
	public function get($id)
	{
		$holidayCampCourseSubFeature = HolidayCampCourseSubFeature::with('holidayCampCourseFeature')->find($id);
				$holidayCampCourseFeatures = HolidayCampCourseFeature::all();
						return view('modules.holidayCampCourseSubFeatures.form', [
			'holidayCampCourseSubFeature' => $holidayCampCourseSubFeature,
							'holidayCampCourseFeatures' => $holidayCampCourseFeatures,
				'allholidayCampCourseFeatures' => HolidayCampCourseFeature::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$holidayCampCourseSubFeature = HolidayCampCourseSubFeature::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$holidayCampCourseSubFeature->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$holidayCampCourseSubFeature->name = $data['name'];
		}
				if(!empty($data['holiday_camp_course_feature_id']))
		{
			$holidayCampCourseSubFeature->holiday_camp_course_feature_id = $data['holiday_camp_course_feature_id'];
		}
				$holidayCampCourseSubFeature->save();
						if(!empty($data['holidayCampCourseFeature']))
		{
			$holidayCampCourseFeature = HolidayCampCourseFeature::find($data['holidayCampCourseFeature']);
						$holidayCampCourseSubFeature->holidayCampCourseFeature()->associate($holidayCampCourseFeature);
					}
		$unsavedholidayCampCourseFeature = $unsavedRelationships['holidayCampCourseFeature'];
		if(!empty($unsavedholidayCampCourseFeature))
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourseFeature);
						$holidayCampCourseSubFeature->holidayCampCourseFeature()->associate($modelInstance);
					}
				$holidayCampCourseSubFeature->save();
		return redirect('/modules/holidayCampCourseSubFeatures');
	}
	
	public function delete($id)
	{
		$holidayCampCourseSubFeature = HolidayCampCourseSubFeature::find($id);
		$holidayCampCourseSubFeature->delete();
		return redirect('/modules/holidayCampCourseSubFeatures');
	}
	
	public function holidayCampCourseSubFeature($id)
	{
		$holidayCampCourseSubFeature = HolidayCampCourseSubFeature::with('holidayCampCourseFeature')->find($id);
		return response()->json($holidayCampCourseSubFeature);
	}
	
	}
?>