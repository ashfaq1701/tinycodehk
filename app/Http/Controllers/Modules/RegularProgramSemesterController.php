<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RegularProgramSemester;
use App\Models\RegularProgramClasseSchedule;
use App\Models\RegularProgram;
use App\Models\Enrollment;
use Log;

class RegularProgramSemesterController extends Controller {
	public function index() {
		$regularProgramSemesters = RegularProgramSemester::with ( 'regularProgramClasseSchedules', 'regularProgram' )->get ();
		return view ( 'modules.regularProgramSemesters.all', [ 
				'regularProgramSemesters' => $regularProgramSemesters 
		] );
	}
	public function search(Request $request) {
		$per_page = \Request::get ( 'per_page' ) ?: 10;
		if ($request ['query']) {
			$request = RegularProgramSemester::with ( 'regularProgramClasseSchedules', 'regularProgram' )->search ( $request ['query'] )->get ();
			$page = $request->has ( 'page' ) ? $request->page - 1 : 0;
			$total = $request->count ();
			$request = $request->slice ( $page * $per_page, $per_page );
			$request = new \Illuminate\Pagination\LengthAwarePaginator ( $request, $total, $per_page );
			return $request;
		}
		return 'not found';
	}
	public function create() {
		$regularPrograms = RegularProgram::all ();
		return view ( 'modules.regularProgramSemesters.form', [ 
			'regularPrograms' => $regularPrograms,
			'allregularPrograms' => RegularProgram::all (),
			'allregularProgramClasseSchedules' => RegularProgramClasseSchedule::all (),
			'allEnrollments' => Enrollment::all()
		] );
	}
	public function store(Request $request) {
		$this->validate ( $request, [ ] );
		$data = $request->all ();
		$unsavedRelationshipsJSON = $data ['unsavedRelationships'];
		$unsavedRelationships = json_decode ( $unsavedRelationshipsJSON, true );
		$regularProgramSemester = new RegularProgramSemester ();
		if (! empty ( $data ['id'] )) {
			$regularProgramSemester->id = $data ['id'];
		}
		if (! empty ( $data ['year'] )) {
			$regularProgramSemester->year = $data ['year'];
		}
		if (! empty ( $data ['name'] )) {
			$regularProgramSemester->name = $data ['name'];
		}
		if (! empty ( $data ['from_month'] )) {
			$regularProgramSemester->from_month = $data ['from_month'];
		}
		if (! empty ( $data ['to_month'] )) {
			$regularProgramSemester->to_month = $data ['to_month'];
		}
		if (! empty ( $data ['duration_weeks'] )) {
			$regularProgramSemester->duration_weeks = $data ['duration_weeks'];
		}
		if (! empty ( $data ['regular_price'] )) {
			$regularProgramSemester->regular_price = $data ['regular_price'];
		}
		if (! empty ( $data ['regular_program_id'] )) {
			$regularProgramSemester->regular_program_id = $data ['regular_program_id'];
		}
		$regularProgramSemester->save ();
		if (! empty ( $data ['regularProgramClasseSchedules'] )) {
			$regularProgramClasseSchedules = explode ( ',', $data ['regularProgramClasseSchedules'] );
			foreach ( $regularProgramClasseSchedules as $singleRegularProgramClasseSchedule ) {
				$regularProgramClasseSchedule = RegularProgramClasseSchedule::find ( $singleRegularProgramClasseSchedule );
				$regularProgramSemester->regularProgramClasseSchedules ()->save ( $regularProgramClasseSchedule );
			}
		}
		$unsavedregularProgramClasseSchedules = $unsavedRelationships ['regularProgramClasseSchedules'];
		foreach ( $unsavedregularProgramClasseSchedules as $unsavedregularProgramClasseSchedule ) {
			$modelInstance = getUnsavedRelationship ( $unsavedregularProgramClasseSchedule );
			if (! empty ( $modelInstance )) {
				$regularProgramSemester->regularProgramClasseSchedules ()->save ( $modelInstance );
			}
		}
		if(! empty ($data['enrollments'])) {
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
				$regularProgramSemester->enrollments()->save($enrollment);
			}
		}
		$unsavedEnrollments = $unsavedRelationships['enrollments'];
		foreach ($unsavedEnrollments as $unsavedEnrollment)
		{
			$modelInstance = getUnsavedRelationship ( $unsavedEnrollment );
			if(!empty($modelInstance))
			{
				$regularProgramSemester->enrollments()->save($modelInstance);
			}
		}
		if (! empty ( $data ['regularProgram'] )) {
			$regularProgram = RegularProgram::find ( $data ['regularProgram'] );
			$regularProgramSemester->regularProgram ()->associate ( $regularProgram );
		}
		$unsavedregularProgram = $unsavedRelationships ['regularProgram'];
		if (! empty ( $unsavedregularProgram )) {
			$modelInstance = getUnsavedRelationship ( $unsavedregularProgram );
			$regularProgramSemester->regularProgram ()->associate ( $modelInstance );
		}
		$regularProgramSemester->save ();
		return redirect ( '/modules/regularProgramSemesters' );
	}
	public function get($id) {
		$regularProgramSemester = RegularProgramSemester::with ( 'regularProgramClasseSchedules', 'regularProgram' )->find ( $id );
		$regularPrograms = RegularProgram::all ();
		$regularProgramClasseSchedulesArray = [ ];
		foreach ( $regularProgramSemester->regularProgramClasseSchedules as $regularProgramClasseSchedule ) {
			
			$regularProgramClasseSchedulesArray [] = $regularProgramClasseSchedule->id;
		}
		$regularProgramClasseSchedulesValue = implode ( ',', $regularProgramClasseSchedulesArray );
		$enrollmentsArray = [];
		foreach ($regularProgramSemester->enrollments as $enrollment)
		{
			$enrollmentsArray[] = $enrollment->id;
		}
		$enrollmentsValue = implode(',', $enrollmentsArray);
		return view ( 'modules.regularProgramSemesters.form', [ 
				'regularProgramSemester' => $regularProgramSemester,
				'regularPrograms' => $regularPrograms,
				'allregularPrograms' => RegularProgram::all (),
				'allregularProgramClasseSchedules' => RegularProgramClasseSchedule::all (),
				'regularProgramClasseSchedulesValue' => $regularProgramClasseSchedulesValue,
				'allEnrollments' => Enrollment::all(),
				'enrollmentsValue' => $enrollmentsValue
		] );
	}
	public function update(Request $request, $id) {
		$this->validate ( $request, [ ] );
		$regularProgramSemester = RegularProgramSemester::find ( $id );
		$data = $request->all ();
		$unsavedRelationshipsJSON = $data ['unsavedRelationships'];
		$unsavedRelationships = json_decode ( $unsavedRelationshipsJSON, true );
		if (! empty ( $data ['id'] )) {
			$regularProgramSemester->id = $data ['id'];
		}
		if (! empty ( $data ['year'] )) {
			$regularProgramSemester->year = $data ['year'];
		}
		if (! empty ( $data ['name'] )) {
			$regularProgramSemester->name = $data ['name'];
		}
		if (! empty ( $data ['from_month'] )) {
			$regularProgramSemester->from_month = $data ['from_month'];
		}
		if (! empty ( $data ['to_month'] )) {
			$regularProgramSemester->to_month = $data ['to_month'];
		}
		if (! empty ( $data ['duration_weeks'] )) {
			$regularProgramSemester->duration_weeks = $data ['duration_weeks'];
		}
		if (! empty ( $data ['regular_price'] )) {
			$regularProgramSemester->regular_price = $data ['regular_price'];
		}
		if (! empty ( $data ['regular_program_id'] )) {
			$regularProgramSemester->regular_program_id = $data ['regular_program_id'];
		}
		$regularProgramSemester->save ();
		
		if(! empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
				$regularProgramSemester->enrollments()->save($enrollment);
			}
		}
		$unsavedEnrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedEnrollments as $unsavedEnrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedEnrollment);
			if(!empty($modelInstance))
			{
				$regularProgramSemester->enrollments()->save($modelInstance);
			}
		}
		
		if (! empty ( $data ['regularProgramClasseSchedules'] )) {
			$regularProgramClasseSchedules = explode ( ',', $data ['regularProgramClasseSchedules'] );
			foreach ( $regularProgramClasseSchedules as $singleRegularProgramClasseSchedule ) {
				$regularProgramClasseSchedule = RegularProgramClasseSchedule::find ( $singleRegularProgramClasseSchedule );
				$regularProgramSemester->regularProgramClasseSchedules ()->save ( $regularProgramClasseSchedule );
			}
		}
		$unsavedregularProgramClasseSchedules = $unsavedRelationships ['regularProgramClasseSchedules'];
		foreach ( $unsavedregularProgramClasseSchedules as $unsavedregularProgramClasseSchedule ) {
			$modelInstance = getUnsavedRelationship ( $unsavedregularProgramClasseSchedule );
			if (! empty ( $modelInstance )) {
				$regularProgramSemester->regularProgramClasseSchedules ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['regularProgram'] )) {
			$regularProgram = RegularProgram::find ( $data ['regularProgram'] );
			$regularProgramSemester->regularProgram ()->associate ( $regularProgram );
		}
		$unsavedregularProgram = $unsavedRelationships ['regularProgram'];
		if (! empty ( $unsavedregularProgram )) {
			$modelInstance = getUnsavedRelationship ( $unsavedregularProgram );
			$regularProgramSemester->regularProgram ()->associate ( $modelInstance );
		}
		$regularProgramSemester->save ();
		return redirect ( '/modules/regularProgramSemesters' );
	}
	public function delete($id) {
		$regularProgramSemester = RegularProgramSemester::find ( $id );
		$regularProgramSemester->delete ();
		return redirect ( '/modules/regularProgramSemesters' );
	}
	public function regularProgramSemester($id) {
		$regularProgramSemester = RegularProgramSemester::with ( 'regularProgramClasseSchedules', 'regularProgram' )->find ( $id );
		return response ()->json ( $regularProgramSemester );
	}
	public function regularProgramClasseSchedules($id) {
		$regularProgramSemester = RegularProgramSemester::find ( $id );
		$regularProgramClasseSchedules = $regularProgramSemester->regularProgramClasseSchedules;
		for($i = 0; $i < count ( $regularProgramClasseSchedules ); $i ++) {
			$regularProgramClasseSchedule = $regularProgramClasseSchedules [$i];
			$regularProgramClasseSchedule->regularProgramSemester = $regularProgramSemester;
			$regularProgramClasseSchedules [$i] = $regularProgramClasseSchedule;
		}
		return view ( 'modules.regularProgramClasseSchedules.all', [ 
				'regularProgramClasseSchedules' => $regularProgramClasseSchedules 
		] );
	}
	public function regularProgramClasseSchedulesApi($id) {
		$regularProgramSemester = RegularProgramSemester::find ( $id );
		$regularProgramClasseSchedules = $regularProgramSemester->regularProgramClasseSchedules;
		return response()->json($regularProgramClasseSchedules->all());
	}
	public function enrollments($id)
	{
		$regularProgramSemester = RegularProgramSemester::with('enrollments', 'enrollments.course')->find($id);
		$enrollments = $regularProgramSemester->enrollments;
		return view('modules.enrollments.all', [
			'enrollments' => $enrollments	
		]);
	}
	public function enrollmentsApi($id)
	{
		$regularProgramSemester = RegularProgramSemester::find($id);
		$enrollments = $regularProgramSemester->enrollments;
		return response()->json($enrollments->all());
	}
}
?>