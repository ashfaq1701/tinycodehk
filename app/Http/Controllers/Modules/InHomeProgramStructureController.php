<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InHomeProgramStructure;
use App\Models\InHomeProgram;
use Log;

class InHomeProgramStructureController extends Controller
{	
	public function index()
	{
		$inHomeProgramStructures = InHomeProgramStructure::with('inHomeProgram')->get();
		return view('modules.inHomeProgramStructures.all', ['inHomeProgramStructures' => $inHomeProgramStructures]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = InHomeProgramStructure::with('inHomeProgram')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$inHomePrograms = InHomeProgram::all();
				return view('modules.inHomeProgramStructures.form', [
					'inHomePrograms' => $inHomePrograms,
			'allinHomePrograms' => InHomeProgram::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$inHomeProgramStructure = new InHomeProgramStructure();
				if(!empty($data['id']))
		{
			$inHomeProgramStructure->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$inHomeProgramStructure->title = $data['title'];
		}
				if(!empty($data['value']))
		{
			$inHomeProgramStructure->value = $data['value'];
		}
				if(!empty($data['in_home_program_id']))
		{
			$inHomeProgramStructure->in_home_program_id = $data['in_home_program_id'];
		}
				$inHomeProgramStructure->save();
						if(!empty($data['inHomeProgram']))
		{
			$inHomeProgram = InHomeProgram::find($data['inHomeProgram']);
						$inHomeProgramStructure->inHomeProgram()->associate($inHomeProgram);
					}
		$unsavedinHomeProgram = $unsavedRelationships['inHomeProgram'];
		if(!empty($unsavedinHomeProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
						$inHomeProgramStructure->inHomeProgram()->associate($modelInstance);
					}
				$inHomeProgramStructure->save();
		return redirect('/modules/inHomeProgramStructures');
	}
	
	public function get($id)
	{
		$inHomeProgramStructure = InHomeProgramStructure::with('inHomeProgram')->find($id);
				$inHomePrograms = InHomeProgram::all();
						return view('modules.inHomeProgramStructures.form', [
			'inHomeProgramStructure' => $inHomeProgramStructure,
							'inHomePrograms' => $inHomePrograms,
				'allinHomePrograms' => InHomeProgram::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$inHomeProgramStructure = InHomeProgramStructure::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$inHomeProgramStructure->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$inHomeProgramStructure->title = $data['title'];
		}
				if(!empty($data['value']))
		{
			$inHomeProgramStructure->value = $data['value'];
		}
				if(!empty($data['in_home_program_id']))
		{
			$inHomeProgramStructure->in_home_program_id = $data['in_home_program_id'];
		}
				$inHomeProgramStructure->save();
						if(!empty($data['inHomeProgram']))
		{
			$inHomeProgram = InHomeProgram::find($data['inHomeProgram']);
						$inHomeProgramStructure->inHomeProgram()->associate($inHomeProgram);
					}
		$unsavedinHomeProgram = $unsavedRelationships['inHomeProgram'];
		if(!empty($unsavedinHomeProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
						$inHomeProgramStructure->inHomeProgram()->associate($modelInstance);
					}
				$inHomeProgramStructure->save();
		return redirect('/modules/inHomeProgramStructures');
	}
	
	public function delete($id)
	{
		$inHomeProgramStructure = InHomeProgramStructure::find($id);
		$inHomeProgramStructure->delete();
		return redirect('/modules/inHomeProgramStructures');
	}
	
	public function inHomeProgramStructure($id)
	{
		$inHomeProgramStructure = InHomeProgramStructure::with('inHomeProgram')->find($id);
		return response()->json($inHomeProgramStructure);
	}
	
	}
?>