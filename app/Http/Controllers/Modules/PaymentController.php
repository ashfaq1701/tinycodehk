<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Enrollment;
use App\Models\User;
use Log;

class PaymentController extends Controller
{	
	public function index()
	{
		$payments = Payment::with('enrollment','user')->get();
		return view('modules.payments.all', ['payments' => $payments]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = Payment::with('enrollment','user')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$enrollments = Enrollment::all();
				$users = User::all();
				return view('modules.payments.form', [
					'enrollments' => $enrollments,
			'allenrollments' => Enrollment::all(),
					'users' => $users,
			'allusers' => User::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$payment = new Payment();
				if(!empty($data['id']))
		{
			$payment->id = $data['id'];
		}
				if(!empty($data['amount']))
		{
			$payment->amount = $data['amount'];
		}
				if(!empty($data['enrollment_id']))
		{
			$payment->enrollment_id = $data['enrollment_id'];
		}
				if(!empty($data['user_id']))
		{
			$payment->user_id = $data['user_id'];
		}
				if(!empty($data['invoice_id']))
		{
			$payment->invoice_id = $data['invoice_id'];
		}
				if(!empty($data['charge_id']))
		{
			$payment->charge_id = $data['charge_id'];
		}
				$payment->save();
						if(!empty($data['enrollment']))
		{
			$enrollment = Enrollment::find($data['enrollment']);
						$payment->enrollment()->associate($enrollment);
					}
		$unsavedenrollment = $unsavedRelationships['enrollment'];
		if(!empty($unsavedenrollment))
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
						$payment->enrollment()->associate($modelInstance);
					}
				if(!empty($data['user']))
		{
			$user = User::find($data['user']);
						$payment->user()->associate($user);
					}
		$unsaveduser = $unsavedRelationships['user'];
		if(!empty($unsaveduser))
		{
			$modelInstance = getUnsavedRelationship($unsaveduser);
						$payment->user()->associate($modelInstance);
					}
				$payment->save();
		return redirect('/modules/payments');
	}
	
	public function get($id)
	{
		$payment = Payment::with('enrollment','user')->find($id);
				$enrollments = Enrollment::all();
				$users = User::all();
						return view('modules.payments.form', [
			'payment' => $payment,
							'enrollments' => $enrollments,
				'allenrollments' => Enrollment::all(),
							'users' => $users,
				'allusers' => User::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$payment = Payment::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$payment->id = $data['id'];
		}
				if(!empty($data['amount']))
		{
			$payment->amount = $data['amount'];
		}
				if(!empty($data['enrollment_id']))
		{
			$payment->enrollment_id = $data['enrollment_id'];
		}
				if(!empty($data['user_id']))
		{
			$payment->user_id = $data['user_id'];
		}
				if(!empty($data['invoice_id']))
		{
			$payment->invoice_id = $data['invoice_id'];
		}
				if(!empty($data['charge_id']))
		{
			$payment->charge_id = $data['charge_id'];
		}
				$payment->save();
						if(!empty($data['enrollment']))
		{
			$enrollment = Enrollment::find($data['enrollment']);
						$payment->enrollment()->associate($enrollment);
					}
		$unsavedenrollment = $unsavedRelationships['enrollment'];
		if(!empty($unsavedenrollment))
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
						$payment->enrollment()->associate($modelInstance);
					}
				if(!empty($data['user']))
		{
			$user = User::find($data['user']);
						$payment->user()->associate($user);
					}
		$unsaveduser = $unsavedRelationships['user'];
		if(!empty($unsaveduser))
		{
			$modelInstance = getUnsavedRelationship($unsaveduser);
						$payment->user()->associate($modelInstance);
					}
				$payment->save();
		return redirect('/modules/payments');
	}
	
	public function delete($id)
	{
		$payment = Payment::find($id);
		$payment->delete();
		return redirect('/modules/payments');
	}
	
	public function payment($id)
	{
		$payment = Payment::with('enrollment','user')->find($id);
		return response()->json($payment);
	}
	
	}
?>