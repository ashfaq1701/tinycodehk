<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Testimonial;
use App\Models\Enrollment;
use Log;

class TestimonialController extends Controller
{	
	public function index()
	{
		$testimonials = Testimonial::with('enrollment')->get();
		return view('modules.testimonials.all', ['testimonials' => $testimonials]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = Testimonial::with('enrollment')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$enrollments = Enrollment::all();
				return view('modules.testimonials.form', [
					'enrollments' => $enrollments,
			'allenrollments' => Enrollment::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$testimonial = new Testimonial();
				if(!empty($data['id']))
		{
			$testimonial->id = $data['id'];
		}
				if(!empty($data['testimonial']))
		{
			$testimonial->testimonial = $data['testimonial'];
		}
				if(!empty($data['student_name']))
		{
			$testimonial->student_name = $data['student_name'];
		}
				if(!empty($data['class']))
		{
			$testimonial->class = $data['class'];
		}
				if(!empty($data['school']))
		{
			$testimonial->school = $data['school'];
		}
				if(!empty($data['course']))
		{
			$testimonial->course = $data['course'];
		}
				if(!empty($data['roll']))
		{
			$testimonial->roll = $data['roll'];
		}
				if(!empty($data['term']))
		{
			$testimonial->term = $data['term'];
		}
				if(!empty($data['year']))
		{
			$testimonial->year = $data['year'];
		}
				if(!empty($data['enrollment_id']))
		{
			$testimonial->enrollment_id = $data['enrollment_id'];
		}
				$testimonial->save();
						if(!empty($data['enrollment']))
		{
			$enrollment = Enrollment::find($data['enrollment']);
						$testimonial->enrollment()->associate($enrollment);
					}
		$unsavedenrollment = $unsavedRelationships['enrollment'];
		if(!empty($unsavedenrollment))
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
						$testimonial->enrollment()->associate($modelInstance);
					}
				$testimonial->save();
		return redirect('/modules/testimonials');
	}
	
	public function get($id)
	{
		$testimonial = Testimonial::with('enrollment')->find($id);
				$enrollments = Enrollment::all();
						return view('modules.testimonials.form', [
			'testimonial' => $testimonial,
							'enrollments' => $enrollments,
				'allenrollments' => Enrollment::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$testimonial = Testimonial::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$testimonial->id = $data['id'];
		}
				if(!empty($data['testimonial']))
		{
			$testimonial->testimonial = $data['testimonial'];
		}
				if(!empty($data['student_name']))
		{
			$testimonial->student_name = $data['student_name'];
		}
				if(!empty($data['class']))
		{
			$testimonial->class = $data['class'];
		}
				if(!empty($data['school']))
		{
			$testimonial->school = $data['school'];
		}
				if(!empty($data['course']))
		{
			$testimonial->course = $data['course'];
		}
				if(!empty($data['roll']))
		{
			$testimonial->roll = $data['roll'];
		}
				if(!empty($data['term']))
		{
			$testimonial->term = $data['term'];
		}
				if(!empty($data['year']))
		{
			$testimonial->year = $data['year'];
		}
				if(!empty($data['enrollment_id']))
		{
			$testimonial->enrollment_id = $data['enrollment_id'];
		}
				$testimonial->save();
						if(!empty($data['enrollment']))
		{
			$enrollment = Enrollment::find($data['enrollment']);
						$testimonial->enrollment()->associate($enrollment);
					}
		$unsavedenrollment = $unsavedRelationships['enrollment'];
		if(!empty($unsavedenrollment))
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
						$testimonial->enrollment()->associate($modelInstance);
					}
				$testimonial->save();
		return redirect('/modules/testimonials');
	}
	
	public function delete($id)
	{
		$testimonial = Testimonial::find($id);
		$testimonial->delete();
		return redirect('/modules/testimonials');
	}
	
	public function testimonial($id)
	{
		$testimonial = Testimonial::with('enrollment')->find($id);
		return response()->json($testimonial);
	}
	
	}
?>