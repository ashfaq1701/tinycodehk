<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HolidayCampCourse;
use App\Models\HolidayCampCourseBringing;
use App\Models\HolidayCampCourseFeature;
use App\Models\HolidayCampCourseHighlight;
use App\Models\HolidayCampModule;
use App\Models\HolidayCamp;
use App\Models\ProgramLevel;
use App\Models\Location;
use Log;

class HolidayCampCourseController extends Controller
{	
	public function index()
	{
		$holidayCampCourses = HolidayCampCourse::with('holidayCampCourseBringings','holidayCampCourseFeatures','holidayCampCourseHighlights','holidayCampModules','locations','holidayCamp','programLevel')->get();
		return view('modules.holidayCampCourses.all', ['holidayCampCourses' => $holidayCampCourses]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = HolidayCampCourse::with('holidayCampCourseBringings','holidayCampCourseFeatures','holidayCampCourseHighlights','holidayCampModules','locations','holidayCamp','programLevel')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$holidayCamps = HolidayCamp::all();
				$programLevels = ProgramLevel::all();
				return view('modules.holidayCampCourses.form', [
					'holidayCamps' => $holidayCamps,
			'allholidayCamps' => HolidayCamp::all(),
					'programLevels' => $programLevels,
			'allprogramLevels' => ProgramLevel::all(),
					'allholidayCampCourseBringings' => HolidayCampCourseBringing::all(),
					'allholidayCampCourseFeatures' => HolidayCampCourseFeature::all(),
					'allholidayCampCourseHighlights' => HolidayCampCourseHighlight::all(),
					'allholidayCampModules' => HolidayCampModule::all(),
					'alllocations' => Location::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$holidayCampCourse = new HolidayCampCourse();
				if(!empty($data['id']))
		{
			$holidayCampCourse->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$holidayCampCourse->name = $data['name'];
		}
				if(!empty($data['program_icon']))
		{
			$holidayCampCourse->program_icon = $data['program_icon'];
		}
				if(!empty($data['program_cover_image']))
		{
			$holidayCampCourse->program_cover_image = $data['program_cover_image'];
		}
				if(!empty($data['program_video_url']))
		{
			$holidayCampCourse->program_video_url = $data['program_video_url'];
		}
				if(!empty($data['language']))
		{
			$holidayCampCourse->language = $data['language'];
		}
				if(!empty($data['description']))
		{
			$holidayCampCourse->description = $data['description'];
		}
				if(!empty($data['holiday_camp_id']))
		{
			$holidayCampCourse->holiday_camp_id = $data['holiday_camp_id'];
		}
				if(!empty($data['program_level_id']))
		{
			$holidayCampCourse->program_level_id = $data['program_level_id'];
		}
				$holidayCampCourse->save();
				if(!empty($data['holidayCampCourseBringings']))
		{
			$holidayCampCourseBringings = explode(',', $data['holidayCampCourseBringings']);
			foreach ($holidayCampCourseBringings as $singleHolidayCampCourseBringing)
			{
				$holidayCampCourseBringing = HolidayCampCourseBringing::find($singleHolidayCampCourseBringing);
								$holidayCampCourse->holidayCampCourseBringings()->save($holidayCampCourseBringing);
							}
		}
		$unsavedholidayCampCourseBringings = $unsavedRelationships['holidayCampCourseBringings'];
		foreach($unsavedholidayCampCourseBringings as $unsavedholidayCampCourseBringing)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourseBringing);
			if(!empty($modelInstance))
			{
								$holidayCampCourse->holidayCampCourseBringings()->save($modelInstance);
							}
		}
				if(!empty($data['holidayCampCourseFeatures']))
		{
			$holidayCampCourseFeatures = explode(',', $data['holidayCampCourseFeatures']);
			foreach ($holidayCampCourseFeatures as $singleHolidayCampCourseFeature)
			{
				$holidayCampCourseFeature = HolidayCampCourseFeature::find($singleHolidayCampCourseFeature);
								$holidayCampCourse->holidayCampCourseFeatures()->save($holidayCampCourseFeature);
							}
		}
		$unsavedholidayCampCourseFeatures = $unsavedRelationships['holidayCampCourseFeatures'];
		foreach($unsavedholidayCampCourseFeatures as $unsavedholidayCampCourseFeature)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourseFeature);
			if(!empty($modelInstance))
			{
								$holidayCampCourse->holidayCampCourseFeatures()->save($modelInstance);
							}
		}
				if(!empty($data['holidayCampCourseHighlights']))
		{
			$holidayCampCourseHighlights = explode(',', $data['holidayCampCourseHighlights']);
			foreach ($holidayCampCourseHighlights as $singleHolidayCampCourseHighlight)
			{
				$holidayCampCourseHighlight = HolidayCampCourseHighlight::find($singleHolidayCampCourseHighlight);
								$holidayCampCourse->holidayCampCourseHighlights()->save($holidayCampCourseHighlight);
							}
		}
		$unsavedholidayCampCourseHighlights = $unsavedRelationships['holidayCampCourseHighlights'];
		foreach($unsavedholidayCampCourseHighlights as $unsavedholidayCampCourseHighlight)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourseHighlight);
			if(!empty($modelInstance))
			{
								$holidayCampCourse->holidayCampCourseHighlights()->save($modelInstance);
							}
		}
				if(!empty($data['holidayCampModules']))
		{
			$holidayCampModules = explode(',', $data['holidayCampModules']);
			foreach ($holidayCampModules as $singleHolidayCampModule)
			{
				$holidayCampModule = HolidayCampModule::find($singleHolidayCampModule);
								$holidayCampCourse->holidayCampModules()->save($holidayCampModule);
							}
		}
		$unsavedholidayCampModules = $unsavedRelationships['holidayCampModules'];
		foreach($unsavedholidayCampModules as $unsavedholidayCampModule)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampModule);
			if(!empty($modelInstance))
			{
								$holidayCampCourse->holidayCampModules()->save($modelInstance);
							}
		}
				if(!empty($data['locations']))
		{
			$locations = explode(',', $data['locations']);
			foreach ($locations as $singleLocation)
			{
				$location = Location::find($singleLocation);
								$holidayCampCourse->locations()->attach($location);
							}
		}
		$unsavedlocations = $unsavedRelationships['locations'];
		foreach($unsavedlocations as $unsavedlocation)
		{
			$modelInstance = getUnsavedRelationship($unsavedlocation);
			if(!empty($modelInstance))
			{
								$holidayCampCourse->locations()->attach($modelInstance);
							}
		}
						if(!empty($data['holidayCamp']))
		{
			$holidayCamp = HolidayCamp::find($data['holidayCamp']);
						$holidayCampCourse->holidayCamp()->associate($holidayCamp);
					}
		$unsavedholidayCamp = $unsavedRelationships['holidayCamp'];
		if(!empty($unsavedholidayCamp))
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCamp);
						$holidayCampCourse->holidayCamp()->associate($modelInstance);
					}
				if(!empty($data['programLevel']))
		{
			$programLevel = ProgramLevel::find($data['programLevel']);
						$holidayCampCourse->programLevel()->associate($programLevel);
					}
		$unsavedprogramLevel = $unsavedRelationships['programLevel'];
		if(!empty($unsavedprogramLevel))
		{
			$modelInstance = getUnsavedRelationship($unsavedprogramLevel);
						$holidayCampCourse->programLevel()->associate($modelInstance);
					}
				$holidayCampCourse->save();
		return redirect('/modules/holidayCampCourses');
	}
	
	public function get($id)
	{
		$holidayCampCourse = HolidayCampCourse::with('holidayCampCourseBringings','holidayCampCourseFeatures','holidayCampCourseHighlights','holidayCampModules','locations','holidayCamp','programLevel')->find($id);
				$holidayCamps = HolidayCamp::all();
				$programLevels = ProgramLevel::all();
						$holidayCampCourseBringingsArray = [];
		foreach($holidayCampCourse->holidayCampCourseBringings as $holidayCampCourseBringing)
		{
			
			$holidayCampCourseBringingsArray[] = $holidayCampCourseBringing->id;
		}
		$holidayCampCourseBringingsValue = implode(',', $holidayCampCourseBringingsArray);
				$holidayCampCourseFeaturesArray = [];
		foreach($holidayCampCourse->holidayCampCourseFeatures as $holidayCampCourseFeature)
		{
			
			$holidayCampCourseFeaturesArray[] = $holidayCampCourseFeature->id;
		}
		$holidayCampCourseFeaturesValue = implode(',', $holidayCampCourseFeaturesArray);
				$holidayCampCourseHighlightsArray = [];
		foreach($holidayCampCourse->holidayCampCourseHighlights as $holidayCampCourseHighlight)
		{
			
			$holidayCampCourseHighlightsArray[] = $holidayCampCourseHighlight->id;
		}
		$holidayCampCourseHighlightsValue = implode(',', $holidayCampCourseHighlightsArray);
				$holidayCampModulesArray = [];
		foreach($holidayCampCourse->holidayCampModules as $holidayCampModule)
		{
			
			$holidayCampModulesArray[] = $holidayCampModule->id;
		}
		$holidayCampModulesValue = implode(',', $holidayCampModulesArray);
				$locationsArray = [];
		foreach($holidayCampCourse->locations as $location)
		{
			
			$locationsArray[] = $location->id;
		}
		$locationsValue = implode(',', $locationsArray);
				return view('modules.holidayCampCourses.form', [
			'holidayCampCourse' => $holidayCampCourse,
							'holidayCamps' => $holidayCamps,
				'allholidayCamps' => HolidayCamp::all(),
							'programLevels' => $programLevels,
				'allprogramLevels' => ProgramLevel::all(),
							'allholidayCampCourseBringings' => HolidayCampCourseBringing::all(),
				'holidayCampCourseBringingsValue' => $holidayCampCourseBringingsValue,
							'allholidayCampCourseFeatures' => HolidayCampCourseFeature::all(),
				'holidayCampCourseFeaturesValue' => $holidayCampCourseFeaturesValue,
							'allholidayCampCourseHighlights' => HolidayCampCourseHighlight::all(),
				'holidayCampCourseHighlightsValue' => $holidayCampCourseHighlightsValue,
							'allholidayCampModules' => HolidayCampModule::all(),
				'holidayCampModulesValue' => $holidayCampModulesValue,
							'alllocations' => Location::all(),
				'locationsValue' => $locationsValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$holidayCampCourse = HolidayCampCourse::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$holidayCampCourse->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$holidayCampCourse->name = $data['name'];
		}
				if(!empty($data['program_icon']))
		{
			$holidayCampCourse->program_icon = $data['program_icon'];
		}
				if(!empty($data['program_cover_image']))
		{
			$holidayCampCourse->program_cover_image = $data['program_cover_image'];
		}
				if(!empty($data['program_video_url']))
		{
			$holidayCampCourse->program_video_url = $data['program_video_url'];
		}
				if(!empty($data['language']))
		{
			$holidayCampCourse->language = $data['language'];
		}
				if(!empty($data['description']))
		{
			$holidayCampCourse->description = $data['description'];
		}
				if(!empty($data['holiday_camp_id']))
		{
			$holidayCampCourse->holiday_camp_id = $data['holiday_camp_id'];
		}
				if(!empty($data['program_level_id']))
		{
			$holidayCampCourse->program_level_id = $data['program_level_id'];
		}
				$holidayCampCourse->save();
				if(!empty($data['holidayCampCourseBringings']))
		{
			$holidayCampCourseBringings = explode(',', $data['holidayCampCourseBringings']);
			foreach ($holidayCampCourseBringings as $singleHolidayCampCourseBringing)
			{
				$holidayCampCourseBringing = HolidayCampCourseBringing::find($singleHolidayCampCourseBringing);
								$holidayCampCourse->holidayCampCourseBringings()->save($holidayCampCourseBringing);
							}
		}
		$unsavedholidayCampCourseBringings = $unsavedRelationships['holidayCampCourseBringings'];
		foreach($unsavedholidayCampCourseBringings as $unsavedholidayCampCourseBringing)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourseBringing);
			if(!empty($modelInstance))
			{
								$holidayCampCourse->holidayCampCourseBringings()->save($modelInstance);
							}
		}
				if(!empty($data['holidayCampCourseFeatures']))
		{
			$holidayCampCourseFeatures = explode(',', $data['holidayCampCourseFeatures']);
			foreach ($holidayCampCourseFeatures as $singleHolidayCampCourseFeature)
			{
				$holidayCampCourseFeature = HolidayCampCourseFeature::find($singleHolidayCampCourseFeature);
								$holidayCampCourse->holidayCampCourseFeatures()->save($holidayCampCourseFeature);
							}
		}
		$unsavedholidayCampCourseFeatures = $unsavedRelationships['holidayCampCourseFeatures'];
		foreach($unsavedholidayCampCourseFeatures as $unsavedholidayCampCourseFeature)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourseFeature);
			if(!empty($modelInstance))
			{
								$holidayCampCourse->holidayCampCourseFeatures()->save($modelInstance);
							}
		}
				if(!empty($data['holidayCampCourseHighlights']))
		{
			$holidayCampCourseHighlights = explode(',', $data['holidayCampCourseHighlights']);
			foreach ($holidayCampCourseHighlights as $singleHolidayCampCourseHighlight)
			{
				$holidayCampCourseHighlight = HolidayCampCourseHighlight::find($singleHolidayCampCourseHighlight);
								$holidayCampCourse->holidayCampCourseHighlights()->save($holidayCampCourseHighlight);
							}
		}
		$unsavedholidayCampCourseHighlights = $unsavedRelationships['holidayCampCourseHighlights'];
		foreach($unsavedholidayCampCourseHighlights as $unsavedholidayCampCourseHighlight)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourseHighlight);
			if(!empty($modelInstance))
			{
								$holidayCampCourse->holidayCampCourseHighlights()->save($modelInstance);
							}
		}
				if(!empty($data['holidayCampModules']))
		{
			$holidayCampModules = explode(',', $data['holidayCampModules']);
			foreach ($holidayCampModules as $singleHolidayCampModule)
			{
				$holidayCampModule = HolidayCampModule::find($singleHolidayCampModule);
								$holidayCampCourse->holidayCampModules()->save($holidayCampModule);
							}
		}
		$unsavedholidayCampModules = $unsavedRelationships['holidayCampModules'];
		foreach($unsavedholidayCampModules as $unsavedholidayCampModule)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampModule);
			if(!empty($modelInstance))
			{
								$holidayCampCourse->holidayCampModules()->save($modelInstance);
							}
		}
				if(!empty($data['locations']))
		{
			$locations = explode(',', $data['locations']);
			foreach ($locations as $singleLocation)
			{
				$location = Location::find($singleLocation);
								$holidayCampCourse->locations()->attach($location);
							}
		}
		$unsavedlocations = $unsavedRelationships['locations'];
		foreach($unsavedlocations as $unsavedlocation)
		{
			$modelInstance = getUnsavedRelationship($unsavedlocation);
			if(!empty($modelInstance))
			{
								$holidayCampCourse->locations()->attach($modelInstance);
							}
		}
						if(!empty($data['holidayCamp']))
		{
			$holidayCamp = HolidayCamp::find($data['holidayCamp']);
						$holidayCampCourse->holidayCamp()->associate($holidayCamp);
					}
		$unsavedholidayCamp = $unsavedRelationships['holidayCamp'];
		if(!empty($unsavedholidayCamp))
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCamp);
						$holidayCampCourse->holidayCamp()->associate($modelInstance);
					}
				if(!empty($data['programLevel']))
		{
			$programLevel = ProgramLevel::find($data['programLevel']);
						$holidayCampCourse->programLevel()->associate($programLevel);
					}
		$unsavedprogramLevel = $unsavedRelationships['programLevel'];
		if(!empty($unsavedprogramLevel))
		{
			$modelInstance = getUnsavedRelationship($unsavedprogramLevel);
						$holidayCampCourse->programLevel()->associate($modelInstance);
					}
				$holidayCampCourse->save();
		return redirect('/modules/holidayCampCourses');
	}
	
	public function delete($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$holidayCampCourse->delete();
		return redirect('/modules/holidayCampCourses');
	}
	
	public function holidayCampCourse($id)
	{
		$holidayCampCourse = HolidayCampCourse::with('holidayCampCourseBringings','holidayCampCourseFeatures','holidayCampCourseHighlights','holidayCampModules','locations','holidayCamp','programLevel')->find($id);
		return response()->json($holidayCampCourse);
	}
	
		public function holidayCampCourseBringings($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$holidayCampCourseBringings = $holidayCampCourse->holidayCampCourseBringings;
		for ($i = 0; $i < count($holidayCampCourseBringings); $i++)
		{
			$holidayCampCourseBringing = $holidayCampCourseBringings[$i];
			$holidayCampCourseBringing->holidayCampCourse = $holidayCampCourse;
			$holidayCampCourseBringings[$i] = $holidayCampCourseBringing;
		}
		return view('modules.holidayCampCourseBringings.all', ['holidayCampCourseBringings' => $holidayCampCourseBringings]);
	}
	
	public function holidayCampCourseBringingsApi($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$holidayCampCourseBringings = $holidayCampCourse->holidayCampCourseBringings;
		return response()->json($holidayCampCourseBringings->all());
	}
		public function holidayCampCourseFeatures($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$holidayCampCourseFeatures = $holidayCampCourse->holidayCampCourseFeatures;
		for ($i = 0; $i < count($holidayCampCourseFeatures); $i++)
		{
			$holidayCampCourseFeature = $holidayCampCourseFeatures[$i];
			$holidayCampCourseFeature->holidayCampCourse = $holidayCampCourse;
			$holidayCampCourseFeatures[$i] = $holidayCampCourseFeature;
		}
		return view('modules.holidayCampCourseFeatures.all', ['holidayCampCourseFeatures' => $holidayCampCourseFeatures]);
	}
	
	public function holidayCampCourseFeaturesApi($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$holidayCampCourseFeatures = $holidayCampCourse->holidayCampCourseFeatures;
		return response()->json($holidayCampCourseFeatures->all());
	}
		public function holidayCampCourseHighlights($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$holidayCampCourseHighlights = $holidayCampCourse->holidayCampCourseHighlights;
		for ($i = 0; $i < count($holidayCampCourseHighlights); $i++)
		{
			$holidayCampCourseHighlight = $holidayCampCourseHighlights[$i];
			$holidayCampCourseHighlight->holidayCampCourse = $holidayCampCourse;
			$holidayCampCourseHighlights[$i] = $holidayCampCourseHighlight;
		}
		return view('modules.holidayCampCourseHighlights.all', ['holidayCampCourseHighlights' => $holidayCampCourseHighlights]);
	}
	
	public function holidayCampCourseHighlightsApi($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$holidayCampCourseHighlights = $holidayCampCourse->holidayCampCourseHighlights;
		return response()->json($holidayCampCourseHighlights->all());
	}
		public function holidayCampModules($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$holidayCampModules = $holidayCampCourse->holidayCampModules;
		for ($i = 0; $i < count($holidayCampModules); $i++)
		{
			$holidayCampModule = $holidayCampModules[$i];
			$holidayCampModule->holidayCampCourse = $holidayCampCourse;
			$holidayCampModules[$i] = $holidayCampModule;
		}
		return view('modules.holidayCampModules.all', ['holidayCampModules' => $holidayCampModules]);
	}
	
	public function holidayCampModulesApi($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$holidayCampModules = $holidayCampCourse->holidayCampModules;
		return response()->json($holidayCampModules->all());
	}
		public function locations($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$locations = $holidayCampCourse->locations;
		for ($i = 0; $i < count($locations); $i++)
		{
			$location = $locations[$i];
			$location->holidayCampCourse = $holidayCampCourse;
			$locations[$i] = $location;
		}
		return view('modules.locations.all', ['locations' => $locations]);
	}
	
	public function locationsApi($id)
	{
		$holidayCampCourse = HolidayCampCourse::find($id);
		$locations = $holidayCampCourse->locations;
		return response()->json($locations->all());
	}
	}
?>