<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RegularProgramEvent;
use App\Models\RegularProgram;
use Log;

class RegularProgramEventController extends Controller
{	
	public function index()
	{
		$regularProgramEvents = RegularProgramEvent::with('regularProgram')->get();
		return view('modules.regularProgramEvents.all', ['regularProgramEvents' => $regularProgramEvents]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = RegularProgramEvent::with('regularProgram')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$regularPrograms = RegularProgram::all();
				return view('modules.regularProgramEvents.form', [
					'regularPrograms' => $regularPrograms,
			'allregularPrograms' => RegularProgram::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$regularProgramEvent = new RegularProgramEvent();
				if(!empty($data['id']))
		{
			$regularProgramEvent->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$regularProgramEvent->title = $data['title'];
		}
				if(!empty($data['description']))
		{
			$regularProgramEvent->description = $data['description'];
		}
				if(!empty($data['regular_program_id']))
		{
			$regularProgramEvent->regular_program_id = $data['regular_program_id'];
		}
				$regularProgramEvent->save();
						if(!empty($data['regularProgram']))
		{
			$regularProgram = RegularProgram::find($data['regularProgram']);
						$regularProgramEvent->regularProgram()->associate($regularProgram);
					}
		$unsavedregularProgram = $unsavedRelationships['regularProgram'];
		if(!empty($unsavedregularProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgram);
						$regularProgramEvent->regularProgram()->associate($modelInstance);
					}
				$regularProgramEvent->save();
		return redirect('/modules/regularProgramEvents');
	}
	
	public function get($id)
	{
		$regularProgramEvent = RegularProgramEvent::with('regularProgram')->find($id);
				$regularPrograms = RegularProgram::all();
						return view('modules.regularProgramEvents.form', [
			'regularProgramEvent' => $regularProgramEvent,
							'regularPrograms' => $regularPrograms,
				'allregularPrograms' => RegularProgram::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$regularProgramEvent = RegularProgramEvent::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$regularProgramEvent->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$regularProgramEvent->title = $data['title'];
		}
				if(!empty($data['description']))
		{
			$regularProgramEvent->description = $data['description'];
		}
				if(!empty($data['regular_program_id']))
		{
			$regularProgramEvent->regular_program_id = $data['regular_program_id'];
		}
				$regularProgramEvent->save();
						if(!empty($data['regularProgram']))
		{
			$regularProgram = RegularProgram::find($data['regularProgram']);
						$regularProgramEvent->regularProgram()->associate($regularProgram);
					}
		$unsavedregularProgram = $unsavedRelationships['regularProgram'];
		if(!empty($unsavedregularProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgram);
						$regularProgramEvent->regularProgram()->associate($modelInstance);
					}
				$regularProgramEvent->save();
		return redirect('/modules/regularProgramEvents');
	}
	
	public function delete($id)
	{
		$regularProgramEvent = RegularProgramEvent::find($id);
		$regularProgramEvent->delete();
		return redirect('/modules/regularProgramEvents');
	}
	
	public function regularProgramEvent($id)
	{
		$regularProgramEvent = RegularProgramEvent::with('regularProgram')->find($id);
		return response()->json($regularProgramEvent);
	}
	
	}
?>