<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InHomeProgramTopic;
use App\Models\InHomeProgram;
use Log;

class InHomeProgramTopicController extends Controller
{	
	public function index()
	{
		$inHomeProgramTopics = InHomeProgramTopic::with('inHomeProgram')->get();
		return view('modules.inHomeProgramTopics.all', ['inHomeProgramTopics' => $inHomeProgramTopics]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = InHomeProgramTopic::with('inHomeProgram')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$inHomePrograms = InHomeProgram::all();
				return view('modules.inHomeProgramTopics.form', [
					'inHomePrograms' => $inHomePrograms,
			'allinHomePrograms' => InHomeProgram::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$inHomeProgramTopic = new InHomeProgramTopic();
				if(!empty($data['id']))
		{
			$inHomeProgramTopic->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$inHomeProgramTopic->title = $data['title'];
		}
				if(!empty($data['in_home_program_id']))
		{
			$inHomeProgramTopic->in_home_program_id = $data['in_home_program_id'];
		}
				$inHomeProgramTopic->save();
						if(!empty($data['inHomeProgram']))
		{
			$inHomeProgram = InHomeProgram::find($data['inHomeProgram']);
						$inHomeProgramTopic->inHomeProgram()->associate($inHomeProgram);
					}
		$unsavedinHomeProgram = $unsavedRelationships['inHomeProgram'];
		if(!empty($unsavedinHomeProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
						$inHomeProgramTopic->inHomeProgram()->associate($modelInstance);
					}
				$inHomeProgramTopic->save();
		return redirect('/modules/inHomeProgramTopics');
	}
	
	public function get($id)
	{
		$inHomeProgramTopic = InHomeProgramTopic::with('inHomeProgram')->find($id);
				$inHomePrograms = InHomeProgram::all();
						return view('modules.inHomeProgramTopics.form', [
			'inHomeProgramTopic' => $inHomeProgramTopic,
							'inHomePrograms' => $inHomePrograms,
				'allinHomePrograms' => InHomeProgram::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$inHomeProgramTopic = InHomeProgramTopic::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$inHomeProgramTopic->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$inHomeProgramTopic->title = $data['title'];
		}
				if(!empty($data['in_home_program_id']))
		{
			$inHomeProgramTopic->in_home_program_id = $data['in_home_program_id'];
		}
				$inHomeProgramTopic->save();
						if(!empty($data['inHomeProgram']))
		{
			$inHomeProgram = InHomeProgram::find($data['inHomeProgram']);
						$inHomeProgramTopic->inHomeProgram()->associate($inHomeProgram);
					}
		$unsavedinHomeProgram = $unsavedRelationships['inHomeProgram'];
		if(!empty($unsavedinHomeProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
						$inHomeProgramTopic->inHomeProgram()->associate($modelInstance);
					}
				$inHomeProgramTopic->save();
		return redirect('/modules/inHomeProgramTopics');
	}
	
	public function delete($id)
	{
		$inHomeProgramTopic = InHomeProgramTopic::find($id);
		$inHomeProgramTopic->delete();
		return redirect('/modules/inHomeProgramTopics');
	}
	
	public function inHomeProgramTopic($id)
	{
		$inHomeProgramTopic = InHomeProgramTopic::with('inHomeProgram')->find($id);
		return response()->json($inHomeProgramTopic);
	}
	
	}
?>