<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attachment;
use App\Models\ProgramClass;
use Log;

class AttachmentController extends Controller
{	
	public function index()
	{
		$attachments = Attachment::with('programClass')->get();
		return view('modules.attachments.all', ['attachments' => $attachments]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = Attachment::with('programClass')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$programClasses = ProgramClass::all();
				return view('modules.attachments.form', [
					'programClasses' => $programClasses,
			'allprogramClasses' => ProgramClass::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$attachment = new Attachment();
				if(!empty($data['id']))
		{
			$attachment->id = $data['id'];
		}
				if(!empty($data['location']))
		{
			$attachment->location = $data['location'];
		}
				if(!empty($data['mime_type']))
		{
			$attachment->mime_type = $data['mime_type'];
		}
				if(!empty($data['class_id']))
		{
			$attachment->class_id = $data['class_id'];
		}
				$attachment->save();
						if(!empty($data['programClass']))
		{
			$programClass = ProgramClass::find($data['programClass']);
						$attachment->programClass()->associate($programClass);
					}
		$unsavedprogramClass = $unsavedRelationships['programClass'];
		if(!empty($unsavedprogramClass))
		{
			$modelInstance = getUnsavedRelationship($unsavedprogramClass);
						$attachment->programClass()->associate($modelInstance);
					}
				$attachment->save();
		return redirect('/modules/attachments');
	}
	
	public function get($id)
	{
		$attachment = Attachment::with('programClass')->find($id);
				$programClasses = ProgramClass::all();
						return view('modules.attachments.form', [
			'attachment' => $attachment,
							'programClasses' => $programClasses,
				'allprogramClasses' => ProgramClass::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$attachment = Attachment::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$attachment->id = $data['id'];
		}
				if(!empty($data['location']))
		{
			$attachment->location = $data['location'];
		}
				if(!empty($data['mime_type']))
		{
			$attachment->mime_type = $data['mime_type'];
		}
				if(!empty($data['class_id']))
		{
			$attachment->class_id = $data['class_id'];
		}
				$attachment->save();
						if(!empty($data['programClass']))
		{
			$programClass = ProgramClass::find($data['programClass']);
						$attachment->programClass()->associate($programClass);
					}
		$unsavedprogramClass = $unsavedRelationships['programClass'];
		if(!empty($unsavedprogramClass))
		{
			$modelInstance = getUnsavedRelationship($unsavedprogramClass);
						$attachment->programClass()->associate($modelInstance);
					}
				$attachment->save();
		return redirect('/modules/attachments');
	}
	
	public function delete($id)
	{
		$attachment = Attachment::find($id);
		$attachment->delete();
		return redirect('/modules/attachments');
	}
	
	public function attachment($id)
	{
		$attachment = Attachment::with('programClass')->find($id);
		return response()->json($attachment);
	}
	
	}
?>