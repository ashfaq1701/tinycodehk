<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ExamScore;
use App\Models\Enrollment;
use App\Models\Exam;
use Log;

class ExamScoreController extends Controller
{	
	public function index()
	{
		$examScores = ExamScore::with('enrollment','exam')->get();
		return view('modules.examScores.all', ['examScores' => $examScores]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = ExamScore::with('enrollment','exam')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$enrollments = Enrollment::all();
				$exams = Exam::all();
				return view('modules.examScores.form', [
					'enrollments' => $enrollments,
			'allenrollments' => Enrollment::all(),
					'exams' => $exams,
			'allexams' => Exam::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$examScore = new ExamScore();
				if(!empty($data['id']))
		{
			$examScore->id = $data['id'];
		}
				if(!empty($data['enrollment_id']))
		{
			$examScore->enrollment_id = $data['enrollment_id'];
		}
				if(!empty($data['marks']))
		{
			$examScore->marks = $data['marks'];
		}
				if(!empty($data['exam_id']))
		{
			$examScore->exam_id = $data['exam_id'];
		}
				$examScore->save();
						if(!empty($data['enrollment']))
		{
			$enrollment = Enrollment::find($data['enrollment']);
						$examScore->enrollment()->associate($enrollment);
					}
		$unsavedenrollment = $unsavedRelationships['enrollment'];
		if(!empty($unsavedenrollment))
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
						$examScore->enrollment()->associate($modelInstance);
					}
				if(!empty($data['exam']))
		{
			$exam = Exam::find($data['exam']);
						$examScore->exam()->associate($exam);
					}
		$unsavedexam = $unsavedRelationships['exam'];
		if(!empty($unsavedexam))
		{
			$modelInstance = getUnsavedRelationship($unsavedexam);
						$examScore->exam()->associate($modelInstance);
					}
				$examScore->save();
		return redirect('/modules/examScores');
	}
	
	public function get($id)
	{
		$examScore = ExamScore::with('enrollment','exam')->find($id);
				$enrollments = Enrollment::all();
				$exams = Exam::all();
						return view('modules.examScores.form', [
			'examScore' => $examScore,
							'enrollments' => $enrollments,
				'allenrollments' => Enrollment::all(),
							'exams' => $exams,
				'allexams' => Exam::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$examScore = ExamScore::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$examScore->id = $data['id'];
		}
				if(!empty($data['enrollment_id']))
		{
			$examScore->enrollment_id = $data['enrollment_id'];
		}
				if(!empty($data['marks']))
		{
			$examScore->marks = $data['marks'];
		}
				if(!empty($data['exam_id']))
		{
			$examScore->exam_id = $data['exam_id'];
		}
				$examScore->save();
						if(!empty($data['enrollment']))
		{
			$enrollment = Enrollment::find($data['enrollment']);
						$examScore->enrollment()->associate($enrollment);
					}
		$unsavedenrollment = $unsavedRelationships['enrollment'];
		if(!empty($unsavedenrollment))
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
						$examScore->enrollment()->associate($modelInstance);
					}
				if(!empty($data['exam']))
		{
			$exam = Exam::find($data['exam']);
						$examScore->exam()->associate($exam);
					}
		$unsavedexam = $unsavedRelationships['exam'];
		if(!empty($unsavedexam))
		{
			$modelInstance = getUnsavedRelationship($unsavedexam);
						$examScore->exam()->associate($modelInstance);
					}
				$examScore->save();
		return redirect('/modules/examScores');
	}
	
	public function delete($id)
	{
		$examScore = ExamScore::find($id);
		$examScore->delete();
		return redirect('/modules/examScores');
	}
	
	public function examScore($id)
	{
		$examScore = ExamScore::with('enrollment','exam')->find($id);
		return response()->json($examScore);
	}
	
	}
?>