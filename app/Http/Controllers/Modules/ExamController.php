<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Exam;
use App\Models\ExamScore;
use App\Models\ProgramClass;
use App\Models\Enrollment;
use Log;

class ExamController extends Controller
{	
	public function index()
	{
		$exams = Exam::with('examScores','enrollments','programClass')->get();
		return view('modules.exams.all', ['exams' => $exams]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = Exam::with('examScores','enrollments','programClass')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$programClasses = ProgramClass::all();
				return view('modules.exams.form', [
					'programClasses' => $programClasses,
			'allprogramClasses' => ProgramClass::all(),
					'allexamScores' => ExamScore::all(),
					'allenrollments' => Enrollment::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$exam = new Exam();
				if(!empty($data['id']))
		{
			$exam->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$exam->title = $data['title'];
		}
				if(!empty($data['exam_date_time']))
		{
			$exam->exam_date_time = $data['exam_date_time'];
		}
				if(!empty($data['description']))
		{
			$exam->description = $data['description'];
		}
				if(!empty($data['class_id']))
		{
			$exam->class_id = $data['class_id'];
		}
				if(!empty($data['total_marks']))
		{
			$exam->total_marks = $data['total_marks'];
		}
				$exam->save();
				if(!empty($data['examScores']))
		{
			$examScores = explode(',', $data['examScores']);
			foreach ($examScores as $singleExamScore)
			{
				$examScore = ExamScore::find($singleExamScore);
								$exam->examScores()->save($examScore);
							}
		}
		$unsavedexamScores = $unsavedRelationships['examScores'];
		foreach($unsavedexamScores as $unsavedexamScore)
		{
			$modelInstance = getUnsavedRelationship($unsavedexamScore);
			if(!empty($modelInstance))
			{
								$exam->examScores()->save($modelInstance);
							}
		}
				if(!empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
								$exam->enrollments()->attach($enrollment);
							}
		}
		$unsavedenrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedenrollments as $unsavedenrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
			if(!empty($modelInstance))
			{
								$exam->enrollments()->attach($modelInstance);
							}
		}
						if(!empty($data['programClass']))
		{
			$programClass = ProgramClass::find($data['programClass']);
						$exam->programClass()->associate($programClass);
					}
		$unsavedprogramClass = $unsavedRelationships['programClass'];
		if(!empty($unsavedprogramClass))
		{
			$modelInstance = getUnsavedRelationship($unsavedprogramClass);
						$exam->programClass()->associate($modelInstance);
					}
				$exam->save();
		return redirect('/modules/exams');
	}
	
	public function get($id)
	{
		$exam = Exam::with('examScores','enrollments','programClass')->find($id);
				$programClasses = ProgramClass::all();
						$examScoresArray = [];
		foreach($exam->examScores as $examScore)
		{
			
			$examScoresArray[] = $examScore->id;
		}
		$examScoresValue = implode(',', $examScoresArray);
				$enrollmentsArray = [];
		foreach($exam->enrollments as $enrollment)
		{
			
			$enrollmentsArray[] = $enrollment->id;
		}
		$enrollmentsValue = implode(',', $enrollmentsArray);
				return view('modules.exams.form', [
			'exam' => $exam,
							'programClasses' => $programClasses,
				'allprogramClasses' => ProgramClass::all(),
							'allexamScores' => ExamScore::all(),
				'examScoresValue' => $examScoresValue,
							'allenrollments' => Enrollment::all(),
				'enrollmentsValue' => $enrollmentsValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$exam = Exam::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$exam->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$exam->title = $data['title'];
		}
				if(!empty($data['exam_date_time']))
		{
			$exam->exam_date_time = $data['exam_date_time'];
		}
				if(!empty($data['description']))
		{
			$exam->description = $data['description'];
		}
				if(!empty($data['class_id']))
		{
			$exam->class_id = $data['class_id'];
		}
				if(!empty($data['total_marks']))
		{
			$exam->total_marks = $data['total_marks'];
		}
				$exam->save();
				if(!empty($data['examScores']))
		{
			$examScores = explode(',', $data['examScores']);
			foreach ($examScores as $singleExamScore)
			{
				$examScore = ExamScore::find($singleExamScore);
								$exam->examScores()->save($examScore);
							}
		}
		$unsavedexamScores = $unsavedRelationships['examScores'];
		foreach($unsavedexamScores as $unsavedexamScore)
		{
			$modelInstance = getUnsavedRelationship($unsavedexamScore);
			if(!empty($modelInstance))
			{
								$exam->examScores()->save($modelInstance);
							}
		}
				if(!empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
								$exam->enrollments()->attach($enrollment);
							}
		}
		$unsavedenrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedenrollments as $unsavedenrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
			if(!empty($modelInstance))
			{
								$exam->enrollments()->attach($modelInstance);
							}
		}
						if(!empty($data['programClass']))
		{
			$programClass = ProgramClass::find($data['programClass']);
						$exam->programClass()->associate($programClass);
					}
		$unsavedprogramClass = $unsavedRelationships['programClass'];
		if(!empty($unsavedprogramClass))
		{
			$modelInstance = getUnsavedRelationship($unsavedprogramClass);
						$exam->programClass()->associate($modelInstance);
					}
				$exam->save();
		return redirect('/modules/exams');
	}
	
	public function delete($id)
	{
		$exam = Exam::find($id);
		$exam->delete();
		return redirect('/modules/exams');
	}
	
	public function exam($id)
	{
		$exam = Exam::with('examScores','enrollments','programClass')->find($id);
		return response()->json($exam);
	}
	
		public function examScores($id)
	{
		$exam = Exam::find($id);
		$examScores = $exam->examScores;
		for ($i = 0; $i < count($examScores); $i++)
		{
			$examScore = $examScores[$i];
			$examScore->exam = $exam;
			$examScores[$i] = $examScore;
		}
		return view('modules.examScores.all', ['examScores' => $examScores]);
	}
	
	public function examScoresApi($id)
	{
		$exam = Exam::find($id);
		$examScores = $exam->examScores;
		return response()->json($examScores->all());
	}
		public function enrollments($id)
	{
		$exam = Exam::find($id);
		$enrollments = $exam->enrollments;
		for ($i = 0; $i < count($enrollments); $i++)
		{
			$enrollment = $enrollments[$i];
			$enrollment->exam = $exam;
			$enrollments[$i] = $enrollment;
		}
		return view('modules.enrollments.all', ['enrollments' => $enrollments]);
	}
	
	public function enrollmentsApi($id)
	{
		$exam = Exam::find($id);
		$enrollments = $exam->enrollments;
		return response()->json($enrollments->all());
	}
	}
?>