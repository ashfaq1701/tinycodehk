<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HolidayCampCourseFeature;
use App\Models\HolidayCampCourseSubFeature;
use App\Models\HolidayCampCourse;
use Log;

class HolidayCampCourseFeatureController extends Controller
{	
	public function index()
	{
		$holidayCampCourseFeatures = HolidayCampCourseFeature::with('holidayCampCourseSubFeatures','holidayCampCourse')->get();
		return view('modules.holidayCampCourseFeatures.all', ['holidayCampCourseFeatures' => $holidayCampCourseFeatures]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = HolidayCampCourseFeature::with('holidayCampCourseSubFeatures','holidayCampCourse')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$holidayCampCourses = HolidayCampCourse::all();
				return view('modules.holidayCampCourseFeatures.form', [
					'holidayCampCourses' => $holidayCampCourses,
			'allholidayCampCourses' => HolidayCampCourse::all(),
					'allholidayCampCourseSubFeatures' => HolidayCampCourseSubFeature::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$holidayCampCourseFeature = new HolidayCampCourseFeature();
				if(!empty($data['id']))
		{
			$holidayCampCourseFeature->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$holidayCampCourseFeature->name = $data['name'];
		}
				if(!empty($data['holiday_camp_course_id']))
		{
			$holidayCampCourseFeature->holiday_camp_course_id = $data['holiday_camp_course_id'];
		}
				$holidayCampCourseFeature->save();
				if(!empty($data['holidayCampCourseSubFeatures']))
		{
			$holidayCampCourseSubFeatures = explode(',', $data['holidayCampCourseSubFeatures']);
			foreach ($holidayCampCourseSubFeatures as $singleHolidayCampCourseSubFeature)
			{
				$holidayCampCourseSubFeature = HolidayCampCourseSubFeature::find($singleHolidayCampCourseSubFeature);
								$holidayCampCourseFeature->holidayCampCourseSubFeatures()->save($holidayCampCourseSubFeature);
							}
		}
		$unsavedholidayCampCourseSubFeatures = $unsavedRelationships['holidayCampCourseSubFeatures'];
		foreach($unsavedholidayCampCourseSubFeatures as $unsavedholidayCampCourseSubFeature)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourseSubFeature);
			if(!empty($modelInstance))
			{
								$holidayCampCourseFeature->holidayCampCourseSubFeatures()->save($modelInstance);
							}
		}
						if(!empty($data['holidayCampCourse']))
		{
			$holidayCampCourse = HolidayCampCourse::find($data['holidayCampCourse']);
						$holidayCampCourseFeature->holidayCampCourse()->associate($holidayCampCourse);
					}
		$unsavedholidayCampCourse = $unsavedRelationships['holidayCampCourse'];
		if(!empty($unsavedholidayCampCourse))
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
						$holidayCampCourseFeature->holidayCampCourse()->associate($modelInstance);
					}
				$holidayCampCourseFeature->save();
		return redirect('/modules/holidayCampCourseFeatures');
	}
	
	public function get($id)
	{
		$holidayCampCourseFeature = HolidayCampCourseFeature::with('holidayCampCourseSubFeatures','holidayCampCourse')->find($id);
				$holidayCampCourses = HolidayCampCourse::all();
						$holidayCampCourseSubFeaturesArray = [];
		foreach($holidayCampCourseFeature->holidayCampCourseSubFeatures as $holidayCampCourseSubFeature)
		{
			
			$holidayCampCourseSubFeaturesArray[] = $holidayCampCourseSubFeature->id;
		}
		$holidayCampCourseSubFeaturesValue = implode(',', $holidayCampCourseSubFeaturesArray);
				return view('modules.holidayCampCourseFeatures.form', [
			'holidayCampCourseFeature' => $holidayCampCourseFeature,
							'holidayCampCourses' => $holidayCampCourses,
				'allholidayCampCourses' => HolidayCampCourse::all(),
							'allholidayCampCourseSubFeatures' => HolidayCampCourseSubFeature::all(),
				'holidayCampCourseSubFeaturesValue' => $holidayCampCourseSubFeaturesValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$holidayCampCourseFeature = HolidayCampCourseFeature::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$holidayCampCourseFeature->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$holidayCampCourseFeature->name = $data['name'];
		}
				if(!empty($data['holiday_camp_course_id']))
		{
			$holidayCampCourseFeature->holiday_camp_course_id = $data['holiday_camp_course_id'];
		}
				$holidayCampCourseFeature->save();
				if(!empty($data['holidayCampCourseSubFeatures']))
		{
			$holidayCampCourseSubFeatures = explode(',', $data['holidayCampCourseSubFeatures']);
			foreach ($holidayCampCourseSubFeatures as $singleHolidayCampCourseSubFeature)
			{
				$holidayCampCourseSubFeature = HolidayCampCourseSubFeature::find($singleHolidayCampCourseSubFeature);
								$holidayCampCourseFeature->holidayCampCourseSubFeatures()->save($holidayCampCourseSubFeature);
							}
		}
		$unsavedholidayCampCourseSubFeatures = $unsavedRelationships['holidayCampCourseSubFeatures'];
		foreach($unsavedholidayCampCourseSubFeatures as $unsavedholidayCampCourseSubFeature)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourseSubFeature);
			if(!empty($modelInstance))
			{
								$holidayCampCourseFeature->holidayCampCourseSubFeatures()->save($modelInstance);
							}
		}
						if(!empty($data['holidayCampCourse']))
		{
			$holidayCampCourse = HolidayCampCourse::find($data['holidayCampCourse']);
						$holidayCampCourseFeature->holidayCampCourse()->associate($holidayCampCourse);
					}
		$unsavedholidayCampCourse = $unsavedRelationships['holidayCampCourse'];
		if(!empty($unsavedholidayCampCourse))
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
						$holidayCampCourseFeature->holidayCampCourse()->associate($modelInstance);
					}
				$holidayCampCourseFeature->save();
		return redirect('/modules/holidayCampCourseFeatures');
	}
	
	public function delete($id)
	{
		$holidayCampCourseFeature = HolidayCampCourseFeature::find($id);
		$holidayCampCourseFeature->delete();
		return redirect('/modules/holidayCampCourseFeatures');
	}
	
	public function holidayCampCourseFeature($id)
	{
		$holidayCampCourseFeature = HolidayCampCourseFeature::with('holidayCampCourseSubFeatures','holidayCampCourse')->find($id);
		return response()->json($holidayCampCourseFeature);
	}
	
		public function holidayCampCourseSubFeatures($id)
	{
		$holidayCampCourseFeature = HolidayCampCourseFeature::find($id);
		$holidayCampCourseSubFeatures = $holidayCampCourseFeature->holidayCampCourseSubFeatures;
		for ($i = 0; $i < count($holidayCampCourseSubFeatures); $i++)
		{
			$holidayCampCourseSubFeature = $holidayCampCourseSubFeatures[$i];
			$holidayCampCourseSubFeature->holidayCampCourseFeature = $holidayCampCourseFeature;
			$holidayCampCourseSubFeatures[$i] = $holidayCampCourseSubFeature;
		}
		return view('modules.holidayCampCourseSubFeatures.all', ['holidayCampCourseSubFeatures' => $holidayCampCourseSubFeatures]);
	}
	
	public function holidayCampCourseSubFeaturesApi($id)
	{
		$holidayCampCourseFeature = HolidayCampCourseFeature::find($id);
		$holidayCampCourseSubFeatures = $holidayCampCourseFeature->holidayCampCourseSubFeatures;
		return response()->json($holidayCampCourseSubFeatures->all());
	}
	}
?>