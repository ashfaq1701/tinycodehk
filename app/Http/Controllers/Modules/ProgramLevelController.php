<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProgramLevel;
use App\Models\HolidayCampCourse;
use Log;

class ProgramLevelController extends Controller
{	
	public function index()
	{
		$programLevels = ProgramLevel::with('holidayCampCourses')->get();
		return view('modules.programLevels.all', ['programLevels' => $programLevels]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = ProgramLevel::with('holidayCampCourses')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				return view('modules.programLevels.form', [
					'allholidayCampCourses' => HolidayCampCourse::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$programLevel = new ProgramLevel();
				if(!empty($data['id']))
		{
			$programLevel->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$programLevel->name = $data['name'];
		}
				$programLevel->save();
				if(!empty($data['holidayCampCourses']))
		{
			$holidayCampCourses = explode(',', $data['holidayCampCourses']);
			foreach ($holidayCampCourses as $singleHolidayCampCourse)
			{
				$holidayCampCourse = HolidayCampCourse::find($singleHolidayCampCourse);
								$programLevel->holidayCampCourses()->save($holidayCampCourse);
							}
		}
		$unsavedholidayCampCourses = $unsavedRelationships['holidayCampCourses'];
		foreach($unsavedholidayCampCourses as $unsavedholidayCampCourse)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
			if(!empty($modelInstance))
			{
								$programLevel->holidayCampCourses()->save($modelInstance);
							}
		}
						$programLevel->save();
		return redirect('/modules/programLevels');
	}
	
	public function get($id)
	{
		$programLevel = ProgramLevel::with('holidayCampCourses')->find($id);
						$holidayCampCoursesArray = [];
		foreach($programLevel->holidayCampCourses as $holidayCampCourse)
		{
			
			$holidayCampCoursesArray[] = $holidayCampCourse->id;
		}
		$holidayCampCoursesValue = implode(',', $holidayCampCoursesArray);
				return view('modules.programLevels.form', [
			'programLevel' => $programLevel,
							'allholidayCampCourses' => HolidayCampCourse::all(),
				'holidayCampCoursesValue' => $holidayCampCoursesValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$programLevel = ProgramLevel::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$programLevel->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$programLevel->name = $data['name'];
		}
				$programLevel->save();
				if(!empty($data['holidayCampCourses']))
		{
			$holidayCampCourses = explode(',', $data['holidayCampCourses']);
			foreach ($holidayCampCourses as $singleHolidayCampCourse)
			{
				$holidayCampCourse = HolidayCampCourse::find($singleHolidayCampCourse);
								$programLevel->holidayCampCourses()->save($holidayCampCourse);
							}
		}
		$unsavedholidayCampCourses = $unsavedRelationships['holidayCampCourses'];
		foreach($unsavedholidayCampCourses as $unsavedholidayCampCourse)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
			if(!empty($modelInstance))
			{
								$programLevel->holidayCampCourses()->save($modelInstance);
							}
		}
						$programLevel->save();
		return redirect('/modules/programLevels');
	}
	
	public function delete($id)
	{
		$programLevel = ProgramLevel::find($id);
		$programLevel->delete();
		return redirect('/modules/programLevels');
	}
	
	public function programLevel($id)
	{
		$programLevel = ProgramLevel::with('holidayCampCourses')->find($id);
		return response()->json($programLevel);
	}
	
		public function holidayCampCourses($id)
	{
		$programLevel = ProgramLevel::find($id);
		$holidayCampCourses = $programLevel->holidayCampCourses;
		for ($i = 0; $i < count($holidayCampCourses); $i++)
		{
			$holidayCampCourse = $holidayCampCourses[$i];
			$holidayCampCourse->programLevel = $programLevel;
			$holidayCampCourses[$i] = $holidayCampCourse;
		}
		return view('modules.holidayCampCourses.all', ['holidayCampCourses' => $holidayCampCourses]);
	}
	
	public function holidayCampCoursesApi($id)
	{
		$programLevel = ProgramLevel::find($id);
		$holidayCampCourses = $programLevel->holidayCampCourses;
		return response()->json($holidayCampCourses->all());
	}
	}
?>