<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RegularProgramFeature;
use App\Models\RegularProgram;
use Log;

class RegularProgramFeatureController extends Controller
{	
	public function index()
	{
		$regularProgramFeatures = RegularProgramFeature::with('regularProgram')->get();
		return view('modules.regularProgramFeatures.all', ['regularProgramFeatures' => $regularProgramFeatures]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = RegularProgramFeature::with('regularProgram')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$regularPrograms = RegularProgram::all();
				return view('modules.regularProgramFeatures.form', [
					'regularPrograms' => $regularPrograms,
			'allregularPrograms' => RegularProgram::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$regularProgramFeature = new RegularProgramFeature();
				if(!empty($data['id']))
		{
			$regularProgramFeature->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$regularProgramFeature->title = $data['title'];
		}
				if(!empty($data['description']))
		{
			$regularProgramFeature->description = $data['description'];
		}
				if(!empty($data['regular_program_id']))
		{
			$regularProgramFeature->regular_program_id = $data['regular_program_id'];
		}
				$regularProgramFeature->save();
						if(!empty($data['regularProgram']))
		{
			$regularProgram = RegularProgram::find($data['regularProgram']);
						$regularProgramFeature->regularProgram()->associate($regularProgram);
					}
		$unsavedregularProgram = $unsavedRelationships['regularProgram'];
		if(!empty($unsavedregularProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgram);
						$regularProgramFeature->regularProgram()->associate($modelInstance);
					}
				$regularProgramFeature->save();
		return redirect('/modules/regularProgramFeatures');
	}
	
	public function get($id)
	{
		$regularProgramFeature = RegularProgramFeature::with('regularProgram')->find($id);
				$regularPrograms = RegularProgram::all();
						return view('modules.regularProgramFeatures.form', [
			'regularProgramFeature' => $regularProgramFeature,
							'regularPrograms' => $regularPrograms,
				'allregularPrograms' => RegularProgram::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$regularProgramFeature = RegularProgramFeature::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$regularProgramFeature->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$regularProgramFeature->title = $data['title'];
		}
				if(!empty($data['description']))
		{
			$regularProgramFeature->description = $data['description'];
		}
				if(!empty($data['regular_program_id']))
		{
			$regularProgramFeature->regular_program_id = $data['regular_program_id'];
		}
				$regularProgramFeature->save();
						if(!empty($data['regularProgram']))
		{
			$regularProgram = RegularProgram::find($data['regularProgram']);
						$regularProgramFeature->regularProgram()->associate($regularProgram);
					}
		$unsavedregularProgram = $unsavedRelationships['regularProgram'];
		if(!empty($unsavedregularProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgram);
						$regularProgramFeature->regularProgram()->associate($modelInstance);
					}
				$regularProgramFeature->save();
		return redirect('/modules/regularProgramFeatures');
	}
	
	public function delete($id)
	{
		$regularProgramFeature = RegularProgramFeature::find($id);
		$regularProgramFeature->delete();
		return redirect('/modules/regularProgramFeatures');
	}
	
	public function regularProgramFeature($id)
	{
		$regularProgramFeature = RegularProgramFeature::with('regularProgram')->find($id);
		return response()->json($regularProgramFeature);
	}
	
	}
?>