<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RegularProgramStructure;
use App\Models\RegularProgram;
use Log;

class RegularProgramStructureController extends Controller
{	
	public function index()
	{
		$regularProgramStructures = RegularProgramStructure::with('regularProgram')->get();
		return view('modules.regularProgramStructures.all', ['regularProgramStructures' => $regularProgramStructures]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = RegularProgramStructure::with('regularProgram')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$regularPrograms = RegularProgram::all();
				return view('modules.regularProgramStructures.form', [
					'regularPrograms' => $regularPrograms,
			'allregularPrograms' => RegularProgram::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$regularProgramStructure = new RegularProgramStructure();
				if(!empty($data['id']))
		{
			$regularProgramStructure->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$regularProgramStructure->title = $data['title'];
		}
				if(!empty($data['regular_program_id']))
		{
			$regularProgramStructure->regular_program_id = $data['regular_program_id'];
		}
				$regularProgramStructure->save();
						if(!empty($data['regularProgram']))
		{
			$regularProgram = RegularProgram::find($data['regularProgram']);
						$regularProgramStructure->regularProgram()->associate($regularProgram);
					}
		$unsavedregularProgram = $unsavedRelationships['regularProgram'];
		if(!empty($unsavedregularProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgram);
						$regularProgramStructure->regularProgram()->associate($modelInstance);
					}
				$regularProgramStructure->save();
		return redirect('/modules/regularProgramStructures');
	}
	
	public function get($id)
	{
		$regularProgramStructure = RegularProgramStructure::with('regularProgram')->find($id);
				$regularPrograms = RegularProgram::all();
						return view('modules.regularProgramStructures.form', [
			'regularProgramStructure' => $regularProgramStructure,
							'regularPrograms' => $regularPrograms,
				'allregularPrograms' => RegularProgram::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$regularProgramStructure = RegularProgramStructure::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$regularProgramStructure->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$regularProgramStructure->title = $data['title'];
		}
				if(!empty($data['regular_program_id']))
		{
			$regularProgramStructure->regular_program_id = $data['regular_program_id'];
		}
				$regularProgramStructure->save();
						if(!empty($data['regularProgram']))
		{
			$regularProgram = RegularProgram::find($data['regularProgram']);
						$regularProgramStructure->regularProgram()->associate($regularProgram);
					}
		$unsavedregularProgram = $unsavedRelationships['regularProgram'];
		if(!empty($unsavedregularProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgram);
						$regularProgramStructure->regularProgram()->associate($modelInstance);
					}
				$regularProgramStructure->save();
		return redirect('/modules/regularProgramStructures');
	}
	
	public function delete($id)
	{
		$regularProgramStructure = RegularProgramStructure::find($id);
		$regularProgramStructure->delete();
		return redirect('/modules/regularProgramStructures');
	}
	
	public function regularProgramStructure($id)
	{
		$regularProgramStructure = RegularProgramStructure::with('regularProgram')->find($id);
		return response()->json($regularProgramStructure);
	}
	
	}
?>