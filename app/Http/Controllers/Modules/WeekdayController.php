<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Weekday;
use App\Models\InHomeProgramSchedule;
use App\Models\RegularProgramClasseSchedule;
use App\Models\InHomeProgram;
use Log;

class WeekdayController extends Controller
{	
	public function index()
	{
		$weekdays = Weekday::with('inHomeProgramSchedules','regularProgramClasseSchedules','inHomePrograms')->get();
		return view('modules.weekdays.all', ['weekdays' => $weekdays]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = Weekday::with('inHomeProgramSchedules','regularProgramClasseSchedules','inHomePrograms')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				return view('modules.weekdays.form', [
					'allinHomeProgramSchedules' => InHomeProgramSchedule::all(),
					'allregularProgramClasseSchedules' => RegularProgramClasseSchedule::all(),
					'allinHomePrograms' => InHomeProgram::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$weekday = new Weekday();
				if(!empty($data['id']))
		{
			$weekday->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$weekday->name = $data['name'];
		}
				$weekday->save();
				if(!empty($data['inHomeProgramSchedules']))
		{
			$inHomeProgramSchedules = explode(',', $data['inHomeProgramSchedules']);
			foreach ($inHomeProgramSchedules as $singleInHomeProgramSchedule)
			{
				$inHomeProgramSchedule = InHomeProgramSchedule::find($singleInHomeProgramSchedule);
								$weekday->inHomeProgramSchedules()->save($inHomeProgramSchedule);
							}
		}
		$unsavedinHomeProgramSchedules = $unsavedRelationships['inHomeProgramSchedules'];
		foreach($unsavedinHomeProgramSchedules as $unsavedinHomeProgramSchedule)
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgramSchedule);
			if(!empty($modelInstance))
			{
								$weekday->inHomeProgramSchedules()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramClasseSchedules']))
		{
			$regularProgramClasseSchedules = explode(',', $data['regularProgramClasseSchedules']);
			foreach ($regularProgramClasseSchedules as $singleRegularProgramClasseSchedule)
			{
				$regularProgramClasseSchedule = RegularProgramClasseSchedule::find($singleRegularProgramClasseSchedule);
								$weekday->regularProgramClasseSchedules()->save($regularProgramClasseSchedule);
							}
		}
		$unsavedregularProgramClasseSchedules = $unsavedRelationships['regularProgramClasseSchedules'];
		foreach($unsavedregularProgramClasseSchedules as $unsavedregularProgramClasseSchedule)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramClasseSchedule);
			if(!empty($modelInstance))
			{
								$weekday->regularProgramClasseSchedules()->save($modelInstance);
							}
		}
				if(!empty($data['inHomePrograms']))
		{
			$inHomePrograms = explode(',', $data['inHomePrograms']);
			foreach ($inHomePrograms as $singleInHomeProgram)
			{
				$inHomeProgram = InHomeProgram::find($singleInHomeProgram);
								$weekday->inHomePrograms()->attach($inHomeProgram);
							}
		}
		$unsavedinHomePrograms = $unsavedRelationships['inHomePrograms'];
		foreach($unsavedinHomePrograms as $unsavedinHomeProgram)
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
			if(!empty($modelInstance))
			{
								$weekday->inHomePrograms()->attach($modelInstance);
							}
		}
						$weekday->save();
		return redirect('/modules/weekdays');
	}
	
	public function get($id)
	{
		$weekday = Weekday::with('inHomeProgramSchedules','regularProgramClasseSchedules','inHomePrograms')->find($id);
						$inHomeProgramSchedulesArray = [];
		foreach($weekday->inHomeProgramSchedules as $inHomeProgramSchedule)
		{
			
			$inHomeProgramSchedulesArray[] = $inHomeProgramSchedule->id;
		}
		$inHomeProgramSchedulesValue = implode(',', $inHomeProgramSchedulesArray);
				$regularProgramClasseSchedulesArray = [];
		foreach($weekday->regularProgramClasseSchedules as $regularProgramClasseSchedule)
		{
			
			$regularProgramClasseSchedulesArray[] = $regularProgramClasseSchedule->id;
		}
		$regularProgramClasseSchedulesValue = implode(',', $regularProgramClasseSchedulesArray);
				$inHomeProgramsArray = [];
		foreach($weekday->inHomePrograms as $inHomeProgram)
		{
			
			$inHomeProgramsArray[] = $inHomeProgram->id;
		}
		$inHomeProgramsValue = implode(',', $inHomeProgramsArray);
				return view('modules.weekdays.form', [
			'weekday' => $weekday,
							'allinHomeProgramSchedules' => InHomeProgramSchedule::all(),
				'inHomeProgramSchedulesValue' => $inHomeProgramSchedulesValue,
							'allregularProgramClasseSchedules' => RegularProgramClasseSchedule::all(),
				'regularProgramClasseSchedulesValue' => $regularProgramClasseSchedulesValue,
							'allinHomePrograms' => InHomeProgram::all(),
				'inHomeProgramsValue' => $inHomeProgramsValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$weekday = Weekday::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$weekday->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$weekday->name = $data['name'];
		}
				$weekday->save();
				if(!empty($data['inHomeProgramSchedules']))
		{
			$inHomeProgramSchedules = explode(',', $data['inHomeProgramSchedules']);
			foreach ($inHomeProgramSchedules as $singleInHomeProgramSchedule)
			{
				$inHomeProgramSchedule = InHomeProgramSchedule::find($singleInHomeProgramSchedule);
								$weekday->inHomeProgramSchedules()->save($inHomeProgramSchedule);
							}
		}
		$unsavedinHomeProgramSchedules = $unsavedRelationships['inHomeProgramSchedules'];
		foreach($unsavedinHomeProgramSchedules as $unsavedinHomeProgramSchedule)
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgramSchedule);
			if(!empty($modelInstance))
			{
								$weekday->inHomeProgramSchedules()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramClasseSchedules']))
		{
			$regularProgramClasseSchedules = explode(',', $data['regularProgramClasseSchedules']);
			foreach ($regularProgramClasseSchedules as $singleRegularProgramClasseSchedule)
			{
				$regularProgramClasseSchedule = RegularProgramClasseSchedule::find($singleRegularProgramClasseSchedule);
								$weekday->regularProgramClasseSchedules()->save($regularProgramClasseSchedule);
							}
		}
		$unsavedregularProgramClasseSchedules = $unsavedRelationships['regularProgramClasseSchedules'];
		foreach($unsavedregularProgramClasseSchedules as $unsavedregularProgramClasseSchedule)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramClasseSchedule);
			if(!empty($modelInstance))
			{
								$weekday->regularProgramClasseSchedules()->save($modelInstance);
							}
		}
				if(!empty($data['inHomePrograms']))
		{
			$inHomePrograms = explode(',', $data['inHomePrograms']);
			foreach ($inHomePrograms as $singleInHomeProgram)
			{
				$inHomeProgram = InHomeProgram::find($singleInHomeProgram);
								$weekday->inHomePrograms()->attach($inHomeProgram);
							}
		}
		$unsavedinHomePrograms = $unsavedRelationships['inHomePrograms'];
		foreach($unsavedinHomePrograms as $unsavedinHomeProgram)
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
			if(!empty($modelInstance))
			{
								$weekday->inHomePrograms()->attach($modelInstance);
							}
		}
						$weekday->save();
		return redirect('/modules/weekdays');
	}
	
	public function delete($id)
	{
		$weekday = Weekday::find($id);
		$weekday->delete();
		return redirect('/modules/weekdays');
	}
	
	public function weekday($id)
	{
		$weekday = Weekday::with('inHomeProgramSchedules','regularProgramClasseSchedules','inHomePrograms')->find($id);
		return response()->json($weekday);
	}
	
		public function inHomeProgramSchedules($id)
	{
		$weekday = Weekday::find($id);
		$inHomeProgramSchedules = $weekday->inHomeProgramSchedules;
		for ($i = 0; $i < count($inHomeProgramSchedules); $i++)
		{
			$inHomeProgramSchedule = $inHomeProgramSchedules[$i];
			$inHomeProgramSchedule->weekday = $weekday;
			$inHomeProgramSchedules[$i] = $inHomeProgramSchedule;
		}
		return view('modules.inHomeProgramSchedules.all', ['inHomeProgramSchedules' => $inHomeProgramSchedules]);
	}
	
	public function inHomeProgramSchedulesApi($id)
	{
		$weekday = Weekday::find($id);
		$inHomeProgramSchedules = $weekday->inHomeProgramSchedules;
		return response()->json($inHomeProgramSchedules->all());
	}
		public function regularProgramClasseSchedules($id)
	{
		$weekday = Weekday::find($id);
		$regularProgramClasseSchedules = $weekday->regularProgramClasseSchedules;
		for ($i = 0; $i < count($regularProgramClasseSchedules); $i++)
		{
			$regularProgramClasseSchedule = $regularProgramClasseSchedules[$i];
			$regularProgramClasseSchedule->weekday = $weekday;
			$regularProgramClasseSchedules[$i] = $regularProgramClasseSchedule;
		}
		return view('modules.regularProgramClasseSchedules.all', ['regularProgramClasseSchedules' => $regularProgramClasseSchedules]);
	}
	
	public function regularProgramClasseSchedulesApi($id)
	{
		$weekday = Weekday::find($id);
		$regularProgramClasseSchedules = $weekday->regularProgramClasseSchedules;
		return response()->json($regularProgramClasseSchedules->all());
	}
		public function inHomePrograms($id)
	{
		$weekday = Weekday::find($id);
		$inHomePrograms = $weekday->inHomePrograms;
		for ($i = 0; $i < count($inHomePrograms); $i++)
		{
			$inHomeProgram = $inHomePrograms[$i];
			$inHomeProgram->weekday = $weekday;
			$inHomePrograms[$i] = $inHomeProgram;
		}
		return view('modules.inHomePrograms.all', ['inHomePrograms' => $inHomePrograms]);
	}
	
	public function inHomeProgramsApi($id)
	{
		$weekday = Weekday::find($id);
		$inHomePrograms = $weekday->inHomePrograms;
		return response()->json($inHomePrograms->all());
	}
	}
?>