<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RegularProgramClasseSchedule;
use App\Models\RegularProgramSemester;
use App\Models\Location;
use App\Models\Weekday;
use Log;

class RegularProgramClasseScheduleController extends Controller
{	
	public function index()
	{
		$regularProgramClasseSchedules = RegularProgramClasseSchedule::with('regularProgramSemester','location','weekday')->get();
		return view('modules.regularProgramClasseSchedules.all', ['regularProgramClasseSchedules' => $regularProgramClasseSchedules]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = RegularProgramClasseSchedule::with('regularProgramSemester','location','weekday')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$regularProgramSemesters = RegularProgramSemester::all();
				$locations = Location::all();
				$weekdays = Weekday::all();
				return view('modules.regularProgramClasseSchedules.form', [
					'regularProgramSemesters' => $regularProgramSemesters,
			'allregularProgramSemesters' => RegularProgramSemester::all(),
					'locations' => $locations,
			'alllocations' => Location::all(),
					'weekdays' => $weekdays,
			'allweekdays' => Weekday::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$regularProgramClasseSchedule = new RegularProgramClasseSchedule();
				if(!empty($data['id']))
		{
			$regularProgramClasseSchedule->id = $data['id'];
		}
				if(!empty($data['from_time']))
		{
			$regularProgramClasseSchedule->from_time = $data['from_time'];
		}
				if(!empty($data['to_time']))
		{
			$regularProgramClasseSchedule->to_time = $data['to_time'];
		}
				if(!empty($data['location_id']))
		{
			$regularProgramClasseSchedule->location_id = $data['location_id'];
		}
				if(!empty($data['weekday_id']))
		{
			$regularProgramClasseSchedule->weekday_id = $data['weekday_id'];
		}
				if(!empty($data['regular_program_semester_id']))
		{
			$regularProgramClasseSchedule->regular_program_semester_id = $data['regular_program_semester_id'];
		}
				$regularProgramClasseSchedule->save();
						if(!empty($data['regularProgramSemester']))
		{
			$regularProgramSemester = RegularProgramSemester::find($data['regularProgramSemester']);
						$regularProgramClasseSchedule->regularProgramSemester()->associate($regularProgramSemester);
					}
		$unsavedregularProgramSemester = $unsavedRelationships['regularProgramSemester'];
		if(!empty($unsavedregularProgramSemester))
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramSemester);
						$regularProgramClasseSchedule->regularProgramSemester()->associate($modelInstance);
					}
				if(!empty($data['location']))
		{
			$location = Location::find($data['location']);
						$regularProgramClasseSchedule->location()->associate($location);
					}
		$unsavedlocation = $unsavedRelationships['location'];
		if(!empty($unsavedlocation))
		{
			$modelInstance = getUnsavedRelationship($unsavedlocation);
						$regularProgramClasseSchedule->location()->associate($modelInstance);
					}
				if(!empty($data['weekday']))
		{
			$weekday = Weekday::find($data['weekday']);
						$regularProgramClasseSchedule->weekday()->associate($weekday);
					}
		$unsavedweekday = $unsavedRelationships['weekday'];
		if(!empty($unsavedweekday))
		{
			$modelInstance = getUnsavedRelationship($unsavedweekday);
						$regularProgramClasseSchedule->weekday()->associate($modelInstance);
					}
				$regularProgramClasseSchedule->save();
		return redirect('/modules/regularProgramClasseSchedules');
	}
	
	public function get($id)
	{
		$regularProgramClasseSchedule = RegularProgramClasseSchedule::with('regularProgramSemester','location','weekday')->find($id);
				$regularProgramSemesters = RegularProgramSemester::all();
				$locations = Location::all();
				$weekdays = Weekday::all();
						return view('modules.regularProgramClasseSchedules.form', [
			'regularProgramClasseSchedule' => $regularProgramClasseSchedule,
							'regularProgramSemesters' => $regularProgramSemesters,
				'allregularProgramSemesters' => RegularProgramSemester::all(),
							'locations' => $locations,
				'alllocations' => Location::all(),
							'weekdays' => $weekdays,
				'allweekdays' => Weekday::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$regularProgramClasseSchedule = RegularProgramClasseSchedule::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$regularProgramClasseSchedule->id = $data['id'];
		}
				if(!empty($data['from_time']))
		{
			$regularProgramClasseSchedule->from_time = $data['from_time'];
		}
				if(!empty($data['to_time']))
		{
			$regularProgramClasseSchedule->to_time = $data['to_time'];
		}
				if(!empty($data['location_id']))
		{
			$regularProgramClasseSchedule->location_id = $data['location_id'];
		}
				if(!empty($data['weekday_id']))
		{
			$regularProgramClasseSchedule->weekday_id = $data['weekday_id'];
		}
				if(!empty($data['regular_program_semester_id']))
		{
			$regularProgramClasseSchedule->regular_program_semester_id = $data['regular_program_semester_id'];
		}
				$regularProgramClasseSchedule->save();
						if(!empty($data['regularProgramSemester']))
		{
			$regularProgramSemester = RegularProgramSemester::find($data['regularProgramSemester']);
						$regularProgramClasseSchedule->regularProgramSemester()->associate($regularProgramSemester);
					}
		$unsavedregularProgramSemester = $unsavedRelationships['regularProgramSemester'];
		if(!empty($unsavedregularProgramSemester))
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramSemester);
						$regularProgramClasseSchedule->regularProgramSemester()->associate($modelInstance);
					}
				if(!empty($data['location']))
		{
			$location = Location::find($data['location']);
						$regularProgramClasseSchedule->location()->associate($location);
					}
		$unsavedlocation = $unsavedRelationships['location'];
		if(!empty($unsavedlocation))
		{
			$modelInstance = getUnsavedRelationship($unsavedlocation);
						$regularProgramClasseSchedule->location()->associate($modelInstance);
					}
				if(!empty($data['weekday']))
		{
			$weekday = Weekday::find($data['weekday']);
						$regularProgramClasseSchedule->weekday()->associate($weekday);
					}
		$unsavedweekday = $unsavedRelationships['weekday'];
		if(!empty($unsavedweekday))
		{
			$modelInstance = getUnsavedRelationship($unsavedweekday);
						$regularProgramClasseSchedule->weekday()->associate($modelInstance);
					}
				$regularProgramClasseSchedule->save();
		return redirect('/modules/regularProgramClasseSchedules');
	}
	
	public function delete($id)
	{
		$regularProgramClasseSchedule = RegularProgramClasseSchedule::find($id);
		$regularProgramClasseSchedule->delete();
		return redirect('/modules/regularProgramClasseSchedules');
	}
	
	public function regularProgramClasseSchedule($id)
	{
		$regularProgramClasseSchedule = RegularProgramClasseSchedule::with('regularProgramSemester','location','weekday')->find($id);
		return response()->json($regularProgramClasseSchedule);
	}
	
	}
?>