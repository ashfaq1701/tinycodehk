<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InHomeProgram;
use App\Models\InHomeProgramFormat;
use App\Models\InHomeProgramPricing;
use App\Models\InHomeProgramSchedule;
use App\Models\InHomeProgramStructure;
use App\Models\InHomeProgramTopic;
use App\Models\Weekday;
use App\Models\Enrollment;
use Log;

class InHomeProgramController extends Controller {
	public function index() {
		$inHomePrograms = InHomeProgram::with ( 'inHomeProgramFormats', 'inHomeProgramPricings', 'inHomeProgramSchedules', 'inHomeProgramStructures', 'inHomeProgramTopics', 'weekdays' )->get ();
		return view ( 'modules.inHomePrograms.all', [ 
				'inHomePrograms' => $inHomePrograms 
		] );
	}
	public function search(Request $request) {
		$per_page = \Request::get ( 'per_page' ) ?: 10;
		if ($request ['query']) {
			$request = InHomeProgram::with ( 'inHomeProgramFormats', 'inHomeProgramPricings', 'inHomeProgramSchedules', 'inHomeProgramStructures', 'inHomeProgramTopics', 'weekdays' )->search ( $request ['query'] )->get ();
			$page = $request->has ( 'page' ) ? $request->page - 1 : 0;
			$total = $request->count ();
			$request = $request->slice ( $page * $per_page, $per_page );
			$request = new \Illuminate\Pagination\LengthAwarePaginator ( $request, $total, $per_page );
			return $request;
		}
		return 'not found';
	}
	public function create() {
		return view ( 'modules.inHomePrograms.form', [ 
				'allinHomeProgramFormats' => InHomeProgramFormat::all (),
				'allinHomeProgramPricings' => InHomeProgramPricing::all (),
				'allinHomeProgramSchedules' => InHomeProgramSchedule::all (),
				'allinHomeProgramStructures' => InHomeProgramStructure::all (),
				'allinHomeProgramTopics' => InHomeProgramTopic::all (),
				'allweekdays' => Weekday::all (),
				'allEnrollments' => Enrollment::all()
		] );
	}
	public function store(Request $request) {
		$this->validate ( $request, [ ] );
		$data = $request->all ();
		$unsavedRelationshipsJSON = $data ['unsavedRelationships'];
		$unsavedRelationships = json_decode ( $unsavedRelationshipsJSON, true );
		$inHomeProgram = new InHomeProgram ();
		if (! empty ( $data ['id'] )) {
			$inHomeProgram->id = $data ['id'];
		}
		if (! empty ( $data ['name'] )) {
			$inHomeProgram->name = $data ['name'];
		}
		if (! empty ( $data ['from_age'] )) {
			$inHomeProgram->from_age = $data ['from_age'];
		}
		if (! empty ( $data ['to_age'] )) {
			$inHomeProgram->to_age = $data ['to_age'];
		}
		if (! empty ( $data ['description_1'] )) {
			$inHomeProgram->description_1 = $data ['description_1'];
		}
		if (! empty ( $data ['description_2'] )) {
			$inHomeProgram->description_2 = $data ['description_2'];
		}
		if (! empty ( $data ['image_1'] )) {
			$inHomeProgram->image_1 = $data ['image_1'];
		}
		if (! empty ( $data ['image_2'] )) {
			$inHomeProgram->image_2 = $data ['image_2'];
		}
		if (! empty ( $data ['language'] )) {
			$inHomeProgram->language = $data ['language'];
		}
		$inHomeProgram->save ();
		
		if(! empty ($data['enrollments'])) {
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
				$inHomeProgram->enrollments()->save($enrollment);
			}
		}
		$unsavedEnrollments = $unsavedRelationships['enrollments'];
		foreach ($unsavedEnrollments as $unsavedEnrollment)
		{
			$modelInstance = getUnsavedRelationship ( $unsavedEnrollment );
			if(!empty($modelInstance))
			{
				$inHomeProgram->enrollments()->save($modelInstance);
			}
		}
		
		if (! empty ( $data ['inHomeProgramFormats'] )) {
			$inHomeProgramFormats = explode ( ',', $data ['inHomeProgramFormats'] );
			foreach ( $inHomeProgramFormats as $singleInHomeProgramFormat ) {
				$inHomeProgramFormat = InHomeProgramFormat::find ( $singleInHomeProgramFormat );
				$inHomeProgram->inHomeProgramFormats ()->save ( $inHomeProgramFormat );
			}
		}
		$unsavedinHomeProgramFormats = $unsavedRelationships ['inHomeProgramFormats'];
		foreach ( $unsavedinHomeProgramFormats as $unsavedinHomeProgramFormat ) {
			$modelInstance = getUnsavedRelationship ( $unsavedinHomeProgramFormat );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->inHomeProgramFormats ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['inHomeProgramPricings'] )) {
			$inHomeProgramPricings = explode ( ',', $data ['inHomeProgramPricings'] );
			foreach ( $inHomeProgramPricings as $singleInHomeProgramPricing ) {
				$inHomeProgramPricing = InHomeProgramPricing::find ( $singleInHomeProgramPricing );
				$inHomeProgram->inHomeProgramPricings ()->save ( $inHomeProgramPricing );
			}
		}
		$unsavedinHomeProgramPricings = $unsavedRelationships ['inHomeProgramPricings'];
		foreach ( $unsavedinHomeProgramPricings as $unsavedinHomeProgramPricing ) {
			$modelInstance = getUnsavedRelationship ( $unsavedinHomeProgramPricing );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->inHomeProgramPricings ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['inHomeProgramSchedules'] )) {
			$inHomeProgramSchedules = explode ( ',', $data ['inHomeProgramSchedules'] );
			foreach ( $inHomeProgramSchedules as $singleInHomeProgramSchedule ) {
				$inHomeProgramSchedule = InHomeProgramSchedule::find ( $singleInHomeProgramSchedule );
				$inHomeProgram->inHomeProgramSchedules ()->save ( $inHomeProgramSchedule );
			}
		}
		$unsavedinHomeProgramSchedules = $unsavedRelationships ['inHomeProgramSchedules'];
		foreach ( $unsavedinHomeProgramSchedules as $unsavedinHomeProgramSchedule ) {
			$modelInstance = getUnsavedRelationship ( $unsavedinHomeProgramSchedule );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->inHomeProgramSchedules ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['inHomeProgramStructures'] )) {
			$inHomeProgramStructures = explode ( ',', $data ['inHomeProgramStructures'] );
			foreach ( $inHomeProgramStructures as $singleInHomeProgramStructure ) {
				$inHomeProgramStructure = InHomeProgramStructure::find ( $singleInHomeProgramStructure );
				$inHomeProgram->inHomeProgramStructures ()->save ( $inHomeProgramStructure );
			}
		}
		$unsavedinHomeProgramStructures = $unsavedRelationships ['inHomeProgramStructures'];
		foreach ( $unsavedinHomeProgramStructures as $unsavedinHomeProgramStructure ) {
			$modelInstance = getUnsavedRelationship ( $unsavedinHomeProgramStructure );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->inHomeProgramStructures ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['inHomeProgramTopics'] )) {
			$inHomeProgramTopics = explode ( ',', $data ['inHomeProgramTopics'] );
			foreach ( $inHomeProgramTopics as $singleInHomeProgramTopic ) {
				$inHomeProgramTopic = InHomeProgramTopic::find ( $singleInHomeProgramTopic );
				$inHomeProgram->inHomeProgramTopics ()->save ( $inHomeProgramTopic );
			}
		}
		$unsavedinHomeProgramTopics = $unsavedRelationships ['inHomeProgramTopics'];
		foreach ( $unsavedinHomeProgramTopics as $unsavedinHomeProgramTopic ) {
			$modelInstance = getUnsavedRelationship ( $unsavedinHomeProgramTopic );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->inHomeProgramTopics ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['weekdays'] )) {
			$weekdays = explode ( ',', $data ['weekdays'] );
			foreach ( $weekdays as $singleWeekday ) {
				$weekday = Weekday::find ( $singleWeekday );
				$inHomeProgram->weekdays ()->attach ( $weekday );
			}
		}
		$unsavedweekdays = $unsavedRelationships ['weekdays'];
		foreach ( $unsavedweekdays as $unsavedweekday ) {
			$modelInstance = getUnsavedRelationship ( $unsavedweekday );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->weekdays ()->attach ( $modelInstance );
			}
		}
		$inHomeProgram->save ();
		return redirect ( '/modules/inHomePrograms' );
	}
	public function get($id) {
		$inHomeProgram = InHomeProgram::with ( 'inHomeProgramFormats', 'inHomeProgramPricings', 'inHomeProgramSchedules', 'inHomeProgramStructures', 'inHomeProgramTopics', 'weekdays' )->find ( $id );
		$inHomeProgramFormatsArray = [ ];
		foreach ( $inHomeProgram->inHomeProgramFormats as $inHomeProgramFormat ) {
			
			$inHomeProgramFormatsArray [] = $inHomeProgramFormat->id;
		}
		$inHomeProgramFormatsValue = implode ( ',', $inHomeProgramFormatsArray );
		$inHomeProgramPricingsArray = [ ];
		foreach ( $inHomeProgram->inHomeProgramPricings as $inHomeProgramPricing ) {
			
			$inHomeProgramPricingsArray [] = $inHomeProgramPricing->id;
		}
		$inHomeProgramPricingsValue = implode ( ',', $inHomeProgramPricingsArray );
		$inHomeProgramSchedulesArray = [ ];
		foreach ( $inHomeProgram->inHomeProgramSchedules as $inHomeProgramSchedule ) {
			
			$inHomeProgramSchedulesArray [] = $inHomeProgramSchedule->id;
		}
		$inHomeProgramSchedulesValue = implode ( ',', $inHomeProgramSchedulesArray );
		$inHomeProgramStructuresArray = [ ];
		foreach ( $inHomeProgram->inHomeProgramStructures as $inHomeProgramStructure ) {
			
			$inHomeProgramStructuresArray [] = $inHomeProgramStructure->id;
		}
		$inHomeProgramStructuresValue = implode ( ',', $inHomeProgramStructuresArray );
		$inHomeProgramTopicsArray = [ ];
		foreach ( $inHomeProgram->inHomeProgramTopics as $inHomeProgramTopic ) {
			
			$inHomeProgramTopicsArray [] = $inHomeProgramTopic->id;
		}
		$inHomeProgramTopicsValue = implode ( ',', $inHomeProgramTopicsArray );
		$weekdaysArray = [ ];
		foreach ( $inHomeProgram->weekdays as $weekday ) {
			
			$weekdaysArray [] = $weekday->id;
		}
		$weekdaysValue = implode ( ',', $weekdaysArray );
		$enrollmentsArray = [];
		foreach ($inHomeProgram->enrollments as $enrollment)
		{
			$enrollmentsArray[] = $enrollment->id;
		}
		$enrollmentsValue = implode(',', $enrollmentsArray);
		return view ( 'modules.inHomePrograms.form', [ 
				'inHomeProgram' => $inHomeProgram,
				'allinHomeProgramFormats' => InHomeProgramFormat::all (),
				'inHomeProgramFormatsValue' => $inHomeProgramFormatsValue,
				'allinHomeProgramPricings' => InHomeProgramPricing::all (),
				'inHomeProgramPricingsValue' => $inHomeProgramPricingsValue,
				'allinHomeProgramSchedules' => InHomeProgramSchedule::all (),
				'inHomeProgramSchedulesValue' => $inHomeProgramSchedulesValue,
				'allinHomeProgramStructures' => InHomeProgramStructure::all (),
				'inHomeProgramStructuresValue' => $inHomeProgramStructuresValue,
				'allinHomeProgramTopics' => InHomeProgramTopic::all (),
				'inHomeProgramTopicsValue' => $inHomeProgramTopicsValue,
				'allweekdays' => Weekday::all (),
				'weekdaysValue' => $weekdaysValue,
				'allEnrollments' => Enrollment::all(),
				'enrollmentsValue' => $enrollmentsValue
		] );
	}
	public function update(Request $request, $id) {
		$this->validate ( $request, [ ] );
		$inHomeProgram = InHomeProgram::find ( $id );
		$data = $request->all ();
		$unsavedRelationshipsJSON = $data ['unsavedRelationships'];
		$unsavedRelationships = json_decode ( $unsavedRelationshipsJSON, true );
		if (! empty ( $data ['id'] )) {
			$inHomeProgram->id = $data ['id'];
		}
		if (! empty ( $data ['name'] )) {
			$inHomeProgram->name = $data ['name'];
		}
		if (! empty ( $data ['from_age'] )) {
			$inHomeProgram->from_age = $data ['from_age'];
		}
		if (! empty ( $data ['to_age'] )) {
			$inHomeProgram->to_age = $data ['to_age'];
		}
		if (! empty ( $data ['description_1'] )) {
			$inHomeProgram->description_1 = $data ['description_1'];
		}
		if (! empty ( $data ['description_2'] )) {
			$inHomeProgram->description_2 = $data ['description_2'];
		}
		if (! empty ( $data ['image_1'] )) {
			$inHomeProgram->image_1 = $data ['image_1'];
		}
		if (! empty ( $data ['image_2'] )) {
			$inHomeProgram->image_2 = $data ['image_2'];
		}
		if (! empty ( $data ['language'] )) {
			$inHomeProgram->language = $data ['language'];
		}
		$inHomeProgram->save ();
		if (! empty ( $data ['inHomeProgramFormats'] )) {
			$inHomeProgramFormats = explode ( ',', $data ['inHomeProgramFormats'] );
			foreach ( $inHomeProgramFormats as $singleInHomeProgramFormat ) {
				$inHomeProgramFormat = InHomeProgramFormat::find ( $singleInHomeProgramFormat );
				$inHomeProgram->inHomeProgramFormats ()->save ( $inHomeProgramFormat );
			}
		}
		$unsavedinHomeProgramFormats = $unsavedRelationships ['inHomeProgramFormats'];
		foreach ( $unsavedinHomeProgramFormats as $unsavedinHomeProgramFormat ) {
			$modelInstance = getUnsavedRelationship ( $unsavedinHomeProgramFormat );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->inHomeProgramFormats ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['inHomeProgramPricings'] )) {
			$inHomeProgramPricings = explode ( ',', $data ['inHomeProgramPricings'] );
			foreach ( $inHomeProgramPricings as $singleInHomeProgramPricing ) {
				$inHomeProgramPricing = InHomeProgramPricing::find ( $singleInHomeProgramPricing );
				$inHomeProgram->inHomeProgramPricings ()->save ( $inHomeProgramPricing );
			}
		}
		$unsavedinHomeProgramPricings = $unsavedRelationships ['inHomeProgramPricings'];
		foreach ( $unsavedinHomeProgramPricings as $unsavedinHomeProgramPricing ) {
			$modelInstance = getUnsavedRelationship ( $unsavedinHomeProgramPricing );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->inHomeProgramPricings ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['inHomeProgramSchedules'] )) {
			$inHomeProgramSchedules = explode ( ',', $data ['inHomeProgramSchedules'] );
			foreach ( $inHomeProgramSchedules as $singleInHomeProgramSchedule ) {
				$inHomeProgramSchedule = InHomeProgramSchedule::find ( $singleInHomeProgramSchedule );
				$inHomeProgram->inHomeProgramSchedules ()->save ( $inHomeProgramSchedule );
			}
		}
		$unsavedinHomeProgramSchedules = $unsavedRelationships ['inHomeProgramSchedules'];
		foreach ( $unsavedinHomeProgramSchedules as $unsavedinHomeProgramSchedule ) {
			$modelInstance = getUnsavedRelationship ( $unsavedinHomeProgramSchedule );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->inHomeProgramSchedules ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['inHomeProgramStructures'] )) {
			$inHomeProgramStructures = explode ( ',', $data ['inHomeProgramStructures'] );
			foreach ( $inHomeProgramStructures as $singleInHomeProgramStructure ) {
				$inHomeProgramStructure = InHomeProgramStructure::find ( $singleInHomeProgramStructure );
				$inHomeProgram->inHomeProgramStructures ()->save ( $inHomeProgramStructure );
			}
		}
		$unsavedinHomeProgramStructures = $unsavedRelationships ['inHomeProgramStructures'];
		foreach ( $unsavedinHomeProgramStructures as $unsavedinHomeProgramStructure ) {
			$modelInstance = getUnsavedRelationship ( $unsavedinHomeProgramStructure );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->inHomeProgramStructures ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['inHomeProgramTopics'] )) {
			$inHomeProgramTopics = explode ( ',', $data ['inHomeProgramTopics'] );
			foreach ( $inHomeProgramTopics as $singleInHomeProgramTopic ) {
				$inHomeProgramTopic = InHomeProgramTopic::find ( $singleInHomeProgramTopic );
				$inHomeProgram->inHomeProgramTopics ()->save ( $inHomeProgramTopic );
			}
		}
		$unsavedinHomeProgramTopics = $unsavedRelationships ['inHomeProgramTopics'];
		foreach ( $unsavedinHomeProgramTopics as $unsavedinHomeProgramTopic ) {
			$modelInstance = getUnsavedRelationship ( $unsavedinHomeProgramTopic );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->inHomeProgramTopics ()->save ( $modelInstance );
			}
		}
		if (! empty ( $data ['weekdays'] )) {
			$weekdays = explode ( ',', $data ['weekdays'] );
			foreach ( $weekdays as $singleWeekday ) {
				$weekday = Weekday::find ( $singleWeekday );
				$inHomeProgram->weekdays ()->attach ( $weekday );
			}
		}
		$unsavedweekdays = $unsavedRelationships ['weekdays'];
		foreach ( $unsavedweekdays as $unsavedweekday ) {
			$modelInstance = getUnsavedRelationship ( $unsavedweekday );
			if (! empty ( $modelInstance )) {
				$inHomeProgram->weekdays ()->attach ( $modelInstance );
			}
		}
		if(! empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
				$inHomeProgram->enrollments()->save($enrollment);
			}
		}
		$unsavedEnrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedEnrollments as $unsavedEnrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedEnrollment);
			if(!empty($modelInstance))
			{
				$inHomeProgram->enrollments()->save($modelInstance);
			}
		}
		$inHomeProgram->save ();
		return redirect ( '/modules/inHomePrograms' );
	}
	public function delete($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgram->delete ();
		return redirect ( '/modules/inHomePrograms' );
	}
	public function inHomeProgram($id) {
		$inHomeProgram = InHomeProgram::with ( 'inHomeProgramFormats', 'inHomeProgramPricings', 'inHomeProgramSchedules', 'inHomeProgramStructures', 'inHomeProgramTopics', 'weekdays' )->find ( $id );
		return response ()->json ( $inHomeProgram );
	}
	public function inHomeProgramFormats($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgramFormats = $inHomeProgram->inHomeProgramFormats;
		for($i = 0; $i < count ( $inHomeProgramFormats ); $i ++) {
			$inHomeProgramFormat = $inHomeProgramFormats [$i];
			$inHomeProgramFormat->inHomeProgram = $inHomeProgram;
			$inHomeProgramFormats [$i] = $inHomeProgramFormat;
		}
		return view ( 'modules.inHomeProgramFormats.all', [ 
				'inHomeProgramFormats' => $inHomeProgramFormats 
		] );
	}
	public function inHomeProgramFormatsApi($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgramFormats = $inHomeProgram->inHomeProgramFormats;
		return response ()->json ( $inHomeProgramFormats->all () );
	}
	public function inHomeProgramPricings($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgramPricings = $inHomeProgram->inHomeProgramPricings;
		for($i = 0; $i < count ( $inHomeProgramPricings ); $i ++) {
			$inHomeProgramPricing = $inHomeProgramPricings [$i];
			$inHomeProgramPricing->inHomeProgram = $inHomeProgram;
			$inHomeProgramPricings [$i] = $inHomeProgramPricing;
		}
		return view ( 'modules.inHomeProgramPricings.all', [ 
				'inHomeProgramPricings' => $inHomeProgramPricings 
		] );
	}
	public function inHomeProgramPricingsApi($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgramPricings = $inHomeProgram->inHomeProgramPricings;
		return response ()->json ( $inHomeProgramPricings->all () );
	}
	public function inHomeProgramSchedules($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgramSchedules = $inHomeProgram->inHomeProgramSchedules;
		for($i = 0; $i < count ( $inHomeProgramSchedules ); $i ++) {
			$inHomeProgramSchedule = $inHomeProgramSchedules [$i];
			$inHomeProgramSchedule->inHomeProgram = $inHomeProgram;
			$inHomeProgramSchedules [$i] = $inHomeProgramSchedule;
		}
		return view ( 'modules.inHomeProgramSchedules.all', [ 
				'inHomeProgramSchedules' => $inHomeProgramSchedules 
		] );
	}
	public function inHomeProgramSchedulesApi($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgramSchedules = $inHomeProgram->inHomeProgramSchedules;
		return response ()->json ( $inHomeProgramSchedules->all () );
	}
	public function inHomeProgramStructures($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgramStructures = $inHomeProgram->inHomeProgramStructures;
		for($i = 0; $i < count ( $inHomeProgramStructures ); $i ++) {
			$inHomeProgramStructure = $inHomeProgramStructures [$i];
			$inHomeProgramStructure->inHomeProgram = $inHomeProgram;
			$inHomeProgramStructures [$i] = $inHomeProgramStructure;
		}
		return view ( 'modules.inHomeProgramStructures.all', [ 
				'inHomeProgramStructures' => $inHomeProgramStructures 
		] );
	}
	public function inHomeProgramStructuresApi($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgramStructures = $inHomeProgram->inHomeProgramStructures;
		return response ()->json ( $inHomeProgramStructures->all () );
	}
	public function inHomeProgramTopics($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgramTopics = $inHomeProgram->inHomeProgramTopics;
		for($i = 0; $i < count ( $inHomeProgramTopics ); $i ++) {
			$inHomeProgramTopic = $inHomeProgramTopics [$i];
			$inHomeProgramTopic->inHomeProgram = $inHomeProgram;
			$inHomeProgramTopics [$i] = $inHomeProgramTopic;
		}
		return view ( 'modules.inHomeProgramTopics.all', [ 
				'inHomeProgramTopics' => $inHomeProgramTopics 
		] );
	}
	public function inHomeProgramTopicsApi($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$inHomeProgramTopics = $inHomeProgram->inHomeProgramTopics;
		return response ()->json ( $inHomeProgramTopics->all () );
	}
	public function weekdays($id) {
		$inHomeProgram = InHomeProgram::find ( $id );
		$weekdays = $inHomeProgram->weekdays;
		for($i = 0; $i < count ( $weekdays ); $i ++) {
			$weekday = $weekdays [$i];
			$weekday->inHomeProgram = $inHomeProgram;
			$weekdays[$i] = $weekday;
		}
		return view('modules.weekdays.all', ['weekdays' => $weekdays]);
	}
	
	public function weekdaysApi($id)
	{
		$inHomeProgram = InHomeProgram::find($id);
		$weekdays = $inHomeProgram->weekdays;
		return response()->json($weekdays->all());
	}
	
	public function enrollments($id)
	{
		$inHomeProram = InHomeProgram::with('enrollments', 'enrollments.course')->find($id);
		$enrollments = $inHomeProram->enrollments;
		return view('modules.enrollments.all', [
			'enrollments' => $enrollments
		]);
	}
	public function enrollmentsApi($id)
	{
		$inHomeProgram = inHomeProgram::find($id);
		$enrollments = $inHomeProgram->enrollments;
		return response()->json($enrollments->all());
	}
}
?>