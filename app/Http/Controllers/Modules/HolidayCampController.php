<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HolidayCamp;
use App\Models\HolidayCampCourse;
use Log;

class HolidayCampController extends Controller
{	
	public function index()
	{
		$holidayCamps = HolidayCamp::with('holidayCampCourses')->get();
		return view('modules.holidayCamps.all', ['holidayCamps' => $holidayCamps]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = HolidayCamp::with('holidayCampCourses')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				return view('modules.holidayCamps.form', [
					'allholidayCampCourses' => HolidayCampCourse::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$holidayCamp = new HolidayCamp();
				if(!empty($data['id']))
		{
			$holidayCamp->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$holidayCamp->name = $data['name'];
		}
				if(!empty($data['description']))
		{
			$holidayCamp->description = $data['description'];
		}
				if(!empty($data['from_date']))
		{
			$holidayCamp->from_date = $data['from_date'];
		}
				if(!empty($data['to_date']))
		{
			$holidayCamp->to_date = $data['to_date'];
		}
				$holidayCamp->save();
				if(!empty($data['holidayCampCourses']))
		{
			$holidayCampCourses = explode(',', $data['holidayCampCourses']);
			foreach ($holidayCampCourses as $singleHolidayCampCourse)
			{
				$holidayCampCourse = HolidayCampCourse::find($singleHolidayCampCourse);
								$holidayCamp->holidayCampCourses()->save($holidayCampCourse);
							}
		}
		$unsavedholidayCampCourses = $unsavedRelationships['holidayCampCourses'];
		foreach($unsavedholidayCampCourses as $unsavedholidayCampCourse)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
			if(!empty($modelInstance))
			{
								$holidayCamp->holidayCampCourses()->save($modelInstance);
							}
		}
						$holidayCamp->save();
		return redirect('/modules/holidayCamps');
	}
	
	public function get($id)
	{
		$holidayCamp = HolidayCamp::with('holidayCampCourses')->find($id);
						$holidayCampCoursesArray = [];
		foreach($holidayCamp->holidayCampCourses as $holidayCampCourse)
		{
			
			$holidayCampCoursesArray[] = $holidayCampCourse->id;
		}
		$holidayCampCoursesValue = implode(',', $holidayCampCoursesArray);
				return view('modules.holidayCamps.form', [
			'holidayCamp' => $holidayCamp,
							'allholidayCampCourses' => HolidayCampCourse::all(),
				'holidayCampCoursesValue' => $holidayCampCoursesValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$holidayCamp = HolidayCamp::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$holidayCamp->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$holidayCamp->name = $data['name'];
		}
				if(!empty($data['description']))
		{
			$holidayCamp->description = $data['description'];
		}
				if(!empty($data['from_date']))
		{
			$holidayCamp->from_date = $data['from_date'];
		}
				if(!empty($data['to_date']))
		{
			$holidayCamp->to_date = $data['to_date'];
		}
				$holidayCamp->save();
				if(!empty($data['holidayCampCourses']))
		{
			$holidayCampCourses = explode(',', $data['holidayCampCourses']);
			foreach ($holidayCampCourses as $singleHolidayCampCourse)
			{
				$holidayCampCourse = HolidayCampCourse::find($singleHolidayCampCourse);
								$holidayCamp->holidayCampCourses()->save($holidayCampCourse);
							}
		}
		$unsavedholidayCampCourses = $unsavedRelationships['holidayCampCourses'];
		foreach($unsavedholidayCampCourses as $unsavedholidayCampCourse)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
			if(!empty($modelInstance))
			{
								$holidayCamp->holidayCampCourses()->save($modelInstance);
							}
		}
						$holidayCamp->save();
		return redirect('/modules/holidayCamps');
	}
	
	public function delete($id)
	{
		$holidayCamp = HolidayCamp::find($id);
		$holidayCamp->delete();
		return redirect('/modules/holidayCamps');
	}
	
	public function holidayCamp($id)
	{
		$holidayCamp = HolidayCamp::with('holidayCampCourses')->find($id);
		return response()->json($holidayCamp);
	}
	
		public function holidayCampCourses($id)
	{
		$holidayCamp = HolidayCamp::find($id);
		$holidayCampCourses = $holidayCamp->holidayCampCourses;
		for ($i = 0; $i < count($holidayCampCourses); $i++)
		{
			$holidayCampCourse = $holidayCampCourses[$i];
			$holidayCampCourse->holidayCamp = $holidayCamp;
			$holidayCampCourses[$i] = $holidayCampCourse;
		}
		return view('modules.holidayCampCourses.all', ['holidayCampCourses' => $holidayCampCourses]);
	}
	
	public function holidayCampCoursesApi($id)
	{
		$holidayCamp = HolidayCamp::find($id);
		$holidayCampCourses = $holidayCamp->holidayCampCourses;
		return response()->json($holidayCampCourses->all());
	}
	}
?>