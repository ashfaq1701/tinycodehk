<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HolidayCampModule;
use App\Models\HolidayCampCourse;
use App\Models\Location;
use App\Models\Enrollment;
use Log;

class HolidayCampModuleController extends Controller {
	public function index() {
		$holidayCampModules = HolidayCampModule::with ( 'holidayCampCourse', 'location' )->get ();
		return view ( 'modules.holidayCampModules.all', [ 
				'holidayCampModules' => $holidayCampModules 
		] );
	}
	public function search(Request $request) {
		$per_page = \Request::get ( 'per_page' ) ?: 10;
		if ($request ['query']) {
			$request = HolidayCampModule::with ( 'holidayCampCourse', 'location' )->search ( $request ['query'] )->get ();
			$page = $request->has ( 'page' ) ? $request->page - 1 : 0;
			$total = $request->count ();
			$request = $request->slice ( $page * $per_page, $per_page );
			$request = new \Illuminate\Pagination\LengthAwarePaginator ( $request, $total, $per_page );
			return $request;
		}
		return 'not found';
	}
	public function create() {
		$holidayCampCourses = HolidayCampCourse::all ();
		$locations = Location::all ();
		return view ( 'modules.holidayCampModules.form', [ 
				'holidayCampCourses' => $holidayCampCourses,
				'allholidayCampCourses' => HolidayCampCourse::all (),
				'locations' => $locations,
				'alllocations' => Location::all (),
				'allEnrollments' => Enrollment::all()
		] );
	}
	public function store(Request $request) {
		$this->validate ( $request, [ ] );
		$data = $request->all ();
		$unsavedRelationshipsJSON = $data ['unsavedRelationships'];
		$unsavedRelationships = json_decode ( $unsavedRelationshipsJSON, true );
		$holidayCampModule = new HolidayCampModule ();
		if (! empty ( $data ['id'] )) {
			$holidayCampModule->id = $data ['id'];
		}
		if (! empty ( $data ['module_code'] )) {
			$holidayCampModule->module_code = $data ['module_code'];
		}
		if (! empty ( $data ['from_date'] )) {
			$holidayCampModule->from_date = $data ['from_date'];
		}
		if (! empty ( $data ['to_date'] )) {
			$holidayCampModule->to_date = $data ['to_date'];
		}
		if (! empty ( $data ['from_time'] )) {
			$holidayCampModule->from_time = $data ['from_time'];
		}
		if (! empty ( $data ['to_time'] )) {
			$holidayCampModule->to_time = $data ['to_time'];
		}
		if (! empty ( $data ['regular_price'] )) {
			$holidayCampModule->regular_price = $data ['regular_price'];
		}
		if (! empty ( $data ['holiday_camp_course_id'] )) {
			$holidayCampModule->holiday_camp_course_id = $data ['holiday_camp_course_id'];
		}
		if (! empty ( $data ['location_id'] )) {
			$holidayCampModule->location_id = $data ['location_id'];
		}
		$holidayCampModule->save ();
		if(! empty ($data['enrollments'])) {
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
				$holidayCampModule->enrollments()->save($enrollment);
			}
		}
		$unsavedEnrollments = $unsavedRelationships['enrollments'];
		foreach ($unsavedEnrollments as $unsavedEnrollment)
		{
			$modelInstance = getUnsavedRelationship ( $unsavedEnrollment );
			if(!empty($modelInstance))
			{
				$holidayCampModule->enrollments()->save($modelInstance);
			}
		}
		if (! empty ( $data ['holidayCampCourse'] )) {
			$holidayCampCourse = HolidayCampCourse::find ( $data ['holidayCampCourse'] );
			$holidayCampModule->holidayCampCourse ()->associate ( $holidayCampCourse );
		}
		$unsavedholidayCampCourse = $unsavedRelationships ['holidayCampCourse'];
		if (! empty ( $unsavedholidayCampCourse )) {
			$modelInstance = getUnsavedRelationship ( $unsavedholidayCampCourse );
			$holidayCampModule->holidayCampCourse ()->associate ( $modelInstance );
		}
		if (! empty ( $data ['location'] )) {
			$location = Location::find ( $data ['location'] );
			$holidayCampModule->location ()->associate ( $location );
		}
		$unsavedlocation = $unsavedRelationships ['location'];
		if (! empty ( $unsavedlocation )) {
			$modelInstance = getUnsavedRelationship ( $unsavedlocation );
			$holidayCampModule->location ()->associate ( $modelInstance );
		}
		$holidayCampModule->save ();
		return redirect ( '/modules/holidayCampModules' );
	}
	public function get($id) {
		$holidayCampModule = HolidayCampModule::with ( 'holidayCampCourse', 'location' )->find ( $id );
		$holidayCampCourses = HolidayCampCourse::all ();
		$locations = Location::all ();
		$enrollmentsArray = [];
		foreach ($holidayCampModule->enrollments as $enrollment)
		{
			$enrollmentsArray[] = $enrollment->id;
		}
		$enrollmentsValue = implode(',', $enrollmentsArray);
		return view ( 'modules.holidayCampModules.form', [ 
				'holidayCampModule' => $holidayCampModule,
				'holidayCampCourses' => $holidayCampCourses,
				'allholidayCampCourses' => HolidayCampCourse::all (),
				'locations' => $locations,
				'alllocations' => Location::all (),
				'allEnrollments' => Enrollment::all(),
				'enrollmentsValue' => $enrollmentsValue
		] );
	}
	public function update(Request $request, $id) {
		$this->validate ( $request, [ ] );
		$holidayCampModule = HolidayCampModule::find ( $id );
		$data = $request->all ();
		$unsavedRelationshipsJSON = $data ['unsavedRelationships'];
		$unsavedRelationships = json_decode ( $unsavedRelationshipsJSON, true );
		if (! empty ( $data ['id'] )) {
			$holidayCampModule->id = $data ['id'];
		}
		if (! empty ( $data ['module_code'] )) {
			$holidayCampModule->module_code = $data ['module_code'];
		}
		if (! empty ( $data ['from_date'] )) {
			$holidayCampModule->from_date = $data ['from_date'];
		}
		if (! empty ( $data ['to_date'] )) {
			$holidayCampModule->to_date = $data ['to_date'];
		}
		if (! empty ( $data ['from_time'] )) {
			$holidayCampModule->from_time = $data ['from_time'];
		}
		if (! empty ( $data ['to_time'] )) {
			$holidayCampModule->to_time = $data ['to_time'];
		}
		if (! empty ( $data ['regular_price'] )) {
			$holidayCampModule->regular_price = $data ['regular_price'];
		}
		if (! empty ( $data ['holiday_camp_course_id'] )) {
			$holidayCampModule->holiday_camp_course_id = $data ['holiday_camp_course_id'];
		}
		if (! empty ( $data ['location_id'] )) {
			$holidayCampModule->location_id = $data ['location_id'];
		}
		$holidayCampModule->save ();
		if(! empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
				$holidayCampModule->enrollments()->save($enrollment);
			}
		}
		$unsavedEnrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedEnrollments as $unsavedEnrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedEnrollment);
			if(!empty($modelInstance))
			{
				$holidayCampModule->enrollments()->save($modelInstance);
			}
		}
		if (! empty ( $data ['holidayCampCourse'] )) {
			$holidayCampCourse = HolidayCampCourse::find ( $data ['holidayCampCourse'] );
			$holidayCampModule->holidayCampCourse ()->associate ( $holidayCampCourse );
		}
		$unsavedholidayCampCourse = $unsavedRelationships ['holidayCampCourse'];
		if (! empty ( $unsavedholidayCampCourse )) {
			$modelInstance = getUnsavedRelationship ( $unsavedholidayCampCourse );
			$holidayCampModule->holidayCampCourse ()->associate ( $modelInstance );
		}
		if (! empty ( $data ['location'] )) {
			$location = Location::find ( $data ['location'] );
			$holidayCampModule->location ()->associate ( $location );
		}
		$unsavedlocation = $unsavedRelationships ['location'];
		if (! empty ( $unsavedlocation )) {
			$modelInstance = getUnsavedRelationship ( $unsavedlocation );
			$holidayCampModule->location ()->associate ( $modelInstance );
		}
		$holidayCampModule->save ();
		return redirect ( '/modules/holidayCampModules' );
	}
	public function delete($id) {
		$holidayCampModule = HolidayCampModule::find ( $id );
		$holidayCampModule->delete ();
		return redirect ( '/modules/holidayCampModules' );
	}
	public function holidayCampModule($id) {
		$holidayCampModule = HolidayCampModule::with ( 'holidayCampCourse', 'location' )->find ( $id );
		return response ()->json ( $holidayCampModule );
	}
	public function enrollments($id) {
		$holidayCampModule = HolidayCampModule::with ( 'enrollments', 'enrollments.course' )->find ( $id );
		$enrollments = $holidayCampModule->enrollments;
		return view ( 'modules.enrollments.all', [ 
				'enrollments' => $enrollments 
		] );
	}
	public function enrollmentsApi($id) {
		$holidayCampModule = HolidayCampModule::find($id);
		$enrollments = $holidayCampModule->enrollments;
		return response()->json($enrollments->all());
	}
	
}
?>