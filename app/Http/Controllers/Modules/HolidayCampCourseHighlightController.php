<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HolidayCampCourseHighlight;
use App\Models\HolidayCampCourse;
use Log;

class HolidayCampCourseHighlightController extends Controller
{	
	public function index()
	{
		$holidayCampCourseHighlights = HolidayCampCourseHighlight::with('holidayCampCourse')->get();
		return view('modules.holidayCampCourseHighlights.all', ['holidayCampCourseHighlights' => $holidayCampCourseHighlights]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = HolidayCampCourseHighlight::with('holidayCampCourse')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$holidayCampCourses = HolidayCampCourse::all();
				return view('modules.holidayCampCourseHighlights.form', [
					'holidayCampCourses' => $holidayCampCourses,
			'allholidayCampCourses' => HolidayCampCourse::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$holidayCampCourseHighlight = new HolidayCampCourseHighlight();
				if(!empty($data['id']))
		{
			$holidayCampCourseHighlight->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$holidayCampCourseHighlight->title = $data['title'];
		}
				if(!empty($data['description']))
		{
			$holidayCampCourseHighlight->description = $data['description'];
		}
				if(!empty($data['holiday_camp_course_id']))
		{
			$holidayCampCourseHighlight->holiday_camp_course_id = $data['holiday_camp_course_id'];
		}
				$holidayCampCourseHighlight->save();
						if(!empty($data['holidayCampCourse']))
		{
			$holidayCampCourse = HolidayCampCourse::find($data['holidayCampCourse']);
						$holidayCampCourseHighlight->holidayCampCourse()->associate($holidayCampCourse);
					}
		$unsavedholidayCampCourse = $unsavedRelationships['holidayCampCourse'];
		if(!empty($unsavedholidayCampCourse))
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
						$holidayCampCourseHighlight->holidayCampCourse()->associate($modelInstance);
					}
				$holidayCampCourseHighlight->save();
		return redirect('/modules/holidayCampCourseHighlights');
	}
	
	public function get($id)
	{
		$holidayCampCourseHighlight = HolidayCampCourseHighlight::with('holidayCampCourse')->find($id);
				$holidayCampCourses = HolidayCampCourse::all();
						return view('modules.holidayCampCourseHighlights.form', [
			'holidayCampCourseHighlight' => $holidayCampCourseHighlight,
							'holidayCampCourses' => $holidayCampCourses,
				'allholidayCampCourses' => HolidayCampCourse::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$holidayCampCourseHighlight = HolidayCampCourseHighlight::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$holidayCampCourseHighlight->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$holidayCampCourseHighlight->title = $data['title'];
		}
				if(!empty($data['description']))
		{
			$holidayCampCourseHighlight->description = $data['description'];
		}
				if(!empty($data['holiday_camp_course_id']))
		{
			$holidayCampCourseHighlight->holiday_camp_course_id = $data['holiday_camp_course_id'];
		}
				$holidayCampCourseHighlight->save();
						if(!empty($data['holidayCampCourse']))
		{
			$holidayCampCourse = HolidayCampCourse::find($data['holidayCampCourse']);
						$holidayCampCourseHighlight->holidayCampCourse()->associate($holidayCampCourse);
					}
		$unsavedholidayCampCourse = $unsavedRelationships['holidayCampCourse'];
		if(!empty($unsavedholidayCampCourse))
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
						$holidayCampCourseHighlight->holidayCampCourse()->associate($modelInstance);
					}
				$holidayCampCourseHighlight->save();
		return redirect('/modules/holidayCampCourseHighlights');
	}
	
	public function delete($id)
	{
		$holidayCampCourseHighlight = HolidayCampCourseHighlight::find($id);
		$holidayCampCourseHighlight->delete();
		return redirect('/modules/holidayCampCourseHighlights');
	}
	
	public function holidayCampCourseHighlight($id)
	{
		$holidayCampCourseHighlight = HolidayCampCourseHighlight::with('holidayCampCourse')->find($id);
		return response()->json($holidayCampCourseHighlight);
	}
	
	}
?>