<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HolidayCampCourseBringing;
use App\Models\HolidayCampCourse;
use Log;

class HolidayCampCourseBringingController extends Controller
{	
	public function index()
	{
		$holidayCampCourseBringings = HolidayCampCourseBringing::with('holidayCampCourse')->get();
		return view('modules.holidayCampCourseBringings.all', ['holidayCampCourseBringings' => $holidayCampCourseBringings]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = HolidayCampCourseBringing::with('holidayCampCourse')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$holidayCampCourses = HolidayCampCourse::all();
				return view('modules.holidayCampCourseBringings.form', [
					'holidayCampCourses' => $holidayCampCourses,
			'allholidayCampCourses' => HolidayCampCourse::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$holidayCampCourseBringing = new HolidayCampCourseBringing();
				if(!empty($data['id']))
		{
			$holidayCampCourseBringing->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$holidayCampCourseBringing->name = $data['name'];
		}
				if(!empty($data['holiday_camp_course_id']))
		{
			$holidayCampCourseBringing->holiday_camp_course_id = $data['holiday_camp_course_id'];
		}
				$holidayCampCourseBringing->save();
						if(!empty($data['holidayCampCourse']))
		{
			$holidayCampCourse = HolidayCampCourse::find($data['holidayCampCourse']);
						$holidayCampCourseBringing->holidayCampCourse()->associate($holidayCampCourse);
					}
		$unsavedholidayCampCourse = $unsavedRelationships['holidayCampCourse'];
		if(!empty($unsavedholidayCampCourse))
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
						$holidayCampCourseBringing->holidayCampCourse()->associate($modelInstance);
					}
				$holidayCampCourseBringing->save();
		return redirect('/modules/holidayCampCourseBringings');
	}
	
	public function get($id)
	{
		$holidayCampCourseBringing = HolidayCampCourseBringing::with('holidayCampCourse')->find($id);
				$holidayCampCourses = HolidayCampCourse::all();
						return view('modules.holidayCampCourseBringings.form', [
			'holidayCampCourseBringing' => $holidayCampCourseBringing,
							'holidayCampCourses' => $holidayCampCourses,
				'allholidayCampCourses' => HolidayCampCourse::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$holidayCampCourseBringing = HolidayCampCourseBringing::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$holidayCampCourseBringing->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$holidayCampCourseBringing->name = $data['name'];
		}
				if(!empty($data['holiday_camp_course_id']))
		{
			$holidayCampCourseBringing->holiday_camp_course_id = $data['holiday_camp_course_id'];
		}
				$holidayCampCourseBringing->save();
						if(!empty($data['holidayCampCourse']))
		{
			$holidayCampCourse = HolidayCampCourse::find($data['holidayCampCourse']);
						$holidayCampCourseBringing->holidayCampCourse()->associate($holidayCampCourse);
					}
		$unsavedholidayCampCourse = $unsavedRelationships['holidayCampCourse'];
		if(!empty($unsavedholidayCampCourse))
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
						$holidayCampCourseBringing->holidayCampCourse()->associate($modelInstance);
					}
				$holidayCampCourseBringing->save();
		return redirect('/modules/holidayCampCourseBringings');
	}
	
	public function delete($id)
	{
		$holidayCampCourseBringing = HolidayCampCourseBringing::find($id);
		$holidayCampCourseBringing->delete();
		return redirect('/modules/holidayCampCourseBringings');
	}
	
	public function holidayCampCourseBringing($id)
	{
		$holidayCampCourseBringing = HolidayCampCourseBringing::with('holidayCampCourse')->find($id);
		return response()->json($holidayCampCourseBringing);
	}
	
	}
?>