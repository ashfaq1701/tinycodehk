<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RegularProgramFormat;
use App\Models\RegularProgram;
use Log;

class RegularProgramFormatController extends Controller
{	
	public function index()
	{
		$regularProgramFormats = RegularProgramFormat::with('regularProgram')->get();
		return view('modules.regularProgramFormats.all', ['regularProgramFormats' => $regularProgramFormats]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = RegularProgramFormat::with('regularProgram')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$regularPrograms = RegularProgram::all();
				return view('modules.regularProgramFormats.form', [
					'regularPrograms' => $regularPrograms,
			'allregularPrograms' => RegularProgram::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$regularProgramFormat = new RegularProgramFormat();
				if(!empty($data['id']))
		{
			$regularProgramFormat->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$regularProgramFormat->title = $data['title'];
		}
				if(!empty($data['regular_program_id']))
		{
			$regularProgramFormat->regular_program_id = $data['regular_program_id'];
		}
				$regularProgramFormat->save();
						if(!empty($data['regularProgram']))
		{
			$regularProgram = RegularProgram::find($data['regularProgram']);
						$regularProgramFormat->regularProgram()->associate($regularProgram);
					}
		$unsavedregularProgram = $unsavedRelationships['regularProgram'];
		if(!empty($unsavedregularProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgram);
						$regularProgramFormat->regularProgram()->associate($modelInstance);
					}
				$regularProgramFormat->save();
		return redirect('/modules/regularProgramFormats');
	}
	
	public function get($id)
	{
		$regularProgramFormat = RegularProgramFormat::with('regularProgram')->find($id);
				$regularPrograms = RegularProgram::all();
						return view('modules.regularProgramFormats.form', [
			'regularProgramFormat' => $regularProgramFormat,
							'regularPrograms' => $regularPrograms,
				'allregularPrograms' => RegularProgram::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$regularProgramFormat = RegularProgramFormat::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$regularProgramFormat->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$regularProgramFormat->title = $data['title'];
		}
				if(!empty($data['regular_program_id']))
		{
			$regularProgramFormat->regular_program_id = $data['regular_program_id'];
		}
				$regularProgramFormat->save();
						if(!empty($data['regularProgram']))
		{
			$regularProgram = RegularProgram::find($data['regularProgram']);
						$regularProgramFormat->regularProgram()->associate($regularProgram);
					}
		$unsavedregularProgram = $unsavedRelationships['regularProgram'];
		if(!empty($unsavedregularProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgram);
						$regularProgramFormat->regularProgram()->associate($modelInstance);
					}
				$regularProgramFormat->save();
		return redirect('/modules/regularProgramFormats');
	}
	
	public function delete($id)
	{
		$regularProgramFormat = RegularProgramFormat::find($id);
		$regularProgramFormat->delete();
		return redirect('/modules/regularProgramFormats');
	}
	
	public function regularProgramFormat($id)
	{
		$regularProgramFormat = RegularProgramFormat::with('regularProgram')->find($id);
		return response()->json($regularProgramFormat);
	}
	
	}
?>