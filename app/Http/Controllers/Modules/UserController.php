<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Enrollment;
use App\Models\Payment;
use App\Models\UsersVoucher;
use App\Models\Role;
use Log;

class UserController extends Controller
{	
	public function index()
	{
		$users = User::with('enrollments','payments','users','usersVouchers','role')->get();
		return view('modules.users.all', ['users' => $users]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = User::with('enrollments','payments','users','usersVouchers','role')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$roles = Role::all();
				return view('modules.users.form', [
					'roles' => $roles,
			'allroles' => Role::all(),
					'allenrollments' => Enrollment::all(),
					'allpayments' => Payment::all(),
					'allusers' => User::all(),
					'allusersVouchers' => UsersVoucher::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
						'name' => 'required',
						'email' => 'required',
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$user = new User();
				if(!empty($data['id']))
		{
			$user->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$user->name = $data['name'];
		}
				if(!empty($data['email']))
		{
			$user->email = $data['email'];
		}
				if(!empty($data['password']))
		{
			$user->password = $data['password'];
		}
				if(!empty($data['remember_token']))
		{
			$user->remember_token = $data['remember_token'];
		}
				if(!empty($data['parent_id']))
		{
			$user->parent_id = $data['parent_id'];
		}
				if(!empty($data['stripe_id']))
		{
			$user->stripe_id = $data['stripe_id'];
		}
				if(!empty($data['card_brand']))
		{
			$user->card_brand = $data['card_brand'];
		}
				if(!empty($data['card_last_4']))
		{
			$user->card_last_4 = $data['card_last_4'];
		}
				if(!empty($data['trial_ends_at']))
		{
			$user->trial_ends_at = $data['trial_ends_at'];
		}
				if(!empty($data['role_id']))
		{
			$user->role_id = $data['role_id'];
		}
				$user->save();
				if(!empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
								$user->enrollments()->save($enrollment);
							}
		}
		$unsavedenrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedenrollments as $unsavedenrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
			if(!empty($modelInstance))
			{
								$user->enrollments()->save($modelInstance);
							}
		}
				if(!empty($data['payments']))
		{
			$payments = explode(',', $data['payments']);
			foreach ($payments as $singlePayment)
			{
				$payment = Payment::find($singlePayment);
								$user->payments()->save($payment);
							}
		}
		$unsavedpayments = $unsavedRelationships['payments'];
		foreach($unsavedpayments as $unsavedpayment)
		{
			$modelInstance = getUnsavedRelationship($unsavedpayment);
			if(!empty($modelInstance))
			{
								$user->payments()->save($modelInstance);
							}
		}
				if(!empty($data['users']))
		{
			$users = explode(',', $data['users']);
			foreach ($users as $singleUser)
			{
				$user = User::find($singleUser);
								$user->users()->save($user);
							}
		}
		$unsavedusers = $unsavedRelationships['users'];
		foreach($unsavedusers as $unsaveduser)
		{
			$modelInstance = getUnsavedRelationship($unsaveduser);
			if(!empty($modelInstance))
			{
								$user->users()->save($modelInstance);
							}
		}
				if(!empty($data['usersVouchers']))
		{
			$usersVouchers = explode(',', $data['usersVouchers']);
			foreach ($usersVouchers as $singleUsersVoucher)
			{
				$usersVoucher = UsersVoucher::find($singleUsersVoucher);
								$user->usersVouchers()->save($usersVoucher);
							}
		}
		$unsavedusersVouchers = $unsavedRelationships['usersVouchers'];
		foreach($unsavedusersVouchers as $unsavedusersVoucher)
		{
			$modelInstance = getUnsavedRelationship($unsavedusersVoucher);
			if(!empty($modelInstance))
			{
								$user->usersVouchers()->save($modelInstance);
							}
		}
						if(!empty($data['role']))
		{
			$role = Role::find($data['role']);
						$user->role()->associate($role);
					}
		$unsavedrole = $unsavedRelationships['role'];
		if(!empty($unsavedrole))
		{
			$modelInstance = getUnsavedRelationship($unsavedrole);
						$user->role()->associate($modelInstance);
					}
				$user->save();
		return redirect('/modules/users');
	}
	
	public function get($id)
	{
		$user = User::with('enrollments','payments','users','usersVouchers','role')->find($id);
				$roles = Role::all();
						$enrollmentsArray = [];
		foreach($user->enrollments as $enrollment)
		{
			
			$enrollmentsArray[] = $enrollment->id;
		}
		$enrollmentsValue = implode(',', $enrollmentsArray);
				$paymentsArray = [];
		foreach($user->payments as $payment)
		{
			
			$paymentsArray[] = $payment->id;
		}
		$paymentsValue = implode(',', $paymentsArray);
				$usersArray = [];
		foreach($user->users as $user)
		{
			
			$usersArray[] = $user->id;
		}
		$usersValue = implode(',', $usersArray);
				$usersVouchersArray = [];
		foreach($user->usersVouchers as $usersVoucher)
		{
			
			$usersVouchersArray[] = $usersVoucher->id;
		}
		$usersVouchersValue = implode(',', $usersVouchersArray);
				return view('modules.users.form', [
			'user' => $user,
							'roles' => $roles,
				'allroles' => Role::all(),
							'allenrollments' => Enrollment::all(),
				'enrollmentsValue' => $enrollmentsValue,
							'allpayments' => Payment::all(),
				'paymentsValue' => $paymentsValue,
							'allusers' => User::all(),
				'usersValue' => $usersValue,
							'allusersVouchers' => UsersVoucher::all(),
				'usersVouchersValue' => $usersVouchersValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
						'name' => 'required',
						'email' => 'required',
			    	]);
		$user = User::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$user->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$user->name = $data['name'];
		}
				if(!empty($data['email']))
		{
			$user->email = $data['email'];
		}
				if(!empty($data['password']))
		{
			$user->password = $data['password'];
		}
				if(!empty($data['remember_token']))
		{
			$user->remember_token = $data['remember_token'];
		}
				if(!empty($data['parent_id']))
		{
			$user->parent_id = $data['parent_id'];
		}
				if(!empty($data['stripe_id']))
		{
			$user->stripe_id = $data['stripe_id'];
		}
				if(!empty($data['card_brand']))
		{
			$user->card_brand = $data['card_brand'];
		}
				if(!empty($data['card_last_4']))
		{
			$user->card_last_4 = $data['card_last_4'];
		}
				if(!empty($data['trial_ends_at']))
		{
			$user->trial_ends_at = $data['trial_ends_at'];
		}
				if(!empty($data['role_id']))
		{
			$user->role_id = $data['role_id'];
		}
				$user->save();
				if(!empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
								$user->enrollments()->save($enrollment);
							}
		}
		$unsavedenrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedenrollments as $unsavedenrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
			if(!empty($modelInstance))
			{
								$user->enrollments()->save($modelInstance);
							}
		}
				if(!empty($data['payments']))
		{
			$payments = explode(',', $data['payments']);
			foreach ($payments as $singlePayment)
			{
				$payment = Payment::find($singlePayment);
								$user->payments()->save($payment);
							}
		}
		$unsavedpayments = $unsavedRelationships['payments'];
		foreach($unsavedpayments as $unsavedpayment)
		{
			$modelInstance = getUnsavedRelationship($unsavedpayment);
			if(!empty($modelInstance))
			{
								$user->payments()->save($modelInstance);
							}
		}
				if(!empty($data['users']))
		{
			$users = explode(',', $data['users']);
			foreach ($users as $singleUser)
			{
				$user = User::find($singleUser);
								$user->users()->save($user);
							}
		}
		$unsavedusers = $unsavedRelationships['users'];
		foreach($unsavedusers as $unsaveduser)
		{
			$modelInstance = getUnsavedRelationship($unsaveduser);
			if(!empty($modelInstance))
			{
								$user->users()->save($modelInstance);
							}
		}
				if(!empty($data['usersVouchers']))
		{
			$usersVouchers = explode(',', $data['usersVouchers']);
			foreach ($usersVouchers as $singleUsersVoucher)
			{
				$usersVoucher = UsersVoucher::find($singleUsersVoucher);
								$user->usersVouchers()->save($usersVoucher);
							}
		}
		$unsavedusersVouchers = $unsavedRelationships['usersVouchers'];
		foreach($unsavedusersVouchers as $unsavedusersVoucher)
		{
			$modelInstance = getUnsavedRelationship($unsavedusersVoucher);
			if(!empty($modelInstance))
			{
								$user->usersVouchers()->save($modelInstance);
							}
		}
						if(!empty($data['role']))
		{
			$role = Role::find($data['role']);
						$user->role()->associate($role);
					}
		$unsavedrole = $unsavedRelationships['role'];
		if(!empty($unsavedrole))
		{
			$modelInstance = getUnsavedRelationship($unsavedrole);
						$user->role()->associate($modelInstance);
					}
				$user->save();
		return redirect('/modules/users');
	}
	
	public function delete($id)
	{
		$user = User::find($id);
		$user->delete();
		return redirect('/modules/users');
	}
	
	public function user($id)
	{
		$user = User::with('enrollments','payments','users','usersVouchers','role')->find($id);
		return response()->json($user);
	}
	
		public function enrollments($id)
	{
		$user = User::find($id);
		$enrollments = $user->enrollments;
		for ($i = 0; $i < count($enrollments); $i++)
		{
			$enrollment = $enrollments[$i];
			$enrollment->user = $user;
			$enrollments[$i] = $enrollment;
		}
		return view('modules.enrollments.all', ['enrollments' => $enrollments]);
	}
	
	public function enrollmentsApi($id)
	{
		$user = User::find($id);
		$enrollments = $user->enrollments;
		return response()->json($enrollments->all());
	}
		public function payments($id)
	{
		$user = User::find($id);
		$payments = $user->payments;
		for ($i = 0; $i < count($payments); $i++)
		{
			$payment = $payments[$i];
			$payment->user = $user;
			$payments[$i] = $payment;
		}
		return view('modules.payments.all', ['payments' => $payments]);
	}
	
	public function paymentsApi($id)
	{
		$user = User::find($id);
		$payments = $user->payments;
		return response()->json($payments->all());
	}
		public function users($id)
	{
		$user = User::find($id);
		$users = $user->users;
		for ($i = 0; $i < count($users); $i++)
		{
			$user = $users[$i];
			$user->user = $user;
			$users[$i] = $user;
		}
		return view('modules.users.all', ['users' => $users]);
	}
	
	public function usersApi($id)
	{
		$user = User::find($id);
		$users = $user->users;
		return response()->json($users->all());
	}
		public function usersVouchers($id)
	{
		$user = User::find($id);
		$usersVouchers = $user->usersVouchers;
		for ($i = 0; $i < count($usersVouchers); $i++)
		{
			$usersVoucher = $usersVouchers[$i];
			$usersVoucher->user = $user;
			$usersVouchers[$i] = $usersVoucher;
		}
		return view('modules.usersVouchers.all', ['usersVouchers' => $usersVouchers]);
	}
	
	public function usersVouchersApi($id)
	{
		$user = User::find($id);
		$usersVouchers = $user->usersVouchers;
		return response()->json($usersVouchers->all());
	}
	}
?>