<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InHomeProgramFormat;
use App\Models\InHomeProgram;
use Log;

class InHomeProgramFormatController extends Controller
{	
	public function index()
	{
		$inHomeProgramFormats = InHomeProgramFormat::with('inHomeProgram')->get();
		return view('modules.inHomeProgramFormats.all', ['inHomeProgramFormats' => $inHomeProgramFormats]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = InHomeProgramFormat::with('inHomeProgram')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$inHomePrograms = InHomeProgram::all();
				return view('modules.inHomeProgramFormats.form', [
					'inHomePrograms' => $inHomePrograms,
			'allinHomePrograms' => InHomeProgram::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$inHomeProgramFormat = new InHomeProgramFormat();
				if(!empty($data['id']))
		{
			$inHomeProgramFormat->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$inHomeProgramFormat->title = $data['title'];
		}
				if(!empty($data['regular_program_id']))
		{
			$inHomeProgramFormat->regular_program_id = $data['regular_program_id'];
		}
				$inHomeProgramFormat->save();
						if(!empty($data['inHomeProgram']))
		{
			$inHomeProgram = InHomeProgram::find($data['inHomeProgram']);
						$inHomeProgramFormat->inHomeProgram()->associate($inHomeProgram);
					}
		$unsavedinHomeProgram = $unsavedRelationships['inHomeProgram'];
		if(!empty($unsavedinHomeProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
						$inHomeProgramFormat->inHomeProgram()->associate($modelInstance);
					}
				$inHomeProgramFormat->save();
		return redirect('/modules/inHomeProgramFormats');
	}
	
	public function get($id)
	{
		$inHomeProgramFormat = InHomeProgramFormat::with('inHomeProgram')->find($id);
				$inHomePrograms = InHomeProgram::all();
						return view('modules.inHomeProgramFormats.form', [
			'inHomeProgramFormat' => $inHomeProgramFormat,
							'inHomePrograms' => $inHomePrograms,
				'allinHomePrograms' => InHomeProgram::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$inHomeProgramFormat = InHomeProgramFormat::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$inHomeProgramFormat->id = $data['id'];
		}
				if(!empty($data['title']))
		{
			$inHomeProgramFormat->title = $data['title'];
		}
				if(!empty($data['regular_program_id']))
		{
			$inHomeProgramFormat->regular_program_id = $data['regular_program_id'];
		}
				$inHomeProgramFormat->save();
						if(!empty($data['inHomeProgram']))
		{
			$inHomeProgram = InHomeProgram::find($data['inHomeProgram']);
						$inHomeProgramFormat->inHomeProgram()->associate($inHomeProgram);
					}
		$unsavedinHomeProgram = $unsavedRelationships['inHomeProgram'];
		if(!empty($unsavedinHomeProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
						$inHomeProgramFormat->inHomeProgram()->associate($modelInstance);
					}
				$inHomeProgramFormat->save();
		return redirect('/modules/inHomeProgramFormats');
	}
	
	public function delete($id)
	{
		$inHomeProgramFormat = InHomeProgramFormat::find($id);
		$inHomeProgramFormat->delete();
		return redirect('/modules/inHomeProgramFormats');
	}
	
	public function inHomeProgramFormat($id)
	{
		$inHomeProgramFormat = InHomeProgramFormat::with('inHomeProgram')->find($id);
		return response()->json($inHomeProgramFormat);
	}
	
	}
?>