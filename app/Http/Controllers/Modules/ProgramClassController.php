<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProgramClass;
use App\Models\Attachment;
use App\Models\Attendence;
use App\Models\Exam;
use App\Models\Enrollment;
use Log;

class ProgramClassController extends Controller
{	
	public function index()
	{
		$programClasses = ProgramClass::with('attachments','attendences','exams','enrollments')->get();
		return view('modules.programClasses.all', ['programClasses' => $programClasses]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = ProgramClass::with('attachments','attendences','exams','enrollments')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				return view('modules.programClasses.form', [
					'allattachments' => Attachment::all(),
					'allattendences' => Attendence::all(),
					'allexams' => Exam::all(),
					'allenrollments' => Enrollment::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$programClass = new ProgramClass();
				if(!empty($data['id']))
		{
			$programClass->id = $data['id'];
		}
				if(!empty($data['course_id']))
		{
			$programClass->course_id = $data['course_id'];
		}
				if(!empty($data['course_type']))
		{
			$programClass->course_type = $data['course_type'];
		}
				if(!empty($data['from_time']))
		{
			$programClass->from_time = $data['from_time'];
		}
				if(!empty($data['to_time']))
		{
			$programClass->to_time = $data['to_time'];
		}
				if(!empty($data['title']))
		{
			$programClass->title = $data['title'];
		}
				if(!empty($data['description']))
		{
			$programClass->description = $data['description'];
		}
				$programClass->save();
				if(!empty($data['attachments']))
		{
			$attachments = explode(',', $data['attachments']);
			foreach ($attachments as $singleAttachment)
			{
				$attachment = Attachment::find($singleAttachment);
								$programClass->attachments()->save($attachment);
							}
		}
		$unsavedattachments = $unsavedRelationships['attachments'];
		foreach($unsavedattachments as $unsavedattachment)
		{
			$modelInstance = getUnsavedRelationship($unsavedattachment);
			if(!empty($modelInstance))
			{
								$programClass->attachments()->save($modelInstance);
							}
		}
				if(!empty($data['attendences']))
		{
			$attendences = explode(',', $data['attendences']);
			foreach ($attendences as $singleAttendence)
			{
				$attendence = Attendence::find($singleAttendence);
								$programClass->attendences()->save($attendence);
							}
		}
		$unsavedattendences = $unsavedRelationships['attendences'];
		foreach($unsavedattendences as $unsavedattendence)
		{
			$modelInstance = getUnsavedRelationship($unsavedattendence);
			if(!empty($modelInstance))
			{
								$programClass->attendences()->save($modelInstance);
							}
		}
				if(!empty($data['exams']))
		{
			$exams = explode(',', $data['exams']);
			foreach ($exams as $singleExam)
			{
				$exam = Exam::find($singleExam);
								$programClass->exams()->save($exam);
							}
		}
		$unsavedexams = $unsavedRelationships['exams'];
		foreach($unsavedexams as $unsavedexam)
		{
			$modelInstance = getUnsavedRelationship($unsavedexam);
			if(!empty($modelInstance))
			{
								$programClass->exams()->save($modelInstance);
							}
		}
				if(!empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
								$programClass->enrollments()->attach($enrollment);
							}
		}
		$unsavedenrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedenrollments as $unsavedenrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
			if(!empty($modelInstance))
			{
								$programClass->enrollments()->attach($modelInstance);
							}
		}
						$programClass->save();
		return redirect('/modules/programClasses');
	}
	
	public function get($id)
	{
		$programClass = ProgramClass::with('attachments','attendences','exams','enrollments')->find($id);
						$attachmentsArray = [];
		foreach($programClass->attachments as $attachment)
		{
			
			$attachmentsArray[] = $attachment->id;
		}
		$attachmentsValue = implode(',', $attachmentsArray);
				$attendencesArray = [];
		foreach($programClass->attendences as $attendence)
		{
			
			$attendencesArray[] = $attendence->id;
		}
		$attendencesValue = implode(',', $attendencesArray);
				$examsArray = [];
		foreach($programClass->exams as $exam)
		{
			
			$examsArray[] = $exam->id;
		}
		$examsValue = implode(',', $examsArray);
				$enrollmentsArray = [];
		foreach($programClass->enrollments as $enrollment)
		{
			
			$enrollmentsArray[] = $enrollment->id;
		}
		$enrollmentsValue = implode(',', $enrollmentsArray);
				return view('modules.programClasses.form', [
			'programClass' => $programClass,
							'allattachments' => Attachment::all(),
				'attachmentsValue' => $attachmentsValue,
							'allattendences' => Attendence::all(),
				'attendencesValue' => $attendencesValue,
							'allexams' => Exam::all(),
				'examsValue' => $examsValue,
							'allenrollments' => Enrollment::all(),
				'enrollmentsValue' => $enrollmentsValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$programClass = ProgramClass::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$programClass->id = $data['id'];
		}
				if(!empty($data['course_id']))
		{
			$programClass->course_id = $data['course_id'];
		}
				if(!empty($data['course_type']))
		{
			$programClass->course_type = $data['course_type'];
		}
				if(!empty($data['from_time']))
		{
			$programClass->from_time = $data['from_time'];
		}
				if(!empty($data['to_time']))
		{
			$programClass->to_time = $data['to_time'];
		}
				if(!empty($data['title']))
		{
			$programClass->title = $data['title'];
		}
				if(!empty($data['description']))
		{
			$programClass->description = $data['description'];
		}
				$programClass->save();
				if(!empty($data['attachments']))
		{
			$attachments = explode(',', $data['attachments']);
			foreach ($attachments as $singleAttachment)
			{
				$attachment = Attachment::find($singleAttachment);
								$programClass->attachments()->save($attachment);
							}
		}
		$unsavedattachments = $unsavedRelationships['attachments'];
		foreach($unsavedattachments as $unsavedattachment)
		{
			$modelInstance = getUnsavedRelationship($unsavedattachment);
			if(!empty($modelInstance))
			{
								$programClass->attachments()->save($modelInstance);
							}
		}
				if(!empty($data['attendences']))
		{
			$attendences = explode(',', $data['attendences']);
			foreach ($attendences as $singleAttendence)
			{
				$attendence = Attendence::find($singleAttendence);
								$programClass->attendences()->save($attendence);
							}
		}
		$unsavedattendences = $unsavedRelationships['attendences'];
		foreach($unsavedattendences as $unsavedattendence)
		{
			$modelInstance = getUnsavedRelationship($unsavedattendence);
			if(!empty($modelInstance))
			{
								$programClass->attendences()->save($modelInstance);
							}
		}
				if(!empty($data['exams']))
		{
			$exams = explode(',', $data['exams']);
			foreach ($exams as $singleExam)
			{
				$exam = Exam::find($singleExam);
								$programClass->exams()->save($exam);
							}
		}
		$unsavedexams = $unsavedRelationships['exams'];
		foreach($unsavedexams as $unsavedexam)
		{
			$modelInstance = getUnsavedRelationship($unsavedexam);
			if(!empty($modelInstance))
			{
								$programClass->exams()->save($modelInstance);
							}
		}
				if(!empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
								$programClass->enrollments()->attach($enrollment);
							}
		}
		$unsavedenrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedenrollments as $unsavedenrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
			if(!empty($modelInstance))
			{
								$programClass->enrollments()->attach($modelInstance);
							}
		}
						$programClass->save();
		return redirect('/modules/programClasses');
	}
	
	public function delete($id)
	{
		$programClass = ProgramClass::find($id);
		$programClass->delete();
		return redirect('/modules/programClasses');
	}
	
	public function programClass($id)
	{
		$programClass = ProgramClass::with('attachments','attendences','exams','enrollments')->find($id);
		return response()->json($programClass);
	}
	
		public function attachments($id)
	{
		$programClass = ProgramClass::find($id);
		$attachments = $programClass->attachments;
		for ($i = 0; $i < count($attachments); $i++)
		{
			$attachment = $attachments[$i];
			$attachment->programClass = $programClass;
			$attachments[$i] = $attachment;
		}
		return view('modules.attachments.all', ['attachments' => $attachments]);
	}
	
	public function attachmentsApi($id)
	{
		$programClass = ProgramClass::find($id);
		$attachments = $programClass->attachments;
		return response()->json($attachments->all());
	}
		public function attendences($id)
	{
		$programClass = ProgramClass::find($id);
		$attendences = $programClass->attendences;
		for ($i = 0; $i < count($attendences); $i++)
		{
			$attendence = $attendences[$i];
			$attendence->programClass = $programClass;
			$attendences[$i] = $attendence;
		}
		return view('modules.attendences.all', ['attendences' => $attendences]);
	}
	
	public function attendencesApi($id)
	{
		$programClass = ProgramClass::find($id);
		$attendences = $programClass->attendences;
		return response()->json($attendences->all());
	}
		public function exams($id)
	{
		$programClass = ProgramClass::find($id);
		$exams = $programClass->exams;
		for ($i = 0; $i < count($exams); $i++)
		{
			$exam = $exams[$i];
			$exam->programClass = $programClass;
			$exams[$i] = $exam;
		}
		return view('modules.exams.all', ['exams' => $exams]);
	}
	
	public function examsApi($id)
	{
		$programClass = ProgramClass::find($id);
		$exams = $programClass->exams;
		return response()->json($exams->all());
	}
		public function enrollments($id)
	{
		$programClass = ProgramClass::find($id);
		$enrollments = $programClass->enrollments;
		for ($i = 0; $i < count($enrollments); $i++)
		{
			$enrollment = $enrollments[$i];
			$enrollment->programClass = $programClass;
			$enrollments[$i] = $enrollment;
		}
		return view('modules.enrollments.all', ['enrollments' => $enrollments]);
	}
	
	public function enrollmentsApi($id)
	{
		$programClass = ProgramClass::find($id);
		$enrollments = $programClass->enrollments;
		return response()->json($enrollments->all());
	}
	}
?>