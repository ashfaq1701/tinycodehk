<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Enrollment;
use App\Models\Attendence;
use App\Models\ExamScore;
use App\Models\Payment;
use App\Models\Testimonial;
use App\Models\UsersVoucher;
use App\Models\User;
use App\Models\ProgramClass;
use App\Models\Exam;
use Log;

class EnrollmentController extends Controller
{	
	public function index()
	{
		$enrollments = Enrollment::with('attendences','examScores','payments','testimonials','programClasses','exams','usersVoucher','user')->get();
		return view('modules.enrollments.all', ['enrollments' => $enrollments]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = Enrollment::with('attendences','examScores','payments','testimonials','programClasses','exams','usersVoucher','user')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$usersVouchers = UsersVoucher::all();
				$users = User::all();
				return view('modules.enrollments.form', [
					'usersVouchers' => $usersVouchers,
			'allusersVouchers' => UsersVoucher::all(),
					'users' => $users,
			'allusers' => User::all(),
					'allattendences' => Attendence::all(),
					'allexamScores' => ExamScore::all(),
					'allpayments' => Payment::all(),
					'alltestimonials' => Testimonial::all(),
					'allprogramClasses' => ProgramClass::all(),
					'allexams' => Exam::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$enrollment = new Enrollment();
				if(!empty($data['id']))
		{
			$enrollment->id = $data['id'];
		}
				if(!empty($data['user_id']))
		{
			$enrollment->user_id = $data['user_id'];
		}
				if(!empty($data['course_id']))
		{
			$enrollment->course_id = $data['course_id'];
		}
				if(!empty($data['course_type']))
		{
			$enrollment->course_type = $data['course_type'];
		}
				if(!empty($data['registration_no']))
		{
			$enrollment->registration_no = $data['registration_no'];
		}
				if(!empty($data['is_paid']))
		{
			$enrollment->is_paid = $data['is_paid'];
		}
				if(!empty($data['total_payment']))
		{
			$enrollment->total_payment = $data['total_payment'];
		}
				if(!empty($data['voucher_applied_id']))
		{
			$enrollment->voucher_applied_id = $data['voucher_applied_id'];
		}
				$enrollment->save();
				if(!empty($data['attendences']))
		{
			$attendences = explode(',', $data['attendences']);
			foreach ($attendences as $singleAttendence)
			{
				$attendence = Attendence::find($singleAttendence);
								$enrollment->attendences()->save($attendence);
							}
		}
		$unsavedattendences = $unsavedRelationships['attendences'];
		foreach($unsavedattendences as $unsavedattendence)
		{
			$modelInstance = getUnsavedRelationship($unsavedattendence);
			if(!empty($modelInstance))
			{
								$enrollment->attendences()->save($modelInstance);
							}
		}
				if(!empty($data['examScores']))
		{
			$examScores = explode(',', $data['examScores']);
			foreach ($examScores as $singleExamScore)
			{
				$examScore = ExamScore::find($singleExamScore);
								$enrollment->examScores()->save($examScore);
							}
		}
		$unsavedexamScores = $unsavedRelationships['examScores'];
		foreach($unsavedexamScores as $unsavedexamScore)
		{
			$modelInstance = getUnsavedRelationship($unsavedexamScore);
			if(!empty($modelInstance))
			{
								$enrollment->examScores()->save($modelInstance);
							}
		}
				if(!empty($data['payments']))
		{
			$payments = explode(',', $data['payments']);
			foreach ($payments as $singlePayment)
			{
				$payment = Payment::find($singlePayment);
								$enrollment->payments()->save($payment);
							}
		}
		$unsavedpayments = $unsavedRelationships['payments'];
		foreach($unsavedpayments as $unsavedpayment)
		{
			$modelInstance = getUnsavedRelationship($unsavedpayment);
			if(!empty($modelInstance))
			{
								$enrollment->payments()->save($modelInstance);
							}
		}
				if(!empty($data['testimonials']))
		{
			$testimonials = explode(',', $data['testimonials']);
			foreach ($testimonials as $singleTestimonial)
			{
				$testimonial = Testimonial::find($singleTestimonial);
								$enrollment->testimonials()->save($testimonial);
							}
		}
		$unsavedtestimonials = $unsavedRelationships['testimonials'];
		foreach($unsavedtestimonials as $unsavedtestimonial)
		{
			$modelInstance = getUnsavedRelationship($unsavedtestimonial);
			if(!empty($modelInstance))
			{
								$enrollment->testimonials()->save($modelInstance);
							}
		}
				if(!empty($data['programClasses']))
		{
			$programClasses = explode(',', $data['programClasses']);
			foreach ($programClasses as $singleProgramClass)
			{
				$programClass = ProgramClass::find($singleProgramClass);
								$enrollment->programClasses()->attach($programClass);
							}
		}
		$unsavedprogramClasses = $unsavedRelationships['programClasses'];
		foreach($unsavedprogramClasses as $unsavedprogramClass)
		{
			$modelInstance = getUnsavedRelationship($unsavedprogramClass);
			if(!empty($modelInstance))
			{
								$enrollment->programClasses()->attach($modelInstance);
							}
		}
				if(!empty($data['exams']))
		{
			$exams = explode(',', $data['exams']);
			foreach ($exams as $singleExam)
			{
				$exam = Exam::find($singleExam);
								$enrollment->exams()->attach($exam);
							}
		}
		$unsavedexams = $unsavedRelationships['exams'];
		foreach($unsavedexams as $unsavedexam)
		{
			$modelInstance = getUnsavedRelationship($unsavedexam);
			if(!empty($modelInstance))
			{
								$enrollment->exams()->attach($modelInstance);
							}
		}
						if(!empty($data['usersVoucher']))
		{
			$usersVoucher = UsersVoucher::find($data['usersVoucher']);
						$enrollment->usersVoucher()->associate($usersVoucher);
					}
		$unsavedusersVoucher = $unsavedRelationships['usersVoucher'];
		if(!empty($unsavedusersVoucher))
		{
			$modelInstance = getUnsavedRelationship($unsavedusersVoucher);
						$enrollment->usersVoucher()->associate($modelInstance);
					}
				if(!empty($data['user']))
		{
			$user = User::find($data['user']);
						$enrollment->user()->associate($user);
					}
		$unsaveduser = $unsavedRelationships['user'];
		if(!empty($unsaveduser))
		{
			$modelInstance = getUnsavedRelationship($unsaveduser);
						$enrollment->user()->associate($modelInstance);
					}
				$enrollment->save();
		return redirect('/modules/enrollments');
	}
	
	public function get($id)
	{
		$enrollment = Enrollment::with('attendences','examScores','payments','testimonials','programClasses','exams','usersVoucher','user')->find($id);
				$usersVouchers = UsersVoucher::all();
				$users = User::all();
						$attendencesArray = [];
		foreach($enrollment->attendences as $attendence)
		{
			
			$attendencesArray[] = $attendence->id;
		}
		$attendencesValue = implode(',', $attendencesArray);
				$examScoresArray = [];
		foreach($enrollment->examScores as $examScore)
		{
			
			$examScoresArray[] = $examScore->id;
		}
		$examScoresValue = implode(',', $examScoresArray);
				$paymentsArray = [];
		foreach($enrollment->payments as $payment)
		{
			
			$paymentsArray[] = $payment->id;
		}
		$paymentsValue = implode(',', $paymentsArray);
				$testimonialsArray = [];
		foreach($enrollment->testimonials as $testimonial)
		{
			
			$testimonialsArray[] = $testimonial->id;
		}
		$testimonialsValue = implode(',', $testimonialsArray);
				$programClassesArray = [];
		foreach($enrollment->programClasses as $programClass)
		{
			
			$programClassesArray[] = $programClass->id;
		}
		$programClassesValue = implode(',', $programClassesArray);
				$examsArray = [];
		foreach($enrollment->exams as $exam)
		{
			
			$examsArray[] = $exam->id;
		}
		$examsValue = implode(',', $examsArray);
				return view('modules.enrollments.form', [
			'enrollment' => $enrollment,
							'usersVouchers' => $usersVouchers,
				'allusersVouchers' => UsersVoucher::all(),
							'users' => $users,
				'allusers' => User::all(),
							'allattendences' => Attendence::all(),
				'attendencesValue' => $attendencesValue,
							'allexamScores' => ExamScore::all(),
				'examScoresValue' => $examScoresValue,
							'allpayments' => Payment::all(),
				'paymentsValue' => $paymentsValue,
							'alltestimonials' => Testimonial::all(),
				'testimonialsValue' => $testimonialsValue,
							'allprogramClasses' => ProgramClass::all(),
				'programClassesValue' => $programClassesValue,
							'allexams' => Exam::all(),
				'examsValue' => $examsValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$enrollment = Enrollment::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$enrollment->id = $data['id'];
		}
				if(!empty($data['user_id']))
		{
			$enrollment->user_id = $data['user_id'];
		}
				if(!empty($data['course_id']))
		{
			$enrollment->course_id = $data['course_id'];
		}
				if(!empty($data['course_type']))
		{
			$enrollment->course_type = $data['course_type'];
		}
				if(!empty($data['registration_no']))
		{
			$enrollment->registration_no = $data['registration_no'];
		}
				if(!empty($data['is_paid']))
		{
			$enrollment->is_paid = $data['is_paid'];
		}
				if(!empty($data['total_payment']))
		{
			$enrollment->total_payment = $data['total_payment'];
		}
				if(!empty($data['voucher_applied_id']))
		{
			$enrollment->voucher_applied_id = $data['voucher_applied_id'];
		}
				$enrollment->save();
				if(!empty($data['attendences']))
		{
			$attendences = explode(',', $data['attendences']);
			foreach ($attendences as $singleAttendence)
			{
				$attendence = Attendence::find($singleAttendence);
								$enrollment->attendences()->save($attendence);
							}
		}
		$unsavedattendences = $unsavedRelationships['attendences'];
		foreach($unsavedattendences as $unsavedattendence)
		{
			$modelInstance = getUnsavedRelationship($unsavedattendence);
			if(!empty($modelInstance))
			{
								$enrollment->attendences()->save($modelInstance);
							}
		}
				if(!empty($data['examScores']))
		{
			$examScores = explode(',', $data['examScores']);
			foreach ($examScores as $singleExamScore)
			{
				$examScore = ExamScore::find($singleExamScore);
								$enrollment->examScores()->save($examScore);
							}
		}
		$unsavedexamScores = $unsavedRelationships['examScores'];
		foreach($unsavedexamScores as $unsavedexamScore)
		{
			$modelInstance = getUnsavedRelationship($unsavedexamScore);
			if(!empty($modelInstance))
			{
								$enrollment->examScores()->save($modelInstance);
							}
		}
				if(!empty($data['payments']))
		{
			$payments = explode(',', $data['payments']);
			foreach ($payments as $singlePayment)
			{
				$payment = Payment::find($singlePayment);
								$enrollment->payments()->save($payment);
							}
		}
		$unsavedpayments = $unsavedRelationships['payments'];
		foreach($unsavedpayments as $unsavedpayment)
		{
			$modelInstance = getUnsavedRelationship($unsavedpayment);
			if(!empty($modelInstance))
			{
								$enrollment->payments()->save($modelInstance);
							}
		}
				if(!empty($data['testimonials']))
		{
			$testimonials = explode(',', $data['testimonials']);
			foreach ($testimonials as $singleTestimonial)
			{
				$testimonial = Testimonial::find($singleTestimonial);
								$enrollment->testimonials()->save($testimonial);
							}
		}
		$unsavedtestimonials = $unsavedRelationships['testimonials'];
		foreach($unsavedtestimonials as $unsavedtestimonial)
		{
			$modelInstance = getUnsavedRelationship($unsavedtestimonial);
			if(!empty($modelInstance))
			{
								$enrollment->testimonials()->save($modelInstance);
							}
		}
				if(!empty($data['programClasses']))
		{
			$programClasses = explode(',', $data['programClasses']);
			foreach ($programClasses as $singleProgramClass)
			{
				$programClass = ProgramClass::find($singleProgramClass);
								$enrollment->programClasses()->attach($programClass);
							}
		}
		$unsavedprogramClasses = $unsavedRelationships['programClasses'];
		foreach($unsavedprogramClasses as $unsavedprogramClass)
		{
			$modelInstance = getUnsavedRelationship($unsavedprogramClass);
			if(!empty($modelInstance))
			{
								$enrollment->programClasses()->attach($modelInstance);
							}
		}
				if(!empty($data['exams']))
		{
			$exams = explode(',', $data['exams']);
			foreach ($exams as $singleExam)
			{
				$exam = Exam::find($singleExam);
								$enrollment->exams()->attach($exam);
							}
		}
		$unsavedexams = $unsavedRelationships['exams'];
		foreach($unsavedexams as $unsavedexam)
		{
			$modelInstance = getUnsavedRelationship($unsavedexam);
			if(!empty($modelInstance))
			{
								$enrollment->exams()->attach($modelInstance);
							}
		}
						if(!empty($data['usersVoucher']))
		{
			$usersVoucher = UsersVoucher::find($data['usersVoucher']);
						$enrollment->usersVoucher()->associate($usersVoucher);
					}
		$unsavedusersVoucher = $unsavedRelationships['usersVoucher'];
		if(!empty($unsavedusersVoucher))
		{
			$modelInstance = getUnsavedRelationship($unsavedusersVoucher);
						$enrollment->usersVoucher()->associate($modelInstance);
					}
				if(!empty($data['user']))
		{
			$user = User::find($data['user']);
						$enrollment->user()->associate($user);
					}
		$unsaveduser = $unsavedRelationships['user'];
		if(!empty($unsaveduser))
		{
			$modelInstance = getUnsavedRelationship($unsaveduser);
						$enrollment->user()->associate($modelInstance);
					}
				$enrollment->save();
		return redirect('/modules/enrollments');
	}
	
	public function delete($id)
	{
		$enrollment = Enrollment::find($id);
		$enrollment->delete();
		return redirect('/modules/enrollments');
	}
	
	public function enrollment($id)
	{
		$enrollment = Enrollment::with('attendences','examScores','payments','testimonials','programClasses','exams','usersVoucher','user')->find($id);
		return response()->json($enrollment);
	}
	
		public function attendences($id)
	{
		$enrollment = Enrollment::find($id);
		$attendences = $enrollment->attendences;
		for ($i = 0; $i < count($attendences); $i++)
		{
			$attendence = $attendences[$i];
			$attendence->enrollment = $enrollment;
			$attendences[$i] = $attendence;
		}
		return view('modules.attendences.all', ['attendences' => $attendences]);
	}
	
	public function attendencesApi($id)
	{
		$enrollment = Enrollment::find($id);
		$attendences = $enrollment->attendences;
		return response()->json($attendences->all());
	}
		public function examScores($id)
	{
		$enrollment = Enrollment::find($id);
		$examScores = $enrollment->examScores;
		for ($i = 0; $i < count($examScores); $i++)
		{
			$examScore = $examScores[$i];
			$examScore->enrollment = $enrollment;
			$examScores[$i] = $examScore;
		}
		return view('modules.examScores.all', ['examScores' => $examScores]);
	}
	
	public function examScoresApi($id)
	{
		$enrollment = Enrollment::find($id);
		$examScores = $enrollment->examScores;
		return response()->json($examScores->all());
	}
		public function payments($id)
	{
		$enrollment = Enrollment::find($id);
		$payments = $enrollment->payments;
		for ($i = 0; $i < count($payments); $i++)
		{
			$payment = $payments[$i];
			$payment->enrollment = $enrollment;
			$payments[$i] = $payment;
		}
		return view('modules.payments.all', ['payments' => $payments]);
	}
	
	public function paymentsApi($id)
	{
		$enrollment = Enrollment::find($id);
		$payments = $enrollment->payments;
		return response()->json($payments->all());
	}
		public function testimonials($id)
	{
		$enrollment = Enrollment::find($id);
		$testimonials = $enrollment->testimonials;
		for ($i = 0; $i < count($testimonials); $i++)
		{
			$testimonial = $testimonials[$i];
			$testimonial->enrollment = $enrollment;
			$testimonials[$i] = $testimonial;
		}
		return view('modules.testimonials.all', ['testimonials' => $testimonials]);
	}
	
	public function testimonialsApi($id)
	{
		$enrollment = Enrollment::find($id);
		$testimonials = $enrollment->testimonials;
		return response()->json($testimonials->all());
	}
		public function programClasses($id)
	{
		$enrollment = Enrollment::find($id);
		$programClasses = $enrollment->programClasses;
		for ($i = 0; $i < count($programClasses); $i++)
		{
			$programClass = $programClasses[$i];
			$programClass->enrollment = $enrollment;
			$programClasses[$i] = $programClass;
		}
		return view('modules.programClasses.all', ['programClasses' => $programClasses]);
	}
	
	public function programClassesApi($id)
	{
		$enrollment = Enrollment::find($id);
		$programClasses = $enrollment->programClasses;
		return response()->json($programClasses->all());
	}
		public function exams($id)
	{
		$enrollment = Enrollment::find($id);
		$exams = $enrollment->exams;
		for ($i = 0; $i < count($exams); $i++)
		{
			$exam = $exams[$i];
			$exam->enrollment = $enrollment;
			$exams[$i] = $exam;
		}
		return view('modules.exams.all', ['exams' => $exams]);
	}
	
	public function examsApi($id)
	{
		$enrollment = Enrollment::find($id);
		$exams = $enrollment->exams;
		return response()->json($exams->all());
	}
	}
?>