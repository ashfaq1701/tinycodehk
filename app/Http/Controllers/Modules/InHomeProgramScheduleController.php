<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InHomeProgramSchedule;
use App\Models\InHomeProgram;
use App\Models\Weekday;
use Log;

class InHomeProgramScheduleController extends Controller
{	
	public function index()
	{
		$inHomeProgramSchedules = InHomeProgramSchedule::with('inHomeProgram','weekday')->get();
		return view('modules.inHomeProgramSchedules.all', ['inHomeProgramSchedules' => $inHomeProgramSchedules]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = InHomeProgramSchedule::with('inHomeProgram','weekday')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$inHomePrograms = InHomeProgram::all();
				$weekdays = Weekday::all();
				return view('modules.inHomeProgramSchedules.form', [
					'inHomePrograms' => $inHomePrograms,
			'allinHomePrograms' => InHomeProgram::all(),
					'weekdays' => $weekdays,
			'allweekdays' => Weekday::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$inHomeProgramSchedule = new InHomeProgramSchedule();
				if(!empty($data['id']))
		{
			$inHomeProgramSchedule->id = $data['id'];
		}
				if(!empty($data['in_home_program_id']))
		{
			$inHomeProgramSchedule->in_home_program_id = $data['in_home_program_id'];
		}
				if(!empty($data['from_time']))
		{
			$inHomeProgramSchedule->from_time = $data['from_time'];
		}
				if(!empty($data['to_time']))
		{
			$inHomeProgramSchedule->to_time = $data['to_time'];
		}
				if(!empty($data['weekday_id']))
		{
			$inHomeProgramSchedule->weekday_id = $data['weekday_id'];
		}
				$inHomeProgramSchedule->save();
						if(!empty($data['inHomeProgram']))
		{
			$inHomeProgram = InHomeProgram::find($data['inHomeProgram']);
						$inHomeProgramSchedule->inHomeProgram()->associate($inHomeProgram);
					}
		$unsavedinHomeProgram = $unsavedRelationships['inHomeProgram'];
		if(!empty($unsavedinHomeProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
						$inHomeProgramSchedule->inHomeProgram()->associate($modelInstance);
					}
				if(!empty($data['weekday']))
		{
			$weekday = Weekday::find($data['weekday']);
						$inHomeProgramSchedule->weekday()->associate($weekday);
					}
		$unsavedweekday = $unsavedRelationships['weekday'];
		if(!empty($unsavedweekday))
		{
			$modelInstance = getUnsavedRelationship($unsavedweekday);
						$inHomeProgramSchedule->weekday()->associate($modelInstance);
					}
				$inHomeProgramSchedule->save();
		return redirect('/modules/inHomeProgramSchedules');
	}
	
	public function get($id)
	{
		$inHomeProgramSchedule = InHomeProgramSchedule::with('inHomeProgram','weekday')->find($id);
				$inHomePrograms = InHomeProgram::all();
				$weekdays = Weekday::all();
						return view('modules.inHomeProgramSchedules.form', [
			'inHomeProgramSchedule' => $inHomeProgramSchedule,
							'inHomePrograms' => $inHomePrograms,
				'allinHomePrograms' => InHomeProgram::all(),
							'weekdays' => $weekdays,
				'allweekdays' => Weekday::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$inHomeProgramSchedule = InHomeProgramSchedule::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$inHomeProgramSchedule->id = $data['id'];
		}
				if(!empty($data['in_home_program_id']))
		{
			$inHomeProgramSchedule->in_home_program_id = $data['in_home_program_id'];
		}
				if(!empty($data['from_time']))
		{
			$inHomeProgramSchedule->from_time = $data['from_time'];
		}
				if(!empty($data['to_time']))
		{
			$inHomeProgramSchedule->to_time = $data['to_time'];
		}
				if(!empty($data['weekday_id']))
		{
			$inHomeProgramSchedule->weekday_id = $data['weekday_id'];
		}
				$inHomeProgramSchedule->save();
						if(!empty($data['inHomeProgram']))
		{
			$inHomeProgram = InHomeProgram::find($data['inHomeProgram']);
						$inHomeProgramSchedule->inHomeProgram()->associate($inHomeProgram);
					}
		$unsavedinHomeProgram = $unsavedRelationships['inHomeProgram'];
		if(!empty($unsavedinHomeProgram))
		{
			$modelInstance = getUnsavedRelationship($unsavedinHomeProgram);
						$inHomeProgramSchedule->inHomeProgram()->associate($modelInstance);
					}
				if(!empty($data['weekday']))
		{
			$weekday = Weekday::find($data['weekday']);
						$inHomeProgramSchedule->weekday()->associate($weekday);
					}
		$unsavedweekday = $unsavedRelationships['weekday'];
		if(!empty($unsavedweekday))
		{
			$modelInstance = getUnsavedRelationship($unsavedweekday);
						$inHomeProgramSchedule->weekday()->associate($modelInstance);
					}
				$inHomeProgramSchedule->save();
		return redirect('/modules/inHomeProgramSchedules');
	}
	
	public function delete($id)
	{
		$inHomeProgramSchedule = InHomeProgramSchedule::find($id);
		$inHomeProgramSchedule->delete();
		return redirect('/modules/inHomeProgramSchedules');
	}
	
	public function inHomeProgramSchedule($id)
	{
		$inHomeProgramSchedule = InHomeProgramSchedule::with('inHomeProgram','weekday')->find($id);
		return response()->json($inHomeProgramSchedule);
	}
	
	}
?>