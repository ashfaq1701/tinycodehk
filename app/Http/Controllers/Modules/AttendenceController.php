<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attendence;
use App\Models\Enrollment;
use App\Models\ProgramClass;
use Log;

class AttendenceController extends Controller
{	
	public function index()
	{
		$attendences = Attendence::with('enrollment','programClass')->get();
		return view('modules.attendences.all', ['attendences' => $attendences]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = Attendence::with('enrollment','programClass')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$enrollments = Enrollment::all();
				$programClasses = ProgramClass::all();
				return view('modules.attendences.form', [
					'enrollments' => $enrollments,
			'allenrollments' => Enrollment::all(),
					'programClasses' => $programClasses,
			'allprogramClasses' => ProgramClass::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$attendence = new Attendence();
				if(!empty($data['id']))
		{
			$attendence->id = $data['id'];
		}
				if(!empty($data['enrollment_id']))
		{
			$attendence->enrollment_id = $data['enrollment_id'];
		}
				if(!empty($data['class_id']))
		{
			$attendence->class_id = $data['class_id'];
		}
				if(!empty($data['attended']))
		{
			$attendence->attended = $data['attended'];
		}
				$attendence->save();
						if(!empty($data['enrollment']))
		{
			$enrollment = Enrollment::find($data['enrollment']);
						$attendence->enrollment()->associate($enrollment);
					}
		$unsavedenrollment = $unsavedRelationships['enrollment'];
		if(!empty($unsavedenrollment))
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
						$attendence->enrollment()->associate($modelInstance);
					}
				if(!empty($data['programClass']))
		{
			$programClass = ProgramClass::find($data['programClass']);
						$attendence->programClass()->associate($programClass);
					}
		$unsavedprogramClass = $unsavedRelationships['programClass'];
		if(!empty($unsavedprogramClass))
		{
			$modelInstance = getUnsavedRelationship($unsavedprogramClass);
						$attendence->programClass()->associate($modelInstance);
					}
				$attendence->save();
		return redirect('/modules/attendences');
	}
	
	public function get($id)
	{
		$attendence = Attendence::with('enrollment','programClass')->find($id);
				$enrollments = Enrollment::all();
				$programClasses = ProgramClass::all();
						return view('modules.attendences.form', [
			'attendence' => $attendence,
							'enrollments' => $enrollments,
				'allenrollments' => Enrollment::all(),
							'programClasses' => $programClasses,
				'allprogramClasses' => ProgramClass::all(),
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$attendence = Attendence::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$attendence->id = $data['id'];
		}
				if(!empty($data['enrollment_id']))
		{
			$attendence->enrollment_id = $data['enrollment_id'];
		}
				if(!empty($data['class_id']))
		{
			$attendence->class_id = $data['class_id'];
		}
				if(!empty($data['attended']))
		{
			$attendence->attended = $data['attended'];
		}
				$attendence->save();
						if(!empty($data['enrollment']))
		{
			$enrollment = Enrollment::find($data['enrollment']);
						$attendence->enrollment()->associate($enrollment);
					}
		$unsavedenrollment = $unsavedRelationships['enrollment'];
		if(!empty($unsavedenrollment))
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
						$attendence->enrollment()->associate($modelInstance);
					}
				if(!empty($data['programClass']))
		{
			$programClass = ProgramClass::find($data['programClass']);
						$attendence->programClass()->associate($programClass);
					}
		$unsavedprogramClass = $unsavedRelationships['programClass'];
		if(!empty($unsavedprogramClass))
		{
			$modelInstance = getUnsavedRelationship($unsavedprogramClass);
						$attendence->programClass()->associate($modelInstance);
					}
				$attendence->save();
		return redirect('/modules/attendences');
	}
	
	public function delete($id)
	{
		$attendence = Attendence::find($id);
		$attendence->delete();
		return redirect('/modules/attendences');
	}
	
	public function attendence($id)
	{
		$attendence = Attendence::with('enrollment','programClass')->find($id);
		return response()->json($attendence);
	}
	
	}
?>