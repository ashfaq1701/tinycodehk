<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Voucher;
use App\Models\UsersVoucher;
use Log;

class VoucherController extends Controller
{	
	public function index()
	{
		$vouchers = Voucher::with('usersVouchers')->get();
		return view('modules.vouchers.all', ['vouchers' => $vouchers]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = Voucher::with('usersVouchers')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				return view('modules.vouchers.form', [
					'allusersVouchers' => UsersVoucher::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$voucher = new Voucher();
				if(!empty($data['id']))
		{
			$voucher->id = $data['id'];
		}
				if(!empty($data['voucher_code']))
		{
			$voucher->voucher_code = $data['voucher_code'];
		}
				if(!empty($data['voucher_type']))
		{
			$voucher->voucher_type = $data['voucher_type'];
		}
				if(!empty($data['percent_discount']))
		{
			$voucher->percent_discount = $data['percent_discount'];
		}
				if(!empty($data['flat_discount']))
		{
			$voucher->flat_discount = $data['flat_discount'];
		}
				$voucher->save();
				if(!empty($data['usersVouchers']))
		{
			$usersVouchers = explode(',', $data['usersVouchers']);
			foreach ($usersVouchers as $singleUsersVoucher)
			{
				$usersVoucher = UsersVoucher::find($singleUsersVoucher);
								$voucher->usersVouchers()->save($usersVoucher);
							}
		}
		$unsavedusersVouchers = $unsavedRelationships['usersVouchers'];
		foreach($unsavedusersVouchers as $unsavedusersVoucher)
		{
			$modelInstance = getUnsavedRelationship($unsavedusersVoucher);
			if(!empty($modelInstance))
			{
								$voucher->usersVouchers()->save($modelInstance);
							}
		}
						$voucher->save();
		return redirect('/modules/vouchers');
	}
	
	public function get($id)
	{
		$voucher = Voucher::with('usersVouchers')->find($id);
						$usersVouchersArray = [];
		foreach($voucher->usersVouchers as $usersVoucher)
		{
			
			$usersVouchersArray[] = $usersVoucher->id;
		}
		$usersVouchersValue = implode(',', $usersVouchersArray);
				return view('modules.vouchers.form', [
			'voucher' => $voucher,
							'allusersVouchers' => UsersVoucher::all(),
				'usersVouchersValue' => $usersVouchersValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$voucher = Voucher::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$voucher->id = $data['id'];
		}
				if(!empty($data['voucher_code']))
		{
			$voucher->voucher_code = $data['voucher_code'];
		}
				if(!empty($data['voucher_type']))
		{
			$voucher->voucher_type = $data['voucher_type'];
		}
				if(!empty($data['percent_discount']))
		{
			$voucher->percent_discount = $data['percent_discount'];
		}
				if(!empty($data['flat_discount']))
		{
			$voucher->flat_discount = $data['flat_discount'];
		}
				$voucher->save();
				if(!empty($data['usersVouchers']))
		{
			$usersVouchers = explode(',', $data['usersVouchers']);
			foreach ($usersVouchers as $singleUsersVoucher)
			{
				$usersVoucher = UsersVoucher::find($singleUsersVoucher);
								$voucher->usersVouchers()->save($usersVoucher);
							}
		}
		$unsavedusersVouchers = $unsavedRelationships['usersVouchers'];
		foreach($unsavedusersVouchers as $unsavedusersVoucher)
		{
			$modelInstance = getUnsavedRelationship($unsavedusersVoucher);
			if(!empty($modelInstance))
			{
								$voucher->usersVouchers()->save($modelInstance);
							}
		}
						$voucher->save();
		return redirect('/modules/vouchers');
	}
	
	public function delete($id)
	{
		$voucher = Voucher::find($id);
		$voucher->delete();
		return redirect('/modules/vouchers');
	}
	
	public function voucher($id)
	{
		$voucher = Voucher::with('usersVouchers')->find($id);
		return response()->json($voucher);
	}
	
		public function usersVouchers($id)
	{
		$voucher = Voucher::find($id);
		$usersVouchers = $voucher->usersVouchers;
		for ($i = 0; $i < count($usersVouchers); $i++)
		{
			$usersVoucher = $usersVouchers[$i];
			$usersVoucher->voucher = $voucher;
			$usersVouchers[$i] = $usersVoucher;
		}
		return view('modules.usersVouchers.all', ['usersVouchers' => $usersVouchers]);
	}
	
	public function usersVouchersApi($id)
	{
		$voucher = Voucher::find($id);
		$usersVouchers = $voucher->usersVouchers;
		return response()->json($usersVouchers->all());
	}
	}
?>