<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Location;
use App\Models\HolidayCampModule;
use App\Models\RegularProgramClasseSchedule;
use App\Models\HolidayCampCourse;
use Log;

class LocationController extends Controller
{	
	public function index()
	{
		$locations = Location::with('holidayCampModules','regularProgramClasseSchedules','holidayCampCourses')->get();
		return view('modules.locations.all', ['locations' => $locations]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = Location::with('holidayCampModules','regularProgramClasseSchedules','holidayCampCourses')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				return view('modules.locations.form', [
					'allholidayCampModules' => HolidayCampModule::all(),
					'allregularProgramClasseSchedules' => RegularProgramClasseSchedule::all(),
					'allholidayCampCourses' => HolidayCampCourse::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$location = new Location();
				if(!empty($data['id']))
		{
			$location->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$location->name = $data['name'];
		}
				if(!empty($data['address_line_1']))
		{
			$location->address_line_1 = $data['address_line_1'];
		}
				if(!empty($data['address_line_2']))
		{
			$location->address_line_2 = $data['address_line_2'];
		}
				if(!empty($data['city']))
		{
			$location->city = $data['city'];
		}
				if(!empty($data['state']))
		{
			$location->state = $data['state'];
		}
				if(!empty($data['country']))
		{
			$location->country = $data['country'];
		}
				if(!empty($data['mobile']))
		{
			$location->mobile = $data['mobile'];
		}
				if(!empty($data['email']))
		{
			$location->email = $data['email'];
		}
				$location->save();
				if(!empty($data['holidayCampModules']))
		{
			$holidayCampModules = explode(',', $data['holidayCampModules']);
			foreach ($holidayCampModules as $singleHolidayCampModule)
			{
				$holidayCampModule = HolidayCampModule::find($singleHolidayCampModule);
								$location->holidayCampModules()->save($holidayCampModule);
							}
		}
		$unsavedholidayCampModules = $unsavedRelationships['holidayCampModules'];
		foreach($unsavedholidayCampModules as $unsavedholidayCampModule)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampModule);
			if(!empty($modelInstance))
			{
								$location->holidayCampModules()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramClasseSchedules']))
		{
			$regularProgramClasseSchedules = explode(',', $data['regularProgramClasseSchedules']);
			foreach ($regularProgramClasseSchedules as $singleRegularProgramClasseSchedule)
			{
				$regularProgramClasseSchedule = RegularProgramClasseSchedule::find($singleRegularProgramClasseSchedule);
								$location->regularProgramClasseSchedules()->save($regularProgramClasseSchedule);
							}
		}
		$unsavedregularProgramClasseSchedules = $unsavedRelationships['regularProgramClasseSchedules'];
		foreach($unsavedregularProgramClasseSchedules as $unsavedregularProgramClasseSchedule)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramClasseSchedule);
			if(!empty($modelInstance))
			{
								$location->regularProgramClasseSchedules()->save($modelInstance);
							}
		}
				if(!empty($data['holidayCampCourses']))
		{
			$holidayCampCourses = explode(',', $data['holidayCampCourses']);
			foreach ($holidayCampCourses as $singleHolidayCampCourse)
			{
				$holidayCampCourse = HolidayCampCourse::find($singleHolidayCampCourse);
								$location->holidayCampCourses()->attach($holidayCampCourse);
							}
		}
		$unsavedholidayCampCourses = $unsavedRelationships['holidayCampCourses'];
		foreach($unsavedholidayCampCourses as $unsavedholidayCampCourse)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
			if(!empty($modelInstance))
			{
								$location->holidayCampCourses()->attach($modelInstance);
							}
		}
						$location->save();
		return redirect('/modules/locations');
	}
	
	public function get($id)
	{
		$location = Location::with('holidayCampModules','regularProgramClasseSchedules','holidayCampCourses')->find($id);
						$holidayCampModulesArray = [];
		foreach($location->holidayCampModules as $holidayCampModule)
		{
			
			$holidayCampModulesArray[] = $holidayCampModule->id;
		}
		$holidayCampModulesValue = implode(',', $holidayCampModulesArray);
				$regularProgramClasseSchedulesArray = [];
		foreach($location->regularProgramClasseSchedules as $regularProgramClasseSchedule)
		{
			
			$regularProgramClasseSchedulesArray[] = $regularProgramClasseSchedule->id;
		}
		$regularProgramClasseSchedulesValue = implode(',', $regularProgramClasseSchedulesArray);
				$holidayCampCoursesArray = [];
		foreach($location->holidayCampCourses as $holidayCampCourse)
		{
			
			$holidayCampCoursesArray[] = $holidayCampCourse->id;
		}
		$holidayCampCoursesValue = implode(',', $holidayCampCoursesArray);
				return view('modules.locations.form', [
			'location' => $location,
							'allholidayCampModules' => HolidayCampModule::all(),
				'holidayCampModulesValue' => $holidayCampModulesValue,
							'allregularProgramClasseSchedules' => RegularProgramClasseSchedule::all(),
				'regularProgramClasseSchedulesValue' => $regularProgramClasseSchedulesValue,
							'allholidayCampCourses' => HolidayCampCourse::all(),
				'holidayCampCoursesValue' => $holidayCampCoursesValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$location = Location::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$location->id = $data['id'];
		}
				if(!empty($data['name']))
		{
			$location->name = $data['name'];
		}
				if(!empty($data['address_line_1']))
		{
			$location->address_line_1 = $data['address_line_1'];
		}
				if(!empty($data['address_line_2']))
		{
			$location->address_line_2 = $data['address_line_2'];
		}
				if(!empty($data['city']))
		{
			$location->city = $data['city'];
		}
				if(!empty($data['state']))
		{
			$location->state = $data['state'];
		}
				if(!empty($data['country']))
		{
			$location->country = $data['country'];
		}
				if(!empty($data['mobile']))
		{
			$location->mobile = $data['mobile'];
		}
				if(!empty($data['email']))
		{
			$location->email = $data['email'];
		}
				$location->save();
				if(!empty($data['holidayCampModules']))
		{
			$holidayCampModules = explode(',', $data['holidayCampModules']);
			foreach ($holidayCampModules as $singleHolidayCampModule)
			{
				$holidayCampModule = HolidayCampModule::find($singleHolidayCampModule);
								$location->holidayCampModules()->save($holidayCampModule);
							}
		}
		$unsavedholidayCampModules = $unsavedRelationships['holidayCampModules'];
		foreach($unsavedholidayCampModules as $unsavedholidayCampModule)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampModule);
			if(!empty($modelInstance))
			{
								$location->holidayCampModules()->save($modelInstance);
							}
		}
				if(!empty($data['regularProgramClasseSchedules']))
		{
			$regularProgramClasseSchedules = explode(',', $data['regularProgramClasseSchedules']);
			foreach ($regularProgramClasseSchedules as $singleRegularProgramClasseSchedule)
			{
				$regularProgramClasseSchedule = RegularProgramClasseSchedule::find($singleRegularProgramClasseSchedule);
								$location->regularProgramClasseSchedules()->save($regularProgramClasseSchedule);
							}
		}
		$unsavedregularProgramClasseSchedules = $unsavedRelationships['regularProgramClasseSchedules'];
		foreach($unsavedregularProgramClasseSchedules as $unsavedregularProgramClasseSchedule)
		{
			$modelInstance = getUnsavedRelationship($unsavedregularProgramClasseSchedule);
			if(!empty($modelInstance))
			{
								$location->regularProgramClasseSchedules()->save($modelInstance);
							}
		}
				if(!empty($data['holidayCampCourses']))
		{
			$holidayCampCourses = explode(',', $data['holidayCampCourses']);
			foreach ($holidayCampCourses as $singleHolidayCampCourse)
			{
				$holidayCampCourse = HolidayCampCourse::find($singleHolidayCampCourse);
								$location->holidayCampCourses()->attach($holidayCampCourse);
							}
		}
		$unsavedholidayCampCourses = $unsavedRelationships['holidayCampCourses'];
		foreach($unsavedholidayCampCourses as $unsavedholidayCampCourse)
		{
			$modelInstance = getUnsavedRelationship($unsavedholidayCampCourse);
			if(!empty($modelInstance))
			{
								$location->holidayCampCourses()->attach($modelInstance);
							}
		}
						$location->save();
		return redirect('/modules/locations');
	}
	
	public function delete($id)
	{
		$location = Location::find($id);
		$location->delete();
		return redirect('/modules/locations');
	}
	
	public function location($id)
	{
		$location = Location::with('holidayCampModules','regularProgramClasseSchedules','holidayCampCourses')->find($id);
		return response()->json($location);
	}
	
		public function holidayCampModules($id)
	{
		$location = Location::find($id);
		$holidayCampModules = $location->holidayCampModules;
		for ($i = 0; $i < count($holidayCampModules); $i++)
		{
			$holidayCampModule = $holidayCampModules[$i];
			$holidayCampModule->location = $location;
			$holidayCampModules[$i] = $holidayCampModule;
		}
		return view('modules.holidayCampModules.all', ['holidayCampModules' => $holidayCampModules]);
	}
	
	public function holidayCampModulesApi($id)
	{
		$location = Location::find($id);
		$holidayCampModules = $location->holidayCampModules;
		return response()->json($holidayCampModules->all());
	}
		public function regularProgramClasseSchedules($id)
	{
		$location = Location::find($id);
		$regularProgramClasseSchedules = $location->regularProgramClasseSchedules;
		for ($i = 0; $i < count($regularProgramClasseSchedules); $i++)
		{
			$regularProgramClasseSchedule = $regularProgramClasseSchedules[$i];
			$regularProgramClasseSchedule->location = $location;
			$regularProgramClasseSchedules[$i] = $regularProgramClasseSchedule;
		}
		return view('modules.regularProgramClasseSchedules.all', ['regularProgramClasseSchedules' => $regularProgramClasseSchedules]);
	}
	
	public function regularProgramClasseSchedulesApi($id)
	{
		$location = Location::find($id);
		$regularProgramClasseSchedules = $location->regularProgramClasseSchedules;
		return response()->json($regularProgramClasseSchedules->all());
	}
		public function holidayCampCourses($id)
	{
		$location = Location::find($id);
		$holidayCampCourses = $location->holidayCampCourses;
		for ($i = 0; $i < count($holidayCampCourses); $i++)
		{
			$holidayCampCourse = $holidayCampCourses[$i];
			$holidayCampCourse->location = $location;
			$holidayCampCourses[$i] = $holidayCampCourse;
		}
		return view('modules.holidayCampCourses.all', ['holidayCampCourses' => $holidayCampCourses]);
	}
	
	public function holidayCampCoursesApi($id)
	{
		$location = Location::find($id);
		$holidayCampCourses = $location->holidayCampCourses;
		return response()->json($holidayCampCourses->all());
	}
	}
?>