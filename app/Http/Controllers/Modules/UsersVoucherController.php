<?php
namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UsersVoucher;
use App\Models\Enrollment;
use App\Models\Voucher;
use App\Models\User;
use Log;

class UsersVoucherController extends Controller
{	
	public function index()
	{
		$usersVouchers = UsersVoucher::with('enrollments','voucher','user')->get();
		return view('modules.usersVouchers.all', ['usersVouchers' => $usersVouchers]);
	}
	
	public function search(Request $request)
	{
		$per_page = \Request::get('per_page') ?: 10;
		if ($request['query']) {
			$request = UsersVoucher::with('enrollments','voucher','user')->search($request['query'])->get();
			$page = $request->has('page') ? $request->page - 1 : 0;
			$total = $request->count();
			$request = $request->slice($page * $per_page, $per_page);
			$request = new \Illuminate\Pagination\LengthAwarePaginator($request, $total, $per_page);
			return  $request;
		}
		return 'not found';
	}
	
	public function create()
	{
				$vouchers = Voucher::all();
				$users = User::all();
				return view('modules.usersVouchers.form', [
					'vouchers' => $vouchers,
			'allvouchers' => Voucher::all(),
					'users' => $users,
			'allusers' => User::all(),
					'allenrollments' => Enrollment::all(),
				]);
	}
	
	public function store(Request $request)
	{
		$this->validate($request, [
			    	]);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
		$usersVoucher = new UsersVoucher();
				if(!empty($data['id']))
		{
			$usersVoucher->id = $data['id'];
		}
				if(!empty($data['user_id']))
		{
			$usersVoucher->user_id = $data['user_id'];
		}
				if(!empty($data['voucher_id']))
		{
			$usersVoucher->voucher_id = $data['voucher_id'];
		}
				if(!empty($data['valid_till']))
		{
			$usersVoucher->valid_till = $data['valid_till'];
		}
				if(!empty($data['is_used']))
		{
			$usersVoucher->is_used = $data['is_used'];
		}
				$usersVoucher->save();
				if(!empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
								$usersVoucher->enrollments()->save($enrollment);
							}
		}
		$unsavedenrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedenrollments as $unsavedenrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
			if(!empty($modelInstance))
			{
								$usersVoucher->enrollments()->save($modelInstance);
							}
		}
						if(!empty($data['voucher']))
		{
			$voucher = Voucher::find($data['voucher']);
						$usersVoucher->voucher()->associate($voucher);
					}
		$unsavedvoucher = $unsavedRelationships['voucher'];
		if(!empty($unsavedvoucher))
		{
			$modelInstance = getUnsavedRelationship($unsavedvoucher);
						$usersVoucher->voucher()->associate($modelInstance);
					}
				if(!empty($data['user']))
		{
			$user = User::find($data['user']);
						$usersVoucher->user()->associate($user);
					}
		$unsaveduser = $unsavedRelationships['user'];
		if(!empty($unsaveduser))
		{
			$modelInstance = getUnsavedRelationship($unsaveduser);
						$usersVoucher->user()->associate($modelInstance);
					}
				$usersVoucher->save();
		return redirect('/modules/usersVouchers');
	}
	
	public function get($id)
	{
		$usersVoucher = UsersVoucher::with('enrollments','voucher','user')->find($id);
				$vouchers = Voucher::all();
				$users = User::all();
						$enrollmentsArray = [];
		foreach($usersVoucher->enrollments as $enrollment)
		{
			
			$enrollmentsArray[] = $enrollment->id;
		}
		$enrollmentsValue = implode(',', $enrollmentsArray);
				return view('modules.usersVouchers.form', [
			'usersVoucher' => $usersVoucher,
							'vouchers' => $vouchers,
				'allvouchers' => Voucher::all(),
							'users' => $users,
				'allusers' => User::all(),
							'allenrollments' => Enrollment::all(),
				'enrollmentsValue' => $enrollmentsValue,
					]);
	}
	
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			    	]);
		$usersVoucher = UsersVoucher::find($id);
		$data = $request->all();
		$unsavedRelationshipsJSON = $data['unsavedRelationships'];
		$unsavedRelationships = json_decode($unsavedRelationshipsJSON, true);
				if(!empty($data['id']))
		{
			$usersVoucher->id = $data['id'];
		}
				if(!empty($data['user_id']))
		{
			$usersVoucher->user_id = $data['user_id'];
		}
				if(!empty($data['voucher_id']))
		{
			$usersVoucher->voucher_id = $data['voucher_id'];
		}
				if(!empty($data['valid_till']))
		{
			$usersVoucher->valid_till = $data['valid_till'];
		}
				if(!empty($data['is_used']))
		{
			$usersVoucher->is_used = $data['is_used'];
		}
				$usersVoucher->save();
				if(!empty($data['enrollments']))
		{
			$enrollments = explode(',', $data['enrollments']);
			foreach ($enrollments as $singleEnrollment)
			{
				$enrollment = Enrollment::find($singleEnrollment);
								$usersVoucher->enrollments()->save($enrollment);
							}
		}
		$unsavedenrollments = $unsavedRelationships['enrollments'];
		foreach($unsavedenrollments as $unsavedenrollment)
		{
			$modelInstance = getUnsavedRelationship($unsavedenrollment);
			if(!empty($modelInstance))
			{
								$usersVoucher->enrollments()->save($modelInstance);
							}
		}
						if(!empty($data['voucher']))
		{
			$voucher = Voucher::find($data['voucher']);
						$usersVoucher->voucher()->associate($voucher);
					}
		$unsavedvoucher = $unsavedRelationships['voucher'];
		if(!empty($unsavedvoucher))
		{
			$modelInstance = getUnsavedRelationship($unsavedvoucher);
						$usersVoucher->voucher()->associate($modelInstance);
					}
				if(!empty($data['user']))
		{
			$user = User::find($data['user']);
						$usersVoucher->user()->associate($user);
					}
		$unsaveduser = $unsavedRelationships['user'];
		if(!empty($unsaveduser))
		{
			$modelInstance = getUnsavedRelationship($unsaveduser);
						$usersVoucher->user()->associate($modelInstance);
					}
				$usersVoucher->save();
		return redirect('/modules/usersVouchers');
	}
	
	public function delete($id)
	{
		$usersVoucher = UsersVoucher::find($id);
		$usersVoucher->delete();
		return redirect('/modules/usersVouchers');
	}
	
	public function usersVoucher($id)
	{
		$usersVoucher = UsersVoucher::with('enrollments','voucher','user')->find($id);
		return response()->json($usersVoucher);
	}
	
		public function enrollments($id)
	{
		$usersVoucher = UsersVoucher::find($id);
		$enrollments = $usersVoucher->enrollments;
		for ($i = 0; $i < count($enrollments); $i++)
		{
			$enrollment = $enrollments[$i];
			$enrollment->usersVoucher = $usersVoucher;
			$enrollments[$i] = $enrollment;
		}
		return view('modules.enrollments.all', ['enrollments' => $enrollments]);
	}
	
	public function enrollmentsApi($id)
	{
		$usersVoucher = UsersVoucher::find($id);
		$enrollments = $usersVoucher->enrollments;
		return response()->json($enrollments->all());
	}
	}
?>