<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use File;
use App\Repositories\UploadRepository;

class UploadController extends Controller
{
	protected $uploadRepository;

	public function __construct(UploadRepository $uploadRepository)
	{
		$this->uploadRepository = $uploadRepository;
	}
	
	public function fileUpload()
	{
		$uploadedFiles = $this->uploadRepository->fileUpload();
	
		return response()->json(["status"=>"ok", "files"=>$uploadedFiles]);
	}
}