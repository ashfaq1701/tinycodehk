<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class MorphMapServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    	Relation::morphMap([
    		'inHome' => 'App\Models\InHomeProgram',
    		'regular' => 'App\Models\RegularProgramSemester',
    		'holidayCamp' => 'App\Models\HolidayCampModule'
    	]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    	
    }
}
